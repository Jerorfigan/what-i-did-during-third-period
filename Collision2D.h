#pragma once

/* Collision2D defines the functionality for resolving collisions in 2D between objects 
   that are comprised of line segments. */

#include <utility>
#include <vector>

#include "CollidableGameObject.h"
#include "LineSegment2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME
{

	class Collision2D
	{
	public:
		typedef std::pair< double, LineSegment2D > CollisionInfo;

		/* ResolveCollision 
		   Summary: Resolves a collision between a non-static CollidableGameObject and one or more static 
		            CollidableGameObjects. The kinematic state for the non-static CollidableGameObject will 
					be updated to reflect the number and type of collision(s) encountered. */
		static void ResolveCollision( CollidableGameObject *nonStaticObject, 
			std::vector< CollidableGameObject* > staticObjects );

	private: 
		/* Gets the collision info for the collision that results from one line segment being displaced
		   into another line segment. */
		static CollisionInfo GetCollisionInfo( const LineSegment2D &lineToDisplace, 
			const LineSegment2D &otherLine, Vector2D displacement );

		/* Returns true if the line segment is located below the point (in Screen coordinates). */
		static bool LineSegmentLocatedBelowPoint( const LineSegment2D &lineSegment, const Point2D &point );
	};

}