#pragma once

/* SpriteAnimator defines the functionality to animate sprites. */

#include <fstream>
#include <map>
#include <string>

#include "IGraphicsProvider.h"
#include "Point2D.h"
#include "Rectangle2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME
{
	class SpriteAnimator
	{
	private:
		struct AnimationData
		{
			std::string _animationName;
			std::string _animationArtFile;
			std::size_t _frameWidth;
			std::size_t _frameHeight;
			std::size_t _numFrames;
			double _targetFrameRate;
			Vector2D _frameCenterOffset;
		};

	public:
		typedef std::map< std::string, AnimationData > Animations;

		/* Initializes a SpriteAnimator from an animation file, a graphics resource and an
		   animation point (the point to center the animation frames on). */
		SpriteAnimator( std::string animationFile, IGraphicsProvider::ResourceId graphicsResource, 
			const Point2D &animationPoint );

		/* PlayAnimation 
		   Summary: Plays the specified animation. Can optionally loop the animation (default is true), 
					rotate each frame (default is no rotation), and use a subrectangle to clip off part 
					of each frame (default is no clipping). 
		   Parameters:
			  animationName: The name of the animation to play. 
			  loop: True/false flag indicating whether to loop the animation (default is true). 
			  frameRotationAngle: The animation frames are rotated by this many degrees (passed as a reference, 
								  so that it may be changed while the animation is playing). 
			  frameSubRect: The animation frames are clipped by this subrectangle. Must fit within the rectangle
							given by the frame width & height. (passed as a reference, so that it may be 
							changed while the animation is playing). */
		void PlayAnimation( std::string animationName, bool loop = true, double *frameRotationAngle = 0, 
			 Rectangle2D *frameSubRect = 0 );
		/* PlayAnimation 
		   Summary: Plays the specified animation from the specified starting frame. Can optionally 
					loop the animation (default is true), rotate each frame (default is no rotation), 
					and use a subrectangle to clip off part of each frame (default is no clipping). 
		   Parameters:
		      animationName: The name of the animation to play. 
			  startingFrame: The frame to start playing the animation from.
			  loop: True/false flag indicating whether to loop the animation (default is true). 
			  frameRotationAngle: The animation frames are rotated by this many degrees (passed as a reference, 
			                      so that it may be changed while the animation is playing). 
			  frameSubRect: The animation frames are clipped by this subrectangle. Must fit within the rectangle
			                given by the frame width & height. (passed as a reference, so that it may be 
							changed while the animation is playing). */
		void PlayAnimation( std::string animationName, std::size_t startingFrame, bool loop = true,
			 double *frameRotationAngle = 0, Rectangle2D *frameSubRect = 0 );

		/* Update
		   Summary: Updates the currently playing animation by advancing it to the next frame if its
		            time to. 
		   Parameters: 
		      elapsedTime: The elapsed time in seconds since the last update. */
		void Update( double elapsedTime );

		std::string GetCurrentAnimation();
		std::size_t GetCurrentFrame();

	private:
		/* Constructs a subrectangle surrounding the specified frame. */
		Rectangle2D ConstructSubrectangle( std::size_t frameWidth, std::size_t frameHeight, std::size_t frameNum ) const; 
		/* Reads animation data from a file stream. Returns true if animation data was read. */
		bool ReadAnimationDataFromFile( std::ifstream &animationFileStream, AnimationData &animationData ) const;

		IGraphicsProvider::ResourceId _graphicsResource;
		const Point2D &_animationPoint;
		std::string _currentAnimation;
		std::size_t _currentFrame;
		bool _isLoopingAnimation;
		double _currentFrameDuration;

		double *_frameRotationAngle;
		Rectangle2D *_frameSubRect;

		Animations _animations;	
	};
}