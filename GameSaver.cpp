/* GameSaver implementation. */

#include "stdafx.h"

#include <fstream>

#include "GameSaver.h"

namespace PROJECT_NAME
{

	/* Constructors */
		
	GameSaver::GameSaver() 
	{
		mSaveData.currentMapIndex = 0;
	}

	/* Methods */

	bool GameSaver::LoadSave( std::string saveFile )
	{
		std::fstream inSaveFileStream( saveFile.c_str(), std::ios::in | std::ios::binary );

		if( inSaveFileStream )
		{
			inSaveFileStream >> mSaveData; 
		}

		inSaveFileStream.close();

		return inSaveFileStream != 0;
	}

	void GameSaver::SaveGame( std::string saveFile ) const
	{
		std::fstream outSaveFileStream( saveFile.c_str(), std::ios::out | std::ios::binary | std::ios::trunc );

		if( !outSaveFileStream )
		{
			// File doesn't exist yet, so create it.
			outSaveFileStream.open( saveFile.c_str(), std::ios::out );
			outSaveFileStream.close();
			outSaveFileStream.open( saveFile.c_str(), std::ios::out | std::ios::binary | std::ios::trunc );
		}

		if( outSaveFileStream )
		{
			outSaveFileStream << mSaveData;
		}

		outSaveFileStream.close();
	}

	const GameSaveData& GameSaver::GetGameSaveData() const
	{
		return mSaveData;
	}

	void GameSaver::SetGameSaveData( const GameSaveData &saveData )
	{
		mSaveData = saveData;
	}

}