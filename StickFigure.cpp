/* StickFigure implementation. */

#include "stdafx.h"

#include <fstream>

#include "Collision2D.h"
#include "DeathRay.h"
#include "FlameTrap.h"
#include "Game.h"
#include "Map.h"
#include "MapExit.h"
#include "Rectangle2D.h"
#include "SawBlade.h"
#include "StickFigure.h"
#include "WorldSimulation.h"
#include "WreckingBall.h"

namespace PROJECT_NAME
{
	/* Initializes a StickFigure from a spawn position. */
	StickFigure::StickFigure( Point2D spawnPosition ) : GameObject( "stick_figure"), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/StickMan.png" ) ), 
		"stick_figure" ), 
		DynamicGameObject( 2, "stick_figure" ), CollidableGameObject( "stick_figure", 
		KinematicState( spawnPosition, Vector2D( 0, 0 ), Vector2D( 0, 0 ) ), false ), _currentState( Idle ), 
		_collidableArea( Rectangle2D( spawnPosition, _collidableWidth, _collidableHeight ) ), 
		_spriteAnimator( RESOURCE_PATH( "Animation/StickFigure/StickFigureAnimationFile.txt" ), GraphicsId(), _kinematicState.GetPosition() ),
		_facingDirection( Vector2D( 1, 0 ) ), _isSliding( false ), _isDying( false ), _causeOfDeath( None ),
		_proceduralAnimator( RESOURCE_PATH( "Animation/Procedural/StickFigure/ProceduralAnimations.txt" ) ), _soundManager( _soundFile )
	{

	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void StickFigure::Draw( double interpolation )
	{
		// Draw the stick figure at the interpolated position between its current position 
		// and next position.
		KinematicState temp = _kinematicState;

		Vector2D currentPosition( temp.GetPosition() );
		temp.UpdatePositionAndVelocity( Game::GetGameUpdatePeriod() );
		Vector2D nextPosition( temp.GetPosition() );

		Vector2D interpolationVector = interpolation * ( nextPosition - currentPosition );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( GraphicsId(), 
			currentPosition + interpolationVector );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );

		_proceduralAnimator.DrawSimulation( interpolation );
	}

	/* Update
	   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
	            last update (which should be accounted for in this update). 
	   Parameters: 
	      elapsedTime: The time elapsed in seconds since last update. */
	void StickFigure::Update( double elapsedTime )
	{
		StickFigureState nextState = GetNextState( _currentState );

		if( nextState != _currentState )
		{
			// new state found, so need to transition to it
			TransitionToState( nextState );
		}

		if( _currentState != Dead )
		{
			_kinematicState.UpdatePositionAndVelocity( elapsedTime );
		
			// Center the stick figure's collidable area around new position.
			// Also update the child _stickPhysicsEntity.
			_collidableArea.UpdatePosition( _kinematicState.GetPosition() );

			// Force transition to Collision state if updating stick figure's position results in a collision
			if( InCollision() )
			{
				TransitionToState( Collision );
			}
		}

		RunAnimationUpdateLogic( elapsedTime );

		// The collision state is a special case where RunSoundUpdateLogic is called when the stick transitions 
		// to the collision state, since we need to know the pre-collision velocity to evaluate whether a sound 
		// should be played. So don't call it twice. 
		if( _currentState != Collision )
			RunSoundUpdateLogic( _currentState );

		// Log information about this StickFigure
		LogStateInfo();
	}

	/* GetCollidableArea
	   Summary: Gets the CollidableArea for this CollidableGameObject. 
	   Returns: The CollidableArea for this CollidableGameObject. */
	CollidableArea* StickFigure::GetCollidableArea()
	{
		return &_collidableArea;
	}

	/* Gets the next state for the stick figure. */
	StickFigure::StickFigureState StickFigure::GetNextState( StickFigureState currentState )
	{
		// Initialize nextState to currentState, that way if we find no state
		// transition this function will return the current state indicating as much.
		StickFigureState nextState = currentState;

		switch( currentState )
		{
			/* IDLE STATE */
		case Idle:
			{
				if( CanFall() )
				{
					nextState = Falling;
				}
				else if( IsKeyDown( KeyboardKey::Up ) )
				{
					nextState = Jumping;
				}
				else if( IsKeyDown( KeyboardKey::Right ) && !IsKeyDown( KeyboardKey::Left ) )
				{
					nextState = RunningRight;
				}
				else if( IsKeyDown( KeyboardKey::Left ) && !IsKeyDown( KeyboardKey::Right ) )
				{
					nextState = RunningLeft;
				}
			}
			break;
			/* RUNNING RIGHT STATE */
		case RunningRight:
			{
				if( CanFall() )
				{
					nextState = Falling;
				}
				else if( !IsKeyDown( KeyboardKey::Right ) || IsKeyDown( KeyboardKey::Left ) )
				{
					nextState = Idle;
				}
				else if( IsKeyDown( KeyboardKey::Up ) )
				{
					nextState = Jumping;
				}
			}
			break;
			/* RUNNING LEFT STATE */
		case RunningLeft:
			{
				if( CanFall() )
				{
					nextState = Falling;
				}
				else if( !IsKeyDown( KeyboardKey::Left ) || IsKeyDown( KeyboardKey::Right ) )
				{
					nextState = Idle;
				}
				else if( IsKeyDown( KeyboardKey::Up ) )
				{
					nextState = Jumping;
				}
			}
			break;
			/* JUMPING STATE */
		case Jumping:
			{
				if( _kinematicState.GetVelocity().Magnitude() > _jumpToFallVelocity )
				{
					nextState = Falling;
				}

				// Allow the stick figure to shorten his jump by applying a downward velocity when the down arrow
				// key is held.
				if( IsKeyDown( KeyboardKey::Down ) )
				{
					_kinematicState.SetVelocity( _kinematicState.GetVelocity() + 
						Game::GetGameUpdatePeriod() * Vector2D( 0, _attractGroundVelocity ) );
				}
			}
			break;
			/* COLLISION STATE */
		case Collision:
			{
				if( _isDying )
				{
					_isDying = false;
					nextState = Dead;
				}
				else if( IsWallSticking() )
				{
					nextState = WallSticking;
				}
				else if( _kinematicState.GetVelocity().IsZero() && _kinematicState.GetAcceleration().IsZero() )
				{
					nextState = Idle;
				}
				else if( !_kinematicState.GetVelocity().IsZero() && _kinematicState.GetAcceleration().IsZero() )
				{
					if( IsBetween( _kinematicState.GetVelocity().Angle(), 0, 90 ) ||
						IsBetween( _kinematicState.GetVelocity().Angle(), 270, 360 ) )
					{
						nextState = RunningRight;
					}
					else
					{
						nextState = RunningLeft;
					}
				}
				else if( EqualDouble( _kinematicState.GetAcceleration().Magnitude(), ACCELERATION_OF_GRAVITY ) )
				{
					nextState = Falling;
				}
				else if( _kinematicState.GetAcceleration().Magnitude() >= _slideThreshold )
				{
					nextState = Sliding;
				}
				else if( _kinematicState.GetAcceleration().Magnitude() < _slideThreshold )
				{
					// Skip transitioning to the idle state if the user is currently holding down the left or right
					// move key. This smooths out the animation when running up or down varying inclines.
					if( IsKeyDown( KeyboardKey::Right ) )
					{
						nextState = RunningRight;
					}
					else if( IsKeyDown( KeyboardKey::Left ) )
					{
						nextState = RunningLeft;
					}
					else
					{
						nextState = Idle;
					}
				}

				// Should never stay in collision state
				assert( nextState != Collision );
			}
			break;
			/* FALLING STATE */
		case Falling:
			{

			}
			break;
			/* SLIDING STATE */
		case Sliding:
			{
				SetFacingDirection();

				if( CanFall() )
				{
					nextState = Falling;
				}
				else if( IsKeyDown( KeyboardKey::Up ) )
				{
					nextState = Jumping;
				}
			}
			break;
			/* DEAD STATE */
		case Dead:
			{
				if( IsKeyDown( KeyboardKey::Space ) )
				{
					nextState = Respawning;
				}
			}
			break;
			/* RESPAWNING STATE */
		case Respawning:
			{
				nextState = Idle;
			}
			break;
			/* WALL STICKING STATE */
		case WallSticking:
			{
				if( _stateTimer.GetElapsedTime() > _wallStickTime )
				{
					if( EqualDouble( _facingDirection.Angle(), 0.0 ) && IsKeyDown( KeyboardKey::Right ) ||
						EqualDouble( _facingDirection.Angle(), 180.0 ) && IsKeyDown( KeyboardKey::Left ) )
					{
						_kinematicState.SetVelocity( _wallJumpLateralVelocity * _facingDirection );
					}

					if( IsKeyDown( KeyboardKey::Up ) || !_kinematicState.GetVelocity().IsZero() )
					{
						if( IsKeyDown( KeyboardKey::Up ) )
						{
							if( _kinematicState.GetVelocity().IsZero() )
							{
								_kinematicState.SetVelocity( _kinematicState.GetVelocity() + 
									_wallFallLateralVelocity * _facingDirection +
									_jumpVelocity * Vector2D( 0, -1 ) ); 
							}
							else
							{
								_kinematicState.SetVelocity( _kinematicState.GetVelocity() + 
									_jumpVelocity * Vector2D( 0, -1 ) ); 
							}
						}
						nextState = Jumping;
					}
					else
					{	
						// Give the stick a bit of velocity to get him away from the wall as hes falling.
						_kinematicState.SetVelocity( _wallFallLateralVelocity * _facingDirection );

						// Invert facing direction since the stick was facing away from the wall in preparation for the jump,
						// however, if we're transitioning to a fall, we want the stick to be facing the wall.
						_facingDirection = -1 * _facingDirection;
						nextState = Falling;
					}
				}
			}
			break;
		}

		return nextState;
	}

	/* Transitions the stick figure to the given state. */
	void StickFigure::TransitionToState( StickFigureState state )
	{
		switch( state )
		{
			/* IDLE STATE */
		case Idle:
			{
				_kinematicState.SetVelocity( Vector2D( 0, 0 ) );
				_kinematicState.SetAcceleration( Vector2D( 0, 0 ) );
			}
			break;
			/* RUNNING RIGHT STATE */
		case RunningRight:
			{
				if( _facingDirection.Angle() > 90 && _facingDirection.Angle() < 270 )
				{
					_facingDirection = -1 * _facingDirection;
				}
				_kinematicState.SetAcceleration( _runAcceleration * _facingDirection );
				_kinematicState.SetMaxVelocity( _maxRunVelocity );
			}
			break;
			/* RUNNING LEFT STATE */
		case RunningLeft:
			{
				if( _facingDirection.Angle() < 90 || _facingDirection.Angle() > 270 )
				{
					_facingDirection = -1 * _facingDirection;
				}
				_kinematicState.SetAcceleration( _runAcceleration * _facingDirection );
				_kinematicState.SetMaxVelocity( _maxRunVelocity );
			}
			break;
			/* JUMPING STATE */
		case Jumping:
			{
				_kinematicState.SetMaxVelocity( _terminalVelocity );

				// If coming from WallSticking, velocity is already set correctly.
				if( _currentState == WallSticking )
				{
					
				}
				else
				{
					_kinematicState.SetVelocity( 
						_kinematicState.GetVelocity().IsZero() ?
						_jumpVelocity * Vector2D( 0, -1 ) :
						_jumpLateralVelocityFactor * _kinematicState.GetVelocity().ProjectOnto( Vector2D( 1, 0 ) ) + 
						_jumpVelocity * Vector2D( 0, -1 ) );
				}

				_kinematicState.SetAcceleration( ACCELERATION_OF_GRAVITY * Vector2D( 0, 1 ) );
			}	
			break;
			/* FALLING STATE */
		case Falling:
			{
				_kinematicState.SetAcceleration( ACCELERATION_OF_GRAVITY * Vector2D( 0, 1 ) );
				_kinematicState.SetMaxVelocity( _terminalVelocity );
			}
			break;
			/* COLLISION STATE */
		case Collision:
			{
				_kinematicState.SetMaxVelocity( _terminalVelocity );

				Handle< Map > mapHandle = Game::GetGameObjectManager().GetGameObject( "map" );
				assert( mapHandle.Valid() );

				KinematicState preCollisionState = _kinematicState;

				// Get the list of colliding objects.
				std::vector< CollidableGameObject* > collidingObjects = mapHandle->GetObjectsCollidingWith( Id() );

				// Currently traps kill you instantly and the actual collision animation is handled with ragdoll physics,
				// so if we've collided with a trap, skip the rest of the collision handling.
				if( HasCollidedWithTrap( collidingObjects ) )
					break;

				// If the stick has collided with the exit, skip collision handling, as this signals we're going to transition levels.
				if( HasCollidedWithExit( collidingObjects ) )
					break;

				// Need to call this here as well as in update, since the determination of whether a collision sound should be played
				// needs to happen before we calculate the post-collision velocity.
				RunSoundUpdateLogic( Collision );

				Collision2D::ResolveCollision( this, collidingObjects );

				// If impact velocity exceeds fall-to-death velocity, revert the
				// velocity to the pre-collision velocity and kill the stick figure. The ragdoll animation will 
				// take the pre-collision velocity as input. 
				if( preCollisionState.GetVelocity().Magnitude() >= _fallToDeathVelocity )
				{
					_kinematicState.SetVelocity( preCollisionState.GetVelocity() );
					_isDying = true;
					_causeOfDeath = SuddenImpact;
					break;
				}

				// If post-collision velocity is opposite in direction from pre-collision velocity and coming
				// from RUNNING RIGHT or RUNNING LEFT state, this signifies we hit a wall, so zero out velocity 
				// and acceleration.
				if( preCollisionState.GetVelocity() == -1 * _kinematicState.GetVelocity() && 
					( _currentState == RunningRight || _currentState == RunningLeft ) )
				{
					_kinematicState.SetVelocity( Vector2D( 0, 0 ) );
					_kinematicState.SetAcceleration( Vector2D( 0, 0 ) );
				}

				SetFacingDirection();

				// Verifies that the stick figure can move in the post-collision velocity/acceleration
				// without collision.
				VerifyCollisionResolution();

				LogStateInfo( false );
			}
			break;
			/* SLIDING STATE */
		case Sliding:
			{
				_isSliding = true;
				_kinematicState.SetMaxVelocity( _terminalVelocity );
			}
			break;
			/* DEAD STATE */
		case Dead:
			{
				_stateTimer.Reset();
			}
			break;
			/* RESPAWNING STATE */
		case Respawning:
			{
				Handle< Map > mapHandle = Game::GetGameObjectManager().GetGameObject( "map" );
				assert( mapHandle.Valid() );
				_kinematicState.SetPosition( mapHandle->GetSpawnPoint() );
				_collidableArea.UpdatePosition( _kinematicState.GetPosition() );
				_kinematicState.SetVelocity( Vector2D( 0, 0 ) );
				_kinematicState.SetAcceleration( Vector2D( 0, 0 ) );
			}
			break;
			/* WALL STICKING STATE */
		case WallSticking:
			{
				_stateTimer.Reset();

				_facingDirection = _kinematicState.GetVelocity().ProjectOnto( Vector2D( 1, 0 ) ).UnitVector();

				_kinematicState.SetVelocity( Vector2D( 0, 0 ) );
				_kinematicState.SetAcceleration( Vector2D( 0, 0 ) );
			}
			break;
		}
		_currentState = state;
	}

	/* Determines the appropriate animation to play for the stick figure based on its 
	   state. */
	void StickFigure::RunAnimationUpdateLogic( double elapsedTime )
	{
		switch( _currentState )
		{
		case Idle:
			{
				if( IsBetween( _facingDirection.Angle(), 0, 90 ) ||
					IsBetween( _facingDirection.Angle(), 270, 360 ) )
				{
					_spriteAnimator.PlayAnimation( "IdleRight" );
				}
				else
				{
					_spriteAnimator.PlayAnimation( "IdleLeft" );
				}
			}
			break;
		case RunningRight:
			{
				/* Play the running animation once velocity is above a lower threshold. This prevents
				   the stick figure's animation from flickering between running/idle when the stick
				   is being run into a wall. */
				if( _kinematicState.GetVelocity().Magnitude() > 5.0 )
				{
					_spriteAnimator.PlayAnimation( "RunningRight" );
				}
			}
			break;
		case RunningLeft:
			{
				/* Play the running animation once velocity is above a lower threshold. This prevents
				   the stick figure's animation from flickering between running/idle when the stick
				   is being run into a wall. */
				if( _kinematicState.GetVelocity().Magnitude() > 5.0 )
				{
					_spriteAnimator.PlayAnimation( "RunningLeft" );
				}
			}
			break;
		case Jumping:
			{
				if( IsBetween( _facingDirection.Angle(), 0, 90 ) ||
					IsBetween( _facingDirection.Angle(), 270, 360 ) )
				{
					_spriteAnimator.PlayAnimation( "JumpingRight", false );
				}
				else
				{
					_spriteAnimator.PlayAnimation( "JumpingLeft", false );
				}
			}
			break;
		case Collision:
			{

			}
			break;
		case Falling:
			{
				if( IsBetween( _facingDirection.Angle(), 0, 90 ) ||
					IsBetween( _facingDirection.Angle(), 270, 360 ) )
				{
					_spriteAnimator.PlayAnimation( "FallingRight" );
				}
				else
				{
					_spriteAnimator.PlayAnimation( "FallingLeft" );
				}
			}
			break;
		case Sliding:
			{
				/* Play the sliding animation once velocity is above a lower threshold. This prevents
				   the stick figure's animation from flickering between sliding/idle when the stick
				   is being run into a slope. */
				if( _kinematicState.GetVelocity().Magnitude() > 10.0 )
				{
					if( IsBetween( _kinematicState.GetAcceleration().Angle(), 0, 90 ) )
					{
						_spriteAnimator.PlayAnimation( "SlidingRight" );
					}
					else
					{
						_spriteAnimator.PlayAnimation( "SlidingLeft" );
					}
				}
			}
			break;
		case WallSticking:
			{
				if( EqualDouble( _facingDirection.Angle(), 180.0 ) )
				{
					_spriteAnimator.PlayAnimation( "WallStickingRight", false );
				}
				else
				{
					_spriteAnimator.PlayAnimation( "WallStickingLeft", false );
				}
			}
			break;
		case Dead:
			{
				_spriteAnimator.PlayAnimation( "None" );
				if( _causeOfDeath == SuddenImpact ||
					_causeOfDeath == WreckingBall ||
					_causeOfDeath == FlameTrap )
				{
					_proceduralAnimator.PlayProceduralAnimation( "ragdoll", _kinematicState.GetPosition(), 
						_kinematicState.GetVelocity() );
				}
				else if( _causeOfDeath == SawBlade )
				{
					// Limit lower velocity when walking into saw blade to maximize gore effect!
					if( _kinematicState.GetVelocity().Magnitude() < 150.0 )
					{
						_kinematicState.SetVelocity( 150.0 * _kinematicState.GetVelocity().UnitVector() );
					}
					_proceduralAnimator.PlayProceduralAnimation( "ragdollexplode", _kinematicState.GetPosition(), 
						_kinematicState.GetVelocity() );
				}
				else if( _causeOfDeath == DeathRay )
				{
					Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );

					// Tell the world simulator to stop computing contacts, as it is too resource intensive when the death ray explode
					// animation plays.
					worldSimulationHandle->ToggleContactComputation( false );

					_proceduralAnimator.PlayProceduralAnimation( "explode", _kinematicState.GetPosition(), 
						_kinematicState.GetVelocity() );
				}
				else
				{
					// Unknown cause of death...crash!
					assert( 1 == 0 );
				}
			}
			break;
		case Respawning:
			{
				Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );

				// Tell the world simulator to start computing contacts again, in case we told it to stop before due to 
				// the death ray explosion animation (which makes it too resource intensive).
				worldSimulationHandle->ToggleContactComputation( true );

				_proceduralAnimator.StopProceduralAnimation();
			}
			break;
		default:
			{
				_spriteAnimator.PlayAnimation( "IdleRight" );
			}
			break;
		}

		_spriteAnimator.Update( elapsedTime );
		_proceduralAnimator.UpdateSimulation();
	}

	/* Determines the appropriate sound to play for the stick figure based on its 
	   state. */
	void StickFigure::RunSoundUpdateLogic( StickFigureState currentState  )
	{
		switch( currentState )
		{
		case Jumping:
			{
				if( ( _spriteAnimator.GetCurrentAnimation() == "JumpingRight" || 
					  _spriteAnimator.GetCurrentAnimation() == "JumpingLeft" ) &&
					_spriteAnimator.GetCurrentFrame() == 1 )
				{
					//_soundManager.QueueToPlay( "jumping", 100.0f, false, false, false );
				}
			}
			break;
		case RunningRight:
		case RunningLeft:
			{
				if( ( _spriteAnimator.GetCurrentAnimation() == "RunningRight" ||
					  _spriteAnimator.GetCurrentAnimation() == "RunningLeft" ) &&
					  ( _spriteAnimator.GetCurrentFrame() == 3 ||
						_spriteAnimator.GetCurrentFrame() == 8 ) )
				{
					_soundManager.QueueToPlay( "running", 100.0f, false, false, false );
				}
			}
			break;
		case Collision:
			{
				if( _kinematicState.GetVelocity().Magnitude() > 25.0 )
				{
					if( _currentState == RunningRight || _currentState == RunningLeft ) 
					{
						// Don't play collision sound when colliding while running. It's annoying, since
						// it gets played everytime the angle of the surface the stick is running on changes.
					}
					else
					{
						double normalizedVelocity = _kinematicState.GetVelocity().Magnitude() / _kinematicState.GetMaxVelocity();

						_soundManager.QueueToPlay( "collision", static_cast< float >( normalizedVelocity * 100.0 ), false, false, false );
					}
				}
			}
			break;
		case Dead:
			{
				// Specific death sounds.
				if( _causeOfDeath == DeathRay )
				{
					_soundManager.QueueToPlay( "death_ray_explode", 100.0f, false, false, false );
				}
				else if( _causeOfDeath == SawBlade )
				{
					_soundManager.QueueToPlay( "saw_blade_buzz", 100.0f, false, false, false );
				}


				// Physics sounds.
				Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );

				double lastContactForce = 0;
				if( ( lastContactForce = worldSimulationHandle->GetLastContactForce() ) > 0 )
				{
					float impactVolume = static_cast< float >( min( lastContactForce * 100.0 + 25.0, 100.0 ) );
					if( impactVolume < 25.0f )
						impactVolume = 0.0f;
					_soundManager.QueueToPlay( "collision", impactVolume, false, true, false );
				}
			}
			break;
		case Sliding:
			{
				_soundManager.QueueToPlay( "sliding", 100.0f, true, false );
			}
			break;
		}

		_soundManager.Update();
	}

	/* Returns true if the specified key is held down. */
	bool StickFigure::IsKeyDown( KeyboardKey::Key key )
	{
		return Game::GetGameServiceManager().GetGraphicsProvider()->IsKeyDown( Game::GetGameAssetManager().GetAppWindow(), key );
	}

	/* Returns true if the stick figure can fall. */
	bool StickFigure::CanFall()
	{
		bool canFall = false;

		/* Return true if the stick figure can be moved downward 1 pixel without collision. */

		KinematicState testState = _kinematicState;
		testState.SetVelocity( Vector2D( 0, 1 ) );
		testState.SetAcceleration( Vector2D( 0, 0 ) );

		testState.UpdatePositionAndVelocity( 1 );

		// Temporarily center collidable area around testState's position to so that we can call into 
		// the Map object to test for a collision (as Map holds a pointer to the bounds of this stick figure's 
		// collidable area). We'll set this back afterward.

		_collidableArea.UpdatePosition( testState.GetPosition() );

		Handle< Map > mapHandle = Game::GetGameObjectManager().GetGameObject( "map" );
		assert( mapHandle.Valid() );

		canFall = mapHandle->GetObjectsCollidingWith( Id() ).size() == 0;

		// Reset center of collidable area for the stick figure
		_collidableArea.UpdatePosition( _kinematicState.GetPosition() );

		return canFall;
	}

	/* Returns true if the stick figure is in a collision with another object. */
	bool StickFigure::InCollision()
	{
		/* Get the map object and use it to test if this stick figure is in a collision with other
		   objects. */
		bool inCollision = false;

		Handle< Map > mapHandle = Game::GetGameObjectManager().GetGameObject( "map" );
		assert( mapHandle.Valid() );

		inCollision = mapHandle->GetObjectsCollidingWith( Id() ).size() != 0;

		return inCollision;
	}

	/* Returns true if the stick figure's physics entity is within the bounds of the map. */
	bool StickFigure::InMapBounds()
	{
		bool inMapBounds = false;

		Handle< Map > mapHandle = Game::GetGameObjectManager().GetGameObject( "map" );
		assert( mapHandle.Valid() );

		if( mapHandle->GetMapBounds().Contains( 
			Point2D( _collidableArea.GetMinX(), _kinematicState.GetPosition()._y ) ) &&
			mapHandle->GetMapBounds().Contains( 
				Point2D( _collidableArea.GetMaxX(), _kinematicState.GetPosition()._y ) ) &&
			mapHandle->GetMapBounds().Contains( 
				Point2D( _kinematicState.GetPosition()._x, _collidableArea.GetMinY() ) ) &&
			mapHandle->GetMapBounds().Contains( 
				Point2D( _kinematicState.GetPosition()._x, _collidableArea.GetMaxY() ) ) )
		{
			inMapBounds = true;
		}

		return inMapBounds;
	}

	/* Returns true if the stick figure is wall sticking (i.e. preparing for a wall jump from 
	   a vertical wall). */
	bool StickFigure::IsWallSticking()
	{
		bool isWallSticking = false;

		// This routine will be called after a collision has been detected & resolved. We'll determine if the 
		// stick figure has collided with a surface that is eligible for wall sticking by looking at its 
		// pre-collision/post-collision velocity vectors. The stick has encountered a surface that is eligible 
		// for wall sticking if 1) the pre-collision vector equals the post-collision vector and the pre-collision 
		// vector has an angle of 0 or 180 or 2) the vector sum of the pre-collision and post-collision vectors
		// has an angle of 90 or 270.
		KinematicState preCollisionState = _kinematicState.GetPreviousPositionAndVelocity();

		if( !preCollisionState.GetVelocity().IsZero() && 
			( EqualDouble( preCollisionState.GetVelocity().Angle(), 0.0 ) ||
			  EqualDouble( preCollisionState.GetVelocity().Angle(), 180.0 ) ) &&
		     preCollisionState.GetVelocity() == -1 * _kinematicState.GetVelocity() )
		{
			isWallSticking = true;
		}
		else 
		{
			Vector2D sum = preCollisionState.GetVelocity() + _kinematicState.GetVelocity();
			if( !preCollisionState.GetVelocity().IsZero() &&
				!_kinematicState.GetVelocity().IsZero() &&
				!sum.IsZero() && ( EqualDouble( sum.Angle(), 90.0 ) || EqualDouble( sum.Angle(), 270.0 ) ) )
			{
				isWallSticking = true;
			}
		}

		return isWallSticking;
	}

	/* Verifies that the stick figure can move with the post-collision velocity and 
	   acceleration, otherwise it zeros them out. */
	void StickFigure::VerifyCollisionResolution()
	{
		KinematicState savedKinematicState = _kinematicState;
		_kinematicState.UpdatePositionAndVelocity( Game::GetGameUpdatePeriod() );
		_collidableArea.UpdatePosition( _kinematicState.GetPosition() );
		_kinematicState = savedKinematicState;
		if( InCollision() )
		{
			_kinematicState.SetVelocity( Vector2D( 0, 0 ) );
			_kinematicState.SetAcceleration( Vector2D( 0, 0 ) );
		}
		_collidableArea.UpdatePosition( _kinematicState.GetPosition() );
	}

	/* Currently, all traps kill you instantly on contact. This functions checks if the stick figure has 
	   collided with a trap given a list of colliding objects and sets the isDying flag/cause of death 
	   variable accordingly. */
	bool StickFigure::HasCollidedWithTrap( std::vector< CollidableGameObject* > collidingObjects )
	{
		bool hasCollidedWithTrap = false;

		for( std::vector< CollidableGameObject* >::const_iterator collidingObjectItr = collidingObjects.begin();
				collidingObjectItr != collidingObjects.end(); ++collidingObjectItr )
		{
			if( PROJECT_NAME::SawBlade *temp = dynamic_cast< PROJECT_NAME::SawBlade* >( *collidingObjectItr ) )
			{
				_isDying = true;
				_causeOfDeath = SawBlade;
				hasCollidedWithTrap = true;
				break;
			}

			if( PROJECT_NAME::DeathRay *temp = dynamic_cast< PROJECT_NAME::DeathRay* >( *collidingObjectItr ) )
			{
				_isDying = true;
				_causeOfDeath = DeathRay;
				hasCollidedWithTrap = true;
				break;
			}

			if( PROJECT_NAME::WreckingBall *temp = dynamic_cast< PROJECT_NAME::WreckingBall* >( *collidingObjectItr ) )
			{
				_isDying = true;
				_causeOfDeath = WreckingBall;
				hasCollidedWithTrap = true;
				break;
			}

			if( PROJECT_NAME::FlameTrap *temp = dynamic_cast< PROJECT_NAME::FlameTrap* >( *collidingObjectItr ) )
			{
				_isDying = true;
				_causeOfDeath = FlameTrap;
				hasCollidedWithTrap = true;
				break;
			}
		}

		return hasCollidedWithTrap;
	}

	/* Checks to see if the stick has collided with the map exit and signals a level transition if so. */
	bool StickFigure::HasCollidedWithExit( std::vector< CollidableGameObject* > collidingObjects )
	{
		bool hasCollidedWithExit = false;

		for( std::vector< CollidableGameObject* >::const_iterator collidingObjectItr = collidingObjects.begin();
				collidingObjectItr != collidingObjects.end(); ++collidingObjectItr )
		{
			if( PROJECT_NAME::MapExit *temp = dynamic_cast< PROJECT_NAME::MapExit* >( *collidingObjectItr ) )
			{
				hasCollidedWithExit = true;

				// Play level victory sound.
				_soundManager.QueueToPlay( "level_victory", 100.0f, false, false, false );

				_soundManager.Update();

				// Signal level transition.
				GameEvent levelTransitionEvent;
				levelTransitionEvent._event = GameEvent::LevelTransition;

				Game::GetGameAssetManager().GetGameEventQueue().PushEvent( levelTransitionEvent );

				break;
			}
		}

		return hasCollidedWithExit;
	}

	/* Sets the facing direction of the stick figure based on its kinematic state. */
	void StickFigure::SetFacingDirection()
	{
		if( _kinematicState.GetAcceleration().IsZero() &&
			!EqualDouble( _facingDirection.Angle(), 0 ) &&
			!EqualDouble( _facingDirection.Angle(), 180 ) )
		{
			_facingDirection = _kinematicState.GetVelocity().IsZero() ? Vector2D( 1, 0 ) :
				_kinematicState.GetVelocity().ProjectOnto( Vector2D( 1, 0 ) ).UnitVector();
		}

		if( !_kinematicState.GetAcceleration().IsZero() &&
			!EqualDouble( _kinematicState.GetAcceleration().Magnitude(), ACCELERATION_OF_GRAVITY ) )
		{
			_facingDirection = !_kinematicState.GetVelocity().IsZero() ?
				_kinematicState.GetVelocity().UnitVector() :
				_kinematicState.GetAcceleration().UnitVector();
		}

	}

	/* Logs information about this StickFigure's state to a log file. */
	void StickFigure::LogStateInfo( bool overwrite )
	{
		std::ofstream logFileStream;

		if( overwrite )
		{
			logFileStream.open( _logFile.c_str() );
		}
		else
		{
			logFileStream.open( _logFile.c_str(), std::ofstream::app );
		}
		assert( logFileStream );

		std::string currentStateString;
		switch( _currentState )
		{
		case Idle:
			currentStateString = "Idle";
			break;
		case RunningRight: 
			currentStateString = "RunningRight";
			break;
		case RunningLeft:
			currentStateString = "RunningLeft";
			break;
		case Jumping:
			currentStateString = "Jumping";
			break;
		case Collision:
			currentStateString = "Collision";
			break;
		case Falling:
			currentStateString = "Falling";
			break;
		case Sliding:
			currentStateString = "Sliding";
			break;
		}

		logFileStream << "Stick Figure State Info: " << std::endl << std::endl
			<< "Current state label = " << currentStateString << std::endl
			<< "Kinematic state: " << std::endl << _kinematicState << std::endl
			<< "Collidable Area: " << std::endl << _collidableArea << std::endl
			<< "Facing Direction = " << _facingDirection << std::endl
			<< "IsSliding = " << _isSliding << std::endl << std::endl;
	}

	/* Logs a message to the StickFigure log file. */
	void StickFigure::Log( std::string message, bool overwrite )
	{
		std::ofstream logFileStream;

		if( overwrite )
		{
			logFileStream.open( _logFile.c_str() );
		}
		else
		{
			logFileStream.open( _logFile.c_str(), std::ofstream::app );
		}
		assert( logFileStream );

		logFileStream << message;
	}

	/* Static data */

	const double StickFigure::_wallStickTime = 0.25; 
	const double StickFigure::_maxRunVelocity = 175.0; 
	const double StickFigure::_runAcceleration = 350.0;
	const double StickFigure::_jumpVelocity = 150.0;
	const double StickFigure::_jumpLateralVelocityFactor = 0.75; 
	const double StickFigure::_slideThreshold = 150.0; 
	const double StickFigure::_terminalVelocity = 300.0;
	const double StickFigure::_wallJumpLateralVelocity = 110.0;
	const double StickFigure::_wallFallLateralVelocity = 15.0;
	const double StickFigure::_jumpToFallVelocity = 200.0;
	const double StickFigure::_fallToDeathVelocity = 290.0;
	const double StickFigure::_attractGroundVelocity = 100.0;

	// Files
	const std::string StickFigure::_logFile = RESOURCE_PATH( "Logs/StickFigureState.txt" );
	const std::string StickFigure::_soundFile = RESOURCE_PATH( "Audio/StickFigure/SoundFile.txt" );
}