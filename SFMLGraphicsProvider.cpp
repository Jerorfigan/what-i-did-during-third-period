/* Implementation for SFMLGraphicsProvider */

#include "stdafx.h"

#include <cmath>

#include "MouseButton.h"
#include "SFMLGraphicsProvider.h"
#include "Timer.h"

namespace PROJECT_NAME
{
	/* Default constructor initializes _nextId member to 1. */
	SFMLGraphicsProvider::SFMLGraphicsProvider() : _nextId( 1 )
	{

	}

	/* InitializeWindow
		   Summary: Initializes a render window. 
		   Parameters: 
		      gameSpace: The rectangle that defines the game space that the window will be responsible for
			             rendering.
			  title (optional): The desired title of the window (default is blank). 
		      width (optional): The desired width in pixels of the window (default will be set internally). 
			  height (optional): The desired height in pixels of the window (default will be set internally). 
			  fullscreen (optional): A boolean flag indicating whether this window should be 
									 fullscreen (default is true).
		   Returns: The ID allocated for this render window, by which the render window may be referenced 
		            from the client app. */
	SFMLGraphicsProvider::ResourceId SFMLGraphicsProvider::InitializeWindow( const Rectangle2D &gameSpace, 
		std::string title, std::size_t width, std::size_t height, bool fullscreen )
	{
		// Keep track of certain render window settings, as we'll store these later.
		RenderWindowSettings settings;

		// Create the window's video mode
		if( width == 0 || height == 0 )
		{
			assert( width == 0 && height == 0 );
			width = sf::VideoMode::GetMode( 0 ).Width;
			height = sf::VideoMode::GetMode( 0 ).Height;
		}
		sf::VideoMode videoMode( width, height );
		assert( videoMode.IsValid() );

		// Create the window's style
		unsigned long windowStyle;
		if( fullscreen )
		{
			windowStyle = sf::Style::Fullscreen;

			settings._isFullscreen = true;
		}
		else
		{
			windowStyle = sf::Style::Close | sf::Style::Titlebar;

			settings._isFullscreen = false;
		}

		_renderWindows[ _nextId ] = Handle< sf::RenderWindow >( 
			new sf::RenderWindow( videoMode, title, windowStyle ) );

		_renderWindows[ _nextId ]->ShowMouseCursor( false );

		// Set window's game space.
		_gameSpaces[ _nextId ] = gameSpace;

		// Set window's view.
		SetView( _nextId );

		// Set the window's settings.
		_renderWindowSettings[ _nextId ] = settings;

		// Set the window's icon.
		sf::Image appIcon;
		appIcon.LoadFromFile( RESOURCE_PATH( "Art/AppIcon.png" ) );

		_renderWindows[ _nextId ]->SetIcon( 128, 128, appIcon.GetPixelsPtr() );

		return _nextId++;
	}

	/* PrepareWindowForDrawing
	   Summary: Prepares the specified render window for drawing (i.e. clears the backbuffer, etc). 
	   Parameters: 
	      windowId: The ID of the render window to be prepared for drawing. */
	void SFMLGraphicsProvider::PrepareWindowForDrawing( ResourceId windowId )
	{
		bool validId = ValidRenderWindow( windowId );
		assert( validId );
		
		if( validId )
		{
			// Clear window to white background
			_renderWindows[ windowId ]->Clear( sf::Color(255,255,255) );
		}
	}

	/* GetEvent
	   Summary: Checks for and retrieves an event from the queue of events that have occurred on this
	            window. Subsequent calls will retrieve additional events from the queue. 
	   Parameters: 
	      windowId: The ID of the render window to check and retrieve events for. 
		  aEvent: An empty GameEvent object that will be filled with the retrieved event if there is one. 
	   Returns: True if an event was retrieved from the queue, false otherwise. */
	bool SFMLGraphicsProvider::GetGameEvent( ResourceId windowId, GameEvent &gameEvent )
	{ 
		bool eventFound = false;

		bool validId = ValidRenderWindow( windowId );
		assert( validId );
		
		if( validId )
		{
			sf::Event wndEvent;
			if( _renderWindows[ windowId ]->GetEvent( wndEvent ) )
			{
				/* Translate sf::Event object into the IGraphicsProvider::GameEvent object that will go back 
				   to client. */
				if( wndEvent.Type == sf::Event::Closed )
				{
					/* User closed window. */

					gameEvent._event = GameEvent::Closed;
					eventFound = true;
				}
				else if( wndEvent.Type == sf::Event::KeyPressed )
				{
					switch( wndEvent.Key.Code )
					{
					case sf::Key::Escape:
						{
							/* User pressed escape key. */

							gameEvent._event = GameEvent::KeyPressed;
							gameEvent._key = KeyboardKey::Escape;
							eventFound = true;
						}
						break;
					case sf::Key::Up:
						{
							/* User pressed up arrow key. */

							gameEvent._event = GameEvent::KeyPressed;
							gameEvent._key = KeyboardKey::Up;
							eventFound = true;
						}
						break;
					case sf::Key::Down:
						{
							/* User pressed down arrow key. */

							gameEvent._event = GameEvent::KeyPressed;
							gameEvent._key = KeyboardKey::Down;
							eventFound = true;
						}
						break;
					case sf::Key::Right:
						{
							/* User pressed right arrow key. */

							gameEvent._event = GameEvent::KeyPressed;
							gameEvent._key = KeyboardKey::Right;
							eventFound = true;
						}
						break;
					case sf::Key::Left:
						{
							/* User pressed left arrow key. */

							gameEvent._event = GameEvent::KeyPressed;
							gameEvent._key = KeyboardKey::Left;
							eventFound = true;
						}
						break;
					}
				}
				else if( wndEvent.Type == sf::Event::TextEntered )
				{
					gameEvent._event = GameEvent::KeyPressed;
					gameEvent._key = KeyboardKey::Character;
					gameEvent._charCode = wndEvent.Text.Unicode;
					eventFound = true;
				}
				else if( wndEvent.Type == sf::Event::MouseMoved )
				{
					gameEvent._event = GameEvent::MouseMoved;
					gameEvent._mouseHotSpot = WindowPointToGamespace( windowId, wndEvent.MouseMove.X,
						wndEvent.MouseMove.Y );
					eventFound = true;
				}
				else if( wndEvent.Type == sf::Event::MouseEntered )
				{
					gameEvent._event = GameEvent::MouseEnteredWindow;
					eventFound = true;
				}
				else if( wndEvent.Type == sf::Event::MouseLeft )
				{
					gameEvent._event = GameEvent::MouseLeftWindow;
					eventFound = true;
				}
				else if( wndEvent.Type == sf::Event::MouseButtonPressed && wndEvent.MouseButton.Button == sf::Mouse::Left )
				{
					/* User clicked the left-mouse button. */

					gameEvent._event = GameEvent::MouseButtonPressed;
					gameEvent._mouseButton = MouseButton::LeftButton;
					gameEvent._mouseHotSpot = WindowPointToGamespace( windowId, wndEvent.MouseButton.X,
						wndEvent.MouseMove.Y );
					eventFound = true;
				}
				else if( wndEvent.Type == sf::Event::MouseButtonReleased && wndEvent.MouseButton.Button == sf::Mouse::Left )
				{
					/* User released the left-mouse button. */

					gameEvent._event = GameEvent::MouseButtonReleased;
					gameEvent._mouseButton = MouseButton::LeftButton;
					gameEvent._mouseHotSpot = WindowPointToGamespace( windowId, wndEvent.MouseButton.X,
						wndEvent.MouseMove.Y );
					eventFound = true;
				}
			}
		}

		return eventFound;
	}

	/* GetFrameTime
	   Summary: Gets the time elapsed in seconds between the last two instances where the specified window was 
	            displayed (i.e. the time that needs to be processed during the next update in the calling
				app).
	   Parameters: 
	      windowId: The ID of the render window to get the frame time for. 
	   Returns: The frame time in seconds. */
	double SFMLGraphicsProvider::GetFrameTime( ResourceId windowId )
	{
		double frameTime = 0.0;

		bool validId = ValidRenderWindow( windowId );
		assert( validId );

		if( validId )
		{
			frameTime = _renderWindows[ windowId ]->GetFrameTime();
		}

		return frameTime;
	}

	/* IsKeyDown
	   Summary: Checks if a window detects a key being held down.
	   Parameters: 
	      windowId: The ID of the window. 
	      key: The key to check.
	   Returns: True if the key is being held down, false otherwise. */
	bool SFMLGraphicsProvider::IsKeyDown( ResourceId windowId, KeyboardKey::Key key )
	{
		bool isKeyDown = false;

		bool validId = ValidRenderWindow( windowId );
		assert( validId );

		if( validId )
		{
			switch( key )
			{
			case KeyboardKey::Escape:
				isKeyDown = _renderWindows[ windowId ]->GetInput().IsKeyDown( sf::Key::Escape );
				break;
			case KeyboardKey::Up:
				isKeyDown = _renderWindows[ windowId ]->GetInput().IsKeyDown( sf::Key::Up );
				break;
			case KeyboardKey::Down:
				isKeyDown = _renderWindows[ windowId ]->GetInput().IsKeyDown( sf::Key::Down );
				break;
			case KeyboardKey::Left:
				isKeyDown = _renderWindows[ windowId ]->GetInput().IsKeyDown( sf::Key::Left );
				break;
			case KeyboardKey::Right:
				isKeyDown = _renderWindows[ windowId ]->GetInput().IsKeyDown( sf::Key::Right );
				break;
			case KeyboardKey::Space:
				isKeyDown = _renderWindows[ windowId ]->GetInput().IsKeyDown( sf::Key::Space );
				break;
			}
		}

		return isKeyDown;
	}

	/* DisplayWindow
	   Summary: Displays the render window to the screen.
	   Parameters: 
	      windowId: The ID of the render window to display. */
	void SFMLGraphicsProvider::DisplayWindow( ResourceId windowId )
	{
		bool validId = ValidRenderWindow( windowId );
		assert( validId );

		if( validId )
		{
			/* Subtract the game space from the view area and cover up the resulting area
			   with black rectangles prior to displaying this render window. */
			Rectangle2D &gameSpace = _gameSpaces[ windowId ];
			sf::View &view = *_renderViews[ windowId ];

			sf::Vector2f viewHalfSize = view.GetHalfSize();
			sf::Vector2f viewCenter = view.GetCenter();

			double viewRectWidth = viewHalfSize.x * 2;
			double viewRectHeight = viewHalfSize.y * 2;
			Point2D viewRectCenter( viewCenter.x, viewCenter.y );
			Rectangle2D viewRect( viewRectCenter, viewRectWidth, viewRectHeight );

			if( EqualDouble( viewRect.Left(), gameSpace.Left() ) )
			{
				/* Difference areas are located at the top and bottom of the game space, so place
				black rectangles there if those areas are non-zero. */

				if( !EqualDouble( viewRect.Bottom(), gameSpace.Bottom() ) )
				{
					sf::Shape topRect;
					topRect.AddPoint( 
						static_cast< float >( viewRect.BottomLeft()._x ),
						static_cast< float >( viewRect.BottomLeft()._y ), 
						sf::Color( 0, 0, 0 ) );
					topRect.AddPoint( 
						static_cast< float >( viewRect.BottomRight()._x ),
						static_cast< float >( viewRect.BottomRight()._y ), 
						sf::Color( 0, 0, 0 ) );
					topRect.AddPoint( 
						static_cast< float >( gameSpace.BottomRight()._x ),
						static_cast< float >( gameSpace.BottomRight()._y ), 
						sf::Color( 0, 0, 0 ) );
					topRect.AddPoint( 
						static_cast< float >( gameSpace.BottomLeft()._x ),
						static_cast< float >( gameSpace.BottomLeft()._y ), 
						sf::Color( 0, 0, 0 ) );

					topRect.EnableFill( true );
					topRect.EnableOutline( false );

					sf::Shape bottomRect;
					bottomRect.AddPoint(
						static_cast< float >( gameSpace.TopLeft()._x ),
						static_cast< float >( gameSpace.TopLeft()._y ),
						sf::Color( 0, 0, 0 ) );
					bottomRect.AddPoint(
						static_cast< float >( gameSpace.TopRight()._x ),
						static_cast< float >( gameSpace.TopRight()._y ),
						sf::Color( 0, 0, 0 ) );
					bottomRect.AddPoint(
						static_cast< float >( viewRect.TopRight()._x ),
						static_cast< float >( viewRect.TopRight()._y ),
						sf::Color( 0, 0, 0 ) );
					bottomRect.AddPoint(
						static_cast< float >( viewRect.TopLeft()._x ),
						static_cast< float >( viewRect.TopLeft()._y ),
						sf::Color( 0, 0, 0 ) );

					bottomRect.EnableFill( true );
					bottomRect.EnableOutline( false );

					_renderWindows[ windowId ]->Draw( topRect );
					_renderWindows[ windowId ]->Draw( bottomRect );
				}

			}
			else
			{
				/* Difference areas are located at the left and right of the game space, so place
				   black rectangles there. */

				sf::Shape leftRect;
				leftRect.AddPoint( 
					static_cast< float >( viewRect.BottomLeft()._x ),
					static_cast< float >( viewRect.BottomLeft()._y ),
					sf::Color( 0, 0, 0 ) );
				leftRect.AddPoint( 
					static_cast< float >( gameSpace.BottomLeft()._x ),
					static_cast< float >( gameSpace.BottomLeft()._y ),
					sf::Color( 0, 0, 0 ) );
				leftRect.AddPoint( 
					static_cast< float >( gameSpace.TopLeft()._x ),
					static_cast< float >( gameSpace.TopLeft()._y ),
					sf::Color( 0, 0, 0 ) );
				leftRect.AddPoint( 
					static_cast< float >( viewRect.TopLeft()._x ),
					static_cast< float >( viewRect.TopLeft()._y ),
					sf::Color( 0, 0, 0 ) );

				leftRect.EnableFill( true );
				leftRect.EnableOutline( false );

				sf::Shape rightRect;
				rightRect.AddPoint( 
					static_cast< float >( gameSpace.BottomRight()._x ), 
					static_cast< float >( gameSpace.BottomRight()._y ),
					sf::Color( 0, 0, 0 ) );
				rightRect.AddPoint( 
					static_cast< float >( viewRect.BottomRight()._x ), 
					static_cast< float >( viewRect.BottomRight()._y ),
					sf::Color( 0, 0, 0 ) );
				rightRect.AddPoint( 
					static_cast< float >( viewRect.TopRight()._x ), 
					static_cast< float >( viewRect.TopRight()._y ),
					sf::Color( 0, 0, 0 ) );
				rightRect.AddPoint( 
					static_cast< float >( gameSpace.TopRight()._x ), 
					static_cast< float >( gameSpace.TopRight()._y ),
					sf::Color( 0, 0, 0 ) );

				rightRect.EnableFill( true );
				rightRect.EnableOutline( false );

				_renderWindows[ windowId ]->Draw( leftRect );
				_renderWindows[ windowId ]->Draw( rightRect );
			}

			_renderWindows[ windowId ]->Display();
		}
	}

	/* GetSupportedDisplayResolutions
	   Summary: Returns a std::vector filled with the supported display resolutions. 
	   Returns: A std::vector filled with the supported display resolutions. */
	std::vector< SFMLGraphicsProvider::DisplayResolution > SFMLGraphicsProvider::GetSupportedDisplayResolutions()
	{
		std::vector< DisplayResolution > supportedResolutions;

		std::size_t videoModesCount = sf::VideoMode::GetModesCount();
		for( std::size_t modeIndex = 0; modeIndex < videoModesCount; ++modeIndex )
		{
			sf::VideoMode mode = sf::VideoMode::GetMode( modeIndex );

			supportedResolutions.push_back( DisplayResolution( mode.Width, mode.Height ) );
		}

		return supportedResolutions;
	}

	/* GetDisplayResolution
	   Summary: Returns the display resolution of a window. 
	   Parameters:
	      windowId: The ID of the window.
	   Returns: The display resolution of the window. */
	SFMLGraphicsProvider::DisplayResolution SFMLGraphicsProvider::GetDisplayResolution( ResourceId windowId )
	{
		DisplayResolution resolution;

		bool validId = ValidRenderWindow( windowId );
		assert( validId );

		if( validId )
		{
			resolution._width = _renderWindows[ windowId ]->GetWidth();
			resolution._height = _renderWindows[ windowId ]->GetHeight();
		}

		return resolution;
	}

	/* IsFullscreen 
		Summary: Determines whether a window is currently running at full screen. 
		Parameters:
		    windowId: The ID of the window to check. 
		Returns: True if the window is running at fullscreen, false otherwise. */
	bool SFMLGraphicsProvider::IsFullscreen( ResourceId windowId )
	{
		bool validId = ValidRenderWindow( windowId );
		assert( validId );

		if( validId )
		{
			return _renderWindowSettings[ windowId ]._isFullscreen;
		}

		return false;
	}

	/* DestroyWindow
		Summary: Destroys a window. 
		Parameters:
		    windowId: The ID of the window to destroy. */
	void SFMLGraphicsProvider::DestroyWindow( ResourceId windowId )
	{
		bool validId = ValidRenderWindow( windowId );
		assert( validId );

		if( validId )
		{
			_renderWindows.erase( windowId );
		}
	}

	/* InitializeString
	   Summary: Initializes a drawable string.
	   Parameters: 
	      contents: The contents that this drawable string should contain. 
	   Returns: The ID allocated for this drawable string, by which the drawable string entity may be
	            referenced from the client app. */
	SFMLGraphicsProvider::ResourceId SFMLGraphicsProvider::InitializeString( std::string contents )
	{
		_drawables[ _nextId ] = Handle< sf::Drawable >( new sf::String( contents ) );
		_drawables[ _nextId ]->SetColor( sf::Color( 0, 0, 0 ) ); 

		return _nextId++;
	}

	/* SetString
	   Summary: Sets a drawable string to a new string value.
	   Parameters: 
		  stringId: The ID of the drawable string.
	      newContents: The new contents that the drawable string should have. */
	void SFMLGraphicsProvider::SetString( ResourceId stringId, std::string newContents )
	{
		bool validId = ValidDrawable( stringId );
		assert( validId );

		if( validId )
		{
			// Convert the sf::Drawable handle to an sf::String handle so that we can call a function unique
			// to sf::String.
			Handle< sf::String > stringHandle( _drawables[ stringId ] );
			assert( stringHandle.Valid() );
			stringHandle->SetText( newContents );
		}
	}

	/* GetPixelLength 
		Summary: Returns the length of the string in pixels. 
		Parameters:
		    stringId: The ID of the string whose length in pixels is returned.
		Returns: The length of the string in pixels. */
	int SFMLGraphicsProvider::GetPixelLength( ResourceId stringId )
	{
		bool validId = ValidDrawable( stringId );
		assert( validId );

		if( validId )
		{
			// Convert the sf::Drawable handle to an sf::String handle so that we can call a function unique
			// to sf::String.
			Handle< sf::String > stringHandle( _drawables[ stringId ] );
			assert( stringHandle.Valid() );
			return static_cast< int >( stringHandle->GetRect().GetWidth() );
		}

		return 0;
	}
	
	/* InitializeSprite
	   Summary: Initializes a sprite.
	   Parameters: 
		  imageFile: The image file to associate with this sprite.
	   Returns: The ID allocated for this sprite, by which the sprite entity may be
	            referenced from the client app. */
	SFMLGraphicsProvider::ResourceId SFMLGraphicsProvider::InitializeSprite( std::string imageFile )
	{
		/* When initializing a sprite, check to see if the associated image has already
		   been loaded into memory. If it has, use the one from memory, otherwise load it. */

		if( IsImageLoaded( imageFile ) )
		{
			/* Image already in memory, so use that to create sprite. */
			_drawables[ _nextId ] = Handle< sf::Drawable >( new sf::Sprite( *_images[ imageFile ] ) );
		}
		else
		{
			LoadImage( imageFile );

			/* Now create sprite */
			_drawables[ _nextId ] = Handle< sf::Drawable >( new sf::Sprite( *_images[ imageFile ] ) );
		}

		return _nextId++;
	}

	/* SetSpriteImage 
	   Summary: Sets the source image for this sprite. 
	   Parameters:
	      spriteId: The ID of the sprite.
		  imageFile: The source image. */
	void SFMLGraphicsProvider::SetSpriteImage( ResourceId spriteId, std::string imageFile )
	{
		bool validId = ValidDrawable( spriteId );
		assert( validId );

		if( validId )
		{
			// Check to see if we need to load the image
			if( !IsImageLoaded( imageFile ) )
			{
				LoadImage( imageFile );
			}

			// Convert the sf::Drawable handle to an sf::Sprite handle so that we can call a function unique
			// to sf::Sprite.
			Handle< sf::Sprite > spriteHandle( _drawables[ spriteId ] );
			assert( spriteHandle.Valid() );
			spriteHandle->SetImage( *_images[ imageFile ] );
		}
	}

	/* SetSpriteSubRectangle
	   Summary: Sets the subrectangle for a sprite.
	   Parameters: 
		  spriteId: The ID of the sprite.
		  rect: The rectangle to set it to. */
	void SFMLGraphicsProvider::SetSpriteSubRectangle( ResourceId spriteId, Rectangle2D rect )
	{
		bool validId = ValidDrawable( spriteId );
		assert( validId );

		if( validId )
		{
			/* Important: Rectangle2D (which adheres to the cartesian coordinate system) is upside down relative to 
			              sf::IntRect (which adheres to the render window coordinate system), to need to reverse top
						  and bottom when converting between the two types. */

			// Convert the sf::Drawable handle to an sf::Sprite handle so that we can call a function unique
			// to sf::Sprite.
			Handle< sf::Sprite > spriteHandle( _drawables[ spriteId ] );
			assert( spriteHandle.Valid() );
			spriteHandle->SetSubRect( 
				sf::IntRect( static_cast< int >( rect.Left() ), 
				             static_cast< int >( rect.Bottom() ), 
							 static_cast< int >( rect.Right() ), 
							 static_cast< int >( rect.Top() ) ) );
		}
	}

	/* GetSpriteSubRectangle
	   Summary: Gets the subrectangle for a sprite.
	   Parameters: 
		  spriteId: The ID of the sprite.
	   Returns: The subrectangle */
	Rectangle2D SFMLGraphicsProvider::GetSpriteSubRectangle( ResourceId spriteId )
	{
		Rectangle2D rect;
		bool validId = ValidDrawable( spriteId );
		assert( validId );

		if( validId )
		{
			/* Important: Rectangle2D (which adheres to the cartesian coordinate system) is upside down relative to 
			              sf::IntRect (which adheres to the render window coordinate system), so need to reverse top and 
						  bottom when converting between the two types. */

			// Convert the sf::Drawable handle to an sf::Sprite handle so that we can call a function unique
			// to sf::Sprite.
			Handle< sf::Sprite > spriteHandle( _drawables[ spriteId ] );
			assert( spriteHandle.Valid() );
			sf::IntRect intRect = spriteHandle->GetSubRect();
			rect = Rectangle2D( Point2D( intRect.Left, intRect.Bottom ),
								Point2D( intRect.Right, intRect.Bottom ),
								Point2D( intRect.Left, intRect.Top ),
								Point2D( intRect.Right, intRect.Top ) );
		}

		return rect;
	}


	/* SetDrawablePosition
	   Summary: Sets the position of a drawable.
	   Parameters: 
		  drawableId: The ID of the drawable.
		  drawablePos: The position of the drawable. */
	void SFMLGraphicsProvider::SetDrawablePosition( ResourceId drawableId, Point2D drawablePos )
	{
		bool validId = ValidDrawable( drawableId );
		assert( validId );

		if( validId )
		{
			_drawables[ drawableId ]->SetPosition( static_cast< float >( drawablePos._x ), 
												   static_cast< float >( drawablePos._y ) );
		}
	}

	/* GetDrawablePosition
	   Summary: Gets the position of a drawable.
	   Parameters: 
		  drawableId: The ID of the drawable.
	   Returns: The position of the drawable. */
	Point2D SFMLGraphicsProvider::GetDrawablePosition( ResourceId drawableId )
	{
		Point2D position;

		bool validId = ValidDrawable( drawableId );
		assert( validId );

		if( validId )
		{
			sf::Vector2f vecPos = _drawables[ drawableId ]->GetPosition();
			position = Point2D( vecPos.x, vecPos.y );
		}

		return position;
	}

	/* SetDrawableCenter
	   Summary: Sets the center point of the drawable.
	   Parameters: 
		  drawableId: The ID of the drawable.
		  drawableCenter: The center point of the drawable. */
	void SFMLGraphicsProvider::SetDrawableCenter( ResourceId drawableId, Point2D drawableCenter )
	{
		bool validId = ValidDrawable( drawableId );
		assert( validId );

		if( validId )
		{
			_drawables[ drawableId ]->SetCenter( static_cast< float >( drawableCenter._x ), 
												 static_cast< float >( drawableCenter._y ) );
		}
	}

	/* SetDrawableRotation
	   Summary: Sets the rotation of the drawable. 
	   Parameters:
		  drawableId: The ID of the drawable.
		  angle: The angle to set the drawable to (in degrees). */
	void SFMLGraphicsProvider::SetDrawableRotation( ResourceId drawableId, double angle )
	{
		bool validId = ValidDrawable( drawableId );
		assert( validId );

		if( validId )
		{
			_drawables[ drawableId ]->SetRotation( static_cast< float >( angle ) );
		}
	}

	/* SetDrawableScale
		Summary: Sets the scale of the drawable.
		Paramters:
		   drawableId: The ID of the drawable. 
		   xScaleFactor: The x-axis scale factor.
		   yScaleFactor: The y-axis scale factor. */
	void SFMLGraphicsProvider::SetDrawableScale( ResourceId drawableId, double xScaleFactor, double yScaleFactor )
	{
		bool validId = ValidDrawable( drawableId );
		assert( validId );

		if( validId )
		{
			_drawables[ drawableId ]->SetScale( static_cast< float >( xScaleFactor ), static_cast< float >( yScaleFactor ) );
		}
	}

	/* DrawDrawable
	   Summary: Draws a drawable in a window.
	   Parameters: 
		  drawableId: The ID of the drawable.
		  windowId: The ID of the window. */
	void SFMLGraphicsProvider::DrawDrawable( ResourceId drawableId, ResourceId windowId )
	{
		bool validId = ValidDrawable( drawableId );
		bool validWindow = ValidRenderWindow( windowId );
		assert( validId && validWindow );

		if( validId && validWindow )
		{
			_renderWindows[ windowId ]->Draw( *_drawables[ drawableId ] );
		}
	}

	/* DestroyDrawable
	   Summary: Destroys a drawable.
	   Parameters: 
		  drawableId: The ID of the drawable. */
	void SFMLGraphicsProvider::DestroyDrawable( ResourceId drawableId )
	{
		bool validId = ValidDrawable( drawableId );
		assert( validId );

		if( validId )
		{
			_drawables.erase( drawableId );
		}
	}

	/* Private function used to verify that the passed-in ResourceId refers to an actual RenderWindow. */
	bool SFMLGraphicsProvider::ValidRenderWindow( ResourceId id )
	{
		bool validRenderWindow = false;

		std::map< ResourceId, Handle< sf::RenderWindow > >::iterator findResult = _renderWindows.find( id );

		if( findResult != _renderWindows.end() )
		{
			validRenderWindow = true;
		}

	    return validRenderWindow;
	}

	/* Private function used to verify that the passed-in ResourceId refers to an actual Drawable. */
	bool SFMLGraphicsProvider::ValidDrawable( ResourceId id )
	{
		bool validDrawable = false;

		std::map< ResourceId, Handle< sf::Drawable > >::iterator findResult = _drawables.find( id );

		if( findResult != _drawables.end() )
		{
			validDrawable = true;
		}

		return validDrawable;
	}

	/* Loads the specified image into the _images container. */
	void SFMLGraphicsProvider::LoadImage( std::string imageFile )
	{
		// Caller should check this already, but we'll check again just in case.
		assert( !IsImageLoaded( imageFile ) );

		/* Image not in memory, so load it. */
		Handle< sf::Image > image( new sf::Image() );
		assert( image->LoadFromFile( imageFile ) );
		_images[ imageFile ] = image;
	}

	/* Returns true if the specified image is already in the _images container. */
	bool SFMLGraphicsProvider::IsImageLoaded( std::string imageFile )
	{
		std::map< std::string, Handle< sf::Image > >::iterator findResult =
			_images.find( imageFile );
		return findResult != _images.end();
	}

	/* Resets the view of a window to the smallest possible that contains the gamespace and matches
	   the aspect ratio of the window. */
	void SFMLGraphicsProvider::SetView( ResourceId id )
	{
		// Get the width and height of the render window. Compare the window's aspect ratio
		// to that of the game space. If the window's aspect ratio is greater than that of the
		// game space, set the view's height to the game space height and compute a width X 
		// such that the view has the same aspect ratio as the window. If the window's aspect
		// ratio is less than that of the game space, set the view's width equal to the game
		// space width and compute a height X such that the view has the same aspect ratio as the
		// window. If the window's aspect ratio matches that of the game space, set the view's
		// width and height equal to that of the game space.
		
		sf::RenderWindow &renderWindow = *_renderWindows[ id ];
		Rectangle2D gameSpace = _gameSpaces[ id ];

		double windowWidth = renderWindow.GetWidth();
		double windowHeight = renderWindow.GetHeight();
		double windowAspect = windowWidth / windowHeight;
		double gameAspect = gameSpace.Width() / gameSpace.Height();

		float viewWidth;
		float viewHeight;

		if( windowAspect > gameAspect || EqualDouble( windowAspect, gameAspect ) )
		{
			viewWidth = static_cast< float >( windowAspect * gameSpace.Height() );
			viewHeight = static_cast< float >( gameSpace.Height() );
		}
		else 
		{
			viewWidth = static_cast< float >( gameSpace.Width() );
			viewHeight = static_cast< float >( gameSpace.Width() / windowAspect );
		}

		sf::Vector2f viewCenter( static_cast< float >( gameSpace.Center()._x ), 
			static_cast< float >( gameSpace.Center()._y ) );
		sf::Vector2f viewHalfSize( viewWidth / 2.0f, viewHeight / 2.0f );

		// Create and store the view in _renderViews, since the render window only holds onto a 
		// reference.
		_renderViews[ id ] = Handle< sf::View >( new sf::View( viewCenter, viewHalfSize ) );

		renderWindow.SetView( *_renderViews[ id ] );
	}

	/* Converts a point in window client coordinates to one in gamespace coordinates. */
	Point2D SFMLGraphicsProvider::WindowPointToGamespace( ResourceId id, int x, int y )
	{
		// Get the dimensions of the window, as well as the dimensions of the view and gamespace associated with 
		// this window.
		double gameWindowWidth = _renderWindows[ id ]->GetWidth();
		double gameWindowHeight = _renderWindows[ id ]->GetHeight();
		double gameSpaceWidth = _gameSpaces[ id ].Width();
		double gameSpaceHeight = _gameSpaces[ id ].Height();
		double gameViewWidth = 2 * _renderViews[ id ]->GetHalfSize().x;
		double gameViewHeight = 2 * _renderViews[ id ]->GetHalfSize().y;

		// The view is constructed to be the smallest rectangle containing the game space and 
		// that is also the same aspect ratio as the window. This means that either the view's width or height
		// (or both) matches the width or height of the gamespace, so determine which dimension matches.
		// The non-matching dimension (if there is one) will be used to compute the thickness of the black
		// bars that extend outside the gamespace.
		double blackBarThickness = 0;
		bool verticalBars = false;
		if( gameViewWidth == gameSpaceWidth )
		{
			blackBarThickness = ( gameViewHeight - gameSpaceHeight ) / 2.0; 
			verticalBars = false;
		}
		else
		{
			blackBarThickness = ( gameViewWidth - gameSpaceWidth ) / 2.0;
			verticalBars = true;
		}

		// Scale the point from window client coordinates to game view coordinates.
		double viewPointX = gameViewWidth * static_cast< double >( x ) / gameWindowWidth; 
		double viewPointY = gameViewHeight * static_cast< double >( y ) / gameWindowHeight; 

		// Take into account the black bar thickness when returning the point.
		Point2D gameSpacePoint;
		gameSpacePoint._x = verticalBars ? viewPointX - blackBarThickness : viewPointX;
		gameSpacePoint._y = verticalBars ? viewPointY : viewPointY - blackBarThickness;

		return gameSpacePoint;
	}

}