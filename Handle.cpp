#pragma once

/* Implements the functionality of a generic handle class. */

#include "stdafx.h"

#include "Handle.h"

namespace PROJECT_NAME
{

	/* Default constructor initializes a handle to nothing. */
	template < typename ObjectType >
	Handle< ObjectType >::Handle() : _objectPtr( 0 ), _useCount( new std::size_t( 1 ) )
	{

	}

	/* Initializes a new handle from a pointer to a dynamically allocated object. */
	template < typename ObjectType >
	Handle< ObjectType >::Handle( ObjectType *newObject ) : _objectPtr( newObject ), _useCount( new std::size_t( 1 ) )
	{
		
	}

	/* Support conversion between handles to different types in an inheritance hierarchy. */
	template < typename ObjectType >
	template < typename foreignType >
	Handle< ObjectType >::Handle( const Handle< foreignType > &foreignHandle )
	{
		/* Perform a dynamic cast on _objectPtr member of foreignHandle to the
		   pointer type of this handle. If the cast succeeds, then this handle
		   will point to the same object as foreign handle. If the cast fails, 
		   this handle will be made a handle to nothing. */
		if( _objectPtr = dynamic_cast< ObjectType* >( foreignHandle._objectPtr ) )
		{
			_useCount = foreignHandle._useCount;
			++*_useCount;
		}
		else
		{
			_useCount = new std::size_t( 1 ); 	
		}
	}

	/* Copy constructor to handle copying of _objectPtr and _useCount. */
	template < typename ObjectType >
	Handle< ObjectType >::Handle( const Handle &copySource ) : _objectPtr( copySource._objectPtr ),
		_useCount( copySource._useCount )
	{
		++*_useCount;
	}

	/* Assignment operator to handle copying of _objectPtr and _useCount. */
	template < typename ObjectType >
	Handle< ObjectType >& Handle< ObjectType >::operator=( const Handle &assignmentSource )
	{
		++*assignmentSource._useCount;
		DecrementUse();
		_objectPtr = assignmentSource._objectPtr;
		_useCount = assignmentSource._useCount;
		return *this;
	}

	/* Dereference operator */
	template < typename ObjectType >
	ObjectType& Handle< ObjectType >::operator*()
	{
		assert( _objectPtr );
		return *_objectPtr;
	}

	/* Dereference operator for a const Handle */
	template < typename ObjectType >
	const ObjectType& Handle< ObjectType >::operator*() const
	{
		assert( _objectPtr );
		return *_objectPtr;
	}

	/* Arrow operator */
	template < typename ObjectType >
	ObjectType* Handle< ObjectType >::operator->()
	{
		assert( _objectPtr );
		return _objectPtr;
	}

	/* Arrow operator for a const Handle */
	template < typename ObjectType >
	const ObjectType* Handle< ObjectType >::operator->() const
	{
		assert( _objectPtr );
		return _objectPtr;
	}

	/* Support =,<,> by comparing underlying objects. */
	template < typename ObjectType >
	bool Handle< ObjectType >::operator==( const Handle &rhs )
	{
		return *this->_objectPtr == *rhs._objectPtr;
	}

	template < typename ObjectType >
	bool Handle< ObjectType >::operator!=( const Handle &rhs )
	{
		return *this->_objectPtr != *rhs._objectPtr;
	}

	/* Valid
	   Summary: Returns true if this handle points to an object, false if it points
	            to nothing. 
	   Returns: True if this handle points to an object, false if it points
	            to nothing. */
	template < typename ObjectType >
	bool Handle< ObjectType >::Valid()
	{
		return _objectPtr != 0;
	}

	/* Destructor to handle deletion of dynamic objects referenced by _objectPtr and _useCount. */
	template < typename ObjectType >
	Handle< ObjectType >::~Handle()
	{
		DecrementUse();
	}

	template < typename ObjectType >
	void Handle< ObjectType >::DecrementUse()
	{
		if( --*_useCount == 0 )
		{
			delete _objectPtr;
			delete _useCount;
		}
	}

}