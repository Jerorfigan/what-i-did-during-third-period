#pragma once

/* StickFigure defines the functionality for a player-controlled stick figure character. */

#include <string>

#include "CollidableGameObject.h"
#include "CollidableRectangle.h"
#include "DynamicGameObject.h"
#include "Game.h"
#include "Handle.h"
#include "Handle.cpp"
#include "KinematicState.h"
#include "ProceduralAnimator.h"
#include "SoundManager.h"
#include "SpriteAnimator.h"
#include "Timer.h"
#include "Vector2D.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class StickFigure : public VisibleGameObject, public DynamicGameObject, public CollidableGameObject
	{
	public:
		enum CauseOfDeath { None, SuddenImpact, SawBlade, DeathRay, WreckingBall, FlameTrap };
		enum StickFigureState { Idle, RunningRight, RunningLeft, Jumping, Collision, Falling, Sliding, Dead, Respawning, WallSticking };

		/* Initializes a StickFigure from a spawn position. */
		StickFigure( Point2D spawnPosition );

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

		/* Update
		   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
					last update (which should be accounted for in this update). 
		   Parameters: 
			  elapsedTime: The time elapsed in seconds since last update. */
		virtual void Update( double elapsedTime );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea();

	private:
		/* Gets the next state for the stick figure. */
		StickFigureState GetNextState( StickFigureState currentState );
		/* Transitions the stick figure to the given state. */
		void TransitionToState( StickFigureState state );
		/* Determines the appropriate animation to play for the stick figure based on its 
		   state and also updates the SpriteAnimator/ProceduralAnimator members. */
		void RunAnimationUpdateLogic( double elapsedTime );
		/* Determines the appropriate sound to play for the stick figure based on its 
		   state. */
		void RunSoundUpdateLogic( StickFigureState currentState );
		/* Returns true if the specified key is held down. */
		bool IsKeyDown( KeyboardKey::Key key );
		/* Returns true if the stick figure can fall. */
		bool CanFall();
		/* Returns true if the stick figure is in a collision with another object. */
		bool InCollision();
		/* Returns true if the stick figure is within the bounds of the map. */
		bool InMapBounds();
		/* Returns true if the stick figure is wall sticking (i.e. preparing for a wall jump from 
		   a vertical wall). */
		bool IsWallSticking();
		/* Verifies that the stick figure can move with the post-collision velocity and 
		   acceleration, otherwise it zeros them out. */
		void VerifyCollisionResolution();
		/* Currently, all traps kill you instantly on contact. This functions checks if the stick figure has 
		   collided with a trap given a list of colliding objects and sets the isDying flag/cause of death 
		   variable accordingly. */
		bool HasCollidedWithTrap( std::vector< CollidableGameObject* > collidingObjects );
		/* Checks to see if the stick has collided with the map exit and signals a level transition if so. */
		bool HasCollidedWithExit( std::vector< CollidableGameObject* > collidingObjects );
		/* Sets the facing direction of the stick figure based on its kinematic state. */
		void SetFacingDirection();
		/* Logs information about this StickFigure's current state to the StickFigure log file. */
		void LogStateInfo( bool overwrite = true );
		/* Logs a message to the StickFigure log file. */
		void Log( std::string message, bool overwrite = true );

		StickFigureState _currentState;
		CollidableRectangle _collidableArea;
		SpriteAnimator _spriteAnimator;
		Vector2D _facingDirection; // unit vector
		bool _isSliding;
		bool _isDying;
		CauseOfDeath _causeOfDeath;
		Timer _stateTimer;
		ProceduralAnimator _proceduralAnimator;
		SoundManager _soundManager;

		static const int _respawnTime = 10; // seconds
		static const double _wallStickTime; // seconds
		static const int _collidableWidth = 10; // in pixels
		static const int _collidableHeight = 60; // in pixels
		static const int _minPhysicsWidth = 1; // in pixels
		static const int _minPhysicsHeight = 1; // in pixels
		static const int _maxPhysicsWidth = 30; // in pixels
		static const int _maxPhysicsHeight = 80; // in pixels
		static const double _maxRunVelocity; // in pixels per second
		static const double _runAcceleration; // in pixels per second per second
		static const double _jumpVelocity; // in pixels per second
		// The percentage of lateral velocity retained when jumping.
		static const double _jumpLateralVelocityFactor; 
		// _slideThreshold is the minimum tangential acceleration that will cause the stick figure to 
		// slide on a slope.
		static const double _slideThreshold; 
		static const double _terminalVelocity; // in pixels per second
		// _wallJumpLateralVelocity is the lateral velocity of the stick figure when jumping from 
		// vertical walls in pixels per second.
		static const double _wallJumpLateralVelocity; 
		// _wallFallLateralVelocity is the lateral velocity of the stick figure when falling from 
		// vertical walls in pixels per second.
		static const double _wallFallLateralVelocity;
		// The velocity at which the stick figure will transition from jumping to falling.
		static const double _jumpToFallVelocity;
		// The velocity at which the stick figure will die when falling.
		static const double _fallToDeathVelocity;
		// The velocity at which you are attracted to the ground when the down arrow is held while
		// jumping.
		static const double _attractGroundVelocity; // pixels / sec
		
		// Files
		static const std::string _logFile;
		static const std::string _soundFile;

		// friend declarations
		friend class StickFigureCollisionPhysics;
	};

}