/* MapLineSketcher implementation. */

#include "stdafx.h"

#include "Game.h"
#include "Handle.h"
#include "Handle.cpp"
#include "MapLine.h"
#include "MapLineSketcher.h"

namespace PROJECT_NAME
{

	/* Initializes a MapLineSketcher from a MapLine object. */
	MapLineSketcher::MapLineSketcher( const MapLine &mapLine ) : ElementSketcher( mapLine.Id() )
	{
		if( mapLine._drawDirection == MapLine::StartToEnd )
		{
			_sketchStartingPoint = mapLine._lineSegment.Start();
			_sketchGrowthVector = Vector2D( mapLine._lineSegment.Start(), 
										    mapLine._lineSegment.End() ).UnitVector();
		}
		else
		{
			_sketchStartingPoint = mapLine._lineSegment.End();
			_sketchGrowthVector = Vector2D( mapLine._lineSegment.End(), 
										    mapLine._lineSegment.Start() ).UnitVector();
		}
		
		// Add 5 to account for the 5-pixel buffer of whitespace in the animation frame for the MapLine.
		_targetMagnitude = mapLine._lineSegment.Length() + 5;
		_currentMagnitude = 0.0;
	}

	/* InitializeSketch
	   Summary: Draws the object as it should appear before the sketch of it has begun (usually blank). */
	void MapLineSketcher::InitializeSketch()
	{
		Handle< VisibleGameObject > visibleGameObjectHandle = Game::GetGameObjectManager().GetGameObject( _objectId );
		assert( visibleGameObjectHandle.Valid() );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( visibleGameObjectHandle->GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	/* Sketch 
	   Summary: Draws the object as it should appear in the next frame of its sketch. */
	void MapLineSketcher::Sketch()
	{
		// Grow the _sketchGrowthVector by _sketchSpeed and derive the new subrectangle from
		// the vector.
		_currentMagnitude += 
			Game::GetGameServiceManager().GetGraphicsProvider()->GetFrameTime( 
				Game::GetGameAssetManager().GetAppWindow() ) * _sketchSpeed;
		if( _currentMagnitude > _targetMagnitude )
		{
			_currentMagnitude = _targetMagnitude;
		}

		// Draw the MapLine with the new subrectangle
		Handle< MapLine > mapLineHandle = Game::GetGameObjectManager().GetGameObject( _objectId );
		assert( mapLineHandle.Valid() );

		mapLineHandle->_subRect = Rectangle2D( 
			Point2D( 0, _currentMagnitude ), 
			Point2D( mapLineHandle->_mapLineWidth, _currentMagnitude ), 
			Point2D( 0, 0 ), 
			Point2D( mapLineHandle->_mapLineWidth, 0 ) );

		// The target FPS for the MapLine animation is 1.0, so by passing in a value greater than 1 here, we'll guarantee that the spriteAnimator
		// re-computes the subrectangle for the animation frame.
		mapLineHandle->_spriteAnimator.Update( 1.1 );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mapLineHandle->GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	/* GetSketchPoint 
	   Summary: Returns the simulated point that the drawing instrument would be at if the sketch was in fact 
				being done by hand. 
	   Returns: The simulated point that the drawing instrument would be at if the sketch was in fact 
				being done by hand.  */
	Point2D MapLineSketcher::GetSketchPoint() const
	{
		return Point2D( _currentMagnitude * _sketchGrowthVector + Vector2D( _sketchStartingPoint ) );
	}

	/* IsSketchFinished
	   Summary: Returns true if this sketch is finished. 
	   Returns: True if this sketch is finished. */
	bool MapLineSketcher::IsSketchFinished() const
	{
		return EqualDouble( _currentMagnitude, _targetMagnitude );
	}

	/* GetSketchDuration
		Summary: Returns the duration this sketch will take to complete in seconds. 
		Returns: The duration this sketch will take to complete in seconds. */
	double MapLineSketcher::GetSketchDuration() const
	{
		return _targetMagnitude / _sketchSpeed;
	}
	
	// The speed at which the MapLine should be sketched (in pixels per second)
	const double MapLineSketcher::_sketchSpeed = 1250;

}