/* CollidableLineSegment implementation. */

#include "stdafx.h"

#include "CollidableCircle.h"
#include "CollidableLineSegment.h"
#include "CollidableRectangle.h"

namespace PROJECT_NAME
{

	/* Initializes a CollidableLineSegment from a LineSegment2D. */
	CollidableLineSegment::CollidableLineSegment( const LineSegment2D &lineSeg ) : _lineSeg( lineSeg )
	{
		_minX = _lineSeg.Start()._x;
		_maxX = _lineSeg.End()._x;

		if( _lineSeg.Start()._y < _lineSeg.End()._y )
		{
			_minY = _lineSeg.Start()._y;
			_maxY = _lineSeg.End()._y;
		}
		else
		{
			_minY = _lineSeg.End()._y;
			_maxY = _lineSeg.Start()._y;
		}
	}

	/* CollidesWith 
	   Summary: Returns true if this CollidableArea overlaps otherArea. 
	   Returns: True if this CollidableArea overlaps otherArea. */
	bool CollidableLineSegment::CollidesWith( const CollidableArea* otherArea ) const
	{
		bool collisionFound = false;

		/* otherArea is one of the following:
		   CollidableRectangle
		   CollidableLineSegment
		   CollidableCircle

		   Determine which and then call the appropriate method to test for 
		   intersection of geometry. */
		if( const CollidableRectangle *collidableRectPtr = dynamic_cast< const CollidableRectangle* >( otherArea ) )
		{
			collisionFound = collidableRectPtr->_rect.Intersects( _lineSeg );
		}
		else if( const CollidableLineSegment *collidableLineSegPtr = dynamic_cast< const CollidableLineSegment* >( otherArea ) )
		{
			collisionFound = collidableLineSegPtr->_lineSeg.Intersects( _lineSeg );
		}
		else if( const CollidableCircle *collidableCirclePtr = dynamic_cast< const CollidableCircle* >( otherArea ) )
		{
			collisionFound = collidableCirclePtr->_circle.Intersects( _lineSeg );
		}
		else
		{
			// shouldn't get here...blow up!
			assert( 1 == 0 );
		}

		return collisionFound;
	}

	/* GetLineSegments 
	   Summary: Returns a vector filled with the line segments that comprise this CollidableArea. 
	   Returns: A vector filled with the line segments that comprise this CollidableArea. */
	std::vector< LineSegment2D > CollidableLineSegment::GetLineSegments() const
	{
		std::vector< LineSegment2D > lineSegments;
		lineSegments.push_back( _lineSeg );
		return lineSegments;
	}

	/* UpdatePosition
	   Summary: Updates the position of this CollidableArea given a reference point (ex: the center of the area). 
	   Parameters:
	      referencePoint: The reference point indicating how to update the position of the CollidableArea. */
	void CollidableLineSegment::UpdatePosition( const Point2D &referencePoint )
	{
		// NOTE: right now this is just stubbed in, as I currently have no use for repositioning
		// a collidable line.
	}

	/* UpdateLineSegment 
		Summary: Updates the LineSegment2D component of this CollidableLineSegment. 
		Parameters:
		    newLineSegment: The new line segment to update it with. */
	void CollidableLineSegment::UpdateLineSegment( const LineSegment2D &newLineSegment )
	{
		_lineSeg = newLineSegment;

		_minX = _lineSeg.Start()._x;
		_maxX = _lineSeg.End()._x;

		if( _lineSeg.Start()._y < _lineSeg.End()._y )
		{
			_minY = _lineSeg.Start()._y;
			_maxY = _lineSeg.End()._y;
		}
		else
		{
			_minY = _lineSeg.End()._y;
			_maxY = _lineSeg.Start()._y;
		}
	}

}