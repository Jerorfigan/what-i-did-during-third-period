/* MapExit implementation. */

#include "stdafx.h"

#include "Game.h"
#include "MapExit.h"

namespace PROJECT_NAME
{

	// Constructors
	
	MapExit::MapExit( GameObject::ObjectId id, Point2D position ) : GameObject( id ),
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( mMapExitImage ), id ),
		CollidableGameObject( id ), mCollidableArea( Rectangle2D( position, mMapExitWidth, mMapExitHeight ) )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( GraphicsId(), 
			Point2D( mMapExitImageWidth / 2.0, mMapExitImageHeight - mMapExitHeight / 2.0 ) );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( GraphicsId(), position );
	}

	// Virtual methods
	
	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void MapExit::Draw( double interpolation )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	/* GetCollidableArea
		Summary: Gets the CollidableArea for this CollidableGameObject. 
		Returns: The CollidableArea for this CollidableGameObject. */
	CollidableArea* MapExit::GetCollidableArea()
	{
		return &mCollidableArea;
	}

	// Static data
	
	const std::string MapExit::mMapExitImage = RESOURCE_PATH( "Art/Map/MapExit/MapExit.png" );

}