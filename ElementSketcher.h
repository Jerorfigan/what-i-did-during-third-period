#pragma once

/* Each ElementSketcher is tied to an object and is capable of drawing that object in visibly incremental steps 
   so as to simulate a hand sketch. */

#include "GameObject.h"
#include "Point2D.h"

namespace PROJECT_NAME
{

	class ElementSketcher
	{
	public:
		/* Initializes an ElementSketcher from an object id. */
		ElementSketcher( GameObject::ObjectId objectId );

		/* InitializeSketch
		   Summary: Draws the object as it should appear before the sketch of it has begun (usually blank). */
		virtual void InitializeSketch() = 0;
		/* Sketch 
		   Summary: Draws the object as it should appear in the next frame of its sketch. */
		virtual void Sketch() = 0;
		/* GetSketchPoint 
		   Summary: Returns the simulated point that the drawing instrument would be at if the sketch was in fact 
		            being done by hand. 
		   Returns: The simulated point that the drawing instrument would be at if the sketch was in fact 
		            being done by hand.  */
		virtual Point2D GetSketchPoint() const = 0;
		/* IsSketchFinished
		   Summary: Returns true if this sketch is finished. 
		   Returns: True if this sketch is finished. */
		virtual bool IsSketchFinished() const = 0;

		/* GetSketchDuration
		   Summary: Returns the duration this sketch will take to complete in seconds. 
		   Returns: The duration this sketch will take to complete in seconds. */
		virtual double GetSketchDuration() const = 0;

		/* GetObjectId 
		   Summary: Returns the id of the object tied to this ElementSketcher. 
		   Returns: The id of the object tied to this ElementSketcher. */
		GameObject::ObjectId GetObjectId() const;

		/* Virtual destructor to allow deletion of derived objects through ElementSketcher pointer. */
		virtual ~ElementSketcher();
	protected:
		GameObject::ObjectId _objectId;
	};

}