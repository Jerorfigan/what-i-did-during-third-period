#pragma once

/* Defines the functionality of a level indicator, which serves up level info. */

#include <string>

#include "Point2D.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class LevelIndicator : public VisibleGameObject
	{
		// Constructors
	public:
		LevelIndicator( std::string info );

		// Virtual methods
	public:
		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

		// Static data
	private:
		static const Point2D mPosition;
	};

}