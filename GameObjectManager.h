#pragma once

/* GameObjectManager defines the functionality to manage and manipulate a collection of game objects. This includes
   collectively updating dynamic game objects, drawing visible game objects, as well as keeping track of all
   collidable objects. */

#include <map>
#include <set>
#include <vector>

#include "CollidableGameObject.h"
#include "DynamicGameObject.h"
#include "GameObject.h"
#include "Handle.h"
#include "Handle.cpp"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class GameObjectManager
	{
	public:
		typedef std::map< GameObject::ObjectId, Handle< GameObject > > GameObjectMap;
		typedef std::map< VisibleGameObject::RenderOrder, Handle< VisibleGameObject > > DrawList;
		typedef std::multimap< DynamicGameObject::UpdatePriorityLevel, Handle< DynamicGameObject > > UpdateList;
		typedef std::set< GameObject::ObjectId > CollidableList;

		/* AddGameObject 
		   Summary: Adds an object to the GameObjectManager. 
		   Parameters:
		      objectHandle: A handle to the object to add. */
		void AddGameObject( Handle< GameObject > objectHandle );
		/* GetGameObject
		   Summary: Retrieves an object from the GameObjectManager. 
		   Parameters:
		      objectId: The id of the object to retrieve. 
		   Returns: A handle to the object matching the passed-in id or a handle to nothing if no
		            matching object is found. */
		Handle< GameObject > GetGameObject( GameObject::ObjectId objectId );
		/* GetGameObjectsOfType 
		   Summary: Returns a std::vector of handles to all objects matching the passed in object's type. 
		   Parameters:
		      object: The object whose type is used to find matching objects.
		   Returns: A std::vector of handles to all objects matching the passed in object's type. */
		template < typename ObjectType >
		std::vector< Handle< ObjectType > > GetGameObjectsOfType( const ObjectType &object );
		/* DestroyGameObject
		   Summary: Removes the object matching the passed-in id from the GameObjectManager. 
		   Parameter:
		      objectId: The id of the object to destroy. */
		void DestroyGameObject( GameObject::ObjectId objectId );

		/* DestroyAllGameObjects
		   Summary: Destroys all objects managed by GameObjectManager. */
		void DestroyAllGameObjects();
		/* DrawAllVisibleGameObjects 
		   Summary: Draws all visible game objects in the correct order they are to be drawn. This 
		            ordering is derived from the objects themselves. */
		void DrawAllVisibleGameObjects( double interpolation );
		/* UpdateAllDynamicGameObjects 
		   Summary: Updates all dynamic game objects in the correct order they are to be updated.
		            This ordering is derived from the objects themselves. */
		void UpdateAllDynamicGameObjects();

		/* IsACollidable 
		   Summary: Returns true if the object matching the passed-in id is a CollidableObject. */
		bool IsACollidable( GameObject::ObjectId objectId );

	private:
		GameObjectMap _gameObjects;
		DrawList _drawList;
		UpdateList _updateList;
		CollidableList _collidableList;
	};

	/* Template function definitions: */

	/* GetGameObjectsOfType 
	   Summary: Returns a std::vector of handles to all objects matching the passed in object's type. 
	   Parameters:
	      object: The object whose type is used to find matching objects.
	   Returns: A std::vector of handles to all objects matching the passed in object's type. */
	template < typename ObjectType >
	std::vector< Handle< ObjectType > > GameObjectManager::GetGameObjectsOfType( const ObjectType &object )
	{
		std::vector< Handle< ObjectType > > objectsOfType;

		// Iterate through all known object handles, try to assign each to an object handle of the desired
		// type. If the assignment is successful, add the desired-type handle to objectsOfType.
		for( GameObjectMap::iterator objectItr = _gameObjects.begin();
			 objectItr != _gameObjects.end(); ++objectItr )
		{
			Handle< ObjectType > desiredTypeHandle = objectItr->second;
			if( desiredTypeHandle.Valid() )
			{
				objectsOfType.push_back( desiredTypeHandle );
			}
		}

		return objectsOfType;
	}

}