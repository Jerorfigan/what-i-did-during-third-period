/* Implementation for GameAssetManager. */

#include "stdafx.h"

#include "GameAssetManager.h"

namespace PROJECT_NAME
{

	/* GetAppWindow 
	   Summary: Returns the id of the application window. 
	   Returns: The id of the application window. */
	IGraphicsProvider::ResourceId GameAssetManager::GetAppWindow()
	{
		return _appWindowId;
	}

	/* SetAppWindow 
	   Summary: Sets the id of the application window. 
	   Parameters: 
	      id: The id of the application window. */
	void GameAssetManager::SetAppWindow( IGraphicsProvider::ResourceId id )
	{
		assert( id != 0 );
		_appWindowId = id;
	}

	/* GetGameEventQueue 
		Summary: Gets the UI event queue. 
		Returns: The UI event queue. */
	GameEventQueue& GameAssetManager::GetGameEventQueue()
	{
		return _gameEventQueue;
	}

}