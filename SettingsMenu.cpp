/* SettingsMenu implementation. */

#include "stdafx.h"

#include <algorithm>

#include "ArrowSelector.h"
#include "CheckBox.h"
#include "DisplayResolution.h"
#include "Handle.h"
#include "Handle.cpp"
#include "IAudioProvider.h"
#include "PushButton.h"
#include "SettingsMenu.h"

namespace PROJECT_NAME
{

	// Constructors

	SettingsMenu::SettingsMenu() : mMenu( Handle< Background >( 
		new Background( RESOURCE_PATH( "Art/Menus/Settings/Background.jpg" ), Rectangle2D( Point2D( 800, 800 ), 1600, 900 ) ) ), 
		&mData )
	{
		Handle< CheckBox > fullscreen( 
			new CheckBox( "fullscreen", 0, Point2D( 600, 350 ), 
			RESOURCE_PATH("Art/Menus/Settings/CheckBoxLabels/FullScreen.png"), FullScreenCheckBox, true ) );

		mMenu.AddControl( fullscreen );

		// Get the resolutions to display in the resolution ArrowSelector.
		mValidDisplayResolutions = 
			Game::GetGameServiceManager().GetGraphicsProvider()->GetSupportedDisplayResolutions();

		// Reverse the order of the display resolutions so that they go from smallest res to largest.
		std::reverse( mValidDisplayResolutions.begin(), mValidDisplayResolutions.end() );

		// Convert these to a std::vector of strings.
		std::vector< std::string > resolutionStrings;

		for( DisplayResolutions::const_iterator resolutionItr = mValidDisplayResolutions.begin();
			 resolutionItr != mValidDisplayResolutions.end(); ++resolutionItr )
		{
			resolutionStrings.push_back( resolutionItr->GetInfoString() );
		}

		Handle< ArrowSelector > resolutionSelector(
			new ArrowSelector( "resolution", 1, Point2D( 850, 400 ), 
							   RESOURCE_PATH( "Art/Menus/Settings/ArrowSelectorLabels/Resolution.png" ),
							   resolutionStrings, ResolutionChanged, resolutionStrings.size() - 1 ) );

		mMenu.AddControl( resolutionSelector ); 

		std::string volumeLevels[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };

		std::vector< std::string > volumeLevelVec( volumeLevels, volumeLevels + sizeof( volumeLevels ) / sizeof( std::string ) );

		Handle< ArrowSelector > soundEffectVolume(
			new ArrowSelector( "sound_vol", 2, Point2D( 850, 500 ), 
				RESOURCE_PATH( "Art/Menus/Settings/ArrowSelectorLabels/SoundEffects.png" ),
				volumeLevelVec, SoundEffectsVolChanged, volumeLevelVec.size() - 1 ) );

		mMenu.AddControl( soundEffectVolume ); 

		Handle< ArrowSelector > musicVolume(
			new ArrowSelector( "music_vol", 3, Point2D( 850, 600 ), 
				RESOURCE_PATH( "Art/Menus/Settings/ArrowSelectorLabels/Music.png" ),
				volumeLevelVec, MusicVolChanged, volumeLevelVec.size() - 1 ) );

		mMenu.AddControl( musicVolume ); 

		Handle< PushButton > backButtonHandle( 
			new PushButton( "back", 4, Rectangle2D( Point2D( 800, 750 ), 200, 75 ),
				RESOURCE_PATH( "Art/Menus/Settings/Buttons/BackButton.png" ), 
				RESOURCE_PATH( "Art/Menus/Settings/Buttons/BackButtonHighlighted.png" ), 
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/BackButtonHighlighted.png" ),
				0, 0, BackButtonClicked ) );

		mMenu.AddControl( backButtonHandle ); 

		// Init menu data

		mData.mFullScreen = true;
		mData.mDisplayResolutionIndex = resolutionStrings.size() - 1;
		mData.mSoundEffectsVolLevel = volumeLevelVec.size() - 1;
		mData.mMusicVolLevel = volumeLevelVec.size() - 1;
	}

	// Methods

	/* Process 
		Summary: Processes the settings menu and returns the next game state. */
	Game::GameState SettingsMenu::Process()
	{
		mMenu.Interact();

		// Re-create the application window if changing from fullscreen to windowed or vice versa or
		// the resolution has changed.
		if( 
			mData.mFullScreen != Game::GetGameServiceManager().GetGraphicsProvider()->IsFullscreen( 
			Game::GetGameAssetManager().GetAppWindow() ) || 
			mValidDisplayResolutions[ mData.mDisplayResolutionIndex ] != 
			Game::GetGameServiceManager().GetGraphicsProvider()->GetDisplayResolution(
				Game::GetGameAssetManager().GetAppWindow() ) 
		  )
		{
			Game::GetGameServiceManager().GetGraphicsProvider()->DestroyWindow( 
				Game::GetGameAssetManager().GetAppWindow() );

			Game::GetGameAssetManager().SetAppWindow( 
				Game::GetGameServiceManager().GetGraphicsProvider()->InitializeWindow( 
					Game::GetGameSpace(), "What I Did During Third Period", 
					mValidDisplayResolutions[ mData.mDisplayResolutionIndex ]._width, 
					mValidDisplayResolutions[ mData.mDisplayResolutionIndex ]._height, mData.mFullScreen ) );
		}

		Game::GetGameServiceManager().GetAudioProvider()->SetMaxVolume( 
			static_cast< float >( mData.mSoundEffectsVolLevel / 10.0 * 100.0 ), IAudioProvider::SoundVolume );

		Game::GetGameServiceManager().GetAudioProvider()->SetMaxVolume( 
			static_cast< float >( mData.mMusicVolLevel / 10.0 * 100.0 ), IAudioProvider::MusicVolume );

		return Game::MainMenu;
	}

	// Menu event handlers

	void FullScreenCheckBox( void* menuData )
	{
		// Toggle the fullscreen member in menuData.
		SettingsMenu::SettingsMenuData* settingsMenuDataPtr = static_cast< SettingsMenu::SettingsMenuData* >( menuData );
	
		settingsMenuDataPtr->mFullScreen = settingsMenuDataPtr->mFullScreen ? false : true;
	}

	void BackButtonClicked( void* menuData )
	{
		// Push the menu exit event onto the UI queue so that the menu processing will
		// terminate.
		GameEvent menuExit;
		menuExit._event = GameEvent::MenuExit;

		Game::GetGameAssetManager().GetGameEventQueue().PushEvent( menuExit );
	}

	void ResolutionChanged( void* menuData, ArrowSelector* arrowSelectorPtr )
	{
		// Set the mDisplayResolutionIndex member in menuData.
		SettingsMenu::SettingsMenuData* settingsMenuDataPtr = static_cast< SettingsMenu::SettingsMenuData* >( menuData );

		settingsMenuDataPtr->mDisplayResolutionIndex = arrowSelectorPtr->GetSelectedIndex();
	}

	void SoundEffectsVolChanged( void* menuData, ArrowSelector* arrowSelectorPtr )
	{
		// Set the mSoundEffectsVolLevel member in menuData.
		SettingsMenu::SettingsMenuData* settingsMenuDataPtr = static_cast< SettingsMenu::SettingsMenuData* >( menuData );

		settingsMenuDataPtr->mSoundEffectsVolLevel = arrowSelectorPtr->GetSelectedIndex();
	}

	void MusicVolChanged( void* menuData, ArrowSelector* arrowSelectorPtr )
	{
		// Set the mMusicVolLevel member in menuData.
		SettingsMenu::SettingsMenuData* settingsMenuDataPtr = static_cast< SettingsMenu::SettingsMenuData* >( menuData );

		settingsMenuDataPtr->mMusicVolLevel = arrowSelectorPtr->GetSelectedIndex();
	}
}