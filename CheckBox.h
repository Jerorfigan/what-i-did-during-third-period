#pragma once

/* Defines the functionality of a checkbox. */

#include "IGraphicsProvider.h"
#include "Rectangle2D.h"
#include "UserControl.h"

namespace PROJECT_NAME
{

	class CheckBox : public UserControl
	{
		// Constructors
	public:
		CheckBox( std::string id, unsigned int menuIndex, Point2D checkBoxPos, std::string labelImage, 
			void (*onActivation)( void* ) = 0, bool initiallyChecked = false );

		// Virtual methods
	public:
		virtual void Init( void* menuData );

		virtual void Draw( void* menuData );

		virtual void OnHoverOver( void* menuData );
		virtual void OnHoverAway( void* menuData );
		virtual void OnActivation( void* menuData );
		virtual void OnDeactivation( void* menuData );
		virtual void OnGainedFocus( void* menuData );
		virtual void OnLostFocus( void* menuData );
		virtual void OnKeyboardChar( void* menuData, char typedChar );

		virtual bool IsTargettedByMouse( Point2D mouseHotspot );

		// Data
	private:
		Rectangle2D mCheckBoxRect;
		IGraphicsProvider::ResourceId mLabel;
		bool mIsChecked;

		void (*mOnActivation)( void* );

		// Static data
	private:
		static const int mCheckBoxSize = 32; // width & height in pixels
		static const int mCheckBoxLabelOffsetX = 60; // in pixels
		static const int mCheckBoxLabelOffsetY = 35; // in pixels
		static const std::string mCheckBoxUncheckedImage;
		static const std::string mCheckBoxCheckedImage;
	};

}