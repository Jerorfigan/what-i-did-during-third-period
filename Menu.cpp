/* Menu implemenation. */

#include "stdafx.h"

#include <sstream>

#include "Game.h"
#include "Menu.h"
#include "GameEventFunctions.h"
#include "GameEvent.h"

namespace PROJECT_NAME
{

	// Constructors

	Menu::Menu( Handle< Background > backgroundHandle, void* menuData ) : mCurrHLControlIndex( 0 ), mCurrFocusedIndex( 0 ), mMenuData( menuData ), 
		mMouseInWindow( false ), mBackgroundHandle( backgroundHandle )
	{

	}

	// Methods

	void Menu::AddControl( Handle< UserControl > control )
	{
		// Verify that there is not already a handle to a control at this index. Skip
		// verification on controls with an index of -1, as these are static controls 
		// that won't be interacted with.
		ControlsByIndex::iterator findResult1;

		if( control->GetMenuIndex() != -1 )
		{
			findResult1 = mControlsByIndex.find( control->GetMenuIndex() );

			assert( findResult1 == mControlsByIndex.end() );
		}

		if( control->GetMenuIndex() == -1 || findResult1 == mControlsByIndex.end() )
		{
			mControlsByIndex.insert( std::make_pair( control->GetMenuIndex(), control ) );

			// Verify that there is not already a handle to a control with this id. Skip
			// verification on controls with an index of -1, for reason stated above.
			ControlsById::iterator findResult2;
			std::string id = control->GetId();

			if( control->GetMenuIndex() != -1 )
			{
				findResult2 = mControlsById.find( id );

				assert( findResult2 == mControlsById.end() );
			}

			if( control->GetMenuIndex() == -1 || findResult2 == mControlsById.end() )
			{
				mControlsById.insert( std::make_pair( id, control ) );
			}
		}
	}
	
	void Menu::Interact()
	{
		Init();

		bool doneInteracting = false;

		while( !doneInteracting )
		{
			GameEvent gameEvent;
			while( CheckForGameEvent( gameEvent ) )
			{
				// Event found, handle it.
				if( gameEvent._event == GameEvent::MenuExit )
				{
					doneInteracting = true;
					break;
				}
				else if( gameEvent._event == GameEvent::MouseMoved )
				{
					if( mCurrHLControl.Valid() )
					{
						if( !mCurrHLControl->IsTargettedByMouse( gameEvent._mouseHotSpot ) )
						{
							mCurrHLControl->OnHoverAway( mMenuData );
							mCurrHLControl = Handle< UserControl >();
						}
					}
					else
					{
						Handle< UserControl > targetControlHandle = GetTargettedControl( gameEvent._mouseHotSpot );
						if( targetControlHandle.Valid() )
						{
							mCurrHLControl = targetControlHandle;
							mCurrHLControlIndex = targetControlHandle->GetMenuIndex();
							mCurrHLControl->OnHoverOver( mMenuData );
						}	
					}

					// Update cursor.
					Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( 
						Game::GetCursorResource(), gameEvent._mouseHotSpot );
				}
				else if( gameEvent._event == GameEvent::MouseEnteredWindow )
				{
					mMouseInWindow = true;
				}
				else if( gameEvent._event == GameEvent::MouseLeftWindow )
				{
					mMouseInWindow = false;
				}
				else if( gameEvent._event == GameEvent::MouseButtonPressed && 
					gameEvent._mouseButton == MouseButton::LeftButton )
				{
					if( mCurrHLControl.Valid() )
					{
						mCurrHLControl->OnActivation( mMenuData );

						if( mCurrHLControl->CanFocus() && ( !mCurrFocusedControl.Valid() || mCurrHLControl != mCurrFocusedControl ) )
						{
							if( mCurrFocusedControl.Valid() )
							{
								mCurrFocusedControl->OnLostFocus( mMenuData );
							}

							mCurrFocusedIndex = mCurrHLControlIndex;
							mCurrFocusedControl = mCurrHLControl;
							mCurrFocusedControl->OnGainedFocus( mMenuData );
						}
					}
				}
				else if( gameEvent._event == GameEvent::KeyPressed && 
						 gameEvent._key == KeyboardKey::Character )
				{
					if( mCurrFocusedControl.Valid() )
					{
						mCurrFocusedControl->OnKeyboardChar( mMenuData, gameEvent._charCode );
					}
				}
			}

			Game::GetGameServiceManager().GetGraphicsProvider()->PrepareWindowForDrawing( 
				Game::GetGameAssetManager().GetAppWindow() );

			// Draw background.
			mBackgroundHandle->Draw( 0 );

			DrawControls();

			// Draw mouse cursor.
			if( mMouseInWindow )
			{
				Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( 
					Game::GetCursorResource(), Game::GetGameAssetManager().GetAppWindow() );
			}

			Game::GetGameServiceManager().GetGraphicsProvider()->DisplayWindow( 
				Game::GetGameAssetManager().GetAppWindow() );
		}
	}
		
	void Menu::Init()
	{
		assert( mControlsByIndex.size() > 0 );

		// Invalidate the currently highlighted/focused controls.
		mCurrHLControl = Handle< UserControl >();
		mCurrFocusedControl = Handle< UserControl >();;

		// Init app window for drawing.
		Game::GetGameServiceManager().GetGraphicsProvider()->PrepareWindowForDrawing( Game::GetGameAssetManager().GetAppWindow() );

		// Iterate through each control and tell it to init and draw itself.
		for( ControlsByIndex::iterator controlItr = mControlsByIndex.begin(); 
			 controlItr != mControlsByIndex.end(); ++controlItr )
		{
			controlItr->second->Init( mMenuData );
			controlItr->second->Draw( mMenuData );
		}

		Game::GetGameServiceManager().GetGraphicsProvider()->DisplayWindow( Game::GetGameAssetManager().GetAppWindow() );

		// Need to create a "mouse entered window" message to guarantee that the cursor
		// is visible in the menu. At the start of the game, this 
		// event is triggered by the graphics provider, but subsequent menus
		// will not be so lucky.
		GameEvent mouseEntered;
		mouseEntered._event = GameEvent::MouseEnteredWindow;
		Game::GetGameAssetManager().GetGameEventQueue().PushEvent( mouseEntered );
	}

	void Menu::DrawControls()
	{
		for( ControlsById::iterator controlHandleItr = mControlsById.begin();
			 controlHandleItr != mControlsById.end(); ++controlHandleItr )
		{
			controlHandleItr->second->Draw( mMenuData );
		}
	}

	Handle< UserControl > Menu::GetTargettedControl( Point2D mouseHotSpot )
	{
		for( ControlsById::iterator controlHandleItr = mControlsById.begin();
			 controlHandleItr != mControlsById.end(); ++controlHandleItr )
		{
			if( controlHandleItr->second->IsTargettedByMouse( mouseHotSpot ) )
			{
				return controlHandleItr->second;
			}
		}

		return Handle< UserControl >();
	}
}