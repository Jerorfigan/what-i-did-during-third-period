#pragma once

#include "stdafx.h"

#include "UserControl.h"

namespace PROJECT_NAME
{

	// Constructors

	UserControl::UserControl( std::string id, unsigned int menuIndex, IGraphicsProvider::ResourceId graphicsId, bool canFocus ) :
		mId( id ), mMenuIndex( menuIndex ), mGraphicsId( graphicsId ), mCanFocus( canFocus )
	{

	}

	// Methods

	std::string UserControl::GetId() const
	{
		return mId;
	}

	unsigned int UserControl::GetMenuIndex() const
	{
		return mMenuIndex;
	}

	bool UserControl::CanFocus() const
	{
		return mCanFocus;
	}

	// Operators
	bool UserControl::operator==( const UserControl& rhs )
	{
		return this->mId == rhs.mId;
	}

	bool UserControl::operator!=( const UserControl& rhs )
	{
		return !( *this == rhs );
	}

	// Virtual methods

	UserControl::~UserControl()
	{

	}

}