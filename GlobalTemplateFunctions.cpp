#pragma once

#include "stdafx.h"

#include <sstream>

namespace PROJECT_NAME
{

	/* Sorts the range denoted by random-access iterator pair start and end using the insertion sort
	   algorithm. Sort is in-place. */
	template < typename IterType >
	void InsertionSort( IterType &start, IterType &end, 
		bool (*firstComesBeforeSecond)( IterType, IterType ) )
	{
		std::size_t curValIndex = 0;
		std::size_t insertionPointIndex = 0;
		while( start + curValIndex != end )
		{
			insertionPointIndex = curValIndex;
			while( insertionPointIndex != 0 )
			{
				--insertionPointIndex;
				if( ( firstComesBeforeSecond == 0 && start[ insertionPointIndex + 1 ] < start[ insertionPointIndex ] ) ||
					( firstComesBeforeSecond != 0 && firstComesBeforeSecond( start + insertionPointIndex + 1, start + insertionPointIndex ) ) )
				{
					// Swap elements at insertionPointIndex and insertionPointIndex + 1
					IterType::value_type temp = start[ insertionPointIndex ];
					start[ insertionPointIndex ] = start[ insertionPointIndex + 1 ];
					start[ insertionPointIndex + 1 ] = temp;
				}
				else
				{
					break;
				}
			}
			++curValIndex;
		}
	}

	template < typename NumericType >
	std::string GetString( NumericType num )
	{
		std::stringstream numStream;
		numStream << num;
		return numStream.str();
	}

}