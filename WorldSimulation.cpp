/* WorldSimulation implementation. */

#include "stdafx.h"

#include "Box2DConversions.h"
#include "Circle2D.h"
#include "Game.h"
#include "Handle.h"
#include "Handle.cpp"
#include "LineSegment2D.h"
#include "Map.h"
#include "MapLine.h"
#include "Rectangle2D.h"
#include "SawBlade.h"
#include "WorldSimulation.h"

namespace PROJECT_NAME
{
	// Internal class implementations

	// ContactListener

	void WorldSimulation::ContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
	{
		Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );

		if( worldSimulationHandle->_computeContacts )
		{
			b2WorldManifold worldManifold;
			contact->GetWorldManifold(&worldManifold);
			b2PointState state1[2], state2[2];
			b2GetPointStates(state1, state2, oldManifold, contact->GetManifold());
			if (state2[0] == b2_addState)
			{
				const b2Body* bodyA = contact->GetFixtureA()->GetBody();
				const b2Body* bodyB = contact->GetFixtureB()->GetBody();
				b2Vec2 point = worldManifold.points[0];
				b2Vec2 vA = bodyA->GetLinearVelocityFromWorldPoint(point);
				b2Vec2 vB = bodyB->GetLinearVelocityFromWorldPoint(point);
				b2Vec2 diff = vB - vA;
				float32 approachVelocity = b2Dot( diff, worldManifold.normal );
			
				// Normalize contact force between 0 and 1.
				worldSimulationHandle->_lastContactForce = approachVelocity < 2.0f ? approachVelocity / 2.0 : 1.0;
			}
		}
	}

	/* Initializes a WorldSimulation. */
	WorldSimulation::WorldSimulation() : GameObject( "WorldSimulation" ), 
		DynamicGameObject( 0, "WorldSimulation" ), _world( b2Vec2(0.0f, 9.8f) ), 
		_physicsTimeStep( 1.0f / 60.0f ), _worldFriction( 0.8f ), _worldRestitution( 0.8f ), 
		_velocityIterations( 8 ), _positionIterations( 3 ), _computeContacts( true )
	{
		// Initialize the static body that we'll attach all edge fixtures to.
		b2BodyDef bodyDef;
		bodyDef.type = b2_staticBody;
		_staticBody = _world.CreateBody( &bodyDef );

		_world.SetContactListener( &_contactListener );
	}

	/* SyncWithMap
		Summary: Updates the world simulation to mirror the current Map object: this includes
		        creating edge shapes for all the MapLine objects, rotating kinematic bodies for
				all the SawBlade objects, etc. */
	void WorldSimulation::SyncWithMap()
	{
		ReplicateMapLines();
		ReplicateMapBoundaries();
		ReplicateSawBlades();
	}

	/* Update
		Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
				last update (which should be accounted for in this update). 
		Parameters: 
			elapsedTime: The time elapsed in seconds since last update. */
	void WorldSimulation::Update( double elapsedTime )
	{
		// We only want to update the b2World when the sum of the frame times since we last updated it 
		// is greater than or equal to the world simulation time step. If it's much greater, we'll need 
		// to do repeated updates. In this way, we keep the b2World updates (which are done with a fixed 
		// time step) in sync with the rest of the game.
		static float32 elapsedTimeSinceLastUpdate = 0.0;
		elapsedTimeSinceLastUpdate += static_cast< float32 >( elapsedTime );

		while( elapsedTimeSinceLastUpdate >= _physicsTimeStep )
		{
			elapsedTimeSinceLastUpdate -= _physicsTimeStep;
			_world.Step( _physicsTimeStep, _velocityIterations, _positionIterations );
		}
	}

	/* SetFriction 
		Summary: Sets the friction value of all the static fixtures in the Box2D world.*/
	void WorldSimulation::SetFriction( float32 friction )
	{
		for( b2Fixture *fixturePtr = _staticBody->GetFixtureList(); fixturePtr; 
			 fixturePtr = fixturePtr->GetNext() )
		{
			fixturePtr->SetFriction( friction );
		}
	}

	/* SetRestitution 
		Summary: Sets the restitution value of all the static fixtures in the Box2D world.*/
	void WorldSimulation::SetRestitution( float32 restitution )
	{
		for( b2Fixture *fixturePtr = _staticBody->GetFixtureList(); fixturePtr; 
			 fixturePtr = fixturePtr->GetNext() )
		{
			fixturePtr->SetRestitution( restitution );
		}
	}

	/* GetWorld 
	   Summary: Returns a reference to the b2World instance. 
	   Returns: A reference to the b2World instance. */
	b2World& WorldSimulation::GetWorld()
	{
		return _world;
	}
	
	/* GetTimeStep 
	   Summary: Returns the timestep used in updating the physics world (in seconds). 
	   Returns: The timestep used in updating the physics world (in seconds). */
	float32 WorldSimulation::GetTimeStep() const
	{
		return _physicsTimeStep;
	}

	/* GetWorldStaticBody
		Summary: Returns the world static body. This is the static body upon which all static fixtures
		        are to be placed.
		Returns: The world static body. */
	b2Body* WorldSimulation::GetWorldStaticBody()
	{
		return _staticBody;
	}

	/* GetLastContactForce
		Summary: Returns the normalized force (between 0 and 1) of the most recent contact (if there was one) 
		        since the last call to this function or 0 if there wasn't one. Each time this function 
				is called, the contact force is reset to 0 in preparation for the next contact. 
		Returns: The normalized force of the most recent contact or 0. */
	double WorldSimulation::GetLastContactForce()
	{
		double lastContactForce = _lastContactForce;
		_lastContactForce = 0.0;

		return lastContactForce;
	}

	void WorldSimulation::ToggleContactComputation( bool turnOn )
	{
		_computeContacts = turnOn;
	}

	/* Replicates all the map lines by creating Box2D objects to represent them in the world 
		simulation. */
	void WorldSimulation::ReplicateMapLines()
	{
		// Get all the map line objects and create edge fixtures for them in the b2World.
		typedef std::vector< Handle< MapLine > > MapLines;

		MapLines mapLines = Game::GetGameObjectManager().GetGameObjectsOfType( MapLine() );

		for( MapLines::const_iterator mapLineItr = mapLines.begin(); 
			 mapLineItr != mapLines.end(); ++mapLineItr )
		{
			LineSegment2D lineSegment = (*mapLineItr)->GetLineSegment();

			b2EdgeShape edgeShape;
			edgeShape.Set( 
				b2Vec2( 
					static_cast< float >( lineSegment.Start()._x * PIXELS_TO_METERS ), 
					static_cast< float >( lineSegment.Start()._y * PIXELS_TO_METERS ) ), 
				b2Vec2( 
				    static_cast< float >( lineSegment.End()._x * PIXELS_TO_METERS ), 
					static_cast< float >( lineSegment.End()._y * PIXELS_TO_METERS ) ) );

			b2FixtureDef fixtureDef;
			fixtureDef.shape = &edgeShape;
			fixtureDef.friction = _worldFriction;
			fixtureDef.restitution = _worldRestitution;

			// Add fixture to static body.
			_staticBody->CreateFixture( &fixtureDef );
		}
	}

	/* Replicates all the map boundaries by creating Box2D objects to represent them in the world 
		simulation. */
	void WorldSimulation::ReplicateMapBoundaries()
	{
		// Also create edge fixtures for the boundaries of the map.
		Handle< Map > mapHandle = Game::GetGameObjectManager().GetGameObject( "map" );
		assert( mapHandle.Valid() );

		Rectangle2D mapBounds = mapHandle->GetMapBounds();

		b2EdgeShape edgeShape;
		edgeShape.Set( 
			b2Vec2( 
				static_cast< float >( mapBounds.TopLeft()._x * PIXELS_TO_METERS ), 
				static_cast< float >( mapBounds.TopLeft()._y * PIXELS_TO_METERS ) ), 
			b2Vec2( 
				static_cast< float >( mapBounds.TopRight()._x * PIXELS_TO_METERS ), 
				static_cast< float >( mapBounds.TopRight()._y * PIXELS_TO_METERS ) ) );

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &edgeShape;
		fixtureDef.friction = _worldFriction;
		fixtureDef.restitution = _worldRestitution;

		_staticBody->CreateFixture( &fixtureDef );

		edgeShape.Set( 
			b2Vec2( 
				static_cast< float >( mapBounds.TopRight()._x * PIXELS_TO_METERS ), 
				static_cast< float >( mapBounds.TopRight()._y * PIXELS_TO_METERS ) ), 
			b2Vec2( 
				static_cast< float >( mapBounds.BottomRight()._x * PIXELS_TO_METERS ), 
				static_cast< float >( mapBounds.BottomRight()._y * PIXELS_TO_METERS ) ) );

		_staticBody->CreateFixture( &fixtureDef );

		edgeShape.Set( 
			b2Vec2( 
				static_cast< float >( mapBounds.BottomRight()._x * PIXELS_TO_METERS ), 
				static_cast< float >( mapBounds.BottomRight()._y * PIXELS_TO_METERS ) ), 
			b2Vec2( 
				static_cast< float >( mapBounds.BottomLeft()._x * PIXELS_TO_METERS ), 
				static_cast< float >( mapBounds.BottomLeft()._y * PIXELS_TO_METERS ) ) );

		_staticBody->CreateFixture( &fixtureDef );

		edgeShape.Set( 
			b2Vec2( 
				static_cast< float >( mapBounds.BottomLeft()._x * PIXELS_TO_METERS ), 
				static_cast< float >( mapBounds.BottomLeft()._y * PIXELS_TO_METERS ) ), 
			b2Vec2( 
				static_cast< float >( mapBounds.TopLeft()._x * PIXELS_TO_METERS ), 
				static_cast< float >( mapBounds.TopLeft()._y * PIXELS_TO_METERS ) ) );

		_staticBody->CreateFixture( &fixtureDef );
	}

	/* Replicates all the saw blades by creating Box2D objects to represent them in the world 
		simulation. */
	void WorldSimulation::ReplicateSawBlades()
	{
		// Get all the SawBlade objects and create rotating kinematic circle fixtures for them in the b2World.
		typedef std::vector< Handle< SawBlade > > SawBlades;

		SawBlades sawBlades = Game::GetGameObjectManager().GetGameObjectsOfType( SawBlade() );

		for( SawBlades::const_iterator sawBladeItr = sawBlades.begin(); 
			 sawBladeItr != sawBlades.end(); ++sawBladeItr )
		{
			Circle2D circle = (*sawBladeItr)->GetGeometry();

			b2CircleShape circleShape;
			circleShape.m_p.Set( 0, 0 );
			circleShape.m_radius = static_cast< float32 >( circle.Radius() * PIXELS_TO_METERS );

			b2FixtureDef fixtureDef;
			fixtureDef.shape = &circleShape;
			fixtureDef.friction = 1.0;
			fixtureDef.density = 1.0;
			fixtureDef.restitution = 1.0;

			b2BodyDef circleBodyDef;
			circleBodyDef.type = b2_kinematicBody;
			circleBodyDef.position.Set( 
				static_cast< float32 >( circle.Center()._x * PIXELS_TO_METERS ),
				static_cast< float32 >( circle.Center()._y * PIXELS_TO_METERS ) );
			circleBodyDef.angularVelocity = -5.0f * 360.0f * DEGREES_TO_RADIANS;

			b2Body *circleBody = _world.CreateBody( &circleBodyDef );
			circleBody->CreateFixture( &fixtureDef );
		}
	}

}