#pragma once

/* Defines the functionality of a mouse/keyboard controlled menu. */

#include <map>
#include <string>

#include "Background.h"
#include "Handle.h"
#include "Handle.cpp"
#include "IGraphicsProvider.h"
#include "Point2D.h"
#include "UserControl.h"

namespace PROJECT_NAME
{

	class Menu
	{
		// Constructors
	public:
		Menu( Handle< Background > backgroundHandle, void* menuData = 0 );

		// Methods
	public:
		void AddControl( Handle< UserControl > control );
		void Interact();
		
	private:
		void Init();
		void DrawControls();
		Handle< UserControl > GetTargettedControl( Point2D mouseHotSpot );

		// Data
	private:
		typedef std::multimap< unsigned int, Handle< UserControl > > ControlsByIndex;
		typedef std::multimap< std::string, Handle< UserControl > > ControlsById;

		// The index/handle of the currently highlighted control.
		unsigned int mCurrHLControlIndex;
		Handle< UserControl > mCurrHLControl;

		// The index/handle of the currently focused control.
		unsigned int mCurrFocusedIndex;
		Handle< UserControl > mCurrFocusedControl;

		ControlsByIndex mControlsByIndex;
		ControlsById mControlsById;
		void* mMenuData;

		Handle< Background > mBackgroundHandle;

		bool mMouseInWindow;
	};

}