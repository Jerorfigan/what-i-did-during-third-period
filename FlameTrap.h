#pragma once

#include "CollidableGameObject.h"
#include "CollidableLineSegment.h"
#include "DynamicGameObject.h"
#include "GameObject.h"
#include "Point2D.h"
#include "SpriteAnimator.h"
#include "Timer.h"
#include "Vector2D.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class FlameTrap : public CollidableGameObject, public DynamicGameObject, public VisibleGameObject
	{
	public:
		enum FlameTrapState { Idle, Spouting };

		/* Initializes a dummy FlameTrap. Used when getting all objects of type FlameTrap from 
		   GameObjectManager. */
		FlameTrap();
		/* Initializes a FlameTrap from an object id, a position, and a vector direction for the flame. */
		FlameTrap( GameObject::ObjectId objectId, Point2D position, Vector2D flameDirection );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea();

		/* Update
		   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
					last update (which should be accounted for in this update). 
		   Parameters: 
			  elapsedTime: The time elapsed in seconds since last update. */
		virtual void Update( double elapsedTime );

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

	private:
		Point2D _position;
		Vector2D _flameDirection;
		double _flameAngle;
		CollidableLineSegment _collidableArea;
		Timer _flameTimer;
		SpriteAnimator _spriteAnimator;
		FlameTrapState _currentState;

		// _flameRange is the max range of the flame when it spouts in pixels.
		static const double _flameRange;
		// _flameSpoutFrequency is the time (in seconds) between flame spouts.
		static const double _flameSpoutPeriod;
		// _flameSpoutDuration is the time (in seconds) that each flame spout lasts.
		static const double _flameSpoutDuration;
	};

}