#pragma once

/* Defines the functionality for an axis-aligned rectangle in 2D. */

#include "Point2D.h"

namespace PROJECT_NAME
{
	class LineSegment2D;

	struct Vector2D;

	class Rectangle2D
	{
	public:
		/* Default constructor initializes a rectangle with all four points set to (0,0). */
		Rectangle2D();
		/* Constructor that initializes a rectangle from passed in points. */
		Rectangle2D( Point2D topLeft, Point2D topRight, Point2D bottomLeft, Point2D bottomRight );
		/* Initializes a Rectangle2D from a center point, width and height. */
		Rectangle2D( Point2D center, double width, double height );
		/* Initializes the smallest rectangle enclosing a vector given its origin. If the vector follows the
		   x or y-axis, the initialized rectangle will get the passed-in axial width. */
		Rectangle2D( Point2D origin, const Vector2D &vector, double axialWidth = 0.0 );

		/* Width
		   Summary: Returns the width of the rectangle. 
		   Returns: The width of the rectangle. */
		double Width() const;
		/* Height
		   Summary: Returns the height of the rectangle.
		   Returns: The height of the rectangle. */
		double Height() const;
		/* Top
		   Summary: Returns the Y coordinate of the points comprising the top edge of the rectangle. 
		   Returns: The Y coordinate of the points comprising the top edge of the rectangle. */
		double Top() const;
		/* Bottom
		   Summary: Returns the Y coordinate of the points comprising the bottom edge of the rectangle. 
		   Returns: The Y coordinate of the points comprising the bottom edge of the rectangle. */
		double Bottom() const;
		/* Left
		   Summary: Returns the X coordinate of the points comprising the left edge of the rectangle. 
		   Returns: The X coordinate of the points comprising the left edge of the rectangle. */
		double Left() const;
		/* Right
		   Summary: Returns the X coordinate of the points comprising the right edge of the rectangle. 
		   Returns: The X coordinate of the points comprising the right edge of the rectangle. */
		double Right() const;
		/* Center
		   Summary: Computes the center point of the rectangle. 
		   Returns: The center point of the rectangle. */
		Point2D Center() const;
		
		/* Contains
		   Summary: Determines whether this rectangle contains the passed-in point. 
		   Parameters: 
			  point: The point to test for containment.
		   Returns: True if this rectangle contains the passed-in point, false otherwise. */
		bool Contains( const Point2D &point ) const;
		/* Intersects
		   Summary: Determines whether this rectangle intersects the passed-in rectangle. 
		   Parameters: 
			  rect: The rectangle to check for intersection with.
		   Returns: True if this rectangle intersects passed-in rectangle, false otherwise. */
		bool Intersects( const Rectangle2D &rect ) const;
		/* Intersects
		   Summary: Determines whether this rectangle intersects the passed-in line segment. 
		   Parameters:
			  lineSeg: The line segment to check for intersection with.
		   Returns: True if this rectangle intersects passed-in line segment, false otherwise. */
		bool Intersects( const LineSegment2D &lineSeg ) const;

		/* TopLeft
		   Summary: Returns the top-left vertex of this rectangle. 
		   Returns: The top-left vertex of this rectangle. */
		const Point2D& TopLeft() const;
		/* TopRight
		   Summary: Returns the top-right vertex of this rectangle. 
		   Returns: The top-right vertex of this rectangle. */
		const Point2D& TopRight() const;
		/* BottomLeft
		   Summary: Returns the bottom-left vertex of this rectangle. 
		   Returns: The bottom-left vertex of this rectangle. */
		const Point2D& BottomLeft() const;
		/* BottomRight
		   Summary: Returns the bottom-right vertex of this rectangle. 
		   Returns: The bottom-right vertex of this rectangle. */
		const Point2D& BottomRight() const;

	private:
		Point2D _topLeft;
		Point2D _topRight;
		Point2D _bottomLeft;
		Point2D _bottomRight;
	};

}