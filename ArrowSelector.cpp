/* ArrowSelector implementation. */

#include "stdafx.h"

#include "ArrowSelector.h"
#include "Game.h"

namespace PROJECT_NAME
{

	// Constructors

	ArrowSelector::ArrowSelector( std::string id, unsigned int menuIndex, Point2D arrowSelectorPos, std::string labelImage, 
		std::vector< std::string > selections, void (*onActivation)( void*, ArrowSelector* ), std::size_t defaultSelectionIndex ) :
		UserControl( id, menuIndex, Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString("") ), 
		mSelections( selections ), mPosition( arrowSelectorPos ),
		mLeftArrowRect( arrowSelectorPos + Vector2D( mArrowWidth / 2.0, mArrowWidth / 2.0 ), mArrowWidth, mArrowWidth ),
		mRightArrowRect( arrowSelectorPos + Vector2D( mRightArrowOffset, 0 ) + Vector2D( mArrowWidth / 2.0, mArrowWidth / 2.0 ), mArrowWidth, mArrowWidth ),
		mLabel( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( labelImage ) ), 
		mLeftArrow( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( mLeftArrowImage ) ),
		mRightArrow( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( mRightArrowImage ) ),
		mLeftArrowTargetted( false ), mCurrentSelectionIndex( defaultSelectionIndex ), mOnActivation( onActivation )
	{

	}

	// Methods

	std::size_t ArrowSelector::GetSelectedIndex() const
	{
		return mCurrentSelectionIndex;
	}

	void ArrowSelector::RepositionSelectionString()
	{
		int selectionPixelLength = Game::GetGameServiceManager().GetGraphicsProvider()->GetPixelLength( mGraphicsId );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( mGraphicsId, 
			Point2D( selectionPixelLength / 2.0, 0 ) );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mGraphicsId, 
			Point2D( mPosition._x + mArrowWidth + ( ( mRightArrowOffset - mArrowWidth ) / 2.0 ), mPosition._y + mValueOffsetY ) );
	}

	// Virtual methods

	void ArrowSelector::Init( void* menuData )
	{
		// Init label.

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( mLabel,
			Point2D( Game::GetGameServiceManager().GetGraphicsProvider()->GetSpriteSubRectangle( mLabel ).Right(), 0 ) );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mLabel, 
			Point2D( mPosition._x - mLabelOffset, mPosition._y ) );

		// Init left & right arrows.

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mLeftArrow, mPosition );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mRightArrow, 
			Point2D( mPosition._x + mRightArrowOffset, mPosition._y ) );

		// Init selection string.

		Game::GetGameServiceManager().GetGraphicsProvider()->SetString( mGraphicsId, mSelections[ mCurrentSelectionIndex ] );

		RepositionSelectionString();
	}

	void ArrowSelector::Draw( void* menuData )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mGraphicsId,
			Game::GetGameAssetManager().GetAppWindow() );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mLabel,
			Game::GetGameAssetManager().GetAppWindow() );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mLeftArrow,
			Game::GetGameAssetManager().GetAppWindow() );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mRightArrow,
			Game::GetGameAssetManager().GetAppWindow() );
	}

	void ArrowSelector::OnHoverOver( void* menuData )
	{

	}

	void ArrowSelector::OnHoverAway( void* menuData )
	{

	}

	void ArrowSelector::OnActivation( void* menuData )
	{
		if( mLeftArrowTargetted )
		{
			mCurrentSelectionIndex = mCurrentSelectionIndex == 0 ? mSelections.size() - 1 : --mCurrentSelectionIndex;
		}
		else
		{
			mCurrentSelectionIndex = ++mCurrentSelectionIndex > mSelections.size() - 1 ? 0 : mCurrentSelectionIndex;
		}

		Game::GetGameServiceManager().GetGraphicsProvider()->SetString( mGraphicsId, mSelections[ mCurrentSelectionIndex ] );

		RepositionSelectionString();

		// If specific event handler specified, call it.
		if( mOnActivation )
		{
			mOnActivation( menuData, this );
		}
	}

	void ArrowSelector::OnDeactivation( void* menuData )
	{

	}

	void ArrowSelector::OnGainedFocus( void* menuData )
	{

	}

	void ArrowSelector::OnLostFocus( void* menuData )
	{

	}

	void ArrowSelector::OnKeyboardChar( void* menuData, char typedChar )
	{

	}

	bool ArrowSelector::IsTargettedByMouse( Point2D mouseHotspot )
	{
		if( mLeftArrowRect.Contains( mouseHotspot ) )
		{
			mLeftArrowTargetted = true;
			return true;
		}
		else if( mRightArrowRect.Contains( mouseHotspot ) )
		{
			mLeftArrowTargetted = false;
			return true;
		}
		else
		{
			return false;
		}
	}

	// Static data

	const std::string ArrowSelector::mLeftArrowImage = RESOURCE_PATH( "Art/UI/ArrowSelector/LeftArrowButton.png" );
	const std::string ArrowSelector::mRightArrowImage = RESOURCE_PATH( "Art/UI/ArrowSelector/RightArrowButton.png" );

}