/* Implementation for VisibleGameObject. */

#include "stdafx.h"

#include "Game.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{
	
	/* Initializes a VisibleGameObject from a RenderOrder and a IGraphicsProvider::ResourceId. */
	VisibleGameObject::VisibleGameObject( IGraphicsProvider::ResourceId graphicsId, ObjectId objectId ) : 
		GameObject( objectId ), _renderOrder( _renderSequence++ ), _graphicsId( graphicsId ), _isVisible( true )
	{

	}

	/* RenderOrder 
	   Summary: Returns the render depth level of this VisibleGameObject. 
	   Returns: The render depth level of this VisibleGameObject. */
	const VisibleGameObject::RenderOrder& VisibleGameObject::GetRenderOrder() const
	{
		return _renderOrder;
	}

    /* GraphicsId 
	   Summary: Returns the id value to the underlying graphics resource for this VisibleGameObject. 
	   Returns: The id value to the underlying graphics resource for this VisibleGameObject. */
	const IGraphicsProvider::ResourceId& VisibleGameObject::GraphicsId() const
	{
		return _graphicsId;
	}

	/* CompareHandles
		   Summary: Compares two VisibleGameObject handles based on the render order of the underlying 
		            VisibleGameObjects. Returns true if lhs has a lower render order than rhs, false otherwise. 
		   Parameters: 
		      lhs: The left-hand-side VisibleGameObject handle. 
			  rhs: The right-hand-side VisibleGameObject handle. 
		   Returns: True if lhs has a lower render order than rhs, false otherwise.  */
	bool VisibleGameObject::CompareHandles( Handle< VisibleGameObject > lhs, Handle< VisibleGameObject > rhs )
	{
		return lhs->GetRenderOrder() < rhs->GetRenderOrder();
	}

	/* IsVisible 
	   Summary: Returns true if this VisibleGameObject is currently visible. 
	   Returns: True if this VisibleGameObject is currently visible. */
	bool VisibleGameObject::IsVisible() const
	{
		return _isVisible;
	}

	/* SetVisibility 
	   Summary: Sets whether this VisibleGameObject is currently visible. 
	   Parameters:
	      visible: Flag indicating whether this VisibleGameObject is currently visible. */
	void VisibleGameObject::SetVisibility( bool isVisible )
	{
		_isVisible = isVisible;
	}

	/* De-allocate the graphics object. */
	VisibleGameObject::~VisibleGameObject()
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DestroyDrawable( _graphicsId );
	}

	// _renderSequence is used to set the render order of VisibleGameObjects. The render order of 
	// VisibleGameObjects is equal to the order in which they are created.
	IntSequence VisibleGameObject::_renderSequence( 1 );
}