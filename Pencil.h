#pragma once

/* Pencil class represents a pencil sprite that floats over the other rendered game objects. */

#include <string>
#include <utility>
#include <vector>

#include "IAudioProvider.h"
#include "Point2D.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{
	class Pencil : public VisibleGameObject
	{
		// Constructors
	public:
		/* Initializes a Pencil. */
		Pencil();

		// Methods
	public:
		/* CycleImage
		   Summary: Cycles the source image used when drawing the pencil. _pencilImages maintains
		            the collection of images to cycle between. */
		void CycleImage();

		/* PlaySketchSound
		   Summary: Plays a sketch sound appropriate for a specified sketch duration. */
		void PlaySketchSound( double sketchDuration );

		// Virtual methods
	public:
		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

	private:
		typedef std::vector< std::pair< std::string, Point2D > > PencilImages;
		typedef std::vector< IAudioProvider::AudioId > PencilSounds;
		
		PencilSounds _pencilStrokesShort;
		PencilSounds _pencilStrokesLong;
		std::size_t _shortStrokeIndex;
		std::size_t _longStrokeIndex;
		IAudioProvider::AudioId _currStrokeSound;

		PencilImages _pencilImages;
		std::size_t _currentImageIndex;


	};

}