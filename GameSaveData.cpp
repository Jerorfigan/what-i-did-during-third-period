/* GameSaveData implementation. */

#include "stdafx.h"

#include "GameSaveData.h"

namespace PROJECT_NAME
{

	/* Filestream input/output operations. */

	std::fstream& operator>>( std::fstream& fileStream, GameSaveData& gameSaveData )
	{
		fileStream >> gameSaveData.currentMapIndex;

		return fileStream;
	}

	std::fstream& operator<<( std::fstream& fileStream, const GameSaveData& gameSaveData )
	{
		fileStream << gameSaveData.currentMapIndex;

		return fileStream;
	}

}