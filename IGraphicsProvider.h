#pragma once

/* Defines abstract functionality for initializing and interacting with a render window, drawing objects to said window, 
   and checking for user input on that window. */

#include <string>
#include <vector>

#include "KeyboardKey.h"
#include "Point2D.h"
#include "Rectangle2D.h"
#include "GameEvent.h"

namespace PROJECT_NAME
{
	/* Note regarding layout of IGraphicsProvider: 
	   There are four categories of functions contained within this class (the functions belonging to a particular group
	   are placed adjacent and 2 lines of whitespace separate the groups). The first group contains functions 
	   related to managing and obtaining information from a render window. The second group contains functions
	   related to managing drawable strings (these are simply string-like objects that can be displayed in the 
	   render window). The third group contains functions related to managing sprites. Both drawable strings and 
	   sprites can more generally be classed as drawables, hence the fourth group contains functions that are 
	   common to all drawables. */
	class IGraphicsProvider
	{
	public:
		typedef std::size_t ResourceId;

		struct DisplayResolution;

		/* InitializeWindow
		   Summary: Initializes a render window. 
		   Parameters: 
			  gameSpace: The rectangle that defines the game space that the window will be responsible for
						 rendering.
			  title (optional): The desired title of the window (default is blank). 
			  width (optional): The desired width in pixels of the window (default will be set internally). 
			  height (optional): The desired height in pixels of the window (default will be set internally). 
			  fullscreen (optional): A boolean flag indicating whether this window should be 
									 fullscreen (default is true).
		   Returns: The ID allocated for this render window, by which the render window may be referenced 
					from the client app. */
		virtual ResourceId InitializeWindow( const Rectangle2D &gameSpace, 
			std::string title = "", std::size_t width = 0, std::size_t height = 0, bool fullscreen = true ) = 0;
		/* PrepareWindowForDrawing
		   Summary: Prepares the specified render window for drawing (i.e. clears the backbuffer, etc). 
		   Parameters: 
		      windowId: The ID of the render window to be prepared for drawing. */
		virtual void PrepareWindowForDrawing( ResourceId windowId ) = 0;
		/* GetEvent
		   Summary: Checks for and retrieves an event from the queue of events that have occurred on this
		            window. Subsequent calls will retrieve additional events from the queue. 
		   Parameters: 
		      windowId: The ID of the render window to check and retrieve events for. 
			  aEvent: An empty GameEvent object that will be filled with the retrieved event if there is one. 
		   Returns: True if an event was retrieved from the queue, false otherwise. */
		virtual bool GetGameEvent( ResourceId windowId, GameEvent &gameEvent ) = 0;
		/* GetFrameTime
		   Summary: Gets the time elapsed in seconds between the last two instances where the specified window was 
					displayed (i.e. the time that needs to be processed during the next update in the calling
					app).
		   Parameters: 
		      windowId: The ID of the render window to get the frame time for. 
		   Returns: The frame time in seconds. */
		virtual double GetFrameTime( ResourceId windowId ) = 0;
		/* IsKeyDown
		   Summary: Checks if a window detects a key being held down.
		   Parameters: 
		      windowId: The ID of the window. 
		      key: The key to check.
		   Returns: True if the key is being held down, false otherwise. */
		virtual bool IsKeyDown( ResourceId windowId, KeyboardKey::Key key ) = 0;
		/* DisplayWindow
		   Summary: Displays the render window to the screen.
		   Parameters: 
		      windowId: The ID of the render window to display. */
		virtual void DisplayWindow( ResourceId windowId ) = 0;
		/* GetSupportedDisplayResolutions
		   Summary: Returns a std::vector filled with the supported display resolutions. 
		   Returns: A std::vector filled with the supported display resolutions. */
		virtual std::vector< DisplayResolution > GetSupportedDisplayResolutions() = 0;
		/* GetDisplayResolution
		   Summary: Returns the display resolution of a window. 
		   Parameters:
		      windowId: The ID of the window.
		   Returns: The display resolution of the window. */
		virtual DisplayResolution GetDisplayResolution( ResourceId windowId ) = 0;
		/* IsFullscreen 
		   Summary: Determines whether a window is currently running at full screen. 
		   Parameters:
		      windowId: The ID of the window to check. 
		   Returns: True if the window is running at fullscreen, false otherwise. */
		virtual bool IsFullscreen( ResourceId windowId ) = 0;
		/* DestroyWindow
		   Summary: Destroys a window. 
		   Parameters:
		      windowId: The ID of the window to destroy. */
		virtual void DestroyWindow( ResourceId windowId ) = 0;

		/* InitializeString
		   Summary: Initializes a drawable string.
		   Parameters: 
		      contents: The contents that this drawable string should contain. 
		   Returns: The ID allocated for this drawable string, by which the drawable string entity may be
		            referenced from the client app. */
		virtual ResourceId InitializeString( std::string contents ) = 0;
		/* SetString
		   Summary: Sets a drawable string to a new string value.
		   Parameters: 
			  stringId: The ID of the drawable string.
		      newContents: The new contents that the drawable string should have. */
		virtual void SetString( ResourceId stringId, std::string newContents ) = 0;
		/* GetPixelLength 
		   Summary: Returns the length of the string in pixels. 
		   Parameters:
		      stringId: The ID of the string whose length in pixels is returned.
		   Returns: The length of the string in pixels. */
		virtual int GetPixelLength( ResourceId stringId ) = 0;

		/* InitializeSprite
		   Summary: Initializes a sprite.
		   Parameters: 
			  imageFile: The image file to associate with this sprite.
		   Returns: The ID allocated for this sprite, by which the sprite entity may be
		            referenced from the client app. */
		virtual ResourceId InitializeSprite( std::string imageFile ) = 0;
		/* SetSpriteImage 
		   Summary: Sets the source image for this sprite. 
		   Parameters:
		      spriteId: The ID of the sprite.
			  imageFile: The source image. */
		virtual void SetSpriteImage( ResourceId spriteId, std::string imageFile ) = 0;
		/* SetSpriteSubRectangle
		   Summary: Sets the subrectangle for a sprite.
		   Parameters: 
			  spriteId: The ID of the sprite.
			  rect: The rectangle to set it to. */
		virtual void SetSpriteSubRectangle( ResourceId spriteId, Rectangle2D rect ) = 0;
		/* GetSpriteSubRectangle
		   Summary: Gets the subrectangle for a sprite.
		   Parameters: 
			  spriteId: The ID of the sprite.
		   Returns: The subrectangle */
		virtual Rectangle2D GetSpriteSubRectangle( ResourceId spriteId ) = 0;


		/* SetDrawablePosition
		   Summary: Sets the position of a drawable.
		   Parameters: 
			  drawableId: The ID of the drawable.
			  drawablePos: The position of the drawable. */
		virtual void SetDrawablePosition( ResourceId drawableId, Point2D drawablePos ) = 0;
		/* GetDrawablePosition
		   Summary: Gets the position of a drawable.
		   Parameters: 
			  drawableId: The ID of the drawable.
		   Returns: The position of the drawable. */
		virtual Point2D GetDrawablePosition( ResourceId drawableId ) = 0;
		/* SetDrawableCenter
		   Summary: Sets the center point of the drawable.
		   Parameters: 
			  drawableId: The ID of the drawable.
			  drawableCenter: The center point of the drawable. */
		virtual void SetDrawableCenter( ResourceId drawableId, Point2D drawableCenter ) = 0;
		/* SetDrawableRotation
		   Summary: Sets the rotation of the drawable. 
		   Parameters:
			  drawableId: The ID of the drawable.
			  angle: The angle to set the drawable to (in degrees). */
		virtual void SetDrawableRotation( ResourceId drawableId, double angle ) = 0;
		/* SetDrawableScale
		   Summary: Sets the scale of the drawable.
		   Paramters:
		      drawableId: The ID of the drawable. 
			  xScaleFactor: The x-axis scale factor.
			  yScaleFactor: The y-axis scale factor. */
		virtual void SetDrawableScale( ResourceId drawableId, double xScaleFactor, double yScaleFactor ) = 0;
		/* DrawDrawable
		   Summary: Draws a drawable in a window.
		   Parameters: 
			  drawableId: The ID of the drawable.
			  windowId: The ID of the window. */
		virtual void DrawDrawable( ResourceId drawableId, ResourceId windowId ) = 0;
		/* DestroyDrawable
		   Summary: Destroys a drawable.
		   Parameters: 
			  drawableId: The ID of the drawable. */
		virtual void DestroyDrawable( ResourceId drawableId ) = 0;

		/* Virtual destructor to allow deletion of derived classes through IGraphicsProvider pointer. */
		virtual ~IGraphicsProvider();
	};

}

#include "DisplayResolution.h"