/* IntSequence implementation. */

#include "stdafx.h"

#include "IntSequence.h"

namespace PROJECT_NAME
{

	/* Initializes an IntSequence with an initial value (1 is default). */
	IntSequence::IntSequence( SequenceValue initialValue ) : _initialValue( initialValue ),
		_sequenceValue( initialValue )
	{

	}

	/* Value 
	   Summary: Returns the value of the current sequence.
	   Returns: The value of the current sequence. */
	IntSequence::SequenceValue IntSequence::Value()
	{
		return _sequenceValue;
	}

	/* Operator: ++ (prefix)
	   Summary: Increment the sequence by 1.
	   Returns: A reference to the incremented sequence value. */
	IntSequence::SequenceValue& IntSequence::operator++()
	{
		return ++_sequenceValue;
	}

	/* Operator: ++ (postfix)
	   Summary: Increment the sequence by 1.
	   Returns: The value of the pre-incremented sequence value. */
	IntSequence::SequenceValue IntSequence::operator++( int )
	{
		return _sequenceValue++;
	}

	/* Reset 
	   Summary: Resets the current sequence value to its initial value. */
	void IntSequence::Reset()
	{
		_sequenceValue = _initialValue;
	}

}