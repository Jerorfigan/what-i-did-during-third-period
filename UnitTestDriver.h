#pragma once

/* Declares the unit test driver function, which kicks off all the unit tests. */

namespace PROJECT_NAME_UnitTests
{

	void UnitTestDriver();

}