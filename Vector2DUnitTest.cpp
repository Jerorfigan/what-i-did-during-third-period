/* Defines the unit test for the Vector2D class. */

#include "stdafx.h"

#include <cmath>

#include "Point2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME_UnitTests
{
	using namespace PROJECT_NAME;

	void Vector2DUnitTest()
	{
		/* Test 1: Exercise the default constructor and verify that it initializes a <0,0> vector. */
		{
			Vector2D vec;
			assert( vec._x == 0 );
			assert( vec._y == 0 );
		}

		/* Test 2: Exercise the non-default constructors and verify they initialize correctly. */
		
		// Case 1: The constructor that takes x and y component values.
		{
			Vector2D vec( 4, 7 );
			assert( vec._x == 4 );
			assert( vec._y == 7 );
		}

		// Case 2: The constructor that takes a Point2D. */
		{
		    Vector2D vec( Point2D( 18, 4 ) );
			assert( vec._x == 18 );
			assert( vec._y == 4 );
		}

		// Case 3: The constructor that takes an origin point and a target point.
		{
			Vector2D vec( Point2D( 1, 1 ), Point2D( 5, 7 ) );
			assert( vec._x == 4 );
			assert( vec._y == 6 );
		}

		/* Test 3: Exercise the Vector2D::Magnitude function and verify that it evaluates the correct 
		           magnitude. */
		{
			Vector2D vec( 4, 3 );
			assert( vec.Magnitude() == 5 );
		}
		
		/* Test 4: Exercise the Vector2D::UnitVector function and verify that it evaluates the correct 
		           unit vector. */
		{
			Vector2D vec( 4, 3 );
			// Account for rounding errors
			assert( abs( vec.UnitVector()._x - 0.8 ) < 0.001 );
			assert( abs( vec.UnitVector()._y - 0.6 ) < 0.001 ); 
		}

		/* Test 5: Exercise the Vector2D::Angle function and verify that it evaluates the correct 
		           angle. */
		{
			Vector2D vec1( 3, 3 );
			Vector2D vec2( 0, 7 );
			Vector2D vec3( -3, 12 );
			Vector2D vec4( -3, 0 );
			Vector2D vec5( -5, -4 );
			Vector2D vec6( 0, -3 );
			Vector2D vec7( 6, -8 );
			Vector2D vec8( 6, 0 );
			// Account for rounding errors
			assert( abs( vec1.UnitVector().Angle() - 45.0 ) < 0.001 ); 
			assert( abs( vec2.UnitVector().Angle() - 90.0 ) < 0.001 ); 
			assert( abs( vec3.UnitVector().Angle() - 104.03624 ) < 0.001 ); 
			assert( abs( vec4.UnitVector().Angle() - 180.0 ) < 0.001 ); 
			assert( abs( vec5.UnitVector().Angle() - 218.65980 ) < 0.001 ); 
			assert( abs( vec6.UnitVector().Angle() - 270.0 ) < 0.001 ); 
			assert( abs( vec7.UnitVector().Angle() - 306.86989 ) < 0.001 ); 
			assert( vec8.UnitVector().Angle() < 0.001 ); 
		}

		/* Test 6: Exercise the Vector2D::ProjectOnto function and verify that it evaluates the correct 
		           vector projection. */
		{
			Vector2D vec( 4, 3 );
			// Account for rounding errors
			assert( abs( vec.ProjectOnto( Vector2D( 6, 0 ) )._x - 4 ) < 0.001 );
			assert( vec.ProjectOnto( Vector2D( 6, 0 ) )._y < 0.001 ); 
			
			assert( abs( vec.ProjectOnto( Vector2D( -6, 0 ) )._x - 4 ) < 0.001 );
			assert( vec.ProjectOnto( Vector2D( -6, 0 ) )._y < 0.001 ); 
			
			assert( abs( vec.ProjectOnto( Vector2D( 1, 7 ) )._x - 0.5 ) < 0.001 );
			assert( abs( vec.ProjectOnto( Vector2D( 1, 7 ) )._y - 3.5 ) < 0.001 ); 
		}

		/* Test 7: Exercise the Vector2D::Rotate function and verify it evaluates the rotated vector 
		           correctly. */
		{
			Vector2D vec( 3, 3 );
			vec.Rotate( 45 );
			// Account for rounding errors
			assert( abs( vec.Angle() - 90 ) < 0.001 );
			assert( vec._x < 0.001 );
			assert( abs( vec._y - 4.24264 ) < 0.001 );
			
			vec.Rotate( -120 );
			assert( abs( vec.Angle() - 330 ) < 0.001 );
			assert( abs( vec._x - 3.67423 ) < 0.001 );
			assert( abs( vec._y + 2.12132 ) < 0.001 );
		}

		/* Test 8: Exercise the Vector2D::ReflectOver function and verify it evaluates the reflected vector
		           correctly. */
		
		// Case 1: Reflect vector in first quadrant over vector running along positive x-axis
		{
			Vector2D vec( 3, 3 );
			Vector2D axis( 3, 0 );
			Vector2D reflection = vec.ReflectOver( axis );
			// Account for rounding errors
			assert( abs( reflection._x - 3 ) < 0.001 );
			assert( abs( reflection._y + 3 ) < 0.001 );

		}

		// Case 2: Reflect vector in first quadrant over vector running along negative x-axis
		{
			Vector2D vec( 3, 3 );
			Vector2D axis( -3, 0 );
			Vector2D reflection = vec.ReflectOver( axis );
			// Account for rounding errors
			assert( abs( reflection._x - 3 ) < 0.001 );
			assert( abs( reflection._y + 3 ) < 0.001 );
		}

		// Case 3: Reflect vector in first quadrant over arbitrary axis
		{
			Vector2D vec( 3, 3 );
			Vector2D axis( 2, 7 );
			Vector2D reflection = vec.ReflectOver( axis );
			// Account for rounding errors
			assert( abs( reflection._x + 0.96226 ) < 0.001 );
			assert( abs( reflection._y - 4.13207 ) < 0.001 );
		}

		/* Test 9: Exercise the * operator that takes a scalar and a vector. */
		{
			Vector2D vec( 3, 5 );
			Vector2D result = 4 * vec;
			assert( result._x == 12.0 );
			assert( result._y == 20.0 );
		}

		/* Test 10: Exercise the - operator that takes two Vector2D arguments. */
		{
			Vector2D vec1( 4, 7 );
			Vector2D vec2( 1, 3 );
			Vector2D result = vec1 - vec2;
			assert( result._x == 3 );
			assert( result._y == 4 );
		}

		/* Test 11: Exercise the Vector2D::IsZero function and verify that it works correctly. */
		{
			Vector2D vec1( 4, 7 );
			Vector2D vec2( 0, 0 );
			assert( vec1.IsZero() == false );
			assert( vec2.IsZero() == true );
		}

		/* Test 12: Exercise the += operator that takes a left-hand-side argument of type Vector2D and a
		            right-hand-side argument of type Vector2D. */
		{
			Vector2D vec1( 10, 5 );
			Vector2D vec2( 3, 3 );
			vec1 += vec2;
			assert( vec1._x == 13 );
			assert( vec1._y == 8 );
		}
	}

}