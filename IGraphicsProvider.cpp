/* Implementation for IGraphicsProvider. */

#include "stdafx.h"

#include "IGraphicsProvider.h"

namespace PROJECT_NAME
{

	/* Virtual destructor to allow deletion of derived classes through IGraphicsProvider pointer. */
	IGraphicsProvider::~IGraphicsProvider() 
	{

	}

}