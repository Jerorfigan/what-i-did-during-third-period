#pragma once

/* Defines the functionality of the settings menu. */

#include "ArrowSelector.h"
#include "Game.h"
#include "Menu.h"

namespace PROJECT_NAME
{
	
	class SettingsMenu
	{
		// Constructors
	public:
		SettingsMenu();

		// Methods
	public:
		/* Process 
		   Summary: Processes the settings menu and returns the next game state. */
		Game::GameState Process();

		// Data
	private:
		struct SettingsMenuData
		{
			bool mFullScreen;
			std::size_t mDisplayResolutionIndex;
			std::size_t mSoundEffectsVolLevel;
			std::size_t mMusicVolLevel;
		};

		Menu mMenu;
		SettingsMenuData mData;

		typedef std::vector< IGraphicsProvider::DisplayResolution > DisplayResolutions;

		DisplayResolutions mValidDisplayResolutions;

		// Menu event handlers (which are made friends)
		friend void FullScreenCheckBox( void* menuData );
		friend void BackButtonClicked( void* menuData );
		friend void ResolutionChanged( void* menuData, ArrowSelector* arrowSelectorPtr );
		friend void SoundEffectsVolChanged( void* menuData, ArrowSelector* arrowSelectorPtr );
		friend void MusicVolChanged( void* menuData, ArrowSelector* arrowSelectorPtr );
	};
}