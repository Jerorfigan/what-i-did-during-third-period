/* SelectProfileMenu implementation. */

#include "stdafx.h"

#include "GameEvent.h"
#include "Point2D.h"
#include "PushButton.h"
#include "SelectProfileMenu.h"
#include "TextBox.h"

namespace PROJECT_NAME
{

	// Constructors

	SelectProfileMenu::SelectProfileMenu() : mMenu( Handle< Background >( 
		new Background( RESOURCE_PATH( "Art/NotebookBackground.jpg" ), Rectangle2D( Point2D( 800, 800 ), 1600, 900 ) ) ), 
		&mData )
	{
		Handle< TextBox > nameTextBoxHandle( 
			new TextBox( "name", 0, RESOURCE_PATH( "Art/Menus/SelectProfile/TextBoxLabels/NameLabel.png" ), Rectangle2D( Point2D( 900, 400 ), 400, 75 ), NameTextChanged ) );

		mMenu.AddControl( nameTextBoxHandle ); 

		Handle< PushButton > acceptButtonHandle( 
			new PushButton( "accept", 1, Rectangle2D( Point2D( 800, 500 ), 250, 75 ),
				RESOURCE_PATH( "Art/Menus/SelectProfile/Buttons/AcceptButton.png" ), 
				RESOURCE_PATH( "Art/Menus/SelectProfile/Buttons/AcceptButtonHighlighted.png" ), 
				RESOURCE_PATH( "Art/Menus/SelectProfile/Buttons/AcceptButtonHighlighted.png" ),
				0, 0, AcceptButtonClicked ) );


		mMenu.AddControl( acceptButtonHandle ); 
	}

	// Methods

	/* Process 
		Summary: Processes the Main menu and returns the next game state. */
	Game::GameState SelectProfileMenu::Process()
	{
		mMenu.Interact();

		Game::SetSelectedProfile( mData.mName );

		return Game::MainMenu;
	}

	// Menu event handlers 

	void NameTextChanged( void* menuData, TextBox* textBox )
	{
		// Set the mSelection member in menuData to indicate that Start was clicked.
		SelectProfileMenu::SelectProfileData* menuDataPtr = static_cast< SelectProfileMenu::SelectProfileData* >( menuData );
	
		menuDataPtr->mName = textBox->GetContents();
	}

	void AcceptButtonClicked( void* menuData )
	{
		SelectProfileMenu::SelectProfileData* menuDataPtr = static_cast< SelectProfileMenu::SelectProfileData* >( menuData );

		// Push the menu exit event onto the UI queue so that the menu processing will
		// terminate. Only allow termination if a valid name was entered.

		if( menuDataPtr->mName.size() > 0 )
		{
			GameEvent menuExit;
			menuExit._event = GameEvent::MenuExit;

			Game::GetGameAssetManager().GetGameEventQueue().PushEvent( menuExit );
		}
	}

}