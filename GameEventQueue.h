#pragma once

/* GameEventQueue defines the functionality for a queue of events signaled by the game/UI logic. */

#include <queue>

#include "GameEvent.h"

namespace PROJECT_NAME
{

	class GameEventQueue
	{
		// Methods
	public:
		/* PushEvent 
		   Summary: Pushes an event to the back of the UI event queue. 
		   Parameters:
				gameEvent: The event to push to the back of the UI event queue. */
		void PushEvent( const GameEvent &gameEvent );
		/* PopEvent 
		   Summary: Returns the event at the front of the UI event queue and pops the queue.
					If the queue is empty, returns a blank event.
		   Returns: The event at the front of the UI event queue. */
		GameEvent PopEvent();
	
		/* IsEmpty 
		   Summary: Returns true if the UI event queue is empty. 
		   Returns: true if the UI event queue is empty. */
		bool IsEmpty();

		// Data
	private:
		std::queue< GameEvent > mEventQueue;
	};

}