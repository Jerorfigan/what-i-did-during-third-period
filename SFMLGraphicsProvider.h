#pragma once

/* SFMLGraphicsProvider implements the graphics functionality described in IGraphicsProvider using
   the Simple Fast Media Library. */

#include <map>
#include <string>

#include "SFML/Graphics.hpp"
#include "Handle.h"
#include "Handle.cpp"
#include "IGraphicsProvider.h"
#include "Point2D.h"

namespace PROJECT_NAME
{

	class SFMLGraphicsProvider : public IGraphicsProvider
	{
	public:
		SFMLGraphicsProvider();

		/* InitializeWindow
		   Summary: Initializes a render window. 
		   Parameters: 
			  gameSpace: The rectangle that defines the game space that the window will be responsible for
						 rendering.
			  title (optional): The desired title of the window (default is blank). 
			  width (optional): The desired width in pixels of the window (default will be set internally). 
			  height (optional): The desired height in pixels of the window (default will be set internally). 
			  fullscreen (optional): A boolean flag indicating whether this window should be 
									 fullscreen (default is true).
		   Returns: The ID allocated for this render window, by which the render window may be referenced 
					from the client app. */
		virtual ResourceId InitializeWindow( const Rectangle2D &gameSpace, 
			std::string title = "", std::size_t width = 0, std::size_t height = 0, bool fullscreen = true );
		/* PrepareWindowForDrawing
		   Summary: Prepares the specified render window for drawing (i.e. clears the backbuffer, etc). 
		   Parameters: 
		      windowId: The ID of the render window to be prepared for drawing. */
		virtual void PrepareWindowForDrawing( ResourceId windowId );
		/* GetEvent
		   Summary: Checks for and retrieves an event from the queue of events that have occurred on this
		            window. Subsequent calls will retrieve additional events from the queue. 
		   Parameters: 
		      windowId: The ID of the render window to check and retrieve events for. 
			  aEvent: An empty GameEvent object that will be filled with the retrieved event if there is one. 
		   Returns: True if an event was retrieved from the queue, false otherwise. */
		virtual bool GetGameEvent( ResourceId windowId, GameEvent &gameEvent );
		/* GetFrameTime
		   Summary: Gets the time elapsed in seconds between the last two instances where the specified window was 
					displayed (i.e. the time that needs to be processed during the next update in the calling
					app).
		   Parameters: 
		      windowId: The ID of the render window to get the frame time for. 
		   Returns: The frame time in seconds. */
		virtual double GetFrameTime( ResourceId windowId );
		/* IsKeyDown
		   Summary: Checks if a window detects a key being held down.
		   Parameters: 
		      windowId: The ID of the window. 
		      key: The key to check.
		   Returns: True if the key is being held down, false otherwise. */
		virtual bool IsKeyDown( ResourceId windowId, KeyboardKey::Key key );
		/* DisplayWindow
		   Summary: Displays the render window to the screen.
		   Parameters: 
		      windowId: The ID of the render window to display. */
		virtual void DisplayWindow( ResourceId windowId );
		/* GetSupportedDisplayResolutions
		   Summary: Returns a std::vector filled with the supported display resolutions. 
		   Returns: A std::vector filled with the supported display resolutions. */
		virtual std::vector< DisplayResolution > GetSupportedDisplayResolutions();
		/* GetDisplayResolution
		   Summary: Returns the display resolution of a window. 
		   Parameters:
		      windowId: The ID of the window.
		   Returns: The display resolution of the window. */
		virtual DisplayResolution GetDisplayResolution( ResourceId windowId );
		/* IsFullscreen 
		   Summary: Determines whether a window is currently running at full screen. 
		   Parameters:
		      windowId: The ID of the window to check. 
		   Returns: True if the window is running at fullscreen, false otherwise. */
		virtual bool IsFullscreen( ResourceId windowId );
		/* DestroyWindow
		   Summary: Destroys a window. 
		   Parameters:
		      windowId: The ID of the window to destroy. */
		virtual void DestroyWindow( ResourceId windowId );


		/* InitializeString
		   Summary: Initializes a drawable string.
		   Parameters: 
		      contents: The contents that this drawable string should contain. 
		   Returns: The ID allocated for this drawable string, by which the drawable string entity may be
		            referenced from the client app. */
		virtual ResourceId InitializeString( std::string contents );
		/* SetString
		   Summary: Sets a drawable string to a new string value.
		   Parameters: 
			  stringId: The ID of the drawable string.
		      newContents: The new contents that the drawable string should have. */
		virtual void SetString( ResourceId stringId, std::string newContents );
		/* GetPixelLength 
		   Summary: Returns the length of the string in pixels. 
		   Parameters:
		      stringId: The ID of the string whose length in pixels is returned.
		   Returns: The length of the string in pixels. */
		virtual int GetPixelLength( ResourceId stringId );

		/* InitializeSprite
		   Summary: Initializes a sprite.
		   Parameters: 
			  imageFile: The image file to associate with this sprite.
		   Returns: The ID allocated for this sprite, by which the sprite entity may be
		            referenced from the client app. */
		virtual ResourceId InitializeSprite( std::string imageFile );
		/* SetSpriteImage 
		   Summary: Sets the source image for this sprite. 
		   Parameters:
		      spriteId: The ID of the sprite.
			  imageFile: The source image. */
		virtual void SetSpriteImage( ResourceId spriteId, std::string imageFile );
		/* SetSpriteSubRectangle
		   Summary: Sets the subrectangle for a sprite.
		   Parameters: 
			  spriteId: The ID of the sprite.
			  rect: The rectangle to set it to. */
		virtual void SetSpriteSubRectangle( ResourceId spriteId, Rectangle2D rect );
		/* GetSpriteSubRectangle
		   Summary: Gets the subrectangle for a sprite.
		   Parameters: 
			  spriteId: The ID of the sprite.
		   Returns: The subrectangle */
		virtual Rectangle2D GetSpriteSubRectangle( ResourceId spriteId );


		/* SetDrawablePosition
		   Summary: Sets the position of a drawable.
		   Parameters: 
			  drawableId: The ID of the drawable.
			  drawablePos: The position of the drawable. */
		virtual void SetDrawablePosition( ResourceId drawableId, Point2D drawablePos );
		/* GetDrawablePosition
		   Summary: Gets the position of a drawable.
		   Parameters: 
			  drawableId: The ID of the drawable.
		   Returns: The position of the drawable. */
		virtual Point2D GetDrawablePosition( ResourceId drawableId );
		/* SetDrawableCenter
		   Summary: Sets the center point of the drawable.
		   Parameters: 
			  drawableId: The ID of the drawable.
			  drawableCenter: The center point of the drawable. */
		virtual void SetDrawableCenter( ResourceId drawableId, Point2D drawableCenter );
		/* SetDrawableRotation
		   Summary: Sets the rotation of the drawable. 
		   Parameters:
			  drawableId: The ID of the drawable.
			  angle: The angle to set the drawable to (in degrees). */
		virtual void SetDrawableRotation( ResourceId drawableId, double angle );
		/* SetDrawableScale
		   Summary: Sets the scale of the drawable.
		   Paramters:
		      drawableId: The ID of the drawable. 
			  xScaleFactor: The x-axis scale factor.
			  yScaleFactor: The y-axis scale factor. */
		virtual void SetDrawableScale( ResourceId drawableId, double xScaleFactor, double yScaleFactor );
		/* DrawDrawable
		   Summary: Draws a drawable in a window.
		   Parameters: 
			  drawableId: The ID of the drawable.
			  windowId: The ID of the window. */
		virtual void DrawDrawable( ResourceId drawableId, ResourceId windowId );
		/* DestroyDrawable
		   Summary: Destroys a drawable.
		   Parameters: 
			  drawableId: The ID of the drawable. */
		virtual void DestroyDrawable( ResourceId drawableId );

	private:
		struct RenderWindowSettings
		{
			bool _isFullscreen;
		};

		/* Private function used to verify that the passed-in ResourceId refers to an actual RenderWindow. */
		bool ValidRenderWindow( ResourceId id );
		/* Private function used to verify that the passed-in ResourceId refers to an actual Drawable. */
		bool ValidDrawable( ResourceId id );
		/* Loads the specified image into the _images container. */
		void LoadImage( std::string imageFile );
		/* Returns true if the specified image is already in the _images container. */
		bool IsImageLoaded( std::string imageFile ); 
		/* Resets the view of a window to the smallest possible that contains the gamespace and matches
		   the aspect ratio of the window. */
		void SetView( ResourceId id );
		/* Converts a point in window client coordinates to one in gamespace coordinates. */
		Point2D WindowPointToGamespace( ResourceId id, int x, int y ); 

		// Each render window has an associated game space and render view
		std::map< ResourceId, Handle< sf::RenderWindow > > _renderWindows;
		std::map< ResourceId, RenderWindowSettings > _renderWindowSettings;
		std::map< ResourceId, Rectangle2D > _gameSpaces;
		std::map< ResourceId, Handle< sf::View > > _renderViews;

		std::map< ResourceId, Handle< sf::Drawable > > _drawables; 
		std::map< std::string, Handle< sf::Image > > _images;

		// Stores the next available resource id to use when instantiating a new resource for the client
		ResourceId _nextId;
	};

}