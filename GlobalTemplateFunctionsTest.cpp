/* Defines the unit test for the global template functions. */

#include "stdafx.h"

#include <vector>

#include "GlobalTemplateFunctions.h"

namespace PROJECT_NAME_UnitTests
{
	using namespace PROJECT_NAME;

	bool GreaterThan( std::vector< int >::iterator first, std::vector< int >::iterator second );

	void GlobalTemplateFunctionsTest()
	{
		/* Test 1: Exercise the InsertionSort function using the default comparator (<) and verify it correctly 
		           sorts a vector in ascending order. */
		{
			std::vector< int > intVec;
			intVec.push_back( 7 );
			intVec.push_back( 13 );
			intVec.push_back( 5 );
			intVec.push_back( 2 );
			intVec.push_back( 9 );

			InsertionSort( intVec.begin(), intVec.end() );

			assert( intVec[0] == 2 );
			assert( intVec[1] == 5 );
			assert( intVec[2] == 7 );
			assert( intVec[3] == 9 );
			assert( intVec[4] == 13 );
		}

		/* Test 2: Exercise the InsertionSort function using an override comparator (>) and verify it correctly 
		           sorts a vector in descending order. */
		{
			std::vector< int > intVec;
			intVec.push_back( 7 );
			intVec.push_back( 13 );
			intVec.push_back( 5 );
			intVec.push_back( 2 );
			intVec.push_back( 9 );

			InsertionSort( intVec.begin(), intVec.end(), GreaterThan );

			assert( intVec[0] == 13 );
			assert( intVec[1] == 9 );
			assert( intVec[2] == 7 );
			assert( intVec[3] == 5 );
			assert( intVec[4] == 2 );
		}
	}

	bool GreaterThan( std::vector< int >::iterator first, std::vector< int >::iterator second )
	{
		return *first > *second;
	}

}