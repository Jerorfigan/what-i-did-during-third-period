/* DisplayResolution implementation. */

#include "stdafx.h"

#include <sstream>

#include "DisplayResolution.h"

namespace PROJECT_NAME
{
	/* Initializes a DisplayResolution from a width and height. */
	IGraphicsProvider::DisplayResolution::DisplayResolution( std::size_t width, std::size_t height ) : 
		_width( width ), _height( height )
	{

	}

	/* GetInfoString
	   Summary: Returns the display resolution information as a std::string. 
	   Returns: The display resolution information as a std::string. */
	std::string IGraphicsProvider::DisplayResolution::GetInfoString() const
	{
		std::stringstream displayResStream;
		displayResStream << _width << "x" << _height;

		return displayResStream.str();
	}

	/* Equality operators */
	bool IGraphicsProvider::DisplayResolution::operator==( const DisplayResolution &rhs )
	{
		return this->_height == rhs._height && this->_width == rhs._width;
	}

	bool IGraphicsProvider::DisplayResolution::operator!=( const DisplayResolution &rhs )
	{
		return !( *this == rhs );
	}

}