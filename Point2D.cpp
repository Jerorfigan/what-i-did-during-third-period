/* Implements the functionality for a 2D Cartesian point. */

#include "stdafx.h"
#include "Point2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME
{

	/* Default constructor initializes a point with _x and _y equal to 0. */
	Point2D::Point2D() : _x( 0 ), _y( 0 ) 
	{

	}
	
	/* Constructor that initializes a point from passed in x and y values. */
	Point2D::Point2D( double x, double y ) : _x( x ), _y( y )
	{

	}

	/* Constructor that initializes a point from a vector. */
	Point2D::Point2D( const Vector2D &vec ) : _x( vec._x ), _y( vec._y )
	{

	}

	/* PointLocation 
	   Summary: Computes the general location of the point ( i.e. first quadrant, positive x-axis, origin, etc).
	   Returns: The general location of the point. */
	Point2D::PointLocation Point2D::GetPointLocation() const
	{
		PointLocation location;

		if( _x > 0 && _y > 0 )
		{
			location = FirstQuadrant;
		}
		else if( _x < 0 && _y > 0 )
		{
			location = SecondQuadrant;
		}
		else if( _x < 0 && _y < 0 )
		{
			location = ThirdQuadrant;
		}
		else if( _x > 0 && _y < 0 )
		{
			location = FourthQuadrant;
		}
		else if( _x > 0 && _y == 0 )
		{
			location = PositiveXAxis;
		}
		else if( _x < 0 && _y == 0 )
		{
			location = NegativeXAxis; 
		}
		else if( _y > 0 && _x == 0 )
		{
			location = PositiveYAxis;
		}
		else if( _y < 0 && _x == 0 )
		{
			location = NegativeYAxis;
		}
		else
		{
			location = Origin;
		}

		return location;
	}

	/* operator: == 
	   Summary: Tests whether two points are equal. Points are equal if _x and _y components are both equal.
	   Returns: True if the points are equal, false otherwise. */
	bool Point2D::operator==( const Point2D &point ) const
	{
		return EqualDouble( _x, point._x ) && EqualDouble( _y, point._y );
	}

	/* operator: != 
	   Summary: Tests whether two points are unequal. Points are unequal if they're not equal (see definition of
	            == operator).
	   Returns: True if the points are unequal, false otherwise. */
	bool Point2D::operator!=( const Point2D &point ) const
	{
		return !( *this == point );
	}

	/* operator: += 
	   Summary: Adds a vector to this point and stores the result in this point.
	   Parameters:
	      vector: The vector to add to this point. 
	   Returns: A reference to this point. */
	Point2D& Point2D::operator+=( const Vector2D &vector )
	{
		*this = *this + vector;
		return *this;
	}

	/* Output operator for a Point2D object. */
	std::ostream& operator<<( std::ostream &outputStream, const Point2D &point )
	{
		outputStream << "( " << point._x << " , " << point._y << " )"; 

		return outputStream;
	}
}