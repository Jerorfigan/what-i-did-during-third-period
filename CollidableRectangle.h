#pragma once

/* CollidableRectangle represents the rectangular area of an object that can be collided with. */

#include <iostream>

#include "CollidableArea.h"
#include "Point2D.h"
#include "Rectangle2D.h"

namespace PROJECT_NAME
{
	class CollidableRectangle : public CollidableArea
	{
	public:
		/* Initializes a CollidableRectangle from a Rectangle2D. */
		CollidableRectangle( const Rectangle2D &rect );

		/* CollidesWith 
		   Summary: Returns true if this CollidableArea overlaps otherArea. 
		   Returns: True if this CollidableArea overlaps otherArea. */
		virtual bool CollidesWith( const CollidableArea* otherArea ) const;

		/* GetLineSegments 
		   Summary: Returns a vector filled with the line segments that comprise this CollidableArea. 
		   Returns: A vector filled with the line segments that comprise this CollidableArea. */
		virtual std::vector< LineSegment2D > GetLineSegments() const;

		/* Resize
		   Summary: Resizes the collidable rectangle. 
		   Parameters:
		      newWidth/newHeight: The new dimensions for the collidable rectangle. */
		void Resize( double newWidth, double newHeight );

		/* UpdatePosition
		   Summary: Updates the position of this CollidableArea given a reference point (ex: the center of the area). 
		   Parameters:
		      referencePoint: The reference point indicating how to update the position of the CollidableArea. */
		virtual void UpdatePosition( const Point2D &referencePoint );

		/* GetRect 
		   Summary: Returns the underlying rectangle object for this CollidableRectangle. 
		   Returns: The underlying rectangle object for this CollidableRectangle. */
		const Rectangle2D& GetRect() const;

	private:
		Rectangle2D _rect;

		friend class CollidableLineSegment;
		friend class CollidableCircle;
		friend std::ostream& operator<<( std::ostream &outputStream, const CollidableRectangle &collidableRect );
	};

	/* Output operator for a CollidableRectangle object. */
	std::ostream& operator<<( std::ostream &outputStream, const CollidableRectangle &collidableRect );
}