#pragma once

/* Defines the functionality of an exit in a map. */

#include "CollidableGameObject.h"
#include "CollidableRectangle.h"
#include "GameObject.h"
#include "Point2D.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class MapExit : public VisibleGameObject, public CollidableGameObject
	{
		// Constructors
	public:
		MapExit( GameObject::ObjectId id, Point2D position );

		// Virtual methods
	public:
		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea();

		// Data
	private:
		CollidableRectangle mCollidableArea;

		// Static data
	private:
		static const int mMapExitWidth = 100; // in pixels
		static const int mMapExitHeight = 100; // in pixels
		static const std::string mMapExitImage;
		static const int mMapExitImageWidth = 100; // in pixels
		static const int mMapExitImageHeight = 120; // in pixels
	};

}