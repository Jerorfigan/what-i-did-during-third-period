#pragma once

/* GameAssetManager defines the functionality for accessing all the assets used by 
   this game. Game assets are entities coupled to the game that need global visibility. */

#include "Timer.h"
#include "IGraphicsProvider.h"
#include "GameEventQueue.h"

namespace PROJECT_NAME
{

	class GameAssetManager
	{
	public:
		/* GetAppWindow 
		   Summary: Returns the id of the application window. 
		   Returns: The id of the application window. */
		IGraphicsProvider::ResourceId GetAppWindow();
		/* SetAppWindow 
		   Summary: Sets the id of the application window. 
		   Parameters: 
		      id: The id of the application window. */
		void SetAppWindow( IGraphicsProvider::ResourceId id );

		/* GetGameEventQueue 
		   Summary: Gets the game event queue. 
		   Returns: The game event queue. */
		GameEventQueue& GetGameEventQueue();

	private:
		IGraphicsProvider::ResourceId _appWindowId;
		GameEventQueue _gameEventQueue;
	};

}