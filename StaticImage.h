#pragma once

/* Defines the functionality of a static image. */

#include "IGraphicsProvider.h"
#include "UserControl.h"

namespace PROJECT_NAME
{

	class StaticImage : public UserControl
	{
		// Constructors
	public:
		StaticImage( std::string imageFile, Point2D position );

		// Virtual methods
	public:
		virtual void Init( void* menuData );

		virtual void Draw( void* menuData );

		virtual void OnHoverOver( void* menuData );
		virtual void OnHoverAway( void* menuData );
		virtual void OnActivation( void* menuData );
		virtual void OnDeactivation( void* menuData );
		virtual void OnGainedFocus( void* menuData );
		virtual void OnLostFocus( void* menuData );
		virtual void OnKeyboardChar( void* menuData, char typedChar );

		virtual bool IsTargettedByMouse( Point2D mouseHotspot );
	};

}