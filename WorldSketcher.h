#pragma once

/* WorldSketcher maintains a queue of ElementSketchers. Each ElementSketcher is tied to an object
   and is capable of drawing that object in visibly incremental steps so as to simulate a hand sketch.
   The WorldSketcher::SketchWorld function iterates through the queue and tells each ElementSketcher
   to sketch itself. */

#include <queue>
#include <vector>

#include "ElementSketcher.h"
#include "Handle.h"
#include "Handle.cpp"
#include "Point2D.h"

namespace PROJECT_NAME
{

	class WorldSketcher
	{
	public:
		/* Initializes a world sketcher with an empty _elementSketchers queue. */
		WorldSketcher();

		/* AddElementSketcher
		   Summary: Adds an ElementSketcher to the queue. 
		   Parameters:
		      elementSketcher: The handle to the ElementSketcher to add. */
		void AddElementSketcher( Handle< ElementSketcher > elementSketcherHandle );

		/* SketchWorld 
		   Summary: Iterates through the queue and tells each ElementSketcher to sketch itself. Once
		            an ElementSketcher has signalled that it is done sketching its element, that ElementSketcher
					is popped from the queue. The function terminates once all ElementSketchers have successfully
					sketched their elements. */
		void SketchWorld();

	private:
		/* Iterates through the queue and tells each ElementSketcher to init itself. */
		void InitWorldSketcher() const; 
		/* Draws each object in the finishedSketches vector. */
		void DrawFinishedSketches( const std::vector< GameObject::ObjectId > &finishedSketches );
		/* Draws the background object stored in the GameObjectManager. */
		void DrawBackground() const;
		/* Draws the pencil object at the specified position. */
		void DrawPencil( Handle< Pencil > pencilHandle, const Point2D &pencilPos ) const;

		std::queue< Handle< ElementSketcher > > _elementSketchers;
	};

}