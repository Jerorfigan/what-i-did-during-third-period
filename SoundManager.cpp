/* SoundManager implementation. */

#include "stdafx.h"

#include <fstream>
#include <sstream>

#include "Game.h"
#include "SoundManager.h"

namespace PROJECT_NAME
{

		// Constructors

		SoundManager::SoundManager( std::string soundFile )
		{
			std::fstream soundFileStream( soundFile.c_str() );
			std::string lineBuffer;

			while( std::getline( soundFileStream, lineBuffer ) )
			{
				// Skip blank lines and comments in sound file.
				if( lineBuffer.empty() || lineBuffer[0] == '#' )
					continue;

				std::stringstream lineStream( lineBuffer );
				std::string soundName;
				std::string filePath;

				// First token is sound name.
				lineStream >> soundName;
				// Second token is file path.
				lineStream >> filePath;

				SoundData data;
				data.mId = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( filePath ) );
				data.mPlayNow = false;

				mSoundDataList.insert( std::make_pair( soundName, data ) );
			}

		}

		// Methods
	
		void SoundManager::QueueToPlay( std::string soundName, float volume, bool loop, bool forceReplay, bool truncate )
		{
			// Verify that this is a valid sound.
			SoundDataList::const_iterator findResult = mSoundDataList.find( soundName );
			assert( findResult != mSoundDataList.end() );
			if( findResult == mSoundDataList.end() )
				return;

			// Add the sound to the play queue if it's not already there.
			bool inQueue = false;
			for( SoundNameList::const_iterator playQueueItr = mPlayQueue.begin();
				 playQueueItr != mPlayQueue.end(); ++playQueueItr )
			{
				if( *playQueueItr == soundName )
				{
					inQueue = true;
					break;
				}
			}

			if( !inQueue )
			{
				mPlayQueue.push_back( soundName );

				mSoundDataList[ soundName ].mLoop = loop;
				mSoundDataList[ soundName ].mPlayNow = forceReplay;
				mSoundDataList[ soundName ].mTruncate = truncate;
				mSoundDataList[ soundName ].mVolume = volume;
			}
		}

		void SoundManager::Update()
		{
			// We're going to turn the play queue into the new play list and clear the play queue, but before
			// doing so, perform the following processing:
			// 1) If a sound is in the play list but not in the play queue, stop playing that
			// sound if mTruncate is true or the sound is looping.
			// 2) If a sound is in the play queue but not in the play list, set the mPlayNow data member for
			// the sound to TRUE.

			for( SoundNameList::const_iterator playListItr = mPlayList.begin();
				 playListItr != mPlayList.end(); ++playListItr )
			{
				if( !IsInSoundList( mPlayQueue, *playListItr ) && 
					( mSoundDataList[ *playListItr ].mTruncate || 
					  Game::GetGameServiceManager().GetAudioProvider()->IsSoundLooping( mSoundDataList[ *playListItr ].mId ) ) )
				{
					Game::GetGameServiceManager().GetAudioProvider()->StopSound( mSoundDataList[ *playListItr ].mId );
				}
			}

			for( SoundNameList::const_iterator playQueueItr = mPlayQueue.begin();
				 playQueueItr != mPlayQueue.end(); ++playQueueItr )
			{
				if( !IsInSoundList( mPlayList, *playQueueItr ) )
				{
					mSoundDataList[ *playQueueItr ].mPlayNow = true;
				}
			}

			// Make the play queue the new play list.
			mPlayList = mPlayQueue;

			// Play all sounds in the play list that have their mPlayNow data member set to true if they're not
			// currently playing. After playing, set mPlayNow to false.
			for( SoundNameList::const_iterator playListItr = mPlayList.begin();
				 playListItr != mPlayList.end(); ++playListItr )
			{
				if( mSoundDataList[ *playListItr ].mPlayNow &&
					!Game::GetGameServiceManager().GetAudioProvider()->IsSoundPlaying( mSoundDataList[ *playListItr ].mId ) )
				{
					Game::GetGameServiceManager().GetAudioProvider()->PlaySound( mSoundDataList[ *playListItr ].mId, mSoundDataList[ *playListItr ].mVolume,
						mSoundDataList[ *playListItr ].mLoop );
					mSoundDataList[ *playListItr ].mPlayNow = false;
				}
			}

			// Clear the play queue.
			mPlayQueue.clear();
		}

		bool SoundManager::IsInSoundList( const SoundNameList &list, SoundNameList::value_type itemToLookFor )
		{
			for( SoundNameList::const_iterator soundNameListItr = list.begin();
				 soundNameListItr != list.end(); ++soundNameListItr )
			{
				if( *soundNameListItr == itemToLookFor )
				{
					return true;
				}
			}

			return false;
		}
}