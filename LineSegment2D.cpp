/* Implements the functionality for a line segment in 2D. */

#include "stdafx.h"

#include <cmath>

#include "LineSegment2D.h"

namespace PROJECT_NAME
{

	/* Default constructor initializes a line segment with start and end set to (0,0). */
	LineSegment2D::LineSegment2D() : _start( Point2D() ), _end( Point2D() )
	{

	}

	/* Constructor that initializes a line segment from passed in points. The start point 
	   will be the left-most point or in a vertical line will be the lowest point. */
	LineSegment2D::LineSegment2D( Point2D point1, Point2D point2 )
	{
		if( point1._x < point2._x || EqualDouble( point1._x, point2._x ) && point1._y < point2._y )
		{
			_start = point1;
			_end = point2;
		}
		else
		{
			_start = point2;
			_end = point1;
		}
		_line = Line2D( _start, _end );
	}

	/* Length 
	   Summary: Returns the length of this LineSegment. 
	   Returns: The the length of this LineSegment. */
	double LineSegment2D::Length() const
	{
		return sqrt( pow( _start._x - _end._x, 2.0 ) + 
			         pow( _start._y - _end._y, 2.0 ) );
	}

	/* Intersects
	   Summary: Determines whether this line segment intersects lineSeg. 
	   Parameters: 
		  lineSeg: The LineSegment2D object to check for intersection with.
	   Returns: True if this line segment intersects lineSeg, false otherwise. */
	bool LineSegment2D::Intersects( const LineSegment2D &lineSeg ) const
	{
		Point2D startPoint, endPoint;
		return Intersects( lineSeg, startPoint, endPoint );
	}

	/* Intersects
	   Summary: Determines whether this line segment intersects lineSeg and returns the 
	            intersection point or line: if there is only 1 intersection point, that 
				point is returned in both intersectionStart and intersectionEnd. If the 
				line segments overlap, intersectionStart will contain the start point of 
				that overlap line and intersectionEnd the end point.
	   Parameters: 
		  lineSeg: The LineSegment2D object to check for intersection with.
		  intersectionStart: Passed out with the intersection point or start point of 
		                     intersection line.
		  intersectionEnd: Passed out with the intersection point or end point of 
		                   intersection line. 
	   Returns: True if this line segment intersects lineSeg, false otherwise. */
	bool LineSegment2D::Intersects( const LineSegment2D &lineSeg, Point2D &intersectionStart,
			Point2D &intersectionEnd ) const
	{
		// Line segment intersection occurs when 1) the lines that comprise those line segments
		// intersect and both line segments contain the intersection point or 2) the line segments 
		// are a part of the same line and they overlap. 

		bool intersects = false;
		Point2D intersectionPoint;

		if( _line == lineSeg._line )
		{
			/* Line segments are part of same line, need to test to see if they overlap. */

			bool thisLineInLineSeg = false;
			bool startOfThisLineInLineSeg = false;
			bool endOfThisLineInLineSeg = false;
			bool lineSegInThisLine = false;

			if( _line.FiniteSlope() )
			{
				/* Lines have finite slope, so test if their x-value ranges overlap. */
				thisLineInLineSeg = 
					( _start._x >= lineSeg._start._x && _end._x <= lineSeg._end._x );
				startOfThisLineInLineSeg = 
					( _start._x >= lineSeg._start._x && _start._x <= lineSeg._end._x && _end._x > lineSeg._end._x );
				endOfThisLineInLineSeg = 
					( _end._x >= lineSeg._start._x && _end._x <= lineSeg._end._x && _start._x < lineSeg._start._x );
				lineSegInThisLine = 
					( lineSeg._start._x >= _start._x && lineSeg._end._x <= _end._x );
				
				if( thisLineInLineSeg ||
			        startOfThisLineInLineSeg ||
					endOfThisLineInLineSeg ||
					lineSegInThisLine )
				{
					intersects = true;
				}
			}
			else
			{
				/* Lines have infinite slope, so test if their y-value ranges overlap. */
				thisLineInLineSeg = 
					( _start._y >= lineSeg._start._y && _end._y <= lineSeg._end._y );
				startOfThisLineInLineSeg = 
					( _start._y >= lineSeg._start._y && _start._y <= lineSeg._end._y && _end._y > lineSeg._end._y );
				endOfThisLineInLineSeg = 
					( _end._y >= lineSeg._start._y && _end._y <= lineSeg._end._y && _start._y < lineSeg._start._y );
				lineSegInThisLine = 
					( lineSeg._start._y >= _start._y && lineSeg._end._y <= _end._y );

				if( thisLineInLineSeg ||
			        startOfThisLineInLineSeg ||
					endOfThisLineInLineSeg ||
					lineSegInThisLine )
				{
					intersects = true;
				}
			}

			if( intersects )
			{
				if( thisLineInLineSeg )
				{
					intersectionStart = _start;
					intersectionEnd = _end;
				}
				else if( startOfThisLineInLineSeg )
				{
					intersectionStart = _start;
					intersectionEnd = lineSeg._end;
				}
				else if( endOfThisLineInLineSeg )
				{
					intersectionStart = lineSeg._start;
					intersectionEnd = _end;
				}
				else
				{
					// lineSeg contained by this line
					intersectionStart = lineSeg._start;
					intersectionEnd = lineSeg._end;
				}
			}
		}
		else if( _line.Intersects( lineSeg._line, intersectionPoint ) )
		{
			/* Lines intersect, so need to test if both line segments contain intersection point. */

			if( Contains( intersectionPoint ) && lineSeg.Contains( intersectionPoint ) )
			{
				intersects = true;
				intersectionStart = intersectionPoint;
				intersectionEnd = intersectionPoint;
			}
		}

		return intersects;
	}

	/* Contains
	   Summary: Determines whether this line segment contains point. 
	   Parameters: 
		  point: The Point2D object to test for containment.
	   Returns: True if this line segment contains point, false otherwise. */
	bool LineSegment2D::Contains( const Point2D &point ) const
	{
		// First determine if the line that comprises this line segment contains the point. 
		// If it doesn't, we can conclude that this line segment does not contain the point. 
		// If it does, then, if the line is non-vertical, test to see if the x-coordinate of 
		// the point falls within the x-value range of the line segment--if it does, the line 
		// segment contains the point. If the line is vertical, test to see if the y-coordinate 
		// of the point falls within the y-value range of the line segment--if it does, the 
		// line segment contains the point.

		bool contains = false;

		if( _line.Contains( point ) )
		{
			if( _line.FiniteSlope() )
			{
				if( point._x >= _start._x && point._x <= _end._x )
				{
					contains = true;
				}
			}
			else
			{
				if( point._y >= _start._y && point._y <= _end._y )
				{
					contains = true;
				}
			}
		}

		return contains;
	}

	/* Start
	   Summary: Returns the starting point of this line. 
	   Returns: The starting point of this line. */
	const Point2D& LineSegment2D::Start() const
	{
		return _start;
	}

	/* End
	   Summary: Returns the ending point of this line. 
	   Returns: The ending point of this line. */
	const Point2D& LineSegment2D::End() const
	{
		return _end;
	}

}