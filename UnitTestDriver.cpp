/* Starting point for all unit tests. */

#include "stdafx.h"

#include "UnitTests.h"

namespace PROJECT_NAME_UnitTests
{

	void UnitTestDriver()
	{
		Line2DUnitTest();
		LineSegment2DUnitTest();
		Point2DUnitTest();
		Rectangle2DUnitTest();
		Vector2DUnitTest();
		GlobalTemplateFunctionsTest();

		//GraphicsInterfaceTest();
	}

}