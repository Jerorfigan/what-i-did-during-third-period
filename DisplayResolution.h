#pragma once

/* DisplayResolution defines the functionality of a display's resolution. */

#include <string>

#include "IGraphicsProvider.h"

namespace PROJECT_NAME
{

	struct IGraphicsProvider::DisplayResolution
	{
		/* Initializes a DisplayResolution from a width and height. */
		DisplayResolution( std::size_t width = 0, std::size_t height = 0 );

		/* GetInfoString
		   Summary: Returns the display resolution information as a std::string. 
		   Returns: The display resolution information as a std::string. */
		std::string GetInfoString() const;

		/* Equality operators */
		bool operator==( const DisplayResolution &rhs );
		bool operator!=( const DisplayResolution &rhs );

		std::size_t _width; // in pixels
		std::size_t _height; // in pixels
	};

}