/* SpriteAnimator implementation. */

#include "stdafx.h"

#include <sstream>

#include "Game.h"
#include "SpriteAnimator.h"

namespace PROJECT_NAME
{

	/* Initializes a SpriteAnimator from an animation file, a graphics resource and an
	   animation point (the point to center the animation frames on). */
	SpriteAnimator::SpriteAnimator( std::string animationFile, IGraphicsProvider::ResourceId graphicsResource, 
		const Point2D &animationPoint ) : _graphicsResource( graphicsResource ), _animationPoint( animationPoint ), 
		_currentFrame( 0 ), _isLoopingAnimation( false ), _currentFrameDuration( 0 ), _frameRotationAngle( 0 ), 
		_frameSubRect( 0 )
	{
		// Read animation data from animation file
		std::ifstream animationFileStream( animationFile.c_str() );
		assert( animationFileStream );

		AnimationData animationData;
		while( ReadAnimationDataFromFile( animationFileStream, animationData ) )
		{
			if( !animationData._animationName.empty() )
			{
				// Make sure animation name has not been encountered already
				Animations::iterator findResult = _animations.find( animationData._animationName );
				assert( findResult == _animations.end() );

				_animations[ animationData._animationName ] = animationData;
			}
		}
	}

	/* PlayAnimation 
	   Summary: Plays the specified animation. Can optionally loop the animation (default is true), 
	            rotate each frame (default is no rotation), and use a subrectangle to clip off part 
				of each frame (default is no clipping). 
	   Parameters:
	      animationName: The name of the animation to play. 
		  loop: True/false flag indicating whether to loop the animation (default is true). 
		  frameRotationAngle: The animation frames are rotated by this many degrees (passed as a reference, 
			                  so that it may be changed while the animation is playing). 
		  frameSubRect: The animation frames are clipped by this subrectangle. Must fit within the rectangle
			            given by the frame width & height. (passed as a reference, so that it may be 
						changed while the animation is playing). */
	void SpriteAnimator::PlayAnimation( std::string animationName, bool loop, double *frameRotationAngle, 
		Rectangle2D *frameSubRect )
	{
		PlayAnimation( animationName, 1, loop, frameRotationAngle, frameSubRect );
	}

	/* PlayAnimation 
		Summary: Plays the specified animation from the specified starting frame. Can optionally 
				loop the animation (default is true), rotate each frame (default is no rotation), 
				and use a subrectangle to clip off part of each frame (default is no clipping). 
		Parameters:
		    animationName: The name of the animation to play. 
			startingFrame: The frame to start playing the animation from.
			loop: True/false flag indicating whether to loop the animation (default is true). 
			frameRotationAngle: The animation frames are rotated by this many degrees (passed as a reference, 
			                    so that it may be changed while the animation is playing). 
			frameSubRect: The animation frames are clipped by this subrectangle. Must fit within the rectangle
			            given by the frame width & height. (passed as a reference, so that it may be 
						changed while the animation is playing). */
	void SpriteAnimator::PlayAnimation( std::string animationName, std::size_t startingFrame, bool loop,
		double *frameRotationAngle, Rectangle2D *frameSubRect )
	{
		// Verify that the animation name is valid
		Animations::iterator findResult = _animations.find( animationName );
		assert( findResult != _animations.end() );

		if( _currentAnimation != animationName )
		{
			_currentAnimation = animationName;
			_currentFrame = startingFrame;
			_currentFrameDuration = 1.0 / _animations[ _currentAnimation ]._targetFrameRate;
			_isLoopingAnimation = loop;
			_frameRotationAngle = frameRotationAngle;
			_frameSubRect = frameSubRect;

			// If a clipping rectangle was passed, verify that it fits inside the frame rectangle.
			if( _frameSubRect )
			{
				assert( _frameSubRect->Width() <= _animations[ _currentAnimation ]._frameWidth );
				assert( _frameSubRect->Height() <= _animations[ _currentAnimation ]._frameHeight );
			}

			Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( _graphicsResource, 
				_animations[ _currentAnimation ]._animationArtFile );

			Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteSubRectangle( _graphicsResource, 
				ConstructSubrectangle( _animations[ _currentAnimation ]._frameWidth, 
				_animations[ _currentAnimation ]._frameHeight, _currentFrame ) );

			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( _graphicsResource, 
				Point2D( _animations[ _currentAnimation ]._frameWidth / 2.0 , 
				_animations[ _currentAnimation ]._frameHeight / 2.0 ) + _animations[ _currentAnimation ]._frameCenterOffset );

			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( _graphicsResource, 
				_animationPoint );

			if( _frameRotationAngle )
			{
				Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableRotation( _graphicsResource, 
					*_frameRotationAngle );
			}
		}
	}

	/* Update
		Summary: Updates the currently playing animation by advancing it to the next frame if its
		        time to. 
		Parameters: 
		    elapsedTime: The elapsed time in seconds since the last update. */
	void SpriteAnimator::Update( double elapsedTime )
	{
		assert( !_currentAnimation.empty() );

		_currentFrameDuration -= elapsedTime;
		if( _currentFrameDuration < 0 )
		{
			_currentFrameDuration = 1.0 / _animations[ _currentAnimation ]._targetFrameRate;

			if( _currentFrame == _animations[ _currentAnimation ]._numFrames )
			{
				if( _isLoopingAnimation )
				{
					_currentFrame = 1;
				}
			}
			else
			{
				++_currentFrame;
			}

			Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteSubRectangle( _graphicsResource, 
				ConstructSubrectangle( _animations[ _currentAnimation ]._frameWidth, 
				_animations[ _currentAnimation ]._frameHeight, _currentFrame ) );
		}

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( _graphicsResource, 
			_animationPoint );

		if( _frameRotationAngle )
		{
			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableRotation( _graphicsResource, 
				*_frameRotationAngle );
		}
	}

	std::string SpriteAnimator::GetCurrentAnimation()
	{
		return _currentAnimation;
	}
	
	std::size_t SpriteAnimator::GetCurrentFrame()
	{
		return _currentFrame;
	}

	/* Constructs a subrectangle surrounding the specified frame. */
	Rectangle2D SpriteAnimator::ConstructSubrectangle( std::size_t frameWidth, std::size_t frameHeight, 
		std::size_t frameNum ) const
	{
		if( _frameSubRect )
		{
			return Rectangle2D( 
				Point2D( frameWidth * ( frameNum - 1 ) + _frameSubRect->Left(), 
				         _frameSubRect->Top() ), 
				Point2D( frameWidth * ( frameNum - 1 ) + _frameSubRect->Right(), 
				         _frameSubRect->Top() ), 
				Point2D( frameWidth * ( frameNum - 1 ) + _frameSubRect->Left(), 
						 _frameSubRect->Bottom() ), 
				Point2D( frameWidth * ( frameNum - 1 ) + _frameSubRect->Right(), 
						 _frameSubRect->Bottom() ) );
		}
		else
		{
			return Rectangle2D( Point2D( frameWidth * ( frameNum - 1 ) + frameWidth / 2, frameHeight / 2 ), 
				frameWidth, frameHeight );
		}
	}

	/* Reads animation data from a file stream. Returns true if animation data was read. */
	bool SpriteAnimator::ReadAnimationDataFromFile( std::ifstream &animationFileStream, 
		AnimationData &animationData ) const
	{
		bool dataRead = false;
		std::string lineBuffer;
		if( std::getline( animationFileStream, lineBuffer ) )
		{
			if( !lineBuffer.empty() && lineBuffer[0] != '#' && lineBuffer[0] != ' ' )
			{
				std::stringstream tokenReader( lineBuffer );
				assert( tokenReader );

				std::string junk;
				tokenReader >> animationData._animationName >> animationData._animationArtFile >> animationData._frameWidth 
					>> animationData._frameHeight >> animationData._numFrames >> animationData._targetFrameRate >> junk 
					>> animationData._frameCenterOffset._x >> junk >> animationData._frameCenterOffset._y >> junk;
				animationData._animationArtFile = RESOURCE_PATH( animationData._animationArtFile );
				assert( tokenReader );
			}
			else
			{
				animationData._animationName = "";
			}
			dataRead = true;
		}
		return dataRead;
	}

}