/* WorldSketcher implementation. */

#include "stdafx.h"

#include <vector>

#include "Game.h"
#include "Handle.h"
#include "Handle.cpp"
#include "MapLine.h"
#include "MapLineSketcher.h"
#include "Pencil.h"
#include "Point2D.h"
#include "Rectangle2D.h"
#include "Timer.h"
#include "WorldSketcher.h"

namespace PROJECT_NAME
{

	/* Initializes a world sketcher with an empty _elementSketchers queue. */
	WorldSketcher::WorldSketcher()
	{
		// Iterate through all the MapLine objects and create MapLineSketchers out of them and add the 
		// MapLineSketchers to the WorldSketcher
		typedef std::vector< Handle< MapLine > > MapLines;

		MapLines mapLines = Game::GetGameObjectManager().GetGameObjectsOfType( MapLine() );

		// Sort the mapLines by render order
		std::sort( mapLines.begin(), mapLines.end(), VisibleGameObject::CompareHandles );

		for( MapLines::iterator mapLineItr = mapLines.begin(); 
				mapLineItr != mapLines.end(); ++mapLineItr )
		{
			// Only sketch visible map lines. Four map lines will be created on the boundaries of the map that won't
			// be visible.
			if( (*mapLineItr)->IsVisible() )
			{
				AddElementSketcher( Handle< MapLineSketcher >( new MapLineSketcher( *(*mapLineItr) ) ) );
			}
		}
	}

	/* AddElementSketcher
	   Summary: Adds an ElementSketcher to the queue. 
	   Parameters:
	      elementSketcher: The handle to the ElementSketcher to add. */
	void WorldSketcher::AddElementSketcher( Handle< ElementSketcher > elementSketcherHandle )
	{
		_elementSketchers.push( elementSketcherHandle );
	}

	/* SketchWorld 
	   Summary: Iterates through the queue and tells each ElementSketcher to sketch itself. Once
	            an ElementSketcher has signalled that it is done sketching its element, that ElementSketcher
				is popped from the queue. The function terminates once all ElementSketchers have successfully
				sketched their elements. */
	void WorldSketcher::SketchWorld()
	{
		InitWorldSketcher();

		/* Show the background for a second before we sketch anything. */
		Game::GetGameServiceManager().GetGraphicsProvider()->PrepareWindowForDrawing( Game::GetGameAssetManager().GetAppWindow() );
		DrawBackground();
		Game::GetGameServiceManager().GetGraphicsProvider()->DisplayWindow( Game::GetGameAssetManager().GetAppWindow() );
		Timer::Sleep( 1 );

		Handle< Pencil > pencilHandle = Game::GetGameObjectManager().GetGameObject( "pencil" );
		assert( pencilHandle.Valid() );

		std::vector< GameObject::ObjectId > finishedSketches;

		while( !_elementSketchers.empty() )
		{
			Handle< ElementSketcher > nextSketcher = _elementSketchers.front();

			pencilHandle->PlaySketchSound( nextSketcher->GetSketchDuration() );

			while( !nextSketcher->IsSketchFinished() )
			{
				Game::GetGameServiceManager().GetGraphicsProvider()->PrepareWindowForDrawing( Game::GetGameAssetManager().GetAppWindow() );

				DrawBackground();
				DrawFinishedSketches( finishedSketches );

				nextSketcher->Sketch();

				DrawPencil( pencilHandle, nextSketcher->GetSketchPoint() );

				Game::GetGameServiceManager().GetGraphicsProvider()->DisplayWindow( Game::GetGameAssetManager().GetAppWindow() );
			}

			finishedSketches.push_back( nextSketcher->GetObjectId() );
			_elementSketchers.pop();

			pencilHandle->CycleImage();
		}

		// Set pencil to invisible now that we're done sketching.
		pencilHandle->SetVisibility( false );
	}

	/* Iterates through the queue and tells each ElementSketcher to init itself. */
	void WorldSketcher::InitWorldSketcher() const
	{
		/* Throw the ElementSketcher handles into a vector, iterate through them, and call InitializeSketcher on
		   each one. */
		typedef std::vector< Handle< ElementSketcher > > ElementSketchers;

		ElementSketchers elementSketchers( _elementSketchers._Get_container().begin(), 
			_elementSketchers._Get_container().end() );

		for( ElementSketchers::iterator elementSketcherItr = elementSketchers.begin();
			 elementSketcherItr != elementSketchers.end(); ++elementSketcherItr )
		{
			(*elementSketcherItr)->InitializeSketch();
		}
	}

	/* Draws each object in the finishedSketches vector. */
	void WorldSketcher::DrawFinishedSketches( const std::vector< GameObject::ObjectId > &finishedSketches )
	{
		for( std::vector< GameObject::ObjectId >::const_iterator objectIdItr = finishedSketches.begin();
			 objectIdItr != finishedSketches.end(); ++objectIdItr )
		{
			Handle< VisibleGameObject > visibleGameObjectHandle = Game::GetGameObjectManager().GetGameObject( *objectIdItr );
			assert( visibleGameObjectHandle.Valid() );
			Rectangle2D rect = Game::GetGameServiceManager().GetGraphicsProvider()->GetSpriteSubRectangle( visibleGameObjectHandle->GraphicsId() );
			Point2D pos = Game::GetGameServiceManager().GetGraphicsProvider()->GetDrawablePosition( visibleGameObjectHandle->GraphicsId() );
			visibleGameObjectHandle->Draw( 0 );
		}
	}

	/* Draws the background object stored in the GameObjectManager. */
	void WorldSketcher::DrawBackground() const
	{
		Handle< VisibleGameObject > backgroundHandle = Game::GetGameObjectManager().GetGameObject( "background" );
		assert( backgroundHandle.Valid() );
		
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( backgroundHandle->GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	/* Draws the pencil object at the specified position. */
	void WorldSketcher::DrawPencil( Handle< Pencil > pencilHandle, const Point2D &pencilPos ) const
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( pencilHandle->GraphicsId(),
			pencilPos );
		pencilHandle->Draw( 0 );
	}

}