/* CollidableCircle implementation. */

#include "stdafx.h"

#include "CollidableCircle.h"
#include "CollidableLineSegment.h"
#include "CollidableRectangle.h"

namespace PROJECT_NAME
{

	/* Initializes a CollidableCircle from a Circle2D. */
	CollidableCircle::CollidableCircle( Circle2D circle ) : _circle( circle )
	{
		_minX = _circle.Center()._x - _circle.Radius();
		_maxX = _circle.Center()._x + _circle.Radius();
		_minY = _circle.Center()._y - _circle.Radius();
		_maxY = _circle.Center()._y + _circle.Radius();
	}

	/* CollidesWith 
		Summary: Returns true if this CollidableArea overlaps otherArea. 
		Returns: True if this CollidableArea overlaps otherArea. */
	bool CollidableCircle::CollidesWith( const CollidableArea* otherArea ) const
	{
		bool collisionFound = false;

		/* otherArea is one of the following:
		   CollidableRectangle
		   CollidableLineSegment
		   CollidableCircle

		   Determine which and then call the appropriate method to test for 
		   intersection of geometry. */
		
		if( const CollidableRectangle *collidableRectPtr = dynamic_cast< const CollidableRectangle* >( otherArea ) )
		{
			collisionFound = _circle.Intersects( collidableRectPtr->_rect );
		}
		else if( const CollidableLineSegment *collidableLineSegPtr = dynamic_cast< const CollidableLineSegment* >( otherArea ) ) 
		{
			collisionFound = _circle.Intersects( collidableLineSegPtr->_lineSeg );
		}
		else if( const CollidableCircle *collidableCirclePtr = dynamic_cast< const CollidableCircle* >( otherArea ) ) 
		{
			collisionFound = _circle.Intersects( collidableCirclePtr->_circle );
		}
		else
		{
			// shouldn't get here...blow up!
			assert( 1 == 0 );
		}

		return collisionFound;
	}

	/* GetLineSegments 
		Summary: Returns a vector filled with the line segments that comprise this CollidableArea. 
		Returns: A vector filled with the line segments that comprise this CollidableArea. */
	std::vector< LineSegment2D > CollidableCircle::GetLineSegments() const
	{
		// Return an empty vector, since a circle contains no line segments.
		return std::vector< LineSegment2D >();
	}

	/* UpdatePosition
		Summary: Updates the position of this CollidableArea given a reference point (ex: the center of the area). 
		Parameters:
			referencePoint: The reference point indicating how to update the position of the CollidableArea. */
	void CollidableCircle::UpdatePosition( const Point2D &referencePoint )
	{
		_circle = Circle2D( referencePoint, _circle.Radius() );

		_minX = _circle.Center()._x - _circle.Radius();
		_maxX = _circle.Center()._x + _circle.Radius();
		_minY = _circle.Center()._y - _circle.Radius();
		_maxY = _circle.Center()._y + _circle.Radius();
	}

	/* Output operator for a CollidableCircle object. */
	std::ostream& operator<<( std::ostream &outputStream, const CollidableCircle &collidableCircle )
	{
		outputStream << "minX = " << collidableCircle._minX << std::endl 
			<< "maxX = " << collidableCircle._maxX << std::endl
			<< "minY = " << collidableCircle._minY << std::endl
			<< "maxY = " << collidableCircle._maxY << std::endl
			<< "center: " << collidableCircle._circle.Center() << std::endl
			<< "radius: " << collidableCircle._circle.Radius();

		return outputStream;
	}

}