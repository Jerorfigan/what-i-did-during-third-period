// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here

/* C++ Standard library headers */
#include <cassert>
#include <cstddef>
#include <cstdlib>

/* "What I Did During Third Period" specific headers */ 
#include "GeneralDefines.h"
#include "GlobalUtilityFunctions.h"