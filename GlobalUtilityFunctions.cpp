
#include "stdafx.h"

#include <cmath>

namespace PROJECT_NAME
{

	/* Radians
	   Summary: Converts a number in degrees to radians. 
	   Parameters: 
		  degrees: The number in degrees. 
	   Returns: The number in radians. */
	double Radians( double degrees )
	{
		return ( PI / 180.0 ) * degrees;
	}

	/* Degrees
	   Summary: Converts a number in radians to degrees. 
	   Parameters: 
		  radians: The number in radians. 
	   Returns: The number in degrees. */
	double Degrees( double radians )
	{
		return ( 180.0 / PI ) * radians;
	}

	/* EqualDouble 
	   Summary: Determines whether two doubles are equal barring rounding error in either. 
	   Parameters: 
	      lhs/rhs: The doubles to test for equality barring rounding error. 
	   Returns: True if the doubles are equal barring rounding error. */
	bool EqualDouble( double lhs, double rhs )
	{
		return abs( lhs - rhs ) < 0.001;
	}

	/* Min
	   Summary: Returns the minimum of two doubles. If they're equal, returns 
	            value1. 
	   Parameters:
	      value1/value2: The double values to test.
	   Returns: The minimum of two doubles. If they're equal, returns value1. */
	double Min( double value1, double value2 )
	{
		double returnValue = value1;
		if( value2 < value1 )
		{
			returnValue = value2;
		}
		return returnValue;
	}

	/* IsBetween 
	   Summary: Determines if the given value is contained within the range. 
	   Parameters:
	      value: The value to test. 
		  rangeStart/rangeEnd: The start and end values of the range. 
		  inclusive: Flag indicating whether the range is inclusive or exclusive of its endpoints
		             (default is inclusive). 
	   Returns: True if the given value is contained within the range. */
	bool IsBetween( double value, double rangeStart, double rangeEnd, bool inclusive )
	{
		bool isInRange = false;
		if( inclusive )
		{
			if( EqualDouble( value, rangeStart ) || EqualDouble( value, rangeEnd ) ||
				value > rangeStart && value < rangeEnd )
			{
				isInRange = true;
			}
		} 
		else
		{
			if( value > rangeStart && value < rangeEnd )
			{
				isInRange = true;
			}
		}
		return isInRange;
	}

	/* Random 
	   Summary: Returns a random integer between the lower limit and upper limit. 
	   Parameters:
	      lowerLimit: The lower limit of the range. 
		  upperLimit: The upper limit of the range.
		  inclusive: A flag indicating whether the range is inclusive (default is true). 
	   Returns: A random integer between the lower limit and upper limit. */
	int Random( int lowerLimit, int upperLimit, bool inclusive )
	{
		if( inclusive )
		{
			assert( lowerLimit <= upperLimit );
			return rand() % ( upperLimit - lowerLimit + 1 ) + lowerLimit;
		}
		else
		{
			assert( lowerLimit < upperLimit - 1 );
			return rand() % ( upperLimit - lowerLimit - 1 ) + lowerLimit + 1;
		}
	}

	/* Random 
	   Summary: Returns a random double between the lower limit and upper limit (range is inclusive). 
	   Parameters:
	      lowerLimit: The lower limit of the range.
		  upperLimit: The upper limit of the range.
	   Returns: A random double between the lower limit and upper limit (range is inclusive). */
	double Random( double lowerLimit, double upperLimit )
	{
		// Get a random fraction between 0 and 1. Add a fraction of the difference between
		// upperLimit and lowerLimit to lowerLimit and return that value.
		double fraction = 
			static_cast< double >( rand() ) / static_cast< double >( RAND_MAX );

		return ( upperLimit - lowerLimit ) * fraction + lowerLimit;
	}

	// Escapes backslashes in a filepath
	std::string EscapeFilePath( const std::string &filepathIn )
	{
		std::string escapedString;
		
		for( std::string::const_iterator strItr = filepathIn.begin(); strItr != filepathIn.end(); ++strItr )
		{
			escapedString.push_back( *strItr );
			if( *strItr == '\\' )
			{
				escapedString.push_back( '\\' );
			}
		}

		return escapedString;
	}

}