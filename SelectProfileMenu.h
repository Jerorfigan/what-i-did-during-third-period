#pragma once

/* Defines the functionality of the Select Profile menu. */

#include "Game.h"
#include "Menu.h"
#include "TextBox.h"

namespace PROJECT_NAME
{
	
	class SelectProfileMenu
	{
		// Constructors
	public:
		SelectProfileMenu();

		// Methods
	public:
		/* Process 
		   Summary: Processes the Main menu and returns the next game state. */
		Game::GameState Process();

		// Data
	private:
		struct SelectProfileData
		{
			std::string mName;
		};

		Menu mMenu;
		SelectProfileData mData;

		// Menu event handlers (which are made friends)
		friend void NameTextChanged( void* menuData, TextBox* textBox );
		friend void AcceptButtonClicked( void* menuData );
	};
}