#pragma once

/* Defines the functionality of a user control manipulated via keyboard/mouse. */

#include <string>

#include "IGraphicsProvider.h"
#include "Point2D.h"

namespace PROJECT_NAME
{

	class UserControl
	{
		// Constructors
	public:
		UserControl( std::string id, unsigned int menuIndex, IGraphicsProvider::ResourceId graphicsId, bool canFocus = false );

		// Methods
	public:
		std::string GetId() const;
		unsigned int GetMenuIndex() const;
		bool CanFocus() const;

		// Operators
	public:
		bool operator==( const UserControl& rhs );
		bool operator!=( const UserControl& rhs );

		// Virtual methods
	public:
		~UserControl();

		// Virtual abstract methods
	public:
		virtual void Init( void* menuData ) = 0;

		virtual void Draw( void* menuData ) = 0;

		virtual void OnHoverOver( void* menuData ) = 0;
		virtual void OnHoverAway( void* menuData ) = 0;
		virtual void OnActivation( void* menuData ) = 0;
		virtual void OnDeactivation( void* menuData ) = 0;
		virtual void OnGainedFocus( void* menuData ) = 0;
		virtual void OnLostFocus( void* menuData ) = 0;
		virtual void OnKeyboardChar( void* menuData, char typedChar ) = 0;

		virtual bool IsTargettedByMouse( Point2D mouseHotspot ) = 0;

	protected:
		std::string mId;
		unsigned int mMenuIndex;
		IGraphicsProvider::ResourceId mGraphicsId;
		bool mCanFocus;
	};

}