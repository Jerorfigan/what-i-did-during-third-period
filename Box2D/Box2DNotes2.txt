30 pixel / 1 meter

art:

limb    12 px wide / 5 px tall - feather 1 px
torso   5 px wide / 20 px tall - feather 1 px
head    radius 8 px

physics:

limb    0.4 m wide /  0.16666666666666666666666666666667 m tall
torso   0.16666666666666666666666666666667 m wide / 0.66666666666666666666666666666667 m tall
head    radius = 0.26666666666666666666666666666667 m