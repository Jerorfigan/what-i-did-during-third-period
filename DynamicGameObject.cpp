/* Implementation for DynamicGameObject. */

#include "stdafx.h"

#include "DynamicGameObject.h"

namespace PROJECT_NAME
{

	/* Initializes a DynamicGameObject from a UpdatePriorityLevel and a GameObject::ObjectId. */
	DynamicGameObject::DynamicGameObject( UpdatePriorityLevel updatePriorityLevel, ObjectId objectId ) :
		GameObject( objectId ), _updatePriorityLevel( updatePriorityLevel ) 
	{

	}

	/* UpdatePriorityLevel 
	   Summary: Returns the update priority level of this DynamicGameObject. 
	   Returns: The update priority level of this DynamicGameObject. */
	const DynamicGameObject::UpdatePriorityLevel& DynamicGameObject::GetUpdatePriorityLevel() const
	{
		return _updatePriorityLevel;
	}

}