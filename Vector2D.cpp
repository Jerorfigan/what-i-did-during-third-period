/* Implements the functionality for a 2D vector. */

#include "stdafx.h"

#include <cmath>

#include "Point2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME
{

	/* Default constructor initializes a vector with _x and _y equal to 0. */
	Vector2D::Vector2D() : _x( 0 ), _y( 0 )
	{

	}

	/* Constructor that initializes a vector from passed in x and y values. */
	Vector2D::Vector2D( double x, double y) : _x( x ) , _y( y )
	{

	}

	/* Initializes a vector from a point. */
	Vector2D::Vector2D( const Point2D &point ) : _x( point._x ), _y( point._y )
	{

	}

	/* Initializes a vector from two points: one denoting the vector's origin, 
		   and the other its target. */
	Vector2D::Vector2D( const Point2D &origin, const Point2D &target )
	{
		// Convert both points to vectors and subtract the origin vector from the target vector
		// and initialize this vector with the result. 
		Vector2D result = Vector2D( target ) - Vector2D( origin );
		_x = result._x;
		_y = result._y;
	}

	/* Magnitude
	   Summary: Calculates the magnitude of this vector. 
	   Returns: The magnitude of this vector. */
	double Vector2D::Magnitude() const
	{
		return sqrt( pow( _x, 2.0 ) + pow( _y, 2.0 ) );
	}

	/* UnitVector
	   Summary: Calculates the unit vector for this vector. 
	   Returns: The unit vector for this vector. */
	Vector2D Vector2D::UnitVector() const
	{
		assert( Magnitude() != 0 );
		return ( 1.0 /  Magnitude() ) * ( *this );
	}

	/* Angle
	   Summary: Calculates the angle in degrees of this vector, where 0 degrees corresponds to the 
	   angle of a vector following the positive x-axis. Range of output is [0-360).
	   Returns: The angle of this vector in the range of [0-360). */
	double Vector2D::Angle() const
	{
		double angle = 0.0;

		// We're going to use the Atan function to compute the angle of the vector. However, since the range of Atan 
		// is 90 to -90, we need to adjust angle based on general location and also prevent division by zero if vector 
		// lies on y-axis
		Point2D::PointLocation location = Point2D( _x, _y ).GetPointLocation();

		// Shouldn't be calling this with a <0,0> vec
		assert( location != Point2D::Origin );

		// Handle y-axis cases
		if( location == Point2D::PositiveYAxis ) 
		{
			angle = 90;
		}
		else if( location == Point2D::NegativeYAxis )
		{
			angle = 270;
		}
		else // Not on y-axis
		{
			angle = Degrees( atan( _y / _x ) );
		}

		if( location == Point2D::SecondQuadrant ||
			location == Point2D::NegativeXAxis  || 
			location == Point2D::ThirdQuadrant )
		{
			angle += 180;
		}
		else if( location == Point2D::FourthQuadrant )
		{
			angle += 360;
		}

		return angle;
	}

	/* IsZero
		   Summary: Returns true if this is a zero vector, false otherwise. 
		   Returns: True if this is a zero vector, false otherwise. */
	bool Vector2D::IsZero() const
	{
		return EqualDouble( _x, 0 ) && EqualDouble( _y, 0 );
	}

	/* ProjectOnto
	   Summary: Calculates the vector projection of this vector onto target vector. 
	   Parameters: 
		  target: The vector to project this vector onto.
	   Returns: Returns the vector projection of this vector onto target vector. */
	Vector2D Vector2D::ProjectOnto( const Vector2D &target ) const
	{
		double angleDelta = Angle() - target.Angle();
		double projectionMagnitude = Magnitude() * cos( Radians( angleDelta ) );

		return projectionMagnitude * target.UnitVector();
	}

	/* ReflectOver
	   Summary: Calculates the reflection of this vector over an arbitrary axis.
	   Parameters:
	      axis: A Vector2D denoting the axis for reflection. 
	   Returns: The reflection of this vector over an arbitrary axis. */
	Vector2D Vector2D::ReflectOver( const Vector2D &axis ) const
	{
		// Find the angle between this vector and the axis: if that angle X is positive,
		// rotate this vector by -2 * X. If X is negative, rotate this vector by 2 * X.
		double angleDelta = Angle() - axis.Angle();

		Vector2D reflection( *this );

		reflection.Rotate( -2 * angleDelta );

		return reflection;
	}

	/* Rotate
	   Summary: Rotates the vector about the origin.
	   Parameteres:
	      degrees: The angle of rotation (+ goes counter clockwise, - clockwise ) */
	void Vector2D::Rotate( double degrees )
	{
		double newAngle = Angle() + degrees;

		// Handle boundary cases; need to keep angle between [0-360) 
		while( newAngle >= 360 || newAngle < 0 )
		{
			if( newAngle >= 360 ) 
			{
				newAngle -= 360;
			}
			else if( newAngle < 0 )
			{
				newAngle += 360;
			}
		}

		double magnitude = Magnitude();
		_x = magnitude * cos( Radians( newAngle ) );
		_y = magnitude * sin( Radians( newAngle ) );
	}

	/* operator: += 
	   Summary: Adds a vector to this vector and stores the result in this vector.
	   Parameters: 
	      vector: The vector to add to this vector. 
	   Returns: A reference to this vector. */
	Vector2D& Vector2D::operator+=( const Vector2D &vector )
	{
		*this = *this + vector;
		return *this;
	}

	/* Definition of * operator for left-hand argument of type double and right-hand argument of type
       Vector2D. */
	Vector2D operator*( const double& scalar, const Vector2D& vector )
	{
		return Vector2D( scalar * vector._x, scalar * vector._y );
	}

	/* Definition of + operator for left-hand argument of type Vector2D and right-hand argument of type
	   Vector2D. */
	Vector2D operator+( const Vector2D &lhsVec, const Vector2D &rhsVec )
	{
		return Vector2D( lhsVec._x + rhsVec._x, lhsVec._y + rhsVec._y );
	}

	/* Definition of - operator for left-hand argument of type Vector2D and right-hand argument of type
	   Vector2D. */
	Vector2D operator-( const Vector2D &lhsVec, const Vector2D &rhsVec )
	{
		return Vector2D( lhsVec._x - rhsVec._x, lhsVec._y - rhsVec._y );
	}

	/* Equality operator for two vectors. */
	bool operator==( const Vector2D &lhsVec, const Vector2D &rhsVec )
	{
		return Point2D( lhsVec ) == Point2D( rhsVec );
	}

	/* Inequality operator for two vectors. */
	bool operator!=( const Vector2D &lhsVec, const Vector2D &rhsVec )
	{
		return !( lhsVec == rhsVec );
	}

	/* Output operator for a Vector2D object. */
	std::ostream& operator<<( std::ostream &outputStream, const Vector2D &vector )
	{
		outputStream << "< " << vector._x << " , " << vector._y << " >";

		return outputStream;
	}
}