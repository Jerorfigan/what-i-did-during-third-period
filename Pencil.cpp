/* Pencil implementation. */

#include "stdafx.h"

#include "Game.h"
#include "Pencil.h"
#include "Point2D.h"

namespace PROJECT_NAME
{

	// Constructors

	/* Initializes a Pencil. */
	Pencil::Pencil() : GameObject( "pencil"),
		VisibleGameObject( 
		Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Pencil/PencilWithHand.png" ) ), "pencil" ),
		_currentImageIndex( 1 ), _shortStrokeIndex( 0 ), _longStrokeIndex( 0 )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( 
			GraphicsId(), Point2D( 350, 298 ) );

		_pencilImages.push_back( std::make_pair( RESOURCE_PATH( "Art/Pencil/PencilWithHand.png" ), Point2D( 350, 298 ) ) );
		_pencilImages.push_back( std::make_pair( RESOURCE_PATH( "Art/Pencil/PencilWithHand2.png" ), Point2D( 478, 294 ) ) );
		_pencilImages.push_back( std::make_pair( RESOURCE_PATH( "Art/Pencil/PencilWithHand3.png" ), Point2D( 633, 296 ) ) );

		/* Setup pencil stroke sounds */

		IAudioProvider::AudioId pencilStrokeShort1 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeShort1.wav") );
		IAudioProvider::AudioId pencilStrokeShort2 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeShort2.wav") );
		IAudioProvider::AudioId pencilStrokeShort3 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeShort3.wav") );
		IAudioProvider::AudioId pencilStrokeShort4 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeShort4.wav") );
		IAudioProvider::AudioId pencilStrokeShort5 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeShort5.wav") );

		IAudioProvider::AudioId pencilStrokeLong1 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeLong1.wav") );
		IAudioProvider::AudioId pencilStrokeLong2 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeLong2.wav") );
		IAudioProvider::AudioId pencilStrokeLong3 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeLong3.wav") );
		IAudioProvider::AudioId pencilStrokeLong4 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeLong4.wav") );
		IAudioProvider::AudioId pencilStrokeLong5 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeLong5.wav") );
		IAudioProvider::AudioId pencilStrokeLong6 = Game::GetGameServiceManager().GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Pencil/PencilStrokeLong6.wav") );

		_pencilStrokesShort.push_back( pencilStrokeShort1 );
		_pencilStrokesShort.push_back( pencilStrokeShort2 );
		_pencilStrokesShort.push_back( pencilStrokeShort3 );
		_pencilStrokesShort.push_back( pencilStrokeShort4 );
		_pencilStrokesShort.push_back( pencilStrokeShort5 );

		_pencilStrokesLong.push_back( pencilStrokeLong1 );
		_pencilStrokesLong.push_back( pencilStrokeLong2 );
		_pencilStrokesLong.push_back( pencilStrokeLong3 );
		_pencilStrokesLong.push_back( pencilStrokeLong4 );
		_pencilStrokesLong.push_back( pencilStrokeLong5 );
		_pencilStrokesLong.push_back( pencilStrokeLong6 );
	}	

	// Methods

	/* CycleImage
	   Summary: Cycles the source image used when drawing the pencil. _pencilImages maintains
	            the collection of images to cycle between. */
	void Pencil::CycleImage()
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( 
			GraphicsId(), _pencilImages[ _currentImageIndex ].first );
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( 
			GraphicsId(), _pencilImages[ _currentImageIndex ].second );
		
		if( ++_currentImageIndex == _pencilImages.size() )
		{
			_currentImageIndex = 0;
		}
	}

	/* PlaySketchSound
		Summary: Plays a sketch sound appropriate for a specified sketch duration. */
	void Pencil::PlaySketchSound( double sketchDuration )
	{
		if( sketchDuration < 0.05 )
		{
			// Don't play any sound if sketch duration is super short.
		}
		else if( sketchDuration > 0.05 && sketchDuration < 0.1 )
		{
			/* Play a pencil short stroke sound. */

			// Stop playing previous pencil stroke.
			Game::GetGameServiceManager().GetAudioProvider()->StopSound( _currStrokeSound );

			// Get next short stroke to play and play it.
			_currStrokeSound = _pencilStrokesShort[ _shortStrokeIndex ];

			Game::GetGameServiceManager().GetAudioProvider()->PlaySound( _currStrokeSound );

			_shortStrokeIndex = ++_shortStrokeIndex > _pencilStrokesShort.size() - 1 ? 0 : _shortStrokeIndex;
		}
		else
		{
			/* Play a pencil long stroke sound. */

			// Stop playing previous pencil stroke.
			Game::GetGameServiceManager().GetAudioProvider()->StopSound( _currStrokeSound );

			// Get next long stroke to play and play it.
			_currStrokeSound = _pencilStrokesLong[ _longStrokeIndex ];

			Game::GetGameServiceManager().GetAudioProvider()->PlaySound( _currStrokeSound );

			_longStrokeIndex = ++_longStrokeIndex > _pencilStrokesLong.size() - 1 ? 0 : _longStrokeIndex;
		}
	}

	// Virtual methods

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void Pencil::Draw( double interpolation )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

}