#pragma once

/* GameSaver holds functionality to save/load the game state to/from a file. Game logic that requires this data must 
   obtain it thru a GameSaver object. */

#include <string>

#include "GameSaveData.h"

namespace PROJECT_NAME
{

	class GameSaver
	{
		/* Constructors */
	public:
		GameSaver();

		/* Methods */
	public:
		/* Returns true if saved game loaded successfully. */
		bool LoadSave( std::string saveFile );
		void SaveGame( std::string saveFile ) const; 

		const GameSaveData& GetGameSaveData() const;
		void SetGameSaveData( const GameSaveData &saveData );

		/* Data */
	private:
		GameSaveData mSaveData;
	};

}