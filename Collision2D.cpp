/* Collision2D implementation. */

#include "stdafx.h"

#include <algorithm>
#include <iterator>

#include "Collision2D.h"
#include "KinematicState.h"
#include "Line2D.h"

namespace PROJECT_NAME
{

	/* ResolveCollision 
	   Summary: Resolves a collision between a non-static CollidableGameObject and one or more static 
	            CollidableGameObjects. The kinematic state for the non-static CollidableGameObject will 
				be updated to reflect the number and type of collision(s) encountered. */
	void Collision2D::ResolveCollision( CollidableGameObject *nonStaticObject, 
		std::vector< CollidableGameObject* > staticObjects )
	{
		// Get the most recent displacement vector for the non-static object
		KinematicState kinematicState = nonStaticObject->GetKinematicState();
		KinematicState prevState = kinematicState.GetPreviousPositionAndVelocity();
		Vector2D displacement( prevState.GetPosition(), kinematicState.GetPosition() );

		// Deconstruct the collidable areas of the colliding objects into their constituent line segments. 
		// Reposition the colldiable area of the non-static object around its previous position prior to 
		// the collision.
		typedef std::vector< LineSegment2D > LineSegments;

		nonStaticObject->GetCollidableArea()->UpdatePosition( prevState.GetPosition() );
		LineSegments nonStaticLineSegments = nonStaticObject->GetCollidableArea()->GetLineSegments();

		LineSegments staticLineSegments;
		for( std::vector< CollidableGameObject* >::const_iterator collidableGameObjectItr = staticObjects.begin();
			 collidableGameObjectItr != staticObjects.end(); ++collidableGameObjectItr )
		{
			LineSegments lineSegments = (*collidableGameObjectItr)->GetCollidableArea()->GetLineSegments();
			std::copy( lineSegments.begin(), lineSegments.end(), std::back_inserter( staticLineSegments ) );
		}

		// Test each line segment of the non-static object to see if it collides with any of the line 
		// segments of the static objects after being displaced, and if it does, get the following 
		// information concerning that collision: 1) the distance to the collision following the displacement 
		// vector and 2) the line segment that acts as the collision surface. Only keep track of the collisions 
		// that occur in the least distance along the displacement vector.
		typedef std::vector< CollisionInfo > Collisions;

		Collisions collisions;

		for( LineSegments::const_iterator nonStaticLineSegItr = nonStaticLineSegments.begin();
			 nonStaticLineSegItr != nonStaticLineSegments.end(); ++nonStaticLineSegItr )
		{
			for( LineSegments::const_iterator staticLineSegItr = staticLineSegments.begin();
				 staticLineSegItr != staticLineSegments.end(); ++staticLineSegItr )
			{
				CollisionInfo collisionInfo = GetCollisionInfo( *nonStaticLineSegItr, *staticLineSegItr, 
					displacement );

				// GetCollisionInfo will return a blank CollisionInfo if no collision was found, so only
				// keep track of non-blank ones
				if( collisionInfo.first != 0 )
				{
					if( collisions.size() == 0 || EqualDouble( collisions[0].first, collisionInfo.first ) )
					{
						collisions.push_back( collisionInfo );
					} 
					else if( collisionInfo.first < collisions[0].first )
					{
						collisions.clear();
						collisions.push_back( collisionInfo );
					}
				}
			}
		}

		// Process the collisions we encountered: the first order of business is to move the non-static 
		// CollidableGameObject back to its previous position (before the collision) and then move it the 
		// maximum distance along its displacemnt vector without causing a collision (so that it rests right
		// next to the object(s) it collided with). This distance is given to us by the first member in our
		// collision info pair(s). We'll offset it by 0.001 to keep the object out of collision.

		assert( collisions.size() != 0 );

		Point2D preCollisionPosition = prevState.GetPosition();
		double displacementOffset = 0.01;
		kinematicState.SetPosition( preCollisionPosition + 
			( collisions[0].first - displacementOffset ) * displacement.UnitVector() );

		// Reposition the collidable area of the non-static object around its new position
		nonStaticObject->GetCollidableArea()->UpdatePosition( kinematicState.GetPosition() );

		// Verify that the non-static object no longer collides with any of the static objects in its new position.
		// If it does, then keep offsetting the object by increasing amounts until its no longer in collision.
		bool collisionFound = false;
		do
		{
			for( std::vector< CollidableGameObject* >::const_iterator collidableGameObjectItr = staticObjects.begin();
				 collidableGameObjectItr != staticObjects.end(); ++collidableGameObjectItr )
			{
				if( nonStaticObject->GetCollidableArea()->CollidesWith( (*collidableGameObjectItr)->GetCollidableArea() ) )
				{
					collisionFound = true;
				}
			}

			if( displacementOffset == collisions[0].first )
			{
				assert( !collisionFound );
			}

			if( collisionFound )
			{
				collisionFound = false;
				displacementOffset = Min( displacementOffset + 0.01, collisions[0].first );
				kinematicState.SetPosition( preCollisionPosition + 
					( collisions[0].first - displacementOffset ) * displacement.UnitVector() );
				nonStaticObject->GetCollidableArea()->UpdatePosition( kinematicState.GetPosition() );
			}
		}
		while( collisionFound );

		// The final order of business is to determine how the velocity and acceleration of the non-static 
		// CollidableGameObject are affected by the collisions.
		
		// First, let's talk about velocity. Each line segment acting as a collision surface will create a 
		// partial altered velocity (PAV) derived from the object's velocity prior to the collision. The PAV 
		// will be computed as follows: if the line acting as the collision surface is located above the 
		// non-static CollidableGameObject or to its left or right (in Screen coordinates), the PAV will be 
		// the reflection of the object's pre-collision velocity across the line segment. Otherwise, it will 
		// be the projection of the object's pre-collision velocity onto the line segment. The sum of all the 
		// PAVs divided by the number of PAVs will give the new velocity for the object.

		// Now let's talk about acceleration. Each line segment acting as a collision surface that is also 
		// located below the non-static CollidableGameObject (in Screen coordinates) will create a partial 
		// altered acceleration (PAA). The PAA will be the projection of the acceleration of gravity onto 
		// the line segment. The sum of all the PAAs divided by the number of PAAs will give the new acceleration 
		// for the object.

		Vector2D netVelocity( 0, 0 );
		Vector2D netAcceleration( 0, 0 );
		bool accelerationAltered = false;
		for( Collisions::const_iterator collisionItr = collisions.begin(); collisionItr != collisions.end();
			 ++collisionItr )
		{
			if( LineSegmentLocatedBelowPoint( collisionItr->second, kinematicState.GetPosition() ) )
			{
				netVelocity += kinematicState.GetVelocity().ProjectOnto( 
					Vector2D( collisionItr->second.Start(), collisionItr->second.End() ) );    

				netAcceleration += Vector2D( 0, ACCELERATION_OF_GRAVITY ).ProjectOnto( 
					Vector2D( collisionItr->second.Start(), collisionItr->second.End() ) ); 

				accelerationAltered = true;
			}
			else
			{
				netVelocity += kinematicState.GetVelocity().ReflectOver( 
					Vector2D( collisionItr->second.Start(), collisionItr->second.End() ) );  
			}
		}

		kinematicState.SetVelocity( ( 1.0 / static_cast< double >( collisions.size() ) ) * netVelocity );

		if( accelerationAltered )
		{
			kinematicState.SetAcceleration( ( 1.0 / static_cast< double >( collisions.size() ) ) * netAcceleration );
		}

		// If velocity or acceleration is approximately zero after collision, zero it out for stability
		if( kinematicState.GetVelocity().IsZero() )
		{
			kinematicState.SetVelocity( Vector2D( 0, 0 ) );
		}
		if( kinematicState.GetAcceleration().IsZero() )
		{
			kinematicState.SetAcceleration( Vector2D( 0, 0 ) );
		}

		nonStaticObject->SetKinematicState( kinematicState );
	}

	/* Gets the collision info for the collision that results from one line segment being displaced
	   into another line segment. */
	Collision2D::CollisionInfo Collision2D::GetCollisionInfo( const LineSegment2D &lineToDisplace, 
			const LineSegment2D &otherLine, Vector2D displacement )
	{
		CollisionInfo collisionInfo;
		collisionInfo.first = 0;

		// Compute the displaced line.
		LineSegment2D displacedLine( lineToDisplace.Start() + displacement, 
			lineToDisplace.End() + displacement );

		// Construct line segments between the corresponding vertices of lineToDisplace and displacedLine. 
		// Check if these lines intersect otherLine and construct a CollisionsInfo pair from the following 
		// information: 1) the distance from the vertex that sits on lineToDisplace to the intersection
		// point and 2) otherLine. Keep track of the CollisionInfo pair with the smallest distance to 
		// intersection point.
		typedef std::vector< LineSegment2D > LineSegments;
		LineSegments vertexPaths;
		vertexPaths.push_back( LineSegment2D( lineToDisplace.Start(), displacedLine.Start() ) );
		vertexPaths.push_back( LineSegment2D( lineToDisplace.End(), displacedLine.End() ) );

		std::size_t intersectionsFound = 0;
		Point2D intersectionPointStart, intersectionPointEnd;
		if( vertexPaths[0].Intersects( otherLine, intersectionPointStart, intersectionPointEnd ) )
		{
			double distanceToIntersection = Min( LineSegment2D( lineToDisplace.Start(), intersectionPointStart ).Length(),
											     LineSegment2D( lineToDisplace.Start(), intersectionPointEnd ).Length() );
			
			collisionInfo.first = distanceToIntersection;
			collisionInfo.second = otherLine;
			++intersectionsFound;
		}

		if( vertexPaths[1].Intersects( otherLine, intersectionPointStart, intersectionPointEnd ) )
		{
			double distanceToIntersection = Min( LineSegment2D( lineToDisplace.End(), intersectionPointStart ).Length(),
											     LineSegment2D( lineToDisplace.End(), intersectionPointEnd ).Length() );
			
			if( collisionInfo.first == 0 || distanceToIntersection < collisionInfo.first )
			{
				collisionInfo.first = distanceToIntersection;
				collisionInfo.second = otherLine;
			}
			++intersectionsFound;
		}

		// If both vertex paths of lineToDisplace intersect otherLine, we can skip the rest of the analysis, as we already have the 
		// correct answer.
		if( intersectionsFound != 2 )
		{
			// Now pretend as though otherLine was displaced with equal magnitude and in the opposite direction.
			LineSegment2D displacedOtherLine( otherLine.Start() + -1 * displacement,
				otherLine.End() + -1 * displacement );

			// Construct line segments between the corresponding vertices of otherLine and displacedOtherLine. 
			// Check if these lines intersect lineToDisplace and construct the CollisionsInfo pair from the following 
			// information: 1) the distance from the vertex that sits on otherLine to the intersection
			// point and 2) lineToDisplace.
			vertexPaths.clear();
			vertexPaths.push_back( LineSegment2D( otherLine.Start(), displacedOtherLine.Start() ) );
			vertexPaths.push_back( LineSegment2D( otherLine.End(), displacedOtherLine.End() ) );

			if( vertexPaths[0].Intersects( lineToDisplace, intersectionPointStart, intersectionPointEnd ) )
			{
				double distanceToIntersection = Min( LineSegment2D( otherLine.Start(), intersectionPointStart ).Length(),
													 LineSegment2D( otherLine.Start(), intersectionPointEnd ).Length() );
				
				if( collisionInfo.first == 0 || distanceToIntersection < collisionInfo.first )
				{
					collisionInfo.first = distanceToIntersection;
					collisionInfo.second = lineToDisplace;
				}
			}

			if( vertexPaths[1].Intersects( lineToDisplace, intersectionPointStart, intersectionPointEnd ) )
			{
				double distanceToIntersection = Min( LineSegment2D( otherLine.End(), intersectionPointStart ).Length(),
													 LineSegment2D( otherLine.End(), intersectionPointEnd ).Length() );
				
				if( collisionInfo.first == 0 || distanceToIntersection < collisionInfo.first )
				{
					collisionInfo.first = distanceToIntersection;
					collisionInfo.second = lineToDisplace;
				}
			}
		}

		return collisionInfo;
	}

	/* Returns true if the line segment is located below the point (in Screen coordinates). */
	bool Collision2D::LineSegmentLocatedBelowPoint( const LineSegment2D &lineSegment, const Point2D &point )
	{
		bool locatedBelow = false;

		// This will be determined by constructing a vertical line that runs through the point and another 
		// line that includes the lineSegment. If these lines intersect below the point, the line segment
		// is determined to be below the point. Because we're evaluating this with respect to Screen 
		// coordinates, this means that the y value of the intersection point should be greater than the
		// y value of the passed-in point for it to be considered below. If these lines intersect above 
		// the point or not at all, the line segment is determined to be above the point.

		Line2D pointLine( point, point + Vector2D( 0, -1 ) );
		Line2D lineSegLine( lineSegment.Start(), lineSegment.End() );

		Point2D intersectionPoint;
		if( pointLine.Intersects( lineSegLine, intersectionPoint ) )
		{
			if( intersectionPoint._y > point._y )
			{
				locatedBelow = true;
			}
		}

		return locatedBelow;
	}

}