#pragma once

/* Defines the abstract functionality of a dynamic game object. */

#include "GameObject.h"

namespace PROJECT_NAME
{

	class DynamicGameObject : public virtual GameObject
	{
	public:
		typedef std::size_t UpdatePriorityLevel;

		/* Initializes a DynamicGameObject from a UpdatePriorityLevel and a GameObject::ObjectId. */
		DynamicGameObject( UpdatePriorityLevel updatePriorityLevel, ObjectId objectId );

		/* Update
		   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
					last update (which should be accounted for in this update). 
		   Parameters: 
			  elapsedTime: The time elapsed in seconds since last update. */
		virtual void Update( double elapsedTime ) = 0;

		/* UpdatePriorityLevel 
		   Summary: Returns the update priority level of this DynamicGameObject. 
		   Returns: The update priority level of this DynamicGameObject. */
		const UpdatePriorityLevel& GetUpdatePriorityLevel() const;

	private:
		// _updatePriorityLevel stores the integral update priority level (starting with 0 and counting up)
		// of a DynamicGameObject. DynamicGameObjects with lower update priority levels
		// are updated before those with higher levels. 
		UpdatePriorityLevel _updatePriorityLevel;
	};

}