/* MapLine implementation. */

#include "stdafx.h"

#include "Game.h"
#include "MapLine.h"

namespace PROJECT_NAME
{
	/* Initializes a dummy MapLine. Used when getting all objects of type MapLine from 
	   GameObjectManager. */
	MapLine::MapLine( GameObject::ObjectId objectId ) : GameObject( objectId ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString("dummy"), objectId ),
		CollidableGameObject( objectId ), _collidableArea( LineSegment2D() ), _type( MapLine::Wall ), 
		_drawDirection( MapLine::StartToEnd ),
		_spriteAnimator( RESOURCE_PATH( "Animation/MapLine/MapLineAnimationFile.txt" ), GraphicsId(), _lineSegment.Start() )
	{

	}

	/* Initializes a MapLine given its object id, a LineSegment2D, its type, the imageFile
	   for its graphic component, and a DrawDirection. */
	MapLine::MapLine( GameObject::ObjectId objectId, const LineSegment2D &lineSeg, MapLineType type, 
		DrawDirection drawDirection ) : GameObject( objectId ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Placeholder.png" ) ), objectId ),
		CollidableGameObject( objectId ), _lineSegment( lineSeg ), _collidableArea( lineSeg ), _type( type ), 
		_drawDirection( drawDirection ), 
		_spriteAnimator( RESOURCE_PATH( "Animation/MapLine/MapLineAnimationFile.txt" ), GraphicsId(), drawDirection ? _lineSegment.End() : _lineSegment.Start() )
	{
		// Verify that lineSeg is a valid line with an actual length
		assert( lineSeg.Start() != lineSeg.End() );

		// Compute angle used in animating the MapLine being drawn.
		_angle = drawDirection ? 
			Vector2D( _lineSegment.End(), _lineSegment.Start() ).Angle() : 
			Vector2D( _lineSegment.Start(), _lineSegment.End() ).Angle();

		// Take into account initial orientation of sprite used in the drawing animation.
		_angle = 90 - _angle;

		// Initialize the subrect used in animating the MapLine being drawn.
		_subRect = Rectangle2D( Point2D( 0, 0.01 ), Point2D( 10, 0.01 ),
			Point2D( 0, 0 ), Point2D( 10, 0 ) );

		_spriteAnimator.PlayAnimation( "MapLine", false, &_angle, &_subRect );

	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void MapLine::Draw( double interpolation )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), Game::GetGameAssetManager().GetAppWindow() );
	}

	/* GetCollidableArea
	   Summary: Gets the CollidableArea for this CollidableGameObject. 
	   Returns: The CollidableArea for this CollidableGameObject. */
	CollidableArea* MapLine::GetCollidableArea()
	{
		return &_collidableArea;
	}

	/* GetType
	   Summary: Returns the type of this map line (as defined by MapLineType enum). 
	   Returns: The type of this map line (as defined by MapLineType enum). */
	MapLine::MapLineType MapLine::GetType() const
	{
		return _type;
	}

	/* GetLineSegment 
	   Summary: Returns the line segment component of this MapLine. 
	   Returns: The line segment component of this MapLine. */
	const LineSegment2D& MapLine::GetLineSegment() const
	{
		return _lineSegment;
	}

	const double MapLine::_mapLineWidth = 10.0;

}