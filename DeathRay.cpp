/* DeathRay implementation. */

#include "stdafx.h"

#include <cmath>

#include "Box2DConversions.h"
#include "DeathRay.h"
#include "Game.h"
#include "LineSegment2D.h"
#include "Vector2D.h"
#include "WorldSimulation.h"

namespace PROJECT_NAME
{

	/* Initializes a dummy DeathRay. Used when getting all objects of type DeathRay from 
		GameObjectManager. */
	DeathRay::DeathRay() : GameObject( "dummy" ), CollidableGameObject( "dummy" ), 
		DynamicGameObject( 1, "dummy" ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString("dummy"), 
		"dummy" ), _collidableArea( LineSegment2D() ), _deathRayEmitter( Point2D() ), _deathRayBeam( Point2D() ) 
	{

	}

	/* Initializes a DeathRay from an object id, origin point, starting angle, lower/upper angle limits and
		angular velocity. */
	DeathRay::DeathRay( GameObject::ObjectId objectId, Point2D origin, double startingAngle, double lowerAngleLimit, 
		double upperAngleLimit, double angularVelocity ) : GameObject( objectId ), CollidableGameObject( objectId ),
		DynamicGameObject( 0, objectId ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Placeholder.png" ) ) , objectId ),
		_origin( origin ), _angle( startingAngle ), _lowerAngleLimit( lowerAngleLimit ), _upperAngleLimit( upperAngleLimit ),
		_angularVelocity( angularVelocity ), _collidableArea( LineSegment2D() ), _deathRayEmitter( origin ), _deathRayBeam( origin )  
	{
		assert( _lowerAngleLimit <= _upperAngleLimit );
		assert( startingAngle >= _lowerAngleLimit && startingAngle <= _upperAngleLimit );
	}

	/* GetCollidableArea
		Summary: Gets the CollidableArea for this CollidableGameObject. 
		Returns: The CollidableArea for this CollidableGameObject. */
	CollidableArea* DeathRay::GetCollidableArea()
	{
		return &_collidableArea;
	}

	/* Update
		Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
				last update (which should be accounted for in this update). 
		Parameters: 
			elapsedTime: The time elapsed in seconds since last update. */
	void DeathRay::Update( double elapsedTime )
	{
		// Update the angle of the ray according to its velocity. If a limit is reached, reverse
		// the direction of rotation by negating the angular velocity.
		_angle += _angularVelocity * elapsedTime;

		if( _angle < _lowerAngleLimit )
		{
			_angle = _lowerAngleLimit;
			_angularVelocity = -_angularVelocity;
		}
		else if( _angle > _upperAngleLimit )
		{
			_angle = _upperAngleLimit;
			_angularVelocity = -_angularVelocity;
		}

		// Compute the endpoint of the ray based on the angle and ray length. We'll use the origin
		// and endpoint as input when interfacing with Box2D to compute the ray's intersection 
		// point.
		Vector2D rayDirection( cos( Radians( _angle ) ), sin( Radians( _angle ) ) );

		Point2D endpoint = _origin + _rayLength * rayDirection;

		// Get the WorldSimulation object, which holds the Box2D b2World object.
		Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );
		assert( worldSimulationHandle.Valid() );

		b2World &world = worldSimulationHandle->GetWorld();

		// Call Box2D's function for resolving the intersection of a ray.
		b2Vec2 startPoint( 
			static_cast< float32 >( _origin._x * PIXELS_TO_METERS ), 
			static_cast< float32 >( _origin._y * PIXELS_TO_METERS ) );
		b2Vec2 endPoint( 
			static_cast< float32 >( endpoint._x * PIXELS_TO_METERS ), 
			static_cast< float32 >( endpoint._y * PIXELS_TO_METERS ) );

		RayCastCallback rayCastCallback;

		world.RayCast( &rayCastCallback, startPoint, endPoint );

		// After calling b2World::RayCast, our RayCastCallback instance now holds the nearest intersection point.
		// So assign the DeathRay member with it.
		_intersectionPoint = rayCastCallback._intersectionPoint;

		// Update the collidable area of the death ray.
		_collidableArea.UpdateLineSegment( LineSegment2D( _origin, _intersectionPoint ) );

		// Update emitter angle and animation.
		_deathRayEmitter._angle = -( _angle - 90.0 );
		_deathRayEmitter._spriteAnimator.Update( elapsedTime );

		// Update beam angle/subrect and animation.
		_deathRayBeam._angle = -( _angle - 90.0 );

		double currentRayLength = LineSegment2D( _origin, _intersectionPoint ).Length() - _beamTruncationValue;
		// If the length of the current ray is 0 or less, set it to 0.1, so that we're never creating a subrectangle
		// with a negative or 0 dimension. 
		if( currentRayLength <= 0 ) 
		{
			currentRayLength = 0.1;
		}
		_deathRayBeam._subRect = Rectangle2D( Point2D( 0, currentRayLength ), Point2D( _deathRayBeamWidth, currentRayLength ), 
			Point2D( 0, 0 ), Point2D( _deathRayBeamWidth, 0 ) );

		_deathRayBeam._spriteAnimator.Update( elapsedTime );

		// Update ray impact effect position/angle and animation.
		_deathRayImpactEffect._position = _intersectionPoint;
		_deathRayImpactEffect._angle = -( rayCastCallback._surfaceNormal.Angle() - 270.0 );
		_deathRayImpactEffect._spriteAnimator.Update( elapsedTime );
	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void DeathRay::Draw( double interpolation )
	{
		/* The responsibility of drawing the death ray is outsourced to the component subclasses:
		   DeathRayEmitter, DeathRayBeam, and DeathRayImpactEffect. */

		_deathRayEmitter.Draw( interpolation );
		_deathRayBeam.Draw( interpolation );
		_deathRayImpactEffect.Draw( interpolation );
	}

	float32 DeathRay::RayCastCallback::ReportFixture( b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction )
	{
		_surfaceNormal = Vector2D( normal.x * METERS_TO_PIXELS, normal.y * METERS_TO_PIXELS );
		_intersectionPoint = Point2D( point.x * METERS_TO_PIXELS, point.y * METERS_TO_PIXELS );

		return fraction;
	}

	const double DeathRay::_rayLength = 2000.0; // in pixel units

	/* DeathRayEmitter implementation. */

	DeathRay::DeathRayEmitter::DeathRayEmitter( Point2D position ) : GameObject( "DeathRayEmitter" ),
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Placeholder.png" ) ), 
			"DeathRayEmitter" ), _position( position ), _angle( 0 ), 
		_spriteAnimator( RESOURCE_PATH( "Animation/DeathRay/DeathRayAnimationFile.txt" ), GraphicsId(), _position )
	{
		_spriteAnimator.PlayAnimation( "EmitterTargetting", true, &_angle );
	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void DeathRay::DeathRayEmitter::Draw( double interpolation )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	/* DeathRayBeam implementation. */

	DeathRay::DeathRayBeam::DeathRayBeam( Point2D origin ) : GameObject( "DeathRayBeam" ),
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Placeholder.png" ) ), 
			"DeathRayBeam" ), _origin( origin ), _angle( 0 ), 
			_subRect( Point2D( 0, _deathRayBeamHeight ), Point2D( _deathRayBeamWidth, _deathRayBeamHeight ), Point2D( 0, 0 ), Point2D( _deathRayBeamWidth, 0 ) ),
		_spriteAnimator( RESOURCE_PATH( "Animation/DeathRay/DeathRayAnimationFile.txt" ), GraphicsId(), _origin )
	{
		_spriteAnimator.PlayAnimation( "DeathRayUndulating", true, &_angle, &_subRect );
	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void DeathRay::DeathRayBeam::Draw( double interpolation )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	/* DeathRayImpactEffect implementation. */

	DeathRay::DeathRayImpactEffect::DeathRayImpactEffect() : GameObject( "DeathRayImpactEffect" ),
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Placeholder.png" ) ), 
			"DeathRayImpactEffect" ), _position( Point2D() ), _angle( 0 ), 
		_spriteAnimator( RESOURCE_PATH( "Animation/DeathRay/DeathRayAnimationFile.txt" ), GraphicsId(), _position )
	{
		_spriteAnimator.PlayAnimation( "DeathRayImpact", true, &_angle );
	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void DeathRay::DeathRayImpactEffect::Draw( double interpolation )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

}