/* GameObjectManger implementation. */

#include "stdafx.h"

#include "Game.h"
#include "GameObjectManager.h"

namespace PROJECT_NAME
{

	/* AddGameObject 
	   Summary: Adds an object to the GameObjectManager. 
	   Parameters:
	      objectHandle: A handle to the object to add. */
	void GameObjectManager::AddGameObject( Handle< GameObject > objectHandle )
	{
		// First verify that an object with this id has not already been added, as 
		// it is an error if it has.
		GameObjectMap::iterator findResult = _gameObjects.find( objectHandle->Id() );
		assert( findResult == _gameObjects.end() );

		_gameObjects[ objectHandle->Id() ] = objectHandle;

		// Determine whether object being added is a VisibleGameObject/DynamicGameObject/CollidableGameObject
		// and add it to the appropriate subcontainers.
		Handle< VisibleGameObject > visibleGameObjectHandle = objectHandle;
		if( visibleGameObjectHandle.Valid() )
		{
			_drawList[ visibleGameObjectHandle->GetRenderOrder() ] = visibleGameObjectHandle;
		}

		Handle< DynamicGameObject > dynamicGameObjectHandle = objectHandle;
		if( dynamicGameObjectHandle.Valid() )
		{
			_updateList.insert( std::make_pair( dynamicGameObjectHandle->GetUpdatePriorityLevel(), dynamicGameObjectHandle ) );
		}

		Handle< CollidableGameObject > collidableGameObjectHandle = objectHandle;
		if( collidableGameObjectHandle.Valid() )
		{
			_collidableList.insert( collidableGameObjectHandle->Id() );
		}
	}

	/* GetGameObject
	   Summary: Retrieves an object from the GameObjectManager. 
	   Parameters:
	      objectId: The id of the object to retrieve. 
	   Returns: A handle to the object matching the passed-in id or a handle to nothing if no
	            matching object is found. */
	Handle< GameObject > GameObjectManager::GetGameObject( GameObject::ObjectId objectId )
	{
		Handle< GameObject > gameObjectHandle;

		GameObjectMap::iterator findResult = _gameObjects.find( objectId );

		if( findResult != _gameObjects.end() )
		{
			gameObjectHandle = findResult->second;
		}
		
		return gameObjectHandle;
	}

	/* DestroyGameObject
	   Summary: Removes the object matching the passed-in id from the GameObjectManager. 
	   Parameter:
	      objectId: The id of the object to destroy. */
	void GameObjectManager::DestroyGameObject( GameObject::ObjectId objectId )
	{
		_gameObjects.erase( objectId );

		// Remove object from _drawList if present
		{
			DrawList::iterator objectToEraseItr;
			for( objectToEraseItr = _drawList.begin();
				 objectToEraseItr != _drawList.end(); ++objectToEraseItr )
			{
				if( objectToEraseItr->second->Id() == objectId )
				{
					_drawList.erase( objectToEraseItr );
					break;
				}
			}
		}

		// Remove object from _updateList if present
		{
			UpdateList::iterator objectToEraseItr;
			for( objectToEraseItr = _updateList.begin();
				 objectToEraseItr != _updateList.end(); ++objectToEraseItr )
			{
				if( objectToEraseItr->second->Id() == objectId )
				{
					_updateList.erase( objectToEraseItr );
					break;
				}
			}
		}

		// Remove object from _collidableList if present
		_collidableList.erase( objectId );
	}

	/* DestroyAllGameObjects
	   Summary: Destroys all objects managed by GameObjectManager. */
	void GameObjectManager::DestroyAllGameObjects()
	{
		// Make a copy of _gameObjects and iterate through the copy when calling DestroyGameObject
		// (which erases elements from _gameObjects) so we don't invalidate iterators.

		GameObjectMap gameObjectsCopy( _gameObjects.begin(), _gameObjects.end() );

		for( GameObjectMap::const_iterator gameObjectMapItr = gameObjectsCopy.begin();
			 gameObjectMapItr != gameObjectsCopy.end(); ++gameObjectMapItr )
		{
			DestroyGameObject( gameObjectMapItr->first );
		}
	}

	/* DrawAllVisibleGameObjects 
	   Summary: Draws all visible game objects in the correct order they are to be drawn. This 
	            ordering is derived from the objects themselves. */
	void GameObjectManager::DrawAllVisibleGameObjects( double interpolation )
	{
		// The objects in _drawList get automatically ordered as they're added to the 
		// list, so all we have to do is iterate over it from beginning to end and draw 
		// each one.
		for( DrawList::iterator objectToDrawItr = _drawList.begin();
			 objectToDrawItr != _drawList.end(); ++objectToDrawItr )
		{
			if( objectToDrawItr->second->IsVisible() )
			{
				objectToDrawItr->second->Draw( interpolation );
			}
		}
	}

	/* UpdateAllDynamicGameObjects 
	   Summary: Updates all dynamic game objects in the correct order they are to be updated.
	            This ordering is derived from the objects themselves. */
    void GameObjectManager::UpdateAllDynamicGameObjects()
	{
		// The objects in _updateList get automatically ordered as they're added to the 
		// list, so all we have to do is iterate over it from beginning to end and update 
		// each one.
		double updatePeriod = Game::GetGameUpdatePeriod();

		for( UpdateList::iterator objectToUpdateItr = _updateList.begin();
			 objectToUpdateItr != _updateList.end(); ++objectToUpdateItr )
		{
			objectToUpdateItr->second->Update( updatePeriod );
		}
	}

	/* IsACollidable 
	   Summary: Returns true if the object matching the passed-in id is a CollidableObject. */
	bool GameObjectManager::IsACollidable( GameObject::ObjectId objectId )
	{
		CollidableList::iterator findResult = _collidableList.find( objectId );

		return findResult != _collidableList.end();
	}

}