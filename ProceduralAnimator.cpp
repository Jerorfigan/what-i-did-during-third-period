/* ProceduralAnimator implementation. */

#include "stdafx.h"

#include <sstream>
#include <utility>

#include "Box2DConversions.h"
#include "Game.h"
#include "ProceduralAnimator.h"
#include "WorldSimulation.h"

namespace PROJECT_NAME
{

	/* Initializes a ProceduralAnimator from an animation file. */
	ProceduralAnimator::ProceduralAnimator( std::string animationFile ) : _isSimulating( false )
	{
		std::ifstream animationFileStream( animationFile.c_str() );
		assert( animationFileStream );

		std::string lineBuffer;
		AnimationData animationData;
		animationData._animationName = "";

		while( std::getline( animationFileStream, lineBuffer ) )
		{
			// Skip empty lines and comment lines.
			if( lineBuffer.empty() || lineBuffer[0] == '#' )
			{
				continue;
			}

			std::stringstream lineTokenizer( lineBuffer );
			std::string token;
			lineTokenizer >> token;

			if( token == "name" )
			{
				// Save previous animation data.
				if( !animationData._animationName.empty() )
				{
					_animations[ animationData._animationName ] = animationData;
				}

				animationData._bodies.clear();
				animationData._bodySprites.clear();
				animationData._fixtures.clear();
				animationData._joints.clear();
				animationData._shapes.clear();

				lineTokenizer >> animationData._animationName;
				assert( !animationData._animationName.empty() );
			}
			else if( token == "settings" )
			{
				ParseSettingsData( lineTokenizer, animationData );
			}
			else if( token == "world" )
			{
				lineTokenizer >> animationData._worldFriction >> animationData._worldRestitution;
				assert( lineTokenizer );
			}
			else if( token == "shape" )
			{
				Shape shape;
				ParseShapeData( lineTokenizer, shape );

				animationData._shapes[ shape._id ] = shape;
			}
			else if( token == "fixture" )
			{
				Fixture fixture;
				ParseFixtureData( lineTokenizer, fixture );

				animationData._fixtures[ fixture._id ] = fixture;
			}
			else if( token == "body" )
			{
				Body body;
				ParseBodyData( lineTokenizer, body );

				animationData._bodies.push_back( body );
			}
			else if( token == "joint" )
			{
				Joint joint;
				ParseJointData( lineTokenizer, joint );
				
				animationData._joints.push_back( joint );
			}
			else if( token == "sprite" )
			{
				Sprite sprite;
				ParseSpriteData( lineTokenizer, sprite, animationData );
				animationData._bodySprites.push_back( sprite );
			}
			else
			{
				// unknown token... blow up!
				assert( 0 == 1 );
			}
		}

		// Save last read animation data.
		if( !animationData._animationName.empty() )
		{
			_animations[ animationData._animationName ] = animationData;
		}
	}

	/* PlayProceduralAnimation
	Summary: Plays a procedural animation for an entity given its start position and velocity. 
	Parameters:
		animationName: The name of the animation.
		entityStartPosition: Used to position the b2Bodies that comprise the animation.
		entityStartVelocity: The default velocity for the b2Bodies that comprise the animation. */
	void ProceduralAnimator::PlayProceduralAnimation( std::string animationName, Point2D entityStartPosition, 
		Vector2D entityStartVelocity )
	{
		// Verify that the animation name is valid.
		std::map< std::string, AnimationData >::iterator findResult = _animations.find( animationName );
		assert( findResult != _animations.end() );

		// Only play the animation if we're not already running a simulation for another animation.
		if( !_isSimulating && findResult != _animations.end() )
		{
			_isSimulating = true;
			_currentAnimation = animationName;

			// Get the animation data for this animation.
			AnimationData animationData = _animations[ _currentAnimation ];

			// Get the WorldSimulation object, which manages the b2World object that we'll use to 
			// create physics entities.
			Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );
			assert( worldSimulationHandle.Valid() );

			b2World &world = worldSimulationHandle->GetWorld();

			// Set the world friction and restitution for this simulation.
			worldSimulationHandle->SetFriction( animationData._worldFriction );
			worldSimulationHandle->SetRestitution( animationData._worldRestitution );

			// Iterate through the body list in the animation data and create b2Body objects in the 
			// WorldSimulation. After creating each one, store it in the _simulatedBodies list 
			// (so that they can be deleted later).
			for( std::vector< Body >::const_iterator bodyItr = animationData._bodies.begin();
				 bodyItr != animationData._bodies.end(); ++bodyItr )
			{
				Shape shapeData = animationData._shapes[ animationData._fixtures[ bodyItr->_fixtureId ]._shapeId ];
				Fixture fixtureData = animationData._fixtures[ bodyItr->_fixtureId ];
				Body bodyData = *bodyItr;

				// Create possible shape defintions.
				b2PolygonShape polygonShape;
				b2CircleShape circleShape;

				// Create fixture definition.
				b2FixtureDef fixtureDef;
				fixtureDef.density = fixtureData._density;
				fixtureDef.friction = fixtureData._friction;
				fixtureDef.restitution = fixtureData._restitution;
				fixtureDef.filter.groupIndex = fixtureData._filterGroupIndex;

				// Create body definition.
				b2BodyDef bodyDef;
				bodyDef.angle = bodyData._rotation * DEGREES_TO_RADIANS;
				bodyDef.bullet = bodyData._bullet;
				bodyDef.position.Set( static_cast< float >( bodyData._position._x ) + 
					static_cast< float >( entityStartPosition._x * PIXELS_TO_METERS ),
					static_cast< float >( bodyData._position._y ) +
					static_cast< float >( entityStartPosition._y * PIXELS_TO_METERS ) );
				if( bodyData._hasCustomVelocity )
				{
					bodyDef.linearVelocity = bodyData._customLinearVelocity;
					bodyDef.angularVelocity = bodyData._customAngularVelocity * DEGREES_TO_RADIANS;
				}
				else
				{
					bodyDef.linearVelocity.Set( static_cast< float >( entityStartVelocity._x * PIXELS_TO_METERS),
						static_cast< float >( entityStartVelocity._y * PIXELS_TO_METERS) );
				}

				if( shapeData._type == Circle )
				{
					circleShape.m_p.Set( 
						static_cast< float >( shapeData._position._x ),
						static_cast< float >( shapeData._position._y ) );
					circleShape.m_radius = shapeData._radius;
					fixtureDef.shape = &circleShape;
				}
				else // rectangle shape
				{
					polygonShape.SetAsBox( 
						static_cast< float >( shapeData._halfWidth ),
						static_cast< float >( shapeData._halfHeight ) );
					fixtureDef.shape = &polygonShape;
				}

				if( bodyData._type == Dynamic )
				{
					bodyDef.type = b2_dynamicBody;
				}

				b2Body *body = world.CreateBody( &bodyDef );
				body->CreateFixture( &fixtureDef );

				_simulatedBodies.push_back( body );
			}

			// Iterate through the joint list in the animation data and create b2Joint objects in the 
			// WorldSimulation. 
			for( std::vector< Joint >::const_iterator jointItr = animationData._joints.begin();
				 jointItr != animationData._joints.end(); ++jointItr )
			{
				Joint jointData = *jointItr;

				if( jointData._jointType == Revolute )
				{
					b2RevoluteJointDef revoluteJointDef;
					revoluteJointDef.bodyA = _simulatedBodies[ jointData._bodyAId - 1 ];
					revoluteJointDef.bodyB = _simulatedBodies[ jointData._bodyBId - 1 ];
					revoluteJointDef.localAnchorA.Set( static_cast< float >( jointData._anchorA._x ),
						static_cast< float >( jointData._anchorA._y ) );
					revoluteJointDef.localAnchorB.Set( static_cast< float >( jointData._anchorB._x ),
						static_cast< float >( jointData._anchorB._y ) );
					revoluteJointDef.collideConnected = false;
					revoluteJointDef.enableLimit = jointData._enableLimit;
					revoluteJointDef.lowerAngle = jointData._lowerAngle * DEGREES_TO_RADIANS;
					revoluteJointDef.upperAngle = jointData._upperAngle * DEGREES_TO_RADIANS;
					_simulatedJoints.push_back( world.CreateJoint( &revoluteJointDef ) );
				}
			}
		}
	}

	/* StopProceduralAnimation 
	   Summary: Stops the currently playing procedural animation and frees resources allocated 
		        for that animation. */
	void ProceduralAnimator::StopProceduralAnimation()
	{
		_isSimulating = false;

		// Get the WorldSimulation object, which manages the b2World object that we'll use to 
		// destroy the bodies used for this animation.
		Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );
		assert( worldSimulationHandle.Valid() );

		b2World &world = worldSimulationHandle->GetWorld();

		// Delete the bodies created for this animation.
		for( std::vector< b2Body* >::iterator bodyItr = _simulatedBodies.begin();
			 bodyItr != _simulatedBodies.end(); ++bodyItr )
		{
			world.DestroyBody( *bodyItr );
		}

		_simulatedBodies.clear();
		_simulatedJoints.clear();

		// If interpolation enabled, clear out the position & rotation dequeues for the sprites associated with this animation.
		if( _animations[ _currentAnimation ]._interpolate )
		{
			for( std::vector< Sprite >::iterator spriteItr = _animations[ _currentAnimation ]._bodySprites.begin();
				spriteItr != _animations[ _currentAnimation ]._bodySprites.end(); ++spriteItr )
			{
				spriteItr->_prevAndCurrPos.clear();
				spriteItr->_prevAndCurrAngle.clear();
			}
		}
	}

	/* UpdateSimulation
	   Summary: Updates the currently playing procedural animation, if there is one. */
	void ProceduralAnimator::UpdateSimulation()
	{
		// Only update the simulation if there's currently one running.
		if( _isSimulating )
		{
			CheckJointThresholds();

			// Get the animation data for the current simulation, which we'll use below.
			std::map< std::string, AnimationData >::const_iterator findResult = _animations.find( _currentAnimation );
			assert( findResult != _animations.end() );
			AnimationData animationData = findResult->second;

			// If interpolation enabled, iterate through the simulated bodies list and get the position and rotation of
			// each body. Store these values in the associated sprite's previous/current position & rotation deques,
			// which will get used later by the draw simulation logic.
			if( animationData._interpolate )
			{
				for( std::vector< b2Body* >::const_iterator bodyItr = _simulatedBodies.begin(); 
					 bodyItr != _simulatedBodies.end(); ++bodyItr )
				{
					b2Vec2 pos = (*bodyItr)->GetPosition();
					float32 angle = (*bodyItr)->GetAngle();

					// Convert to Point2D and double respectively.
					Point2D position( pos.x * METERS_TO_PIXELS, pos.y * METERS_TO_PIXELS );
					double angleInDegrees = -angle * RADIANS_TO_DEGREES;

					std::vector< b2Body* >::difference_type spriteIndex = bodyItr - _simulatedBodies.begin();

					animationData._bodySprites[ spriteIndex ]._prevAndCurrPos.push_front( position );
					// Only keep track of the previous and current positions.
					if( animationData._bodySprites[ spriteIndex ]._prevAndCurrPos.size() > 2 )
					{
						animationData._bodySprites[ spriteIndex ]._prevAndCurrPos.pop_back();
					}
					animationData._bodySprites[ spriteIndex ]._prevAndCurrAngle.push_front( angleInDegrees );
					// Only keep track of the previous and current angles.
					if( animationData._bodySprites[ spriteIndex ]._prevAndCurrAngle.size() > 2 )
					{
						animationData._bodySprites[ spriteIndex ]._prevAndCurrAngle.pop_back();
					}
				}

				// Save the modified animation data.
				_animations[ _currentAnimation ] = animationData;
			}
		}
	}

	/* DrawSimulation
	   Summary: Draws the currently playing procedural animation, if there is one. */
	void ProceduralAnimator::DrawSimulation( double interpolation ) const
	{
		// Only draw the simulation if there's currently one running.
		if( _isSimulating )
		{
			std::map< std::string, AnimationData >::const_iterator findResult = _animations.find( _currentAnimation );
			assert( findResult != _animations.end() );
			AnimationData animationData = findResult->second;

			// If interpolation is enabled, previous/current position & rotation data is stored in the sprite object 
			// associated with each body, so iterate through the sprite list and draw each sprite at its interpolated 
			// position.
			if( animationData._interpolate )
			{
				for( std::vector< Sprite >::const_iterator spriteItr = animationData._bodySprites.begin();
						spriteItr != animationData._bodySprites.end(); ++spriteItr )
				{
					Point2D interpolatedPosition;
					double interpolatedAngle = 0;
				
					if( !spriteItr->_prevAndCurrPos.empty() )
					{
						// Set the interpolated position to an intermediate position between the previous and current
						// position of the sprite. If we only have one position recorded so far, just use that.
						if( spriteItr->_prevAndCurrPos.size() == 1 )
						{
							interpolatedPosition = spriteItr->_prevAndCurrPos.front();
						}
						else
						{
							interpolatedPosition = interpolation * 
								( Vector2D( spriteItr->_prevAndCurrPos[0] ) - Vector2D( spriteItr->_prevAndCurrPos[1] ) ) +
								Vector2D( spriteItr->_prevAndCurrPos[1] );
						}

						// Set the interpolated angle to an intermediate angle between the previous and current
						// angle of the sprite. If we only have one angle recorded so far, just use that.
						if( spriteItr->_prevAndCurrAngle.size() == 1 )
						{
							interpolatedAngle = spriteItr->_prevAndCurrAngle.front();
						}
						else
						{
							interpolatedAngle = interpolation * ( spriteItr->_prevAndCurrAngle[0] - spriteItr->_prevAndCurrAngle[1] ) + 
								spriteItr->_prevAndCurrAngle[1];
						}
					
						Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( 
							spriteItr->_graphicsId, interpolatedPosition );
						Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableRotation(
							spriteItr->_graphicsId, interpolatedAngle );
						Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( 
							spriteItr->_graphicsId, Game::GetGameAssetManager().GetAppWindow() );	
					}
				}
			}
			else
			{
				// If not interpolating, iterate through the simulated bodies list, get each body's position & rotation, 
				// and draw its associated sprite with those parameters.
				for( std::vector< b2Body* >::const_iterator bodyItr = _simulatedBodies.begin(); 
					 bodyItr != _simulatedBodies.end(); ++bodyItr )
				{
					b2Vec2 pos = (*bodyItr)->GetPosition();
					float32 angle = (*bodyItr)->GetAngle();

					// Convert to Point2D and double respectively.
					Point2D position( pos.x * METERS_TO_PIXELS, pos.y * METERS_TO_PIXELS );
					double angleInDegrees = -angle * RADIANS_TO_DEGREES;

					std::vector< b2Body* >::difference_type spriteIndex = bodyItr - _simulatedBodies.begin();

					Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( 
						animationData._bodySprites[ spriteIndex ]._graphicsId, position );
					Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableRotation(
						animationData._bodySprites[ spriteIndex ]._graphicsId, angleInDegrees );
					Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( 
						animationData._bodySprites[ spriteIndex ]._graphicsId, 
						Game::GetGameAssetManager().GetAppWindow() );	
				}
			}
		}
	}

	/* Checks the _simulatedJoints to see if any joints are experiencing reaction forces/torques greater 
	   than their respective force thresholds and, if so, destroys them. */
	void ProceduralAnimator::CheckJointThresholds()
	{
		// Get the WorldSimulation object, which we'll go through to update the b2World object.
		Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );
		assert( worldSimulationHandle.Valid() );

		// Get the current animation data.
		std::map< std::string, AnimationData >::const_iterator findResult = _animations.find( _currentAnimation ); 
		assert( findResult != _animations.end() );
		AnimationData animationData = findResult->second;

		for( std::vector< b2Joint* >::iterator jointItr = _simulatedJoints.begin(); 
			 jointItr != _simulatedJoints.end(); ++jointItr )
		{
			std::vector< b2Joint* >::difference_type jointIndex = jointItr - _simulatedJoints.begin();

			if( *jointItr != 0 &&
				( (*jointItr)->GetReactionForce( worldSimulationHandle->GetTimeStep() ).Length() > 
				animationData._joints[ jointIndex ]._thresholdForce ||
				(*jointItr)->GetReactionTorque( worldSimulationHandle->GetTimeStep() ) > 
				animationData._joints[ jointIndex ]._thresholdForce ) )
			{
				worldSimulationHandle->GetWorld().DestroyJoint( (*jointItr) );
				*jointItr = 0;
			}
		}
	}

	/* Parses the animation settings data from a stringstream holding the settings input data from the animation file. */
	void ProceduralAnimator::ParseSettingsData( std::stringstream &lineStream, AnimationData &animationData )
	{
		std::string interpolate;

		lineStream >> interpolate;

		if( interpolate == "true" )
		{
			animationData._interpolate = true;
		}
		else
		{
			animationData._interpolate = false;
		}
	}

	/* Parses shape data from a stringstream holding input data for a given line in the animation file. */
	void ProceduralAnimator::ParseShapeData( std::stringstream &lineStream, Shape &shape ) const
	{
		std::string shapeType;
		std::string junk;
		std::string shapePositionX;
		std::string shapePositionY;
		std::string shapeRadius;
		std::string shapeHalfWidth;
		std::string shapeHalfHeight;

		lineStream >> shape._id >> shapeType;

		if( shapeType == "Circle" )
		{
			shape._type = Circle;
			lineStream >> junk >> shapePositionX >> junk >> shapePositionY >> junk >> shapeRadius;

			ResolveNumericRange( shapePositionX, shape._position._x );
			ResolveNumericRange( shapePositionY, shape._position._y );
			ResolveNumericRange( shapeRadius, shape._radius );
		}
		else if( shapeType == "Rectangle" )
		{
			shape._type = Rectangle;
			lineStream >> junk >> shapeHalfWidth >> shapeHalfHeight;
			ResolveNumericRange( shapeHalfWidth, shape._halfWidth );
			ResolveNumericRange( shapeHalfHeight, shape._halfHeight );
		}
		else
		{
			// Unknown shape type...blow up!
			assert( 0 == 1 );
		}

		assert( lineStream );
	}

	/* Parses fixture data from a stringstream holding input data for a given line in the animation file. */
	void ProceduralAnimator::ParseFixtureData( std::stringstream &lineStream, Fixture &fixture ) const
	{
		std::string fixtureDensity;
		std::string fixtureFriction;
		std::string fixtureRestitution;

		lineStream >> fixture._id >> fixtureDensity >> fixtureFriction >> fixtureRestitution >> fixture._shapeId 
			>> fixture._filterGroupIndex;

		ResolveNumericRange( fixtureDensity, fixture._density );
		ResolveNumericRange( fixtureFriction, fixture._friction );
		ResolveNumericRange( fixtureRestitution, fixture._restitution );

		assert( lineStream );
	}

	/* Parses body data from a stringstream holding input data for a given line in the animation file. */
	void ProceduralAnimator::ParseBodyData( std::stringstream &lineStream, Body &body ) const
	{
		std::string bodyType;
		std::string junk;
		std::string bulletFlag;
		std::string hasCustomVelocity;
		std::string bodyPositionX;
		std::string bodyPositionY;
		std::string bodyRotation;
		std::string bodyCustomLinearVelocityX;
		std::string bodyCustomLinearVelocityY;
		std::string bodyCustomAngularVelocity;

		lineStream >> body._id >> bodyType >> junk >> bodyPositionX >> junk >> bodyPositionY >> junk 
			>> bodyRotation >> bulletFlag >> body._fixtureId >> hasCustomVelocity;

		ResolveNumericRange( bodyPositionX, body._position._x );
		ResolveNumericRange( bodyPositionY, body._position._y );
		ResolveNumericRange( bodyRotation, body._rotation );

		if( hasCustomVelocity == "true" )
		{
			body._hasCustomVelocity = true;
			lineStream >> junk >> bodyCustomLinearVelocityX >> junk >> bodyCustomLinearVelocityY >> junk >>
				bodyCustomAngularVelocity;

			ResolveNumericRange( bodyCustomLinearVelocityX, body._customLinearVelocity.x );
			ResolveNumericRange( bodyCustomLinearVelocityY, body._customLinearVelocity.y );
			ResolveNumericRange( bodyCustomAngularVelocity, body._customAngularVelocity );
		}
		else
		{
			body._hasCustomVelocity = false;
		}

		if( bodyType == "Dynamic" )
		{
			body._type = Dynamic;
		}
		else
		{
			// Unknown body type...blow up!
			assert( 0 == 1 );
		}

		if( bulletFlag == "true" )
		{
			body._bullet = true;
		}
		else 
		{
			body._bullet = false;
		}

		assert( lineStream );
	}

	/* Parses joint data from a stringstream holding input data for a given line in the animation file. */
	void ProceduralAnimator::ParseJointData( std::stringstream &lineStream, Joint &joint ) const
	{
		std::string jointType;
		std::string junk;
		std::string enableLimitFlag;
		std::string jointAnchorAx;
		std::string jointAnchorAy;
		std::string jointAnchorBx;
		std::string jointAnchorBy;
		std::string jointLowerAngle;
		std::string jointUpperAngle;
		std::string jointThresholdForce;

		lineStream >> jointType >> joint._bodyAId >> joint._bodyBId >> junk >> jointAnchorAx >> junk >> jointAnchorAy
			>> junk >> junk >> jointAnchorBx >> junk >> jointAnchorBy >> junk >> enableLimitFlag >> jointLowerAngle 
			>> jointUpperAngle >> jointThresholdForce;

		ResolveNumericRange( jointAnchorAx, joint._anchorA._x );
		ResolveNumericRange( jointAnchorAy, joint._anchorA._y );
		ResolveNumericRange( jointAnchorBx, joint._anchorB._x );
		ResolveNumericRange( jointAnchorBy, joint._anchorB._y );
		ResolveNumericRange( jointLowerAngle, joint._lowerAngle );
		ResolveNumericRange( jointUpperAngle, joint._upperAngle );
		ResolveNumericRange( jointThresholdForce, joint._thresholdForce );

		if( jointType == "Revolute" )
		{
			joint._jointType = Revolute;
		}
		else
		{
			// Unknown joint type...blow up!
			assert( 1 == 0 );
		}

		if( enableLimitFlag == "true" )
		{
			joint._enableLimit = true;
		}
		else
		{
			joint._enableLimit = false;
		}

		assert( lineStream );
	}

	/* Parses sprite data from a stringstream holding input data for a given line in the animation file. */
	void ProceduralAnimator::ParseSpriteData( std::stringstream &lineStream, Sprite &sprite, AnimationData &animationData ) const
	{
		std::string artFile;
		lineStream >> artFile;
		assert( lineStream );

		// Check if sprite should be scaled based on associated shape size (currently only supported for 
		// circle shapes).
		std::size_t shapeId;
		double spriteScaleFactor = 1.0;

		if( lineStream >> shapeId )
		{
			float32 maxRadius;
			lineStream >> maxRadius;
			assert( lineStream );

			std::map< std::size_t, Shape >::iterator findResult = animationData._shapes.find( shapeId );
			assert( findResult != animationData._shapes.end() );
			assert( findResult->second._type == Circle );

			spriteScaleFactor = findResult->second._radius / maxRadius;
		}

		sprite._graphicsId =
			Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( artFile ) );
				
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( sprite._graphicsId,
			Point2D( Game::GetGameServiceManager().GetGraphicsProvider()->GetSpriteSubRectangle( sprite._graphicsId ).Width() / 2.0,
					    Game::GetGameServiceManager().GetGraphicsProvider()->GetSpriteSubRectangle( sprite._graphicsId ).Height() / 2.0 ) );

		if( !EqualDouble( spriteScaleFactor, 1.0 ) )
		{
			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableScale( sprite._graphicsId, spriteScaleFactor, spriteScaleFactor );
		}
	}

}