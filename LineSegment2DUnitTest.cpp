/* Defines the unit test for the LineSegment2D class. */

#include "stdafx.h"

#include <cmath>

#include "LineSegment2D.h"

namespace PROJECT_NAME_UnitTests
{
	using namespace PROJECT_NAME;

	void LineSegment2DUnitTest()
	{
		/* Test 1: Exercise the default constructor and verify that it 
				   initializes a line segment with start and end points set to (0,0). */
		{
			LineSegment2D lineSeg;
			assert( lineSeg.Start() == Point2D( 0, 0 ) );
			assert( lineSeg.End() == Point2D( 0, 0 ) );
		}

		/* Test 2: Exercise the constructor that takes two points and verify that it initializes
		           the line segment correctly. */
		
		// Case 1: Line segment is non-vertical.
		{
			LineSegment2D lineSeg( Point2D( 1, 1 ), Point2D( 5, 5 ) );
			assert( lineSeg.Start() == Point2D( 1, 1 ) );
			assert( lineSeg.End() == Point2D( 5, 5 ) );
		}

		// Case 2: Line segment is vertical.
		{
			LineSegment2D lineSeg( Point2D( 1, 1 ), Point2D( 1, 5 ) );
			assert( lineSeg.Start() == Point2D( 1, 1 ) );
			assert( lineSeg.End() == Point2D( 1, 5 ) );
		}

		// Case 3: Points are backward
		{
			LineSegment2D lineSeg( Point2D( 3, 5 ), Point2D( 1, 1 ) );
			assert( lineSeg.Start() == Point2D( 1, 1 ) );
			assert( lineSeg.End() == Point2D( 3, 5 ) );
		}

		/* Test 3: Test the LineSegment2D::Intersects function and verify it determines whether two line segments
		           intersect correctly. */
		
		// Case 1: Lines segments intersect, 1 is vertical.
		{
			LineSegment2D lineSeg( Point2D( 1, 1 ), Point2D( 5, 5 ) );
			LineSegment2D lineSeg2( Point2D( 3, 1 ), Point2D( 3, 5 ) );
			assert( lineSeg.Intersects( lineSeg2 ) );
		}

		// Case 2: Lines segments intersect, neither is vertical.
		{
			LineSegment2D lineSeg( Point2D( 1, 1 ), Point2D( 5, 5 ) );
			LineSegment2D lineSeg2( Point2D( 1, 5 ), Point2D( 5, 1 ) );
			assert( lineSeg.Intersects( lineSeg2 ) );
		}

		// Case 3: Lines segments do not intersect, 1 is vertical.
		{
			LineSegment2D lineSeg( Point2D( 1, 1 ), Point2D( 5, 5 ) );
			LineSegment2D lineSeg2( Point2D( 3, 10 ), Point2D( 3, 12 ) );
			assert( !lineSeg.Intersects( lineSeg2 ) );
		}

		// Case 4: Lines segments do not intersect, neither is vertical.
		{
			LineSegment2D lineSeg( Point2D( 1, 1 ), Point2D( 5, 5 ) );
			LineSegment2D lineSeg2( Point2D( 1, 5 ), Point2D( 2, 4 ) );
			assert( !lineSeg.Intersects( lineSeg2 ) );
		}

		/* Test 4: Exercise the LineSegment2D::Intersects function that takes two reference parameters of type 
		           Point2D and verify it returns the intersection point or line correctly. */
		
		// Case 1: LineSegments intersect at a single point, neither is vertical
		{
			LineSegment2D lineSeg1( Point2D( 1, 1 ), Point2D( 6, 6 ) );
			LineSegment2D lineSeg2( Point2D( 1, 4 ), Point2D( 6, 0 ) );
			Point2D start, end;
			assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 2.666666, 2.666666 ) );
			assert( end == Point2D( 2.666666, 2.666666 ) );
		}

		// Case 2: LineSegments intersect at a single point, 1 line is vertical
		{
			LineSegment2D lineSeg1( Point2D( 2, 0 ), Point2D( 2, 6 ) );
			LineSegment2D lineSeg2( Point2D( 1, 4 ), Point2D( 6, 0 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 2, 3.2 ) );
			assert( end == Point2D( 2, 3.2 ) );
		}

		// Case 3: LineSegments don't intersect, neither is vertical
		{
			LineSegment2D lineSeg1( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			LineSegment2D lineSeg2( Point2D( 1, 2 ), Point2D( 2, 3 ) );
			Point2D start, end;
		    assert( !lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 0, 0 ) );
			assert( end == Point2D( 0, 0 ) );
		}

		// Case 4: LineSegments don't intersect, 1 is vertical
		{
			LineSegment2D lineSeg1( Point2D( 0, 1 ), Point2D( 0, 2 ) );
			LineSegment2D lineSeg2( Point2D( 1, 2 ), Point2D( 2, 3 ) );
			Point2D start, end;
		    assert( !lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 0, 0 ) );
			assert( end == Point2D( 0, 0 ) );
		}

		// Case 5: LineSegments don't intersect, both are vertical
		{
			LineSegment2D lineSeg1( Point2D( 1, 0 ), Point2D( 1, 5 ) );
			LineSegment2D lineSeg2( Point2D( 2, 0 ), Point2D( 2, 5 ) );
			Point2D start, end;
		    assert( !lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 0, 0 ) );
			assert( end == Point2D( 0, 0 ) );
		}

		// Case 6: lineSeg2 contains LineSeg1, neither is vertical
		{
			LineSegment2D lineSeg1( Point2D( 2, 2 ), Point2D( 4, 4 ) );
			LineSegment2D lineSeg2( Point2D( 1, 1 ), Point2D( 5, 5 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 2, 2 ) );
			assert( end == Point2D( 4, 4 ) );
		}

		// Case 7: LineSeg1.start overlaps LineSeg2, neither is vertical
		{
			LineSegment2D lineSeg1( Point2D( 3, 3 ), Point2D( 6, 6 ) );
			LineSegment2D lineSeg2( Point2D( 1, 1 ), Point2D( 5.5, 5.5 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 3, 3 ) );
			assert( end == Point2D( 5.5, 5.5 ) );
		}

		// Case 8: LineSeg1.end overlaps LineSeg2, neither is vertical
		{
			LineSegment2D lineSeg1( Point2D( 0, 0 ), Point2D( 2, 2 ) );
			LineSegment2D lineSeg2( Point2D( 1, 1 ), Point2D( 5.5, 5.5 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 1, 1 ) );
			assert( end == Point2D( 2, 2 ) );
		}

		// Case 9: LineSeg1 contains LineSeg2, neither is vertical
		{
			LineSegment2D lineSeg1( Point2D( 0.5, 0.5 ), Point2D( 9, 9 ) );
			LineSegment2D lineSeg2( Point2D( 1, 1 ), Point2D( 5, 5 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 1, 1 ) );
			assert( end == Point2D( 5, 5 ) );
		}

		// Case 10: LineSeg2 contains LineSeg1, both are vertical
		{
			LineSegment2D lineSeg1( Point2D( 1, 1 ), Point2D( 1, 3 ) );
			LineSegment2D lineSeg2( Point2D( 1, 0 ), Point2D( 1, 9 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 1, 1 ) );
			assert( end == Point2D( 1, 3 ) );
		}

		// Case 11: LineSeg1.start overlaps LineSeg2, both are vertical
		{
			LineSegment2D lineSeg1( Point2D( 1, 5 ), Point2D( 1, 12 ) );
			LineSegment2D lineSeg2( Point2D( 1, 0 ), Point2D( 1, 9 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 1, 5 ) );
			assert( end == Point2D( 1, 9 ) );
		}

		// Case 12: LineSeg1.end overlaps LineSeg2, both are vertical
		{
			LineSegment2D lineSeg1( Point2D( 1, -5 ), Point2D( 1, 3 ) );
			LineSegment2D lineSeg2( Point2D( 1, 0 ), Point2D( 1, 9 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 1, 0 ) );
			assert( end == Point2D( 1, 3 ) );
		}

		// Case 13: LineSeg1 contains LineSeg2, both are vertical
		{
			LineSegment2D lineSeg1( Point2D( 1, 1 ), Point2D( 1, 3 ) );
			LineSegment2D lineSeg2( Point2D( 1, 1.75 ), Point2D( 1, 2.333 ) );
			Point2D start, end;
		    assert( lineSeg1.Intersects( lineSeg2, start, end ) );
			assert( start == Point2D( 1, 1.75 ) );
			assert( end == Point2D( 1, 2.333 ) );
		}

		/* Test 5: Test the LineSegment2D::Contains function and verify that it determines whether a line
		           segment contains a point correctly. */
		{
			LineSegment2D lineSeg( Point2D( 1, 1 ), Point2D( 5, 5 ) );
			assert( lineSeg.Contains( Point2D( 3, 3 ) ) );
			assert( !lineSeg.Contains( Point2D( 3, 17 ) ) );
		}

		/* Test 6: Exercise the LineSegment2D::Length function and verify it determines the length correctly. */
		{
			LineSegment2D lineSeg( Point2D( 1, 1 ), Point2D( 5, 7 ) );
			// account for rounding error
			assert( abs( lineSeg.Length() - 7.211102 ) < 0.001 );
		}

	}

}