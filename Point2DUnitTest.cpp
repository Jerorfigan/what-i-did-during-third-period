/* Defines the unit test for the LineSegment2D class. */

#include "stdafx.h"

#include "Point2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME_UnitTests
{
	using namespace PROJECT_NAME;

	void Point2DUnitTest()
	{
		/* Test 1: Exercise the default constructor and verify that it initializes the point to be (0,0). */
		{
			Point2D point;
			assert( point._x == 0 );
			assert( point._y == 0 );
		}

		/* Test 2: Exercise the constructor that takes two values and verify it initializes the point correctly. */
		{
			Point2D point( 2, 5 );
			assert( point._x == 2 );
			assert( point._y == 5 );
		}

		/* Test 3: Exercise the Point2D::GetPointLocation function and verify that it determines a point's
		           gerenal location correctly. */
		{
			Point2D point1( 2, 5 );
			Point2D point2( 0, 5 );
			Point2D point3( -2, 5 );
			Point2D point4( -2, 0 );
			Point2D point5( -2, -5 );
			Point2D point6( 0, -5 );
			Point2D point7( 5, -5 );
			Point2D point8( 5, 0 );
			Point2D point9( 0, 0 );
			assert( point1.GetPointLocation() == Point2D::FirstQuadrant );
			assert( point2.GetPointLocation() == Point2D::PositiveYAxis );
			assert( point3.GetPointLocation() == Point2D::SecondQuadrant );
			assert( point4.GetPointLocation() == Point2D::NegativeXAxis );
			assert( point5.GetPointLocation() == Point2D::ThirdQuadrant );
			assert( point6.GetPointLocation() == Point2D::NegativeYAxis );
			assert( point7.GetPointLocation() == Point2D::FourthQuadrant );
			assert( point8.GetPointLocation() == Point2D::PositiveXAxis );
			assert( point9.GetPointLocation() == Point2D::Origin );
		}

		/* Test 4: Exercise the Point2D::operator== function and verify it determines equality correctly. */
		{
			Point2D point1( 2, 5 );
			Point2D point2( 0, 5 );
			assert( point1 == point1 );
			assert( !( point1 == point2 ) );
		}

		/* Test 5: Exercise the Point2D::operator!= function and verify it determines inequality correctly. */
		{
			Point2D point1( 2, 5 );
			Point2D point2( 0, 5 );
			assert( point1 != point2 );
			assert( !( point1 != point1 ) );
		}

		/* Test 6: Initialize a point from a vector. */
		{
			Vector2D vec( 1, 3 );
			Point2D point( vec );
			assert( point._x == 1 );
			assert( point._y == 3 );
		}

		/* Test 7: Exercise the += operator that takes a left-hand-side argument of Point2D and a 
		           right-hand-side argument of type Vector2D. */
		{
			Point2D point( 1, 2 );
			Vector2D vec( 3, 1 );
			point += vec;
			assert( point._x == 4 );
			assert( point._y == 3 );
		}
	}

}