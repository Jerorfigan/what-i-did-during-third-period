#pragma once

#include <map>
#include <string>

#include "IAudioProvider.h"
#include "SFML/Audio.hpp"
#include "SoundFileCache.h"

namespace PROJECT_NAME
{

	class SFMLAudioProvider : public IAudioProvider
	{
		// Internal classes
	private:
		struct SoundData
		{
			std::string mSoundFilePath;
			float mRawSoundVolume;
		};

		// Typedefs
	private:
		typedef std::map< IAudioProvider::AudioId, SoundData > SoundIdToSoundData; 
		typedef std::map< IAudioProvider::AudioId, sf::Sound > SoundIdToSoundMap;

		// Constructors
	public:
	  SFMLAudioProvider();

		// Methods
	private:
		bool ValidSound( AudioId id );
		bool ValidMusic( AudioId id );

		// Virtual methods
	public:
	  virtual AudioId InitSound( std::string filePath, bool isMusic = false );
	  virtual void PlaySound( AudioId id, float volume = 100, bool looping = false );
	  virtual void SetMaxVolume( float volume, int flags );
	  virtual void StopSound( AudioId id );
	  virtual void StopAllSounds();
	  virtual void DestroySound( AudioId id );

	  virtual bool IsSoundPlaying( AudioId id );
	  virtual bool IsSoundLooping( AudioId id );

	  // Data
	private:
	  static const int MAX_SOUND_CHANNELS = 10;

	  SoundIdToSoundData mSoundMap;
	  SoundIdToSoundData mMusicMap;
	  SoundFileCache mSoundFileCache;
	  SoundIdToSoundMap   mCurrentSounds;
	  IAudioProvider::AudioId mCurrentSongId;
	  float mMaxSoundVolume;
	  float mMaxMusicVolume;

	  IAudioProvider::AudioId mResourceIdSeq;
	};

}