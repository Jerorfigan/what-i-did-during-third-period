/* PushButton implementation. */

#include "stdafx.h"

#include "Game.h"
#include "PushButton.h"

namespace PROJECT_NAME
{

	// Constructors

	PushButton::PushButton( std::string id, unsigned int menuIndex, Rectangle2D clickableArea, std::string image, std::string hoverImage, 
			std::string activatedImage,
			void (*onHoverOver)( void* ), void (*onHoverAway)( void* ), void (*onActivation)( void* ), 
			void (*onDeactivation)( void* ) ) :
		UserControl( id, menuIndex, Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Placeholder.png" ) ) ),
		mClickableArea( clickableArea), mImage( image ), mHoverImage( hoverImage ), mActivatedImage( activatedImage ),
		mOnHoverOver( onHoverOver ), mOnHoverAway( onHoverAway ), mOnActivation( onActivation ), mOnDeactivation( onDeactivation )
	{
		
	}

	// Virtual methods

	void PushButton::Init( void* menuData )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( mGraphicsId, mImage );
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mGraphicsId, 
			mClickableArea.BottomLeft() );
	}

	void PushButton::Draw( void* menuData )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mGraphicsId, 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	void PushButton::OnHoverOver( void* menuData )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( mGraphicsId, mHoverImage );

		// If specific event handler specified, call it.
		if( mOnHoverOver )
		{
			mOnHoverOver( menuData );
		}
	}
	void PushButton::OnHoverAway( void* menuData )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( mGraphicsId, mImage );

		// If specific event handler specified, call it.
		if( mOnHoverAway )
		{
			mOnHoverAway( menuData );
		}
	}
	void PushButton::OnActivation( void* menuData )
	{
		// If specific event handler specified, call it.
		if( mOnActivation )
		{
			mOnActivation( menuData );
		}
	}
	void PushButton::OnDeactivation( void* menuData )
	{
		// If specific event handler specified, call it.
		if( mOnDeactivation )
		{
			mOnDeactivation( menuData );
		}
	}

	void PushButton::OnGainedFocus( void* menuData )
	{

	}

	void PushButton::OnLostFocus( void* menuData )
	{

	}

	void PushButton::OnKeyboardChar( void* menuData, char typedChar )
	{

	}

	bool PushButton::IsTargettedByMouse( Point2D mouseHotspot )
	{
		return mClickableArea.Contains( mouseHotspot );
	}

}