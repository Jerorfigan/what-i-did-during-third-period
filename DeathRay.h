#pragma once

/* DeathRay defines the functionality for a ray-based trap in the game world. */

#include "Box2D/Box2D.h"
#include "CollidableGameObject.h"
#include "CollidableLineSegment.h"
#include "DynamicGameObject.h"
#include "GameObject.h"
#include "Point2D.h"
#include "SpriteAnimator.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class DeathRay : public CollidableGameObject, public DynamicGameObject, public VisibleGameObject  
	{
	public:
		/* Initializes a dummy DeathRay. Used when getting all objects of type DeathRay from 
		   GameObjectManager. */
		DeathRay();

		/* Initializes a DeathRay from an object id, origin point, starting angle, lower/upper angle limits and
		   angular velocity. */
		DeathRay( GameObject::ObjectId objectId, Point2D origin, double startingAngle, double lowerAngleLimit, 
			double upperAngleLimit, double angularVelocity );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea();

		/* Update
		   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
					last update (which should be accounted for in this update). 
		   Parameters: 
			  elapsedTime: The time elapsed in seconds since last update. */
		virtual void Update( double elapsedTime );

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

	private:
		/* Internal class used in interfacing with Box2D to do the ray intersection computation. */
		struct RayCastCallback : public b2RayCastCallback
		{
			float32 ReportFixture( b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction );

			Vector2D _surfaceNormal;
			Point2D _intersectionPoint;
		};

		/* Subclass used to draw the emitter component of the death ray. */
		struct DeathRayEmitter : public VisibleGameObject  
		{
			DeathRayEmitter( Point2D position );

			/* Draw
			   Summary: Draws this VisibleGameObject. 
			   Parameters: 
				  interpolation: An interpolation value between 0 and 1 that gives derived classes 
								 the opportunity to interpolate their drawing methods to improve
								 animations of fast moving objects. */
			virtual void Draw( double interpolation );

			Point2D _position;
			double _angle;
			SpriteAnimator _spriteAnimator;
		};

		/* Subclass used to draw the beam component of the death ray. */
		struct DeathRayBeam : public VisibleGameObject
		{
			DeathRayBeam( Point2D origin );

			/* Draw
			   Summary: Draws this VisibleGameObject. 
			   Parameters: 
				  interpolation: An interpolation value between 0 and 1 that gives derived classes 
								 the opportunity to interpolate their drawing methods to improve
								 animations of fast moving objects. */
			virtual void Draw( double interpolation );

			Point2D _origin;
			double _angle;
			Rectangle2D _subRect;
			SpriteAnimator _spriteAnimator;
		};

		/* Subclass used to draw the impact effect component of the death ray. */
		struct DeathRayImpactEffect : public VisibleGameObject
		{
			DeathRayImpactEffect();

			/* Draw
			   Summary: Draws this VisibleGameObject. 
			   Parameters: 
				  interpolation: An interpolation value between 0 and 1 that gives derived classes 
								 the opportunity to interpolate their drawing methods to improve
								 animations of fast moving objects. */
			virtual void Draw( double interpolation );

			Point2D _position;
			double _angle;
			SpriteAnimator _spriteAnimator;
		};

		Point2D _origin;
		Point2D _intersectionPoint;
		double _angle; // in degrees
		double _lowerAngleLimit; // in degrees
		double _upperAngleLimit; // in degrees
		double _angularVelocity; // in degrees per second
		CollidableLineSegment _collidableArea;
		DeathRayEmitter _deathRayEmitter;
		DeathRayBeam _deathRayBeam;
		DeathRayImpactEffect _deathRayImpactEffect;

		static const int _deathRayBeamWidth = 12; // in pixels
		static const int _deathRayBeamHeight = 2000; // in pixels
		// _beamTruncationValue is used to shorten the length of the beam a little
		// so that it doesn't overlap the impact effect.
		static const int _beamTruncationValue = 19; // in pixels
		static const double _rayLength; // in pixel units
	};

}