#pragma once

/* Defines the functionality of a push button control. */

#include <string>

#include "Rectangle2D.h"
#include "UserControl.h"

namespace PROJECT_NAME
{

	class PushButton : public UserControl
	{
		// Constructors
	public:
		PushButton( std::string id, unsigned int menuIndex, Rectangle2D clickableArea, std::string image, std::string hoverImage = "", 
			std::string activatedImage = "",
			void (*onHoverOver)( void* ) = 0, void (*onHoverAway)( void* ) = 0, void (*onActivation)( void* ) = 0, 
			void (*onDeactivation)( void* ) = 0 );

		// Virtual methods
	public:
		virtual void Init( void* menuData );

		virtual void Draw( void* menuData );

		virtual void OnHoverOver( void* menuData );
		virtual void OnHoverAway( void* menuData );
		virtual void OnActivation( void* menuData );
		virtual void OnDeactivation( void* menuData );
		virtual void OnGainedFocus( void* menuData );
		virtual void OnLostFocus( void* menuData );
		virtual void OnKeyboardChar( void* menuData, char typedChar );

		virtual bool IsTargettedByMouse( Point2D mouseHotspot );

	private:
		Rectangle2D mClickableArea;
		std::string mImage;
		std::string mHoverImage;
		std::string mActivatedImage;

		// Callback function pointers
		void (*mOnHoverOver)( void* );
		void (*mOnHoverAway)( void* );
		void (*mOnActivation)( void* );
		void (*mOnDeactivation)( void* );
	};

}