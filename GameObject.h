#pragma once

/* Defines the abstract functionality of a game object. */

#include <string>

namespace PROJECT_NAME
{

	class GameObject
	{
	public:
		typedef std::string ObjectId;

		/* Initialize a GameObject from an ObjectId. */
		GameObject( ObjectId id );

		/* Id 
		   Summary: Returns the id of this GameObject. 
		   Returns: The id of this GameObject. */
		const ObjectId& Id() const;

		/* Virtual destructor to allow deletion of derived objects through GameObject pointer. */
		virtual ~GameObject();
	private:
		ObjectId _id;
	};

}