#pragma once

/* Defines the abstract functionality of a collidable game object. */

#include "CollidableArea.h"
#include "GameObject.h"
#include "Handle.h"
#include "KinematicState.h"
#include "Vector2D.h"

namespace PROJECT_NAME
{

	class CollidableGameObject : public virtual GameObject
	{
	public:
		/* Initializes a CollidableGameObject from an ObjectId, a KinematicState and a flag denoting 
		   whether this CollidableGameObject is static. */
		CollidableGameObject( ObjectId objectId, const KinematicState &kinematicState = KinematicState(), 
			bool isStatic = true );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea() = 0;

		/* GetKinematicState 
		   Summary: Gets the kinematic state of this CollidableGameObject. 
		   Returns: The kinematic state of this CollidableGameObject. */
		const KinematicState& GetKinematicState() const;
		/* SetKinematicState 
		   Summary: Sets the kinematic state of this CollidableGameObject. 
		   Parameters: 
		      kinematicState: The kinematic state to set it to. */
		void SetKinematicState( const KinematicState &kinematicState ); 

	protected:
		KinematicState _kinematicState;
		// Static CollidableGameObjects can not have their kinematic state changed after creation
		bool _isStatic;
	};

}