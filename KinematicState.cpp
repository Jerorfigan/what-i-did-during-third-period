/* KinematicState implementation. */

#include "stdafx.h"

#include "KinematicState.h"

namespace PROJECT_NAME
{

	/* Initializes a KinematicState with position, velocity, acceleration, max velocity and max acceleration
		   set to 0. */
	KinematicState::KinematicState() : _position( 0, 0 ), _velocity( 0, 0 ), _acceleration( 0, 0 ), 
		_maxVelocity( 0 ), _maxAcceleration( 0 )
	{
	}

	/* Initializes a KinematicState from a position, velocity, acceleration, max velocity and max acceleration. */
	KinematicState::KinematicState( Point2D position, Vector2D velocity, 
		Vector2D acceleration, double maxVelocity, double maxAcceleration ) : _position( position ),
		_velocity( velocity ), _acceleration( acceleration ), _maxVelocity( maxVelocity ), 
		_maxAcceleration( maxAcceleration )
	{
		LimitVelocity();
		LimitAcceleration();
	}

	/* UpdatePositionAndVelocity 
	   Summary: Updates the position and velocity using the kinematic equations and the specified time in 
	            seconds. 
	   Parameters:
	      time: The time in seconds to use in the kinematic equations. */
	void KinematicState::UpdatePositionAndVelocity( double time )
	{
		// Save off a copy of the position and velocity prior to this update so that when we call
		// GetPreviousPositionAndVelocity we'll have them.
		_prevPosition = _position;
		_prevVelocity = _velocity;

		_lastUpdateTime = time;

		/* Need to check to see if 1) we're at max velocity, in which case we simply update position
		   by time * velocity or 2) we're going to reach max velocity on this update, in which case 
		   we need to split the position update into two parts or 3) no max velocity is set or we're not
		   going to reach max velocity on this update, so update normally with appropriate kinematic 
		   equation. */

		if( _maxVelocity != 0 )
		{
			if( EqualDouble( _velocity.Magnitude(), _maxVelocity ) )
			{
				_position += time * _velocity;
			}
			else if( ( _velocity + time * _acceleration ).Magnitude() > _maxVelocity )
			{
				double maxTime = ( _maxVelocity - _velocity.Magnitude() ) / _acceleration.Magnitude(); 

				_position += maxTime * _velocity + 0.5 * maxTime * maxTime * _acceleration;
				_velocity += time * _acceleration;
				LimitVelocity();
				_position += ( time - maxTime ) * _velocity;
			}
			else
			{
				_position += time * _velocity + 0.5 * time * time * _acceleration;
				_velocity += time * _acceleration;
			}
		}
		else
		{
			_position += time * _velocity + 0.5 * time * time * _acceleration;
			_velocity += time * _acceleration;
		}
	}

	/* GetPreviousPositionAndVelocity
	   Summary: Returns a copy of this KinematicState with the position and velocity set to the values they
	            had prior to the last call to UpdatePositionAndVelocity. 
	   Returns: A copy of this KinematicState with the position and velocity set to the values they
	            had prior to the last call to UpdatePositionAndVelocity. */
	KinematicState KinematicState::GetPreviousPositionAndVelocity() const
	{
		KinematicState previousKinematicState = *this;

		previousKinematicState.SetPosition( _prevPosition );
		previousKinematicState.SetVelocity( _prevVelocity );

		return previousKinematicState;
	}

	/* GetPosition
	   Summary: Returns the position of this KinematicState. 
	   Returns: The position of this KinematicState. */
	const Point2D& KinematicState::GetPosition() const
	{
		return _position;
	}

	/* GetVelocity
	   Summary: Returns the velocity of this KinematicState. 
	   Returns: The velocity of this KinematicState. */
	const Vector2D& KinematicState::GetVelocity() const
	{
		return _velocity;
	}

	/* GetMaxVelocity 
		Summary: Returns the scalar value of the maximum velocity of this KinematicState. 
		Returns: The scalar value of the maximum velocity of this KinematicState. */
	double KinematicState::GetMaxVelocity() const
	{
		return _maxVelocity;
	}

	/* GetAcceleration
	   Summary: Returns the acceleration of this KinematicState. 
	   Returns: The acceleration of this KinematicState. */
	const Vector2D& KinematicState::GetAcceleration() const
	{
		return _acceleration;
	}

	/* SetPosition
	   Summary: Sets the position of this KinematicState. 
	   Parameters: 
	      position: The position to set it to. */
	void KinematicState::SetPosition( const Point2D &position )
	{
		_position = position;
	}

	/* SetVelocity
	   Summary: Sets the velocity of this KinematicState. Makes sure magnitude of new velocity
		        is less than or equal to _maxVelocity.
	   Parameters: 
	      velocity: The velocity to set it to. */
	void KinematicState::SetVelocity( const Vector2D &velocity )
	{
		_velocity = velocity;

		LimitVelocity();
	}

	/* SetAcceleration
	   Summary: Sets the acceleration of this KinematicState. Makes sure magnitude of new acceleration
		        is less than or equal to _maxAcceleration.  
	   Parameters: 
	      acceleration: The acceleration to set it to. */
	void KinematicState::SetAcceleration( const Vector2D &acceleration )
	{
		_acceleration = acceleration;

		LimitAcceleration();
	}

	/* SetMaxVelocity
	   Summary: Sets the max velocity of this KinematicState. 
	   Parameters: 
	      maxVelocity: The max velocity to set it to. */
	void KinematicState::SetMaxVelocity( double maxVelocity )
	{
		_maxVelocity = maxVelocity;

		LimitVelocity();
	}

	/* SetMaxAcceleration
	   Summary: Sets the max acceleration of this KinematicState. 
	   Parameters: 
	      maxAcceleration: The max acceleration to set it to. */
	void KinematicState::SetMaxAcceleration( double maxAcceleration )
	{
		_maxAcceleration = maxAcceleration;

		LimitAcceleration();
	}

	/* Checks if _velocity has exceeded max and, if so, re-scales it to max. */
	void KinematicState::LimitVelocity()
	{
		if( _maxVelocity != 0 && _velocity.Magnitude() > _maxVelocity )
		{
			_velocity = _maxVelocity * _velocity.UnitVector();
		}
	}

	/* Checks if _acceleration has exceeded max and, if so, re-scales it to max. */
	void KinematicState::LimitAcceleration()
	{
		if( _maxAcceleration != 0 && _acceleration.Magnitude() > _maxAcceleration )
		{
			_acceleration = _maxAcceleration * _acceleration.UnitVector();
		}
	}

	/* Output operator for a KinematicState object. */
	std::ostream& operator<<( std::ostream &outputStream, const KinematicState &kinematicState )
	{
		outputStream << "Position = " << kinematicState._position << std::endl
			<< "Velocity = " << kinematicState._velocity << std::endl
			<< "Acceleration = " << kinematicState._acceleration << std::endl
			<< "Max Velocity = " << kinematicState._maxVelocity << std::endl
			<< "Max Acceleration = " << kinematicState._maxAcceleration << std::endl
			<< "Prev Position = " << kinematicState._prevPosition << std::endl 
			<< "Prev Velocity = " << kinematicState._prevVelocity;

		return outputStream;
	}

}