#pragma once

/* Defines the functionality of a generic handle class. */

namespace PROJECT_NAME
{

	template < typename ObjectType >
	class Handle
	{
	public:
		/* Default constructor initializes a handle to nothing. */
		Handle();
		/* Initializes a new handle from a pointer to a dynamically allocated object. */
		Handle( ObjectType *newObject );
		/* Support conversion between handles to different types in an inheritance hierarchy. */
		template < typename foreignType >
		Handle( const Handle< foreignType > &foreignHandle );

		/* Copy control to handle copying of _objectPtr and _useCount. */
		Handle( const Handle &copySource );
		Handle& operator=( const Handle &assignmentSource );

		/* Support dereference and arrow operators. */
		ObjectType& operator*();
		const ObjectType& operator*() const;
		ObjectType* operator->();
		const ObjectType* operator->() const;

		/* Support =,<,> by comparing underlying objects. */
		bool operator==( const Handle &rhs );
		bool operator!=( const Handle &rhs );

		/* Valid
		   Summary: Returns true if this handle points to an object, false if it points
		            to nothing. 
		   Returns: True if this handle points to an object, false if it points
		            to nothing. */
		bool Valid();

		/* Destructor to handle deletion of dynamic objects referenced by _objectPtr and _useCount. */
		~Handle();

	private:
		void DecrementUse();

		ObjectType *_objectPtr;
		std::size_t *_useCount;

		template < typename OtherType > friend class Handle;
	};

}