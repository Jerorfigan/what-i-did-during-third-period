#pragma once

/* KinematicState holds the variables and functions concerning an object in motion with constant 
   acceleration. */

#include <iostream>

#include "Point2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME
{

	class KinematicState
	{
	public:
		/* Initializes a KinematicState with position, velocity, acceleration, max velocity and max acceleration
		   set to 0. */
		KinematicState();

		/* Initializes a KinematicState from a position, velocity, acceleration, max velocity and max acceleration. */
		KinematicState( Point2D position, Vector2D velocity, 
			            Vector2D acceleration, double maxVelocity = 0, double maxAcceleration = 0 );

		/* UpdatePositionAndVelocity 
		   Summary: Updates the position and velocity using the kinematic equations and the specified time in 
		            seconds. 
		   Parameters:
		      time: The time in seconds to use in the kinematic equations. */
		void UpdatePositionAndVelocity( double time );

		/* GetPreviousPositionAndVelocity
		   Summary: Returns a copy of this KinematicState with the position and velocity set to the values they
		            had prior to the last call to UpdatePositionAndVelocity. 
		   Returns: A copy of this KinematicState with the position and velocity set to the values they
		            had prior to the last call to UpdatePositionAndVelocity. */
		KinematicState GetPreviousPositionAndVelocity() const;

		/* GetPosition
		   Summary: Returns the position of this KinematicState. 
		   Returns: The position of this KinematicState. */
		const Point2D& GetPosition() const;
		/* GetVelocity
		   Summary: Returns the velocity of this KinematicState. 
		   Returns: The velocity of this KinematicState. */
		const Vector2D& GetVelocity() const;
		/* GetMaxVelocity 
			Summary: Returns the scalar value of the maximum velocity of this KinematicState. 
			Returns: The scalar value of the maximum velocity of this KinematicState. */
		double GetMaxVelocity() const;
		/* GetAcceleration
		   Summary: Returns the acceleration of this KinematicState. 
		   Returns: The acceleration of this KinematicState. */
		const Vector2D& GetAcceleration() const;

		/* SetPosition
		   Summary: Sets the position of this KinematicState. 
		   Parameters: 
		      position: The position to set it to. */
		void SetPosition( const Point2D &position );
		/* SetVelocity
		   Summary: Sets the velocity of this KinematicState. Makes sure magnitude of new velocity
		            is less than or equal to _maxVelocity.
		   Parameters: 
		      velocity: The velocity to set it to. */
		void SetVelocity( const Vector2D &velocity );
		/* SetAcceleration
		   Summary: Sets the acceleration of this KinematicState. Makes sure magnitude of new acceleration
		            is less than or equal to _maxAcceleration. 
		   Parameters: 
		      acceleration: The acceleration to set it to. */
		void SetAcceleration( const Vector2D &acceleration );
		/* SetMaxVelocity
		   Summary: Sets the max velocity of this KinematicState. 
		   Parameters: 
		      maxVelocity: The max velocity to set it to. */
		void SetMaxVelocity( double maxVelocity );
		/* SetMaxAcceleration
		   Summary: Sets the max acceleration of this KinematicState. 
		   Parameters: 
		      maxAcceleration: The max acceleration to set it to. */
		void SetMaxAcceleration( double maxAcceleration );

	private:
		/* Checks if _velocity has exceeded max and, if so, re-scales it to max. */
		void LimitVelocity();
		/* Checks if _acceleration has exceeded max and, if so, re-scales it to max. */
		void LimitAcceleration();

		Point2D _position;
		Vector2D _velocity;
		Vector2D _acceleration;

		Point2D _prevPosition;
		Vector2D _prevVelocity;

		double _maxVelocity;
		double _maxAcceleration;

		// The value of the time parameter passed in the last call to UpdatePositionAndVelocity
		double _lastUpdateTime;

		friend std::ostream& operator<<( std::ostream &outputStream, const KinematicState &kinematicState );
	};

	/* Output operator for a KinematicState object. */
	std::ostream& operator<<( std::ostream &outputStream, const KinematicState &kinematicState );

}