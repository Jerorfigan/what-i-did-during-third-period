/* Defines the unit test for the Line2D class. */

#include "stdafx.h"

#include "Line2D.h"

namespace PROJECT_NAME_UnitTests
{
	using namespace PROJECT_NAME;

	void Line2DUnitTest()
	{
		/* Test 1: Exercise the default constructor and verify that it 
				   initializes a line with a finite slope of 0 and y-intercept of 0. */
		{
			Line2D line;
			assert( line.Slope() == 0.0 );
			assert( line.YIntercept() == 0.0 );
		}

		/* Test 2: Exercise the constructor that takes two point arguments and verify that it
				   sets _slope, _yIntercept, _xIntercept, and _finiteSlope correctly. */
		
		// Case 1: Initialize a line with a finite slope.
		{
			Line2D line( Point2D( 2, 2 ), Point2D( 4, 5 ) );
			assert( line.Slope() == 1.5 );
			assert( line.YIntercept() == -1 );
			assert( line.FiniteSlope() == true );
		}

		// Case 2: Initialize a line with an infinite slope.
		{
			Line2D line( Point2D( 2, 2 ), Point2D( 2, 5 ) );
			assert( line.XIntercept() == 2 );
			assert( line.FiniteSlope() == false );
		}

		/* Test 3: Exercise Line2D::Contains function and verify that it determines whether a line 
		           contains a point correctly. */
		
		// Case 1: Call Line2D::Contains on a line with finite slope.
		{
			Line2D line( Point2D( 1, 3 ), Point2D( 6, 8 ) );
			assert( line.Contains( Point2D( 2, 4 ) ) );
			assert( !line.Contains( Point2D( 2, -5 ) ) );
		}

		// Case 2: Call Line2D::Contains on a line with infinite slope.
		{
			Line2D line( Point2D( 1, 3 ), Point2D( 1, 10 ) );
			assert( line.Contains( Point2D( 1, 50 ) ) );
			assert( !line.Contains( Point2D( 2, 15 ) ) );
		}

		/* Test 4: Exercise Line2D::Intersects function and verify that it determines whether two lines
		           intersect correctly. */

		// Case 1: Lines are parallel with finite slopes.
		{
			Line2D line1( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			Line2D line2( Point2D( 1, 5 ), Point2D( 2, 6 ) );
			Point2D intersectionPoint;
			assert( !line1.Intersects( line2, intersectionPoint ) );
		}

		// Case 2: Lines are the same with finite slopes.
		{
			Line2D line1( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			Line2D line2( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			Point2D intersectionPoint;
			assert( !line1.Intersects( line2, intersectionPoint ) );
		}

		// Case 3: Both lines have unequal finite slopes.
		{
			Line2D line1( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			Line2D line2( Point2D( 1, 2 ), Point2D( 2, 1 ) );
			Point2D intersectionPoint;
			assert( line1.Intersects( line2, intersectionPoint ) );
			assert( intersectionPoint == Point2D( 1.5, 1.5 ) );
		}

		// Case 4: Line 1 has an infinite slope and line 2 has a finite slope
		{
			Line2D line1( Point2D( 3, 1 ), Point2D( 3, 2 ) );
			Line2D line2( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			Point2D intersectionPoint;
			assert( line1.Intersects( line2, intersectionPoint ) );
			assert( intersectionPoint == Point2D( 3, 3 ) );
		}

		// Case 5: Line 1 has a finite slope and line 2 has an infinite slope
		{
			Line2D line1( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			Line2D line2( Point2D( 3.75, 1 ), Point2D( 3.75, 5 ) );
			Point2D intersectionPoint;
			assert( line1.Intersects( line2, intersectionPoint ) );
			assert( intersectionPoint == Point2D( 3.75, 3.75 ) );
		}

		// Case 6: Line 1 and line 2 have infinite slopes and are the same line
		{
			Line2D line1( Point2D( 3.75, 1 ), Point2D( 3.75, 2 ) );
			Line2D line2( Point2D( 3.75, 1 ), Point2D( 3.75, 5 ) );
			Point2D intersectionPoint;
			assert( !line1.Intersects( line2, intersectionPoint ) );
		}

		// Case 7: Line 1 and line 2 have infinite slopes and are different lines
		{
			Line2D line1( Point2D( 17, 1 ), Point2D( 17, 2 ) );
			Line2D line2( Point2D( 3.75, 1 ), Point2D( 3.75, 5 ) );
			Point2D intersectionPoint;
			assert( !line1.Intersects( line2, intersectionPoint ) );
		}

		/* Test 5: Exercise Line2D::operator== function and verify it determines equality correctly. */
		
		// Case 1: Lines are equal with finite slopes
		{
			Line2D line1( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			Line2D line2( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			assert( line1 == line2 );
		}

		// Case 2: Lines are unequal with finite slopes
		{
			Line2D line1( Point2D( 1, 7 ), Point2D( 2, 2 ) );
			Line2D line2( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			assert( !( line1 == line2 ) );
		}

		// Case 3: Lines are equal with infinite slopes
		{
			Line2D line1( Point2D( 1, 7 ), Point2D( 1, 2 ) );
			Line2D line2( Point2D( 1, 4 ), Point2D( 1, 10 ) );
			assert( line1 == line2 );
		}

		// Case 4: Lines are unequal with infinite slopes
		{
			Line2D line1( Point2D( 3, 7 ), Point2D( 3, 2 ) );
			Line2D line2( Point2D( 1, 4 ), Point2D( 1, 10 ) );
			assert( !( line1 == line2 ) );
		}

		/* Test 6: Exercise Line2D::operator!= function and verify it determines correctly. */
		
		// Case 1: Lines are equal with finite slopes
		{
			Line2D line1( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			Line2D line2( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			assert( !( line1 != line2 ) );
		}

		// Case 2: Lines are unequal with finite slopes
		{
			Line2D line1( Point2D( 1, 7 ), Point2D( 2, 2 ) );
			Line2D line2( Point2D( 1, 1 ), Point2D( 2, 2 ) );
			assert( line1 != line2 );
		}

		// Case 3: Lines are equal with infinite slopes
		{
			Line2D line1( Point2D( 1, 7 ), Point2D( 1, 2 ) );
			Line2D line2( Point2D( 1, 4 ), Point2D( 1, 10 ) );
			assert( !( line1 != line2 ) );
		}

		// Case 4: Lines are unequal with infinite slopes
		{
			Line2D line1( Point2D( 3, 7 ), Point2D( 3, 2 ) );
			Line2D line2( Point2D( 1, 4 ), Point2D( 1, 10 ) );
			assert( line1 != line2 );
		}
	}

}