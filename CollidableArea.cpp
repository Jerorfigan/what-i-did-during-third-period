/* CollidableArea implementation. */

#include "stdafx.h"

#include "CollidableArea.h"

namespace PROJECT_NAME
{

	/* Initializes a CollidableArea with a minimum bounding rectangle with x and y limits of 0. */
	CollidableArea::CollidableArea() : _minX( 0 ), _maxX( 0 ), _minY( 0 ), _maxY( 0 )
	{

	}

	/* GetMinX
	   Summary: Gets the minimum x value of the minimum bounding rectangle for this CollidableArea. 
	   Returns: The minimum x value of the minimum bounding rectangle for this CollidableArea.  */
	const double& CollidableArea::GetMinX() const
	{
		return _minX;
	}

	/* GetMaxX
	   Summary: Gets the maximum x value of the minimum bounding rectangle for this CollidableArea. 
	   Returns: The maximum x value of the minimum bounding rectangle for this CollidableArea.  */
	const double& CollidableArea::GetMaxX() const
	{
		return _maxX;
	}

	/* GetMinY
	   Summary: Gets the minimum y value of the minimum bounding rectangle for this CollidableArea. 
	   Returns: The minimum y value of the minimum bounding rectangle for this CollidableArea.  */
	const double& CollidableArea::GetMinY() const
	{
		return _minY;
	}

	/* GetMaxY
	   Summary: Gets the maximum y value of the minimum bounding rectangle for this CollidableArea. 
	   Returns: The maximum y value of the minimum bounding rectangle for this CollidableArea.  */
	const double& CollidableArea::GetMaxY() const
	{
		return _maxY;
	}

	/* Virtual destructor to allow deletion of a derived object through pointer to CollidableArea. */
	CollidableArea::~CollidableArea()
	{
	}

}