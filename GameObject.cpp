/* GameObject implementation. */

#include "stdafx.h"

#include "GameObject.h"

namespace PROJECT_NAME
{

	/* Initialize a GameObject from an ObjectId. */
	GameObject::GameObject( ObjectId id ) : _id( id )
	{

	}

	/* Id 
	   Summary: Returns the id of this GameObject. 
	   Returns: The id of this GameObject. */
	const GameObject::ObjectId& GameObject::Id() const
	{
		return _id;
	}

	/* Virtual destructor to allow deletion of derived objects through GameObject pointer. */
	GameObject::~GameObject()
	{

	}

}