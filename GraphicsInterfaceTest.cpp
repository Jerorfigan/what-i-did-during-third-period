/* Defines the unit test for the graphics interface. */

#include "stdafx.h"

#include "Game.h"
#include "IGraphicsProvider.h"
#include "Point2D.h"
#include "Rectangle2D.h"
#include "Vector2D.h"
#include "GameEvent.h"

namespace PROJECT_NAME_UnitTests
{
	using namespace PROJECT_NAME;

	void GraphicsInterfaceTest()
	{
		/* Test 1: Exercise the graphics interface by creating a render window and drawing to it. */

		// Register services so that we have a graphics provider we can use for this test
		Game::RegisterServices();

		// Initialize a render window
		std::size_t windowWidth = 800;
		std::size_t windowHeight = 600;
		Rectangle2D gameSpace( Point2D( 400, 300 ), 800, 600 );
		IGraphicsProvider::ResourceId mainWindow = 
			Game::GetGameServiceManager().GetGraphicsProvider()->InitializeWindow( gameSpace, "Test Window", windowWidth, windowHeight, false );

		// Initialize a sprite
		IGraphicsProvider::ResourceId tiger1 = 
			Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( "Art/tiger1.jpg" );
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableRotation( tiger1, 45.0 );

		// Set the sprite center
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( tiger1, Point2D( 22, 25 ) );
		// Set the sprite subrect
		Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteSubRectangle( tiger1, 
			Rectangle2D( Point2D( 244, 100 ), Point2D( 288, 100 ), Point2D( 244, 50 ), Point2D( 288, 50 ) ) ); 
		// Set the sprite position
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( tiger1, Point2D( 22, 25 ) );

		// Verify the sprite's subrect
		Rectangle2D tiger1subRect = Game::GetGameServiceManager().GetGraphicsProvider()->GetSpriteSubRectangle( tiger1 );
		assert( tiger1subRect.Left() == 244 );
		assert( tiger1subRect.Right() == 288 );
		assert( tiger1subRect.Top() == 100 );
		assert( tiger1subRect.Bottom() == 50 );

		// Initialize another sprite and repeat the processing done on the first sprite
		IGraphicsProvider::ResourceId tiger2 = 
			Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( "Art/tiger2.jpg" );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( tiger2, Point2D( 65, 55 ) );
		Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteSubRectangle( tiger2, 
			Rectangle2D( Point2D( 170, 250 ), Point2D( 300, 250 ), Point2D( 170, 140 ), Point2D( 300, 140 ) ) ); 
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( tiger2, Point2D( windowWidth - 65, windowHeight - 55 ) );

		Rectangle2D tiger2subRect = Game::GetGameServiceManager().GetGraphicsProvider()->GetSpriteSubRectangle( tiger2 );
		assert( tiger2subRect.Left() == 170 );
		assert( tiger2subRect.Right() == 300 );
		assert( tiger2subRect.Top() == 250 );
		assert( tiger2subRect.Bottom() == 140 );

		// Initialize a third sprite using the same image as the second sprite
		IGraphicsProvider::ResourceId tiger3 = 
			Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( "Art/tiger2.jpg" );

		// Initialize some text
		IGraphicsProvider::ResourceId tiger1Speech = 
			Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString( "Imma lonely tiger!!!!1" );
		// Set text position
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( tiger1Speech, Point2D( 44, 55 ) );

		// Initialize some more text
		IGraphicsProvider::ResourceId tiger2Speech = 
			Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString( "..." );
		// Set text position
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( tiger2Speech, Point2D( windowWidth, windowHeight - 55 ) );

		Vector2D tiger1Velocity( 50, 50 );
		Vector2D tiger2Velocity( -70, -70 );

		double timeElapsed = 0.0;
		bool testFinished = false;
		while( !testFinished )
		{
			// Check for window close event
			GameEvent gameEvent;
			while( Game::GetGameServiceManager().GetGraphicsProvider()->GetGameEvent( mainWindow, gameEvent ) )
			{
				if( gameEvent._event == GameEvent::Closed )
				{
					testFinished = true;
				}
			}

			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableRotation( tiger1, 45.0 );

			// Prepare window for drawing
			Game::GetGameServiceManager().GetGraphicsProvider()->PrepareWindowForDrawing( mainWindow );

			// Update tiger sprites and speech
			if( Game::GetGameServiceManager().GetGraphicsProvider()->IsKeyDown( mainWindow, KeyboardKey::Up ) ||
				timeElapsed > 5.0 )
			{
				Game::GetGameServiceManager().GetGraphicsProvider()->SetString( tiger1Speech, "Oh hai! I didnt see u there" );
				Game::GetGameServiceManager().GetGraphicsProvider()->SetString( tiger2Speech, "LOLZ!!!! u teh small one :-)" );
			}	

		    Point2D tiger1Pos = Game::GetGameServiceManager().GetGraphicsProvider()->GetDrawablePosition( tiger1 );
			Point2D tiger1NewPos = tiger1Pos + Game::GetGameServiceManager().GetGraphicsProvider()->GetFrameTime( mainWindow ) * tiger1Velocity;
			if( tiger1NewPos._x < 22 || tiger1NewPos._x > windowWidth - 22 ) tiger1Velocity._x *= -1;
			if( tiger1NewPos._y < 25 || tiger1NewPos._y > windowHeight - 25 ) tiger1Velocity._y *= -1;
			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( tiger1, tiger1NewPos );

			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( tiger1Speech,
				Game::GetGameServiceManager().GetGraphicsProvider()->GetDrawablePosition( tiger1Speech ) + 
				Game::GetGameServiceManager().GetGraphicsProvider()->GetFrameTime( mainWindow ) * tiger1Velocity );

			Point2D tiger2Pos = Game::GetGameServiceManager().GetGraphicsProvider()->GetDrawablePosition( tiger2 );
			Point2D tiger2NewPos = tiger2Pos + Game::GetGameServiceManager().GetGraphicsProvider()->GetFrameTime( mainWindow ) * tiger2Velocity;
			if( tiger2NewPos._x < 65 || tiger2NewPos._x > windowWidth - 55 ) tiger2Velocity._x *= -1;
			if( tiger2NewPos._y < 65 || tiger2NewPos._y > windowHeight - 55 ) tiger2Velocity._y *= -1;
			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( tiger2, tiger2NewPos );

			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( tiger2Speech,
				Game::GetGameServiceManager().GetGraphicsProvider()->GetDrawablePosition( tiger2Speech ) + 
				Game::GetGameServiceManager().GetGraphicsProvider()->GetFrameTime( mainWindow ) * tiger2Velocity );

			// Draw tiger sprites
			Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( tiger1, mainWindow );
			Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( tiger2, mainWindow );
			Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( tiger1Speech, mainWindow );
			Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( tiger2Speech, mainWindow );

			Game::GetGameServiceManager().GetGraphicsProvider()->DisplayWindow( mainWindow );

			timeElapsed += Game::GetGameServiceManager().GetGraphicsProvider()->GetFrameTime( mainWindow );
		}

		// Destroy tiger sprites and speech
		Game::GetGameServiceManager().GetGraphicsProvider()->DestroyDrawable( tiger1 );
		Game::GetGameServiceManager().GetGraphicsProvider()->DestroyDrawable( tiger2 );
		Game::GetGameServiceManager().GetGraphicsProvider()->DestroyDrawable( tiger1Speech );
		Game::GetGameServiceManager().GetGraphicsProvider()->DestroyDrawable( tiger2Speech );
	}
}