/* UIEventQueue implementation. */

#include "stdafx.h"

#include "GameEventQueue.h"

namespace PROJECT_NAME
{
	// Methods

	void GameEventQueue::PushEvent( const GameEvent &gameEvent )
	{
		mEventQueue.push( gameEvent );
	}

	GameEvent GameEventQueue::PopEvent()
	{
		GameEvent temp;

		if( !IsEmpty() )
		{
			temp = mEventQueue.front();
			mEventQueue.pop();
		}

		return temp;
	}
	
	bool GameEventQueue::IsEmpty()
	{
		return mEventQueue.empty();
	}

}