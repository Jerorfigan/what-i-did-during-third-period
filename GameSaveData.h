#pragma once

/* Structure that holds all the game save data. */

#include <fstream>

namespace PROJECT_NAME
{

	struct GameSaveData
	{
		std::size_t currentMapIndex;
	};

	/* Filestream input/output operations. */

	std::fstream& operator>>( std::fstream& fileStream, GameSaveData& gameSaveData );
	std::fstream& operator<<( std::fstream& fileStream, const GameSaveData& gameSaveData );
}