#pragma once

/* Defines the functionality for a 2D vector. */

#include <iostream>

namespace PROJECT_NAME
{
	struct Point2D;

	struct Vector2D
	{
		/* Default constructor initializes a vector with _x and _y equal to 0. */
		Vector2D();
		/* Constructor that initializes a vector from passed in x and y values. */
		Vector2D( double x, double y);
		/* Constructor that initializes a vector from a point. */
		Vector2D( const Point2D &point );
		/* Initializes a vector from two points: one denoting the vector's origin, 
		   and the other its target. */
		Vector2D( const Point2D &origin, const Point2D &target );

		/* Magnitude
		   Summary: Calculates the magnitude of this vector. 
		   Returns: The magnitude of this vector. */
		double Magnitude() const;
		/* UnitVector
		   Summary: Calculates the unit vector for this vector. 
		   Returns: The unit vector for this vector. */
		Vector2D UnitVector() const;
		/* Angle
		   Summary: Calculates the angle in degrees of this vector, where 0 degrees corresponds to the 
		   angle of a vector following the positive x-axis. 
		   Returns: The angle of this vector. */
		double Angle() const;
		/* IsZero
		   Summary: Returns true if this is a zero vector, false otherwise. 
		   Returns: True if this is a zero vector, false otherwise. */
		bool IsZero() const;

		/* ProjectOnto
		   Summary: Calculates the vector projection of this vector onto target vector. 
		   Parameters: 
			  target: The vector to project this vector onto.
		   Returns: Returns the vector projection of this vector onto target vector. */
		Vector2D ProjectOnto( const Vector2D &target ) const;
		/* ReflectOver
		   Summary: Calculates the reflection of this vector over an arbitrary axis.
		   Parameters:
		      axis: A Vector2D denoting the axis for reflection. 
		   Returns: The reflection of this vector over an arbitrary axis. */
		Vector2D ReflectOver( const Vector2D &axis ) const;
		/* Rotate
		   Summary: Rotates the vector about the origin.
		   Parameteres:
		      degrees: The angle of rotation (+ goes counter clockwise, - clockwise ) */
		void Rotate( double degrees );

		/* operator: += 
		   Summary: Adds a vector to this vector and stores the result in this vector.
		   Parameters: 
		      vector: The vector to add to this vector. 
		   Returns: A reference to this vector. */
		Vector2D& operator+=( const Vector2D &vector );

		double _x;
		double _y;
	};

	/* Declaration of * operator for left-hand argument of type double and right-hand argument of type
	   Vector2D. */
	Vector2D operator*( const double& scalar, const Vector2D& vector ); 

	/* Declaration of + operator for left-hand argument of type Vector2D and right-hand argument of type
	   Vector2D. */
	Vector2D operator+( const Vector2D &lhsVec, const Vector2D &rhsVec );
	/* Declaration of - operator for left-hand argument of type Vector2D and right-hand argument of type
	   Vector2D. */
	Vector2D operator-( const Vector2D &lhsVec, const Vector2D &rhsVec );

	/* Equality operator for two vectors. */
	bool operator==( const Vector2D &lhsVec, const Vector2D &rhsVec );
	/* Inequality operator for two vectors. */
	bool operator!=( const Vector2D &lhsVec, const Vector2D &rhsVec );

	/* Output operator for a Vector2D object. */
	std::ostream& operator<<( std::ostream &outputStream, const Vector2D &vector );
}