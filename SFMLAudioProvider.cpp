/* SFMLAudioProvider implementation. */

#include "stdafx.h"

#include "SFMLAudioProvider.h"
#include "SoundFileCache.h"

namespace PROJECT_NAME
{
	// Constructors

	SFMLAudioProvider::SFMLAudioProvider() :
	mCurrentSongId( 0 ), mResourceIdSeq( 1 ), mMaxSoundVolume( 100 ), mMaxMusicVolume( 100 )
	{
	}

	// Methods

	bool SFMLAudioProvider::ValidSound( AudioId id )
	{
		SoundIdToSoundData::const_iterator findResult = mSoundMap.find( id );

		return findResult != mSoundMap.end();
	}

	bool SFMLAudioProvider::ValidMusic( AudioId id )
	{
		SoundIdToSoundData::const_iterator findResult = mMusicMap.find( id );

		return findResult != mMusicMap.end();
	}


	// Virtual methods

	SFMLAudioProvider::AudioId SFMLAudioProvider::InitSound( std::string filePath, bool isMusic )
	{
		SoundData data;
		data.mSoundFilePath = filePath;
		data.mRawSoundVolume = 100.0;

		if( isMusic )
		{
			mMusicMap[ mResourceIdSeq ] = data;
			// Call GetSong on the cache object to get the song loaded into the cache.
			mSoundFileCache.GetSong( filePath );
		}
		else
		{
			mSoundMap[ mResourceIdSeq ] = data;
			// Call GetSound on the cache object to get the sound loaded into the cache.
			mSoundFileCache.GetSound( filePath );	
		}

		return mResourceIdSeq++;
	}

	void SFMLAudioProvider::PlaySound( AudioId id, float volume, bool looping )
	{
		if( ValidSound( id ) )
		{
			// Remove sounds from current sounds that are no longer playing.
			SoundIdToSoundMap currentSoundsCopy( mCurrentSounds );

			for( SoundIdToSoundMap::const_iterator currentSoundsCopyItr = currentSoundsCopy.begin();
				currentSoundsCopyItr != currentSoundsCopy.end(); ++currentSoundsCopyItr )
			{
				if( currentSoundsCopyItr->second.GetStatus() == sf::Sound::Stopped )
				{
					mCurrentSounds.erase( currentSoundsCopyItr->first );
				}
			}

			// If all sound channels are in use, do nothing for now
			if( mCurrentSounds.size() < MAX_SOUND_CHANNELS )
			{
				mCurrentSounds.insert( std::make_pair( id, mSoundFileCache.GetSound( mSoundMap[ id ].mSoundFilePath ) ) );
				mCurrentSounds[ id ].SetLoop( looping );
				// Scale volume by current max.
				mCurrentSounds[ id ].SetVolume( volume * mMaxSoundVolume / 100.0f );
				mCurrentSounds[ id ].Play();

				// Save off the raw sound volume which we'll use later when scaling the currently playing sounds/music
				// when the volume settings are changed.
				mSoundMap[ id ].mRawSoundVolume = volume;
			}
		}
		else if( ValidMusic( id ) )
		{
			sf::Music * currentSong;
			try
			{
				currentSong = mSoundFileCache.GetSong( mMusicMap[ id ].mSoundFilePath );
			}
			catch(SoundNotFoundExeception&)
			{
				// This one is dire, means we couldn't find or load the selected song
				// So, lets exit!
				return;
			}
			// See if prior song is playing still, if so, stop it
			if( mCurrentSongId != 0 )
			{
				try
				{
					sf::Music* priorSong = mSoundFileCache.GetSong( mMusicMap[ mCurrentSongId ].mSoundFilePath );
					if(priorSong->GetStatus() != sf::Sound::Stopped)
					{
						priorSong->Stop();
					}
				}
				catch(SoundNotFoundExeception&)
				{
					// Do nothing, this exception isn't dire.  It simply means the previous sound we were
					// trying to stop wasn't located.
				}
		
			}
			mCurrentSongId = id;
			currentSong->SetLoop(looping);
			currentSong->SetVolume( volume * mMaxMusicVolume / 100.0f );
			currentSong->Play();

			// Save off the raw sound volume which we'll use later when scaling the currently playing sounds/music
			// when the volume settings are changed.
			mMusicMap[ id ].mRawSoundVolume = volume;
		}
	
	}

	void SFMLAudioProvider::SetMaxVolume( float volume, int flags )
	{
		if( flags & SoundVolume )
		{
			mMaxSoundVolume = volume;
		}
		if( flags & MusicVolume )
		{
			mMaxMusicVolume = volume;
		}

		// Re-scale the currently playing sounds/music according to the new maxes.
		for( SoundIdToSoundMap::iterator currentSoundsItr = mCurrentSounds.begin();
				currentSoundsItr != mCurrentSounds.end(); ++currentSoundsItr )
		{
			currentSoundsItr->second.SetVolume( ( mSoundMap[ currentSoundsItr->first ].mRawSoundVolume / 100.0f ) * mMaxSoundVolume );
		}
		
		if( mCurrentSongId != 0 )
		{
			sf::Music * currentSong = mSoundFileCache.GetSong( mMusicMap[ mCurrentSongId ].mSoundFilePath );

			currentSong->SetVolume( ( mMusicMap[ mCurrentSongId ].mRawSoundVolume / 100.0f ) * mMaxMusicVolume );
		}
	}

	void SFMLAudioProvider::StopSound( AudioId id )
	{
		if( ValidSound( id ) )
		{
			SoundIdToSoundMap::iterator findResult = mCurrentSounds.find( id );

			if( findResult != mCurrentSounds.end() )
			{
				findResult->second.Stop();
			}
		}
	}

	void SFMLAudioProvider::StopAllSounds() 
	{
		for(int i = 0; i < MAX_SOUND_CHANNELS; i++)
		{
			mCurrentSounds[i].Stop();
		}
	
		if( mCurrentSongId != 0 )
		{
			sf::Music * currentSong = mSoundFileCache.GetSong( mMusicMap[ mCurrentSongId ].mSoundFilePath );
			if(currentSong->GetStatus() == sf::Sound::Playing)
			{
				currentSong->Stop(); 
			}
		}
	}
	
	void SFMLAudioProvider::DestroySound( AudioId id )
	{
		if( ValidSound( id ) )
		{
			mSoundMap.erase( id );
		}
		else if( ValidMusic( id ) )
		{
			mMusicMap.erase( id );
		}
	}

	bool SFMLAudioProvider::IsSoundPlaying( AudioId id )
	{
		if( ValidSound( id ) )
		{
			SoundIdToSoundMap::iterator findResult = mCurrentSounds.find( id );

			if( findResult != mCurrentSounds.end() )
			{
				return findResult->second.GetStatus() == sf::Sound::Playing;
			}
		}
		else if( mCurrentSongId == id )
		{
			return mSoundFileCache.GetSong( mMusicMap[ mCurrentSongId ].mSoundFilePath )->GetStatus() == sf::Music::Playing;	
		}
		return false;
	}


	bool SFMLAudioProvider::IsSoundLooping( AudioId id )
	{
		if( ValidSound( id ) )
		{
			SoundIdToSoundMap::iterator findResult = mCurrentSounds.find( id );

			if( findResult != mCurrentSounds.end() )
			{
				return findResult->second.GetLoop();
			}
		}
		else if( mCurrentSongId == id )
		{
			return mSoundFileCache.GetSong( mMusicMap[ mCurrentSongId ].mSoundFilePath )->GetLoop();	
		}
		return false;
	}

}