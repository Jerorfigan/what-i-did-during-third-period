#pragma once

/* Defines the functionality of a text box that the user may enter text into. */

#include <string>

#include "IGraphicsProvider.h"
#include "Rectangle2D.h"
#include "UserControl.h"

namespace PROJECT_NAME
{

	class TextBox : public UserControl
	{
		/* Constructors */
	public:
		TextBox( std::string id, unsigned int menuIndex, std::string labelImage, Rectangle2D textRect,
			void (*onKeyboardChar)(void*, TextBox*) = 0, bool allowSpaces = false );

		/* Methods */
	public:
		void RepositionCursor();
		std::string GetContents() const;

	private:
		bool IsValid( char &c );

		/* Virtual methods */
	public:
		virtual void Init( void* menuData );

		virtual void Draw( void* menuData );

		virtual void OnHoverOver( void* menuData );
		virtual void OnHoverAway( void* menuData );
		virtual void OnActivation( void* menuData );
		virtual void OnDeactivation( void* menuData );
		virtual void OnGainedFocus( void* menuData );
		virtual void OnLostFocus( void* menuData );
		virtual void OnKeyboardChar( void* menuData, char typedChar );

		virtual bool IsTargettedByMouse( Point2D mouseHotspot );

		/* Data */
	private:
		Rectangle2D mTextRect;
		std::string mContents;
		IGraphicsProvider::ResourceId mLabel;
		IGraphicsProvider::ResourceId mCursor;
		IGraphicsProvider::ResourceId mText;
		bool mHasFocus;
		int mCursorOffset;
		bool mAllowSpaces;

		void (*mOnKeyboardChar)(void*, TextBox*);

		/* Static data */
	private:
		static const std::string mBoxImageFile;
		static const std::string mCursorImageFile;
		static const int mCharacterLimit = 15;
		static const int mLabelOffsetX = 20; // pixels
		static const int mTextOffsetX = 20; // pixels
		static const int mTextOffsetY = 15; // pixels
		static const char mValidCharacters[];
	};

}