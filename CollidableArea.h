#pragma once

/* CollidableArea represents the abstract area of an object that can be collided with. */

#include <vector>

#include "Handle.h"
#include "Handle.cpp"
#include "LineSegment2D.h"

namespace PROJECT_NAME
{

	class CollidableArea
	{
	public:
		/* Initializes a CollidableArea with a minimum bounding rectangle with x and y limits of 0. */
		CollidableArea();

		/* CollidesWith 
		   Summary: Returns true if this CollidableArea overlaps otherArea. 
		   Returns: True if this CollidableArea overlaps otherArea. */
		virtual bool CollidesWith( const CollidableArea* otherArea ) const = 0;

		/* GetLineSegments 
		   Summary: Returns a vector filled with the line segments that comprise this CollidableArea. 
		   Returns: A vector filled with the line segments that comprise this CollidableArea. */
		virtual std::vector< LineSegment2D > GetLineSegments() const = 0;

		/* UpdatePosition
		   Summary: Updates the position of this CollidableArea given a reference point (ex: the center of the area). 
		   Parameters:
		      referencePoint: The reference point indicating how to update the position of the CollidableArea. */
		virtual void UpdatePosition( const Point2D &referencePoint ) = 0;

		/* GetMinX
		   Summary: Gets the minimum x value of the minimum bounding rectangle for this CollidableArea. 
		   Returns: The minimum x value of the minimum bounding rectangle for this CollidableArea.  */
		const double& GetMinX() const;
		/* GetMaxX
		   Summary: Gets the maximum x value of the minimum bounding rectangle for this CollidableArea. 
		   Returns: The maximum x value of the minimum bounding rectangle for this CollidableArea.  */
		const double& GetMaxX() const;
		/* GetMinY
		   Summary: Gets the minimum y value of the minimum bounding rectangle for this CollidableArea. 
		   Returns: The minimum y value of the minimum bounding rectangle for this CollidableArea.  */
		const double& GetMinY() const;
		/* GetMaxY
		   Summary: Gets the maximum y value of the minimum bounding rectangle for this CollidableArea. 
		   Returns: The maximum y value of the minimum bounding rectangle for this CollidableArea.  */
		const double& GetMaxY() const;

		/* Virtual destructor to allow deletion of a derived object through pointer to CollidableArea. */
		virtual ~CollidableArea();
	protected:
		// The min/max x and y values of the minimum bounding rectangle for this CollidableArea.
		double _minX;
		double _maxX;
		double _minY;
		double _maxY;
	};

}