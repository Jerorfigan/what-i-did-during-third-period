/* LevelIndicator implementation. */

#include "stdafx.h"

#include "Game.h"
#include "LevelIndicator.h"

namespace PROJECT_NAME
{

	// Constructors
	
	LevelIndicator::LevelIndicator( std::string info ) : GameObject( "level_indicator" ),
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString( info ) , "level_indicator" )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( GraphicsId(), 
			mPosition );
	}

	// Virtual methods
	
	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void LevelIndicator::Draw( double interpolation )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(),
			Game::GetGameAssetManager().GetAppWindow() );
	}

	// Static data

	const Point2D LevelIndicator::mPosition( 1400, 75 );
}