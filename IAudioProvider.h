#pragma once

/* Defines the abstract functionality of an audio provider. */

#include <string>

namespace PROJECT_NAME
{

	class IAudioProvider
	{ 
		// Enumerations
	public:
		enum VolumeFlags
		{
			SoundVolume = 1,
			MusicVolume = 2
		};

		// Typedefs
	public:
		typedef std::size_t AudioId;

		// Destructor.
	public:
	  virtual ~IAudioProvider();

	  // Pure virtual methods.
	public:
	  virtual AudioId InitSound( std::string filePath, bool isMusic = false ) = 0;
	  virtual void PlaySound( AudioId id, float volume = 100, bool looping = false ) = 0;
	  virtual void SetMaxVolume( float volume, int flags ) = 0;
	  virtual void StopSound( AudioId id ) = 0;
	  virtual void StopAllSounds() = 0;
	  virtual void DestroySound( AudioId id ) = 0;

	  virtual bool IsSoundPlaying( AudioId id ) = 0;
	  virtual bool IsSoundLooping( AudioId id ) = 0;

	};

}