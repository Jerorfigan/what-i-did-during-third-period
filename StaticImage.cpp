/* StaticImage implementation. */

#include "stdafx.h"

#include "Game.h"
#include "StaticImage.h"

namespace PROJECT_NAME
{

	// Constructors

	StaticImage::StaticImage( std::string imageFile, Point2D position ) : 
		UserControl( "static", -1, Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( imageFile ) )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mGraphicsId, position );
	}

	// Virtual methods

	void StaticImage::Init( void* menuData )
	{

	}

	void StaticImage::Draw( void* menuData )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mGraphicsId, 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	void StaticImage::OnHoverOver( void* menuData )
	{

	}

	void StaticImage::OnHoverAway( void* menuData )
	{

	}

	void StaticImage::OnActivation( void* menuData )
	{

	}

	void StaticImage::OnDeactivation( void* menuData )
	{

	}

	void StaticImage::OnGainedFocus( void* menuData )
	{

	}

	void StaticImage::OnLostFocus( void* menuData )
	{

	}

	void StaticImage::OnKeyboardChar( void* menuData, char typedChar )
	{

	}

	bool StaticImage::IsTargettedByMouse( Point2D mouseHotspot )
	{
		return false;
	}

}