#pragma once

/* All global defines go here */

#define PROJECT_NAME WhatIDidDuringThirdPeriod
#define PI 3.14159
#define ACCELERATION_OF_GRAVITY 220.0 // in pixels per second per second
#define MAX_UPDATE_RATE 300.0
#define MIN_UPDATE_RATE 15.0
#define RESOURCE_PATH( x ) std::string("../") + std::string( x )