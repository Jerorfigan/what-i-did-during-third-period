/* GameServiceManager implementation. */

#include "stdafx.h"

#include "GameServiceManager.h"

namespace PROJECT_NAME
{
	/* GetGraphicsProvider 
	   Summary: Returns a handle to the graphics provider. 
	   Returns: A handle to the graphics provider. */
	Handle< IGraphicsProvider > GameServiceManager::GetGraphicsProvider()
	{
		return _graphicsProvider;
	}

	/* SetGraphicsProvider 
	   Summary: Establishes the service provider that will act as graphics provider. 
	   Parameters:
	      graphicsProvider: A handle to the service provider that will act as graphics provider. */
	void GameServiceManager::SetGraphicsProvider( Handle< IGraphicsProvider > graphicsProvider )
	{
		_graphicsProvider = graphicsProvider;
	}

	/* GetAudioProvider 
		Summary: Returns a handle to the audio provider. 
		Returns: A handle to the audio provider. */
	Handle< IAudioProvider > GameServiceManager::GetAudioProvider()
	{
		return _audioProvider;
	}

	/* SetAudioProvider 
		Summary: Establishes the service provider that will act as audio provider. 
		Parameters:
		    audioProvider: A handle to the service provider that will act as audio provider. */
	void GameServiceManager::SetAudioProvider( Handle< IAudioProvider > audioProvider )
	{
		_audioProvider = audioProvider;
	}
}