#pragma once

/* WreckingBall defines the functionality for a pendulum-based trap in the game world. */

#include "Box2D/Box2D.h"
#include "CollidableCircle.h"
#include "CollidableGameObject.h"
#include "DynamicGameObject.h"
#include "GameObject.h"
#include "Point2D.h"
#include "Rectangle2D.h"
#include "SpriteAnimator.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class WreckingBall : public CollidableGameObject, public DynamicGameObject, public VisibleGameObject
	{
	public:
		/* Initializes a dummy WreckingBall. Used when getting all objects of type WreckingBall from 
		   GameObjectManager. */
		WreckingBall(); 

		/* Initializes a WreckingBall from an object id, point of rotation, starting angle, and rope length. */
		WreckingBall( GameObject::ObjectId objectId, Point2D rotationPoint, double startingAngle, double ropeLength );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea();

		/* Update
		   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
					last update (which should be accounted for in this update). 
		   Parameters: 
			  elapsedTime: The time elapsed in seconds since last update. */
		virtual void Update( double elapsedTime );

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

		/* Deletes the Box2D simulated components of the WreckingBall. */
		~WreckingBall();

	private:
		/* Subclass used to draw the rope component of the WreckingBall. */
		struct WreckingBallRope : public VisibleGameObject
		{
			WreckingBallRope( Point2D rotationPoint, double ropeLength );

			/* Draw
			   Summary: Draws this VisibleGameObject. 
			   Parameters: 
				  interpolation: An interpolation value between 0 and 1 that gives derived classes 
								 the opportunity to interpolate their drawing methods to improve
								 animations of fast moving objects. */
			virtual void Draw( double interpolation );

			Point2D _rotationPoint;
			double _angle;
			double _prevAngle;
			Rectangle2D _subRect;
			SpriteAnimator _spriteAnimator;
		};

		/* Initializes the Box2D simulated components of the WreckingBall. */
		void InitializeSimulatedComponents();

		Point2D _rotationPoint;
		Point2D _ballPosition;
		Point2D _prevBallPosition;
		double _ropeLength;
		CollidableCircle _collidableArea;
		b2Body *_dynamicBall;
		b2Body *_kinematicBall;
		WreckingBallRope _wreckingBallRope;
		b2Vec2 _maxVelocity;
		bool _maxVelocityFound;
		bool _ballIsVertical;

		static const int _wreckingBallRopeWidth = 5; // in pixels
		// _wreckingBallRopeTruncation is the number of pixels to truncate the wrecking ball 
		// rope to keep it from overlapping the wrecking ball.
		static const int _wreckingBallRopeTruncation = 20; 
		static const int _spriteWidth = 45; // in pixels
		static const int _spriteHeight = 45; // in pixels
		static const double _ballRadius; // in pixels
	};

}