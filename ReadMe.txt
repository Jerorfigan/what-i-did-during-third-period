 ===============================
|What I Did During Third Period |
 ===============================

This is a 2D platformer written in C++ that utilizes Simple Fast Media Library and Box2D.

To compile this code, you will need to grab SFML v1.6 from here: http://www.sfml-dev.org/download.php.

You will also need to grab Box2D v2.2.1 from here: https://code.google.com/p/box2d/downloads/list.

While the code may work with newer or older versions of those libraries, the versions specified were
the ones I developed with.

