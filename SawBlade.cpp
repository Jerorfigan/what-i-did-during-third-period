/* SawBlade implementation. */

#include "stdafx.h"

#include "Game.h"
#include "SawBlade.h"

namespace PROJECT_NAME
{
	/* Initializes a dummy SawBlade. Used when getting all objects of type SawBlade from 
		GameObjectManager. */
	SawBlade::SawBlade() : GameObject( "dummy" ), CollidableGameObject( "dummy" ), DynamicGameObject( 1, "dummy" ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString("dummy"), 
		"dummy" ), _circle( Point2D(), 0 ), _angle( 0 ), _collidableArea( Circle2D( Point2D(), 0 ) )
	{

	}

	/* Initializes a SawBlade given its position. */
	SawBlade::SawBlade( GameObject::ObjectId objectId, Point2D position ) : GameObject( objectId ), 
		CollidableGameObject( objectId ), DynamicGameObject( 0, objectId ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Traps/SawBlade/SawBlade.png" ) ), 
		objectId ), _circle( position, _collidableRadius ), _collidableArea( Circle2D( position, _collidableRadius ) ), _angle( 0 )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( GraphicsId(), 
			Point2D( static_cast< double >( _spriteWidth ) / 2.0, static_cast< double >( _spriteHeight ) / 2.0 ) );
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( GraphicsId(), position );
	}

	/* GetCollidableArea
		Summary: Gets the CollidableArea for this CollidableGameObject. 
		Returns: The CollidableArea for this CollidableGameObject. */
	CollidableArea* SawBlade::GetCollidableArea()
	{
		return &_collidableArea;
	}

	/* Update
		Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
				last update (which should be accounted for in this update). 
		Parameters: 
			elapsedTime: The time elapsed in seconds since last update. */
	void SawBlade::Update( double elapsedTime )
	{
		// Update the angle of the saw blade.
		_angle += _angularVelocity * elapsedTime;

		// Keep angle between -360 and 360 (exclusive).
		while( _angle >= 360.0 )
		{
			_angle -= 360.0;
		}
		while( _angle <= -360.0 )
		{
			_angle += 360.0;
		}
	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void SawBlade::Draw( double interpolation )
	{
		// Draw the saw blade at the interpolated angle between its current angle and future angle.
		double futureAngle = _angle + _angularVelocity * Game::GetGameUpdatePeriod();
		double interpolatedAngle = interpolation * ( futureAngle - _angle ) + _angle;

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableRotation( GraphicsId(), interpolatedAngle );
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	/* GetGeometry 
		Summary: Returns the Circle2D object that this SawBlade is comprised of. 
		Returns: The Circle2D object that this SawBlade is comprised of. */
	const Circle2D& SawBlade::GetGeometry() const
	{
		return _circle;
	}

	const double SawBlade::_angularVelocity = 0.60 * 360;
}