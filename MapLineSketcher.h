#pragma once

#include "ElementSketcher.h"

namespace PROJECT_NAME
{
	class MapLine;

	class MapLineSketcher : public ElementSketcher
	{
	public:
		/* Initializes a MapLineSketcher from a MapLine object. */
		MapLineSketcher( const MapLine &mapLine );

		/* InitializeSketch
		   Summary: Draws the object as it should appear before the sketch of it has begun (usually blank). */
		virtual void InitializeSketch();
		/* Sketch 
		   Summary: Draws the object as it should appear in the next frame of its sketch. */
		virtual void Sketch();
		/* GetSketchPoint 
		   Summary: Returns the simulated point that the drawing instrument would be at if the sketch was in fact 
					being done by hand. 
		   Returns: The simulated point that the drawing instrument would be at if the sketch was in fact 
					being done by hand.  */
		virtual Point2D GetSketchPoint() const;
		/* IsSketchFinished
		   Summary: Returns true if this sketch is finished. 
		   Returns: True if this sketch is finished. */
		virtual bool IsSketchFinished() const;

		/* GetSketchDuration
		   Summary: Returns the duration this sketch will take to complete in seconds. 
		   Returns: The duration this sketch will take to complete in seconds. */
		virtual double GetSketchDuration() const;

	private:
		Point2D _sketchStartingPoint;
		// _sketchGrowthVector will grow in the direction of the MapLine until it equals the MapLine. It will
		// be used to construct the incremental subrectangles that will be used to draw the increasing fractions
		// of the MapLine.
		Vector2D _sketchGrowthVector;
		// The target magnitude to grow the _sketchGrowthVector to.
		double _targetMagnitude;
		// The current magnitude of the _sketchGrowthVector.
		double _currentMagnitude;

		// The speed at which the MapLine should be sketched (in pixels per second)
		static const double _sketchSpeed;
	};

}