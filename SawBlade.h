#pragma once

/* SawBlade defines the functionality for a saw blade trap in the game world. */

#include "Circle2D.h"
#include "CollidableCircle.h"
#include "CollidableGameObject.h"
#include "DynamicGameObject.h"
#include "GameObject.h"
#include "SpriteAnimator.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class SawBlade : public CollidableGameObject, public DynamicGameObject, public VisibleGameObject
	{
	public:
		/* Initializes a dummy SawBlade. Used when getting all objects of type SawBlade from 
		   GameObjectManager. */
		SawBlade(); 

		/* Initializes a SawBlade given its object id and position. */
		SawBlade( GameObject::ObjectId objectId, Point2D position );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea();

		/* Update
		   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
					last update (which should be accounted for in this update). 
		   Parameters: 
			  elapsedTime: The time elapsed in seconds since last update. */
		virtual void Update( double elapsedTime );

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

		/* GetGeometry 
		   Summary: Returns the Circle2D object that this SawBlade is comprised of. 
		   Returns: The Circle2D object that this SawBlade is comprised of. */
		const Circle2D& GetGeometry() const;

	private:
		Circle2D _circle;
		CollidableCircle _collidableArea;
		double _angle;

		static const int _spriteWidth = 75; // in pixels
		static const int _spriteHeight = 75; // in pixels
		static const int _collidableRadius = 37; // in pixels
		static const double _angularVelocity; // in degrees per second
	};

}