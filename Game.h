#pragma once

/* Class Game defines the high-level functionality for starting and running the game. */

#include "GameObjectManager.h"
#include "GameAssetManager.h"
#include "GameServiceManager.h"
#include "IGraphicsProvider.h"
#include "Rectangle2D.h"
#include "UnitTests.h"

namespace PROJECT_NAME
{
	// forward declarations
	class MainMenu;

	class Game
	{
	public:
		enum GameState { Initializing, SelectProfileMenu, MainMenu, SettingsMenu, StartingLevel, Playing, Exiting, Shutdown }; 

		/* Start
		   Summary: Starts the game by initializing game resouces and calling GameLoop. */
		static void Start();

		/* GetGameObjectManager 
		   Summary: Returns the game object manager used by this game. 
		   Returns: The game object manager used by this game. */
		static GameObjectManager& GetGameObjectManager();

		/* GetGameAssetManager
		   Summary: Returns the game asset manager used by this game.
		   Returns: The game asset manager used by this game. */
		static GameAssetManager& GetGameAssetManager();

		/* GetGetGameServiceManager
		   Summary: Returns the game service manager used by this game.
		   Returns: The game service manager used by this game. */
		static GameServiceManager& GetGameServiceManager();

		/* GetDisplayResolution 
		   Summary: Returns the display resolution that the game is currently running at. 
		   Returns: The display resolution that the game is currently running at. */
		static const IGraphicsProvider::DisplayResolution& GetDisplayResolution();

		/* GetGameSpace 
		   Summary: Returns the rectangle representing the game space. The game space is a virtual space
		            containing every point that an object in the game could occupy. 
		   Returns: The rectangle representing the game space. */
		static const Rectangle2D& GetGameSpace();

		/* GetGameUpdatePeriod
		   Summary: Returns the fixed time in seconds between each game state update.
		   Returns: The fixed time in seconds between each game state update. */
		static double GetGameUpdatePeriod();

		/* GetCursorResource 
		   Summary: Returns the resource id of the mouse cursor. 
		   Returns: The resource id of the mouse cursor. */
		static IGraphicsProvider::ResourceId GetCursorResource();

		static void SetSelectedProfile( std::string profileName );

		/* Static data */
	private:
		/* Registers all the services used by this game. */
		static void RegisterServices();
		/* GameStateManager manages the different states that the game can be in and calls the appropriate 
           functions for handling said states. These functions in turn handle switching between states. States 
		   include: Main Menu, Starting New Game, Playing, etc. */
		static void GameStateManager();
		/* The main game loop, responsible for updating/rendering the game as it's being played. */
		static void GameLoop();
		/* Reads in the list naming all the map files. */
		static void LoadMapFileNames();

		static GameState _gameState;

		static GameObjectManager _gameObjectManager;
		static GameAssetManager _gameAssetManager;
		static GameServiceManager _gameServiceManager;

		static IGraphicsProvider::DisplayResolution _displayResolution;
		static const Rectangle2D _gameSpace;

		// _gameStateUpdateFrequency dictates how many times per second the state of the game is updated 
		// while it is being played.
		static std::size_t _gameStateUpdateFrequency; 

		static IGraphicsProvider::ResourceId _cursor;

		static std::vector< std::string > mMaps;
		static std::size_t mCurrentMapIndex;
		static const std::string mMapFileList;

		static IAudioProvider::AudioId classRoomAmbience;

		static std::string mSelectedProfile;

		/* Friend declarations. */

		friend void PROJECT_NAME_UnitTests::GraphicsInterfaceTest();
	};

}