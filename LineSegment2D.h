#pragma once

/* Defines the functionality for a line segment in 2D. */

#include "Line2D.h"
#include "Point2D.h"

namespace PROJECT_NAME
{

	class LineSegment2D
	{
	public:
		/* Default constructor initializes a line segment with start and end set to (0,0). */
		LineSegment2D();
		/* Constructor that initializes a line segment from passed in points. The start point 
		   will be the left-most point or in a vertical line will be the lowest point. */
		LineSegment2D( Point2D point1, Point2D point2 );

		/* Length 
		   Summary: Returns the length of this LineSegment. 
		   Returns: The the length of this LineSegment. */
		double Length() const;

		/* Intersects
		   Summary: Determines whether this line segment intersects lineSeg. 
		   Parameters: 
			  lineSeg: The LineSegment2D object to check for intersection with.
		   Returns: True if this line segment intersects lineSeg, false otherwise. */
		bool Intersects( const LineSegment2D &lineSeg ) const; 
		/* Intersects
		   Summary: Determines whether this line segment intersects lineSeg and returns the 
					intersection point or line: if there is only 1 intersection point, that 
					point is returned in both intersectionStart and intersectionEnd. If the 
					line segments overlap, intersectionStart will contain the start point of 
					that overlap line and intersectionEnd the end point.
		   Parameters: 
			  lineSeg: The LineSegment2D object to check for intersection with.
			  intersectionStart: Passed out with the intersection point or start point of 
								 intersection line.
			  intersectionEnd: Passed out with the intersection point or end point of 
							   intersection line. 
		   Returns: True if this line segment intersects lineSeg, false otherwise. */
		bool Intersects( const LineSegment2D &lineSeg, Point2D &intersectionStart,
			Point2D &intersectionEnd ) const; 

		/* Contains
		   Summary: Determines whether this line segment contains point. 
		   Parameters: 
			  point: The Point2D object to test for containment.
		   Returns: True if this line segment contains point, false otherwise. */
		bool Contains( const Point2D &point ) const;

		/* Start
		   Summary: Returns the starting point of this line. 
		   Returns: The starting point of this line. */
		const Point2D& Start() const;
		/* End
		   Summary: Returns the ending point of this line. 
		   Returns: The ending point of this line. */
		const Point2D& End() const;

	private:
		Point2D _start;
		Point2D _end;
		Line2D _line; // The line that comprises this line segment
	};

}