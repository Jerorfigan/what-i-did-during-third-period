/* Circle2D implementation. */

#include "stdafx.h"

#include <cmath>
#include <vector>

#include "Circle2D.h"
#include "Line2D.h"
#include "LineSegment2D.h"

namespace PROJECT_NAME
{

		// Initializes a circle at (0,0) with a radius of 1.
		Circle2D::Circle2D() : _center( 0, 0 ), _radius( 1.0 )
		{

		}

		// Initializes a circle at center with the specified radius.
		Circle2D::Circle2D( Point2D center, double radius ) : _center( center ), _radius( radius )
		{

		}

		/* Contains 
		   Summary: Returns true if this circle contains the passed in point. 
		   Parameters:
		      point: The point to test against. 
		   Returns: True if this circle contains the passed in point. */
		bool Circle2D::Contains( Point2D point ) const
		{
			LineSegment2D lineSeg( _center, point );

			return lineSeg.Length() <= _radius;
		}

		/* Intersects
		   Summary: Determines whether this circle intersects the passed in rectangle. 
		   Parameters: 
		      rect: The rectangle to test for intersection against. 
		   Returns: True if this circle intersects the passed in Rectangle. */
		bool Circle2D::Intersects( const Rectangle2D &rect ) const
		{
			bool intersects = false;

			/* This circle intersects the passed in rectangle if 1) the circle contains any of the 
			   vertices of the passed in rectangle or 2) the rectangle contains the center point of the
			   circle or 3) any of the sides of the rectangle intersect the circle. */

			if( Contains( rect.TopLeft() ) ||
				Contains( rect.TopRight() ) ||
				Contains( rect.BottomLeft() ) ||
				Contains( rect.BottomRight() ) )
			{
				intersects = true;
			}

			// If test 1 did not prove intersection, continue with test 2.
			if( !intersects )
			{
				if( rect.Contains( _center ) )
				{
					intersects = true;
				}
			}

			// If test 1 & 2 did not prove intersection, continue with test 3.
			if( !intersects )
			{
				typedef std::vector< LineSegment2D > LineSegments;

				LineSegments rectangleSides;
				rectangleSides.push_back( LineSegment2D( rect.TopLeft(), rect.TopRight() ) );
				rectangleSides.push_back( LineSegment2D( rect.BottomLeft(), rect.TopLeft() ) );
				rectangleSides.push_back( LineSegment2D( rect.BottomLeft(), rect.BottomRight() ) );
				rectangleSides.push_back( LineSegment2D( rect.BottomRight(), rect.TopRight() ) );

				for( LineSegments::const_iterator currentSegment = rectangleSides.begin();
					 currentSegment != rectangleSides.end(); ++currentSegment )
				{
					if( Intersects( *currentSegment ) )
					{
						intersects = true;
						break;
					}
				}
			}

			return intersects;
		}

		/* Intersects
		   Summary: Determines whether this circle intersects the passed in line segment. 
		   Parameters: 
		      lineSeg: The line segment to test for intersection against. 
		   Returns: True if this circle intersects the passed in line segment. */
		bool Circle2D::Intersects( const LineSegment2D &lineSeg ) const
		{
			bool intersects = false;

			/* This circle intersects the passed in line segment if it intersects the line that that
			   segment comprises and if the line segment contains one or both of the intersection
			   points. To determine line / circle intersection, we'll need to plug the equation for the line
			   into the equation for a circle, which yields a quadratic equation, and solve for the roots. */

			Line2D line( lineSeg.Start(), lineSeg.End() );

			// Find the coefficients of the quadratic equation and then sovle for the roots. 
			double a, b, c;

			if( line.FiniteSlope() )
			{
				a = 1 + pow( line.Slope(), 2.0 );
				b = 2 * line.Slope() * ( line.YIntercept() - _center._y ) - 2 * _center._x;
				c = pow( _center._x, 2.0 ) + pow( line.YIntercept() - _center._y, 2.0 ) - pow( _radius, 2.0 );
			}
			else
			{
				a = 1.0;
				b = -2.0 * _center._y;
				c = pow( _center._y, 2.0 ) - pow( _radius, 2.0 ) + pow( line.XIntercept() - _center._x, 2.0 );
			}

			double discriminant = pow( b, 2.0 ) - 4 * a * c;

			if( discriminant >= 0 )
			{
				// There is one or more real roots, therefore intersection between the line and the circle exists.
				// Now find points of intersection and check to see if the line segment contains them.

				double x1, x2, y1, y2;

				if( line.FiniteSlope() )
				{
					x1 = ( -b + sqrt( discriminant ) ) / 2.0 * a;
					x2 = ( -b - sqrt( discriminant ) ) / 2.0 * a;

					y1 = line.Slope() * x1 + line.YIntercept();
					y2 = line.Slope() * x2 + line.YIntercept();
				}
				else
				{
					x1 = x2 = line.XIntercept();

					y1 = ( -b + sqrt( discriminant ) ) / 2.0 * a;
					y2 = ( -b - sqrt( discriminant ) ) / 2.0 * a;
				}

				if( lineSeg.Contains( Point2D( x1, y1 ) ) ||
					lineSeg.Contains( Point2D( x2, y2 ) ) )
				{
					intersects = true;
				}
			}

			return intersects;
		}

		/* Intersects
		   Summary: Determines whether this circle intersects the passed in circle. 
		   Parameters: 
		      circle: The circle to test for intersection against. 
		   Returns: True if this circle intersects the passed in circle. */
		bool Circle2D::Intersects( const Circle2D &circle ) const
		{
			LineSegment2D lineSeg( _center, circle._center );

			return lineSeg.Length() <= 2.0 * _radius; 
		}

		/* Center
		   Summary: Returns the center point of the circle. 
		   Returns: The center point of the circle. */
		const Point2D& Circle2D::Center() const
		{
			return _center;
		}

		/* Radius
		   Summary: Returns the radius of the circle. 
		   Returns: The radius of the circle. */
		double Circle2D::Radius() const
		{
			return _radius;
		}

}