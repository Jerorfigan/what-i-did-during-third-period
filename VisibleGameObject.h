#pragma once

/* Defines the abstract functionality of a visible game object. */

#include "GameObject.h"
#include "Handle.h"
#include "Handle.cpp"
#include "IGraphicsProvider.h"
#include "IntSequence.h"

namespace PROJECT_NAME
{

	class VisibleGameObject : public virtual GameObject
	{
	public:
		typedef IntSequence::SequenceValue RenderOrder;

		/* Initializes a VisibleGameObject from a IGraphicsProvider::ResourceId, and
		   a GameObject::ObjectId. */
		VisibleGameObject( IGraphicsProvider::ResourceId graphicsId, ObjectId objectId );

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation ) = 0;

		/* RenderOrder 
		   Summary: Returns the order that this VisibleGameObject should be rendered, with 1 being first. 
		   Returns: The render depth level of this VisibleGameObject. */
		const RenderOrder& GetRenderOrder() const;

		/* GraphicsId 
		   Summary: Returns the id value to the underlying graphics resource for this VisibleGameObject. 
		   Returns: The id value to the underlying graphics resource for this VisibleGameObject. */
		const IGraphicsProvider::ResourceId& GraphicsId() const;

		/* CompareHandles
		   Summary: Compares two VisibleGameObject handles based on the render order of the underlying 
		            VisibleGameObjects. Returns true if lhs has a lower render order than rhs, false otherwise. 
		   Parameters: 
		      lhs: The left-hand-side VisibleGameObject handle. 
			  rhs: The right-hand-side VisibleGameObject handle. 
		   Returns: True if lhs has a lower render order than rhs, false otherwise.  */
		static bool CompareHandles( Handle< VisibleGameObject > lhs, Handle< VisibleGameObject > rhs );

		/* IsVisible 
		   Summary: Returns true if this VisibleGameObject is currently visible. 
		   Returns: True if this VisibleGameObject is currently visible. */
		bool IsVisible() const;
		/* SetVisibility 
		   Summary: Sets whether this VisibleGameObject is currently visible. 
		   Parameters:
		      visible: Flag indicating whether this VisibleGameObject is currently visible. */
		void SetVisibility( bool isVisible );

		/* De-allocate the graphics object. */
		virtual ~VisibleGameObject();

	protected:
		// flag indicating whether this VisibleGameObject is currently visible.
		bool _isVisible;

	private:
		// _renderOrder stores the order that this object should be rendered. The render order of 
		// VisibleGameObjects is equal to the order in which they are created.
		RenderOrder _renderOrder;
		
		// The id value for the underlying graphics resource for this VisibleGameObject 
		IGraphicsProvider::ResourceId _graphicsId;

		// _renderSequence is used to set the render order of VisibleGameObjects. The render order of 
		// VisibleGameObjects is equal to the order in which they are created.
		static IntSequence _renderSequence;
	};

}