/* CheckBox implementation. */

#include "stdafx.h"

#include "CheckBox.h"
#include "Game.h"

namespace PROJECT_NAME
{

	// Constructors
	
	CheckBox::CheckBox( std::string id, unsigned int menuIndex, Point2D checkBoxPos, std::string labelImage, 
		void (*onActivation)( void* ), bool initiallyChecked ) : 
		UserControl( id, menuIndex, Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( mCheckBoxUncheckedImage ) ),
		mCheckBoxRect( Rectangle2D( checkBoxPos, mCheckBoxSize, mCheckBoxSize) ), 
		mLabel( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( labelImage ) ), mIsChecked( initiallyChecked ),
		mOnActivation( onActivation )
	{

	}

	// Virtual methods
	
	void CheckBox::Init( void* menuData )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( mGraphicsId, 
			Point2D( mCheckBoxSize / 2.0, mCheckBoxSize / 2.0 ) );
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mGraphicsId, 
			mCheckBoxRect.Center() );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mLabel, 
			Point2D( mCheckBoxRect.Center()._x + mCheckBoxLabelOffsetX, mCheckBoxRect.Center()._y - mCheckBoxLabelOffsetY ) );

		if( mIsChecked )
		{
			Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( mGraphicsId, mCheckBoxCheckedImage );
		}
		else
		{
			Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( mGraphicsId, mCheckBoxUncheckedImage );
		}
	}

	void CheckBox::Draw( void* menuData )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mGraphicsId, 
			Game::GetGameAssetManager().GetAppWindow() );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mLabel, 
			Game::GetGameAssetManager().GetAppWindow() );
	}

	void CheckBox::OnHoverOver( void* menuData )
	{

	}

	void CheckBox::OnHoverAway( void* menuData )
	{

	}

	void CheckBox::OnActivation( void* menuData )
	{
		if( mIsChecked )
		{
			mIsChecked = false;

			Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( mGraphicsId, mCheckBoxUncheckedImage );
		}
		else
		{
			mIsChecked = true;

			Game::GetGameServiceManager().GetGraphicsProvider()->SetSpriteImage( mGraphicsId, mCheckBoxCheckedImage );
		}

		// If specific event handler specified, call it.
		if( mOnActivation )
		{
			mOnActivation( menuData );
		}
	}

	void CheckBox::OnDeactivation( void* menuData )
	{

	}

	void CheckBox::OnGainedFocus( void* menuData )
	{

	}

	void CheckBox::OnLostFocus( void* menuData )
	{

	}

	void CheckBox::OnKeyboardChar( void* menuData, char typedChar )
	{

	}

	bool CheckBox::IsTargettedByMouse( Point2D mouseHotspot )
	{
		return mCheckBoxRect.Contains( mouseHotspot );
	}

	// Static data
	
	const std::string CheckBox::mCheckBoxUncheckedImage = RESOURCE_PATH( "Art/UI/CheckBox/CheckBoxUnchecked.png" );
	const std::string CheckBox::mCheckBoxCheckedImage = RESOURCE_PATH( "Art/UI/CheckBox/CheckBoxChecked.png" );
}