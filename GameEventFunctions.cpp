/* Definition of game event-related functions. */

#include "stdafx.h"

#include "Game.h"

namespace PROJECT_NAME
{

	bool CheckForGameEvent( GameEvent &gameEvent )
	{
		// First try to get an event from the queue maintained by the graphics provider. If one isn't 
		// found there, try the game event queue maintained by the application. 
		if( Game::GetGameServiceManager().GetGraphicsProvider()->GetGameEvent( 
			Game::GetGameAssetManager().GetAppWindow(), 
			gameEvent ) )
		{
			return true;
		}
		else
		{
			if( !Game::GetGameAssetManager().GetGameEventQueue().IsEmpty() )
			{
				gameEvent = Game::GetGameAssetManager().GetGameEventQueue().PopEvent();

				return true;
			}
			else
			{
				return false;
			}
		}
	}

}