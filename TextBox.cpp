/* TextBox implementation. */

#include "stdafx.h"

#include "Game.h"
#include "Point2D.h"
#include "TextBox.h"

namespace PROJECT_NAME
{
	const char TextBox::mValidCharacters[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

	/* Constructors */

	TextBox::TextBox( std::string id, unsigned int menuIndex, std::string labelImage, Rectangle2D textRect, 
		void (*onKeyboardChar)(void*, TextBox*), bool allowSpaces ) :
		UserControl( id, menuIndex, Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( mBoxImageFile ), true ),
		mTextRect( textRect ), mLabel( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( labelImage ) ), 
		mCursor( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( mCursorImageFile ) ),
		mOnKeyboardChar( onKeyboardChar ), mHasFocus( false ), mCursorOffset( 0 ), mAllowSpaces( allowSpaces )
	{

	}

	/* Methods */

	void TextBox::RepositionCursor()
	{
		mCursorOffset = Game::GetGameServiceManager().GetGraphicsProvider()->GetPixelLength( mText );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mCursor, mTextRect.BottomLeft() + Vector2D( mCursorOffset, 0 ) );
	}

	std::string TextBox::GetContents() const
	{
		return mContents;
	}

	bool TextBox::IsValid( char &c )
	{
		bool validChar = false;

		std::size_t numValidChars = sizeof( mValidCharacters );
		for( int i = 0; i < numValidChars; ++i )
		{
			// must be alphabet character, upper or lower
			if( mValidCharacters[i] == c || 
				mValidCharacters[i] == ( c - 32) )
			{
				// Force to uppercase
				if( mValidCharacters[i] == ( c - 32) )
					c -= 32;
				validChar = true;
				break;
			}
		}
		return validChar;
	}

	/* Virtual methods */

	void TextBox::Init( void* menuData )
	{
		// Position textbox image.
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mGraphicsId, mTextRect.BottomLeft() );

		// Position textbox label.
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( mLabel, Point2D( 
			Game::GetGameServiceManager().GetGraphicsProvider()->GetSpriteSubRectangle( mLabel ).Width(), 0 ) );

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mLabel, 
			mTextRect.BottomLeft() + Vector2D( -mLabelOffsetX, 0 ) );

		// Initialize text.
		mText = Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString( "" );
		
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( mText, mTextRect.BottomLeft() + Vector2D( mTextOffsetX, mTextOffsetY ) );
		
		RepositionCursor();
	}

	void TextBox::Draw( void* menuData )
	{
		// Draw textbox box.
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mGraphicsId, 
			Game::GetGameAssetManager().GetAppWindow() );

		// Draw textbox label.
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mLabel, 
			Game::GetGameAssetManager().GetAppWindow() );

		// Draw text.
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mText, 
			Game::GetGameAssetManager().GetAppWindow() );

		// If has focus, draw cursor.
		if( mHasFocus )
		{
			Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( mCursor,
				Game::GetGameAssetManager().GetAppWindow() );
		}
	}

	void TextBox::OnHoverOver( void* menuData )
	{
		// do nothing.
	}

	void TextBox::OnHoverAway( void* menuData )
	{
		// do nothing.
	}

	void TextBox::OnActivation( void* menuData )
	{
		// do nothing.
	}

	void TextBox::OnDeactivation( void* menuData )
	{
		// do nothing.
	}

	void TextBox::OnGainedFocus( void* menuData )
	{
		mHasFocus = true;
	}

	void TextBox::OnLostFocus( void* menuData )
	{
		mHasFocus = false;
	}

	void TextBox::OnKeyboardChar( void* menuData, char typedChar )
	{
		// If typed char was a backspace, then delete a letter from mContents.
		if( typedChar == 8 )
		{
			if( mContents.size() > 0 )
			{
				mContents.pop_back();
			}
		}
		else
		{
			if( mContents.size() < mCharacterLimit && ( typedChar != 32 || mAllowSpaces ) )
			{
				if( IsValid( typedChar ) )
				{
					mContents += typedChar;
				}
			}
		}

		Game::GetGameServiceManager().GetGraphicsProvider()->SetString( mText, mContents );

		RepositionCursor();

		// If specific event handler specified, call it.
		if( mOnKeyboardChar )
		{
			mOnKeyboardChar( menuData, this );
		}
	}

	bool TextBox::IsTargettedByMouse( Point2D mouseHotspot )
	{
		return mTextRect.Contains( mouseHotspot );
	}

	/* Static data */

	const std::string TextBox::mBoxImageFile = RESOURCE_PATH( "Art/UI/TextBox/TextBoxBox.png" );
	const std::string TextBox::mCursorImageFile = RESOURCE_PATH( "Art/UI/TextBox/TextBoxCursor.png" );
}