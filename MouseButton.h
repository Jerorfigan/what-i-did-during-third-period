#pragma once 

/* Defines an enumeration of all the mouse buttons that will be referenced in the game. */

namespace PROJECT_NAME
{

	struct MouseButton
	{
		enum Button { LeftButton };
	};

}