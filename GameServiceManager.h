#pragma once

/* GameServiceManager defines the interface for registering and accessing all the services utilized by this 
   game. An example of a service is the graphics provider utilized by the game via which the game does its
   rendering. */

#include "Handle.h"
#include "Handle.cpp"
#include "IGraphicsProvider.h"
#include "IAudioProvider.h"

namespace PROJECT_NAME
{

	class GameServiceManager
	{
		// Graphics stuff.
	public:
		/* GetGraphicsProvider 
		   Summary: Returns a handle to the graphics provider. 
		   Returns: A handle to the graphics provider. */
		Handle< IGraphicsProvider > GetGraphicsProvider();
		/* SetGraphicsProvider 
		   Summary: Establishes the service provider that will act as graphics provider. 
		   Parameters:
		      graphicsProvider: A handle to the service provider that will act as graphics provider. */
		void SetGraphicsProvider( Handle< IGraphicsProvider > graphicsProvider );

		// Audio stuff.
	public:
		/* GetAudioProvider 
		   Summary: Returns a handle to the audio provider. 
		   Returns: A handle to the audio provider. */
		Handle< IAudioProvider > GetAudioProvider();
		/* SetAudioProvider 
		   Summary: Establishes the service provider that will act as audio provider. 
		   Parameters:
		      audioProvider: A handle to the service provider that will act as audio provider. */
		void SetAudioProvider( Handle< IAudioProvider > audioProvider );

	private:
		/* A handle to the graphics provider utilized by the client. */
		Handle< IGraphicsProvider > _graphicsProvider;
		/* A handle to the audio provider utilized by the client. */
		Handle< IAudioProvider > _audioProvider;
	};

}