#pragma once

/* ProceduralAnimator defines the functionality to perform procedural animations using the Box2D physics engine. */

#include <fstream>
#include <map>
#include <deque>
#include <string>
#include <vector>

#include "Box2D/Box2D.h"
#include "IGraphicsProvider.h"
#include "Point2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME
{
	class ProceduralAnimator
	{
	private:
		/* Internal data types used to interface with Box2D. */

		enum BodyType { Dynamic };
		enum ShapeType { Circle, Rectangle };
		enum JointType { Revolute };

		struct Shape
		{
			std::size_t _id;
			ShapeType _type;
			Point2D _position;
			float32 _radius;
			float32 _halfWidth;
			float32 _halfHeight;
		};

		struct Fixture
		{
			std::size_t _id;
			float32 _density;
			float32 _friction;
			float32 _restitution;
			std::size_t _shapeId;
			int16 _filterGroupIndex;
		};

		struct Body
		{
			std::size_t _id;
			BodyType _type;
			Point2D _position;
			float32 _rotation;
			bool _bullet;
			std::size_t _fixtureId;
			bool _hasCustomVelocity;
			b2Vec2 _customLinearVelocity;
			float32 _customAngularVelocity;
		};

		struct Joint
		{
			JointType _jointType;
			std::size_t _bodyAId;
			std::size_t _bodyBId;
			Point2D _anchorA;
			Point2D _anchorB;
			bool _enableLimit;
			float32 _lowerAngle;
			float32 _upperAngle;
			float32 _thresholdForce;
		};

		struct Sprite
		{
			IGraphicsProvider::ResourceId _graphicsId;
			std::deque< Point2D > _prevAndCurrPos;
			std::deque< double > _prevAndCurrAngle;
		};

		struct AnimationData
		{
			std::string _animationName;
			bool _interpolate;
			float32 _worldFriction;
			float32 _worldRestitution;
			std::map< std::size_t, Shape > _shapes;
			std::map< std::size_t, Fixture > _fixtures; 
			std::vector< Body > _bodies;
			std::vector< Joint > _joints;
			std::vector< Sprite > _bodySprites;
		};

	public: 
		/* Initializes a ProceduralAnimator from an animation file. */
		ProceduralAnimator( std::string animationFile );

		/* PlayProceduralAnimation
			Summary: Plays a procedural animation for an entity given its start position and velocity. 
			Parameters:
				animationName: The name of the animation.
				entityStartPosition: Used to position the b2Bodies that comprise the animation.
				entityStartVelocity: The default velocity for the b2Bodies that comprise the animation. */
		void PlayProceduralAnimation( std::string animationName, Point2D entityStartPosition,
			Vector2D entityStartVelocity );
		/* StopProceduralAnimation 
		   Summary: Stops the currently playing procedural animation and frees resources allocated 
		            for that animation. */
		void StopProceduralAnimation();

		/* UpdateSimulation
		   Summary: Updates the currently playing procedural animation, if there is one. */
		void UpdateSimulation();
		/* DrawSimulation
		   Summary: Draws the currently playing procedural animation, if there is one. */
		void DrawSimulation( double interpolation ) const;

	private:
		/* Checks the joints of the simulatedBodies to see if any joints are experiencing reaction
		   forces/torques greater than the thresholds and, if so, breaking them (i.e. destroying them). */
		void CheckJointThresholds();
		/* Parses the animation settings data from a stringstream holding the settings input data from the animation file. */
		void ParseSettingsData( std::stringstream &lineStream, AnimationData &animationData );
		/* Parses shape data from a stringstream holding input data for a given line in the animation file. */
		void ParseShapeData( std::stringstream &lineStream, Shape &shape ) const;
		/* Parses fixture data from a stringstream holding input data for a given line in the animation file. */
		void ParseFixtureData( std::stringstream &lineStream, Fixture &fixture ) const;
		/* Parses body data from a stringstream holding input data for a given line in the animation file. */
		void ParseBodyData( std::stringstream &lineStream, Body &body ) const;
		/* Parses joint data from a stringstream holding input data for a given line in the animation file. */
		void ParseJointData( std::stringstream &lineStream, Joint &joint ) const;
		/* Parses sprite data from a stringstream holding input data for a given line in the animation file. */
		void ParseSpriteData( std::stringstream &lineStream, Sprite &Sprite, AnimationData &animationData ) const;
		/* Handles generating a number in the specified range (read from the animation file) and assigning it to the
		   numeric parameter. */
		template < typename NumericType >
		void ResolveNumericRange( std::string range, NumericType &numeric ) const;

		bool _isSimulating;

		std::string _currentAnimation;
		std::map< std::string, AnimationData > _animations;
		std::vector< b2Body* > _simulatedBodies;
		std::vector< b2Joint* > _simulatedJoints;
	};

	/* ProceduralAnimator template functions: */

	/* Handles generating a number in the specified range (read from the animation file) and assigning it to the
	   numeric parameter. */
	template < typename NumericType >
	void ProceduralAnimator::ResolveNumericRange( std::string range, NumericType &numeric ) const
	{
		// Was a range specified? 
		std::string::size_type index;
		if( ( index = range.find( "," ) )  != std::string::npos )
		{
			// Then parse out the upper and lower limits of the range, and assign to 
			// numeric a random value in that range. 
			double lowerLimit = 0.0, upperLimit = 0.0;

			std::stringstream lowerLimitStream( range.substr( 0, index ) );
			std::stringstream upperLimitStream( range.substr( index + 1 ) );

			lowerLimitStream >> lowerLimit;
			upperLimitStream >> upperLimit;

			assert( lowerLimitStream );
			assert( upperLimitStream );

			numeric = static_cast< NumericType >( Random( lowerLimit, upperLimit ) );
		}
		else // Nope, just a number, so assign it straight over.
		{
			std::stringstream rangeStream( range );
			rangeStream >> numeric;
			assert( rangeStream );
		}
	}
}