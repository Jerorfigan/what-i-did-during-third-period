#pragma once

/* CollidableLineSegment represents the line segment of an object that can be collided with. */

#include "CollidableArea.h"
#include "LineSegment2D.h"

namespace PROJECT_NAME
{
	class CollidableLineSegment : public CollidableArea
	{
	public:
		/* Initializes a CollidableLineSegment from a LineSegment2D. */
		CollidableLineSegment( const LineSegment2D &lineSeg );

		/* CollidesWith 
		   Summary: Returns true if this CollidableArea overlaps otherArea. 
		   Returns: True if this CollidableArea overlaps otherArea. */
		virtual bool CollidesWith( const CollidableArea* otherArea ) const;

		/* GetLineSegments 
		   Summary: Returns a vector filled with the line segments that comprise this CollidableArea. 
		   Returns: A vector filled with the line segments that comprise this CollidableArea. */
		virtual std::vector< LineSegment2D > GetLineSegments() const;

		/* UpdatePosition
		   Summary: Updates the position of this CollidableArea given a reference point (ex: the center of the area). 
		   Parameters:
		      referencePoint: The reference point indicating how to update the position of the CollidableArea. */
		virtual void UpdatePosition( const Point2D &referencePoint );

		/* UpdateLineSegment 
		   Summary: Updates the LineSegment2D component of this CollidableLineSegment. 
		   Parameters:
		      newLineSegment: The new line segment to update it with. */
		void UpdateLineSegment( const LineSegment2D &newLineSegment );

	private:
		LineSegment2D _lineSeg;

		friend class CollidableRectangle;
		friend class CollidableCircle;
	};
}