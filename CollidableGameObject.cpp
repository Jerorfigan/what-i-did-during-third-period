/* Implementation for CollidableGameObject. */

#include "stdafx.h"

#include "CollidableGameObject.h"

namespace PROJECT_NAME
{
	/* Initializes a CollidableGameObject from an ObjectId, a KinematicState and a flag denoting 
		   whether this CollidableGameObject is static. */
	CollidableGameObject::CollidableGameObject( ObjectId objectId, const KinematicState &kinematicState, 
		bool isStatic ) : GameObject( objectId ), _kinematicState( kinematicState ), _isStatic( isStatic )
	{

	}

	/* GetKinematicState 
	   Summary: Gets the kinematic state of this CollidableGameObject. 
	   Returns: The kinematic state of this CollidableGameObject. */
	const KinematicState& CollidableGameObject::GetKinematicState() const
	{
		return _kinematicState;
	}

	/* SetKinematicState 
	   Summary: Sets the kinematic state of this CollidableGameObject. 
	   Parameters: 
	      kinematicState: The kinematic state to set it to. */
	void CollidableGameObject::SetKinematicState( const KinematicState &kinematicState )
	{
		assert( !_isStatic );
		_kinematicState = kinematicState;
	}
}