/* MainMenu implementation. */

#include "stdafx.h"

#include "Background.h"
#include "Handle.h"
#include "Handle.cpp"
#include "MainMenu.h"
#include "PushButton.h"
#include "StaticImage.h"

namespace PROJECT_NAME
{
	// Constructors
	
	MainMenu::MainMenu() : mMenu( Handle< Background >( 
		new Background( RESOURCE_PATH( "Art/Menus/MainMenu/Background.jpg" ), Rectangle2D( Point2D( 800, 800 ), 1600, 900 ) ) ), 
		&mData )
	{
		Handle< PushButton > startButtonHandle( 
			new PushButton( "start", 0, Rectangle2D( Point2D( 800, 375 ), 200, 75 ),
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/StartButton.png" ), 
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/StartButtonHighlighted.png" ), 
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/StartButtonHighlighted.png" ),
				0, 0, StartButtonClicked ) );


		mMenu.AddControl( startButtonHandle ); 

		Handle< PushButton > settingsButtonHandle( 
			new PushButton( "settings", 1, Rectangle2D( Point2D( 800, 450 ), 275, 75 ),
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/SettingsButton.png" ), 
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/SettingsButtonHighlighted.png" ), 
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/SettingsButtonHighlighted.png" ),
				0, 0, SettingsButtonClicked ) );

		mMenu.AddControl( settingsButtonHandle ); 

		Handle< PushButton > exitButtonHandle( 
			new PushButton( "exit", 2, Rectangle2D( Point2D( 800, 525 ), 200, 75 ),
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/ExitButton.png" ), 
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/ExitButtonHighlighted.png" ), 
				RESOURCE_PATH( "Art/Menus/MainMenu/Buttons/ExitButtonHighlighted.png" ),
				0, 0, ExitButtonClicked ) );

		mMenu.AddControl( exitButtonHandle ); 

		// Images
		Handle< StaticImage > eyesDrawing( 
			new StaticImage( RESOURCE_PATH( "Art/Menus/MainMenu/Drawings/eyes.png" ), Point2D( 500, 75 ) ) );

		mMenu.AddControl( eyesDrawing );

		Handle< StaticImage > manDrawing( 
			new StaticImage( RESOURCE_PATH( "Art/Menus/MainMenu/Drawings/man.png" ), Point2D( 100, 75 ) ) );

		mMenu.AddControl( manDrawing );

		Handle< StaticImage > headsDrawing( 
			new StaticImage( RESOURCE_PATH( "Art/Menus/MainMenu/Drawings/head.png" ), Point2D( 375, 300 ) ) );

		mMenu.AddControl( headsDrawing );

		Handle< StaticImage > raptorDrawing( 
			new StaticImage( RESOURCE_PATH( "Art/Menus/MainMenu/Drawings/raptor.png" ), Point2D( 400, 550 ) ) );

		mMenu.AddControl( raptorDrawing );

		Handle< StaticImage > elfDrawing( 
			new StaticImage( RESOURCE_PATH( "Art/Menus/MainMenu/Drawings/elf.png" ), Point2D( 900, 375 ) ) );

		mMenu.AddControl( elfDrawing );

		Handle< StaticImage > portraitDrawing( 
			new StaticImage( RESOURCE_PATH( "Art/Menus/MainMenu/Drawings/portrait.png" ), Point2D( 1000, 100 ) ) );

		mMenu.AddControl( portraitDrawing );
	}

	// Methods

	/* Process 
		Summary: Processes the Main menu and returns the next game state. */
	Game::GameState MainMenu::Process()
	{
		mMenu.Interact();

		switch( mData.mSelection )
		{
		case MainMenuData::Start:
			return Game::StartingLevel;
		case MainMenuData::Settings:
			return Game::SettingsMenu;
		case MainMenuData::Exit:
			return Game::Exiting;
		}
		return Game::Exiting;
	}

	// Menu event handlers

	void StartButtonClicked( void* menuData )
	{
		// Set the mSelection member in menuData to indicate that Start was clicked.
		MainMenu::MainMenuData* menuDataPtr = static_cast< MainMenu::MainMenuData* >( menuData );
	
		menuDataPtr->mSelection = MainMenu::MainMenuData::Start;

		// Push the menu exit event onto the UI queue so that the menu processing will
		// terminate.
		GameEvent menuExit;
		menuExit._event = GameEvent::MenuExit;

		Game::GetGameAssetManager().GetGameEventQueue().PushEvent( menuExit );
	}

	void SettingsButtonClicked( void* menuData )
	{
		// Set the mSelection member in menuData to indicate that Start was clicked.
		MainMenu::MainMenuData* menuDataPtr = static_cast< MainMenu::MainMenuData* >( menuData );
	
		menuDataPtr->mSelection = MainMenu::MainMenuData::Settings;

		// Push the menu exit event onto the UI queue so that the menu processing will
		// terminate.
		GameEvent menuExit;
		menuExit._event = GameEvent::MenuExit;

		Game::GetGameAssetManager().GetGameEventQueue().PushEvent( menuExit );
	}

	void ExitButtonClicked( void* menuData )
	{
		// Set the mSelection member in menuData to indicate that Start was clicked.
		MainMenu::MainMenuData* menuDataPtr = static_cast< MainMenu::MainMenuData* >( menuData );
	
		menuDataPtr->mSelection = MainMenu::MainMenuData::Exit;

		// Push the menu exit event onto the UI queue so that the menu processing will
		// terminate.
		GameEvent menuExit;
		menuExit._event = GameEvent::MenuExit;

		Game::GetGameAssetManager().GetGameEventQueue().PushEvent( menuExit );
	}

}