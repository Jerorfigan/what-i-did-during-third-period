/* Implements the functionality for a line in 2D. */ 

#include "stdafx.h"
#include "Line2D.h"

namespace PROJECT_NAME
{
	/* Initializes a line with a finite slope of 0 and y-intercept of 0. */
	Line2D::Line2D() : _slope( 0.0 ), _yIntercept( 0.0 ), _finiteSlope( true )
	{

	}

	/* Initializes a line from the passed in points on the line. */
	Line2D::Line2D( Point2D point1, Point2D point2 )
	{
		// When calculating the slope of the line, make sure to subtract the left-most
		// point from the right-most point. Also check whether line has infinite slope.

		if( EqualDouble( point1._x, point2._x ) )
		{
			/* Line has infinite slope, so just need to set x-intercept. */

			_xIntercept = point1._x;
			_finiteSlope = false;
		}
		else
		{
			/* Line has finite slope, so find it as well as y-intercept. */

			_finiteSlope = true;

			if( point1._x < point2._x )
			{
				_slope = ( point2._y - point1._y ) / ( point2._x - point1._x );
			}
			else
			{
				_slope = ( point1._y - point2._y ) / ( point1._x - point2._x );
			}

			_yIntercept = point1._y - _slope * point1._x;
		}
	}

	/* Contains
	   Summary: Determines whether this line contains point. 
	   Parameters: 
		  point: The Point2D object to test for containment.
	   Returns: True if this line contains point, false otherwise. */
	bool Line2D::Contains( const Point2D &point ) const
	{
		bool contains = false;

		if( _finiteSlope )
		{
			if( EqualDouble( point._y, _slope * point._x + _yIntercept ) )
			{
				contains = true;
			}
		}
		else
		{
			if( EqualDouble( point._x, _xIntercept ) )
			{
				contains = true;
			}
		}

		return contains;
	}

	/* Intersects
	   Summary: Determines whether this line intersects the passed-in line. If the lines
				are equal, this function will return false, thus equality must be determined
				with the == operator.
	   Parameters: 
		  line: The line to test for intersection with this line.
		  intersectionPoint: Is set to the point of intersection, if one exists.
	   Returns: True if this line intersects the passed-in line, false otherwise. */
	bool Line2D::Intersects( const Line2D &line, Point2D &intersectionPoint ) const
	{
		bool linesIntersect = false;

		if( _finiteSlope && line._finiteSlope && !EqualDouble( _slope, line._slope ) )
		{
			/* Lines have finite slope and aren't parallel (or the same line). Can set line equations equal
			   to one another to find intersection point. */

			linesIntersect = true;

			intersectionPoint = Point2D( ( _yIntercept - line._yIntercept ) / ( line._slope - _slope ) , 
										 _slope * ( _yIntercept - line._yIntercept ) / ( line._slope - _slope ) + _yIntercept );
		}
		else if( !_finiteSlope && line._finiteSlope )
		{
			/* This line has infinite slope but passed-in line has finite slope. Can solve passed-in line's equation using
			   this line's x-intercept to find intersection point. */

			linesIntersect = true;

			intersectionPoint = Point2D( _xIntercept, line._slope * _xIntercept + line._yIntercept );
		}
		else if( _finiteSlope && !line._finiteSlope )
		{
			/* This line has finite slope but passed-in line has infinite slope. Can solve this line's equation using
			   passed-in line's x-intercept to find intersection point. */

			linesIntersect = true;

			intersectionPoint = Point2D( line._xIntercept, _slope * line._xIntercept + _yIntercept );
		}

		return linesIntersect;
	}

	/* operator: ==
	   Summary: Tests two lines for equality. Two lines are equal if they have finite slope and
				their slope and y-intercepts match or if they have infinite slope and their 
				x-intercepts match.
	   Parameters: 
		  line: The line to test for equality with this line.
	   Returns: True if the lines are equal. */
	bool Line2D::operator==( const Line2D &line ) const
	{
		bool linesEqual = false;

		if( _finiteSlope && line._finiteSlope )
		{
			linesEqual = EqualDouble( _slope, line._slope ) && EqualDouble( _yIntercept, line._yIntercept );
		}
		else if( !_finiteSlope && !line._finiteSlope )
		{
			linesEqual = EqualDouble( _xIntercept, line._xIntercept );
		}

		return linesEqual;
	}

	/* operator: !=
	   Summary: Tests two lines for inequality. Two lines are unequal if they're not equal (see 
		        definition of == operator).
	   Parameters: 
		  line: The line to test for inequality with this line.
	   Returns: True if the lines are unequal. */
	bool Line2D::operator!=( const Line2D &line ) const
	{
		return !( *this == line );
	}

	/* Slope
	   Summary: Returns the slope of the line. Return value undefined if line has infinite slope. 
	   Returns: The slope of the line. Return value undefined if line has infinite slope. */
	double Line2D::Slope() const
	{
		return _slope;
	}

	/* YIntercept
	   Summary: Returns the y-intercept of the line. Return value undefined if line has infinite slope. 
	   Returns: The y-intercept of the line. Return value undefined if line has infinite slope. */
	double Line2D::YIntercept() const
	{
		return _yIntercept;
	}

	/* XIntercept
	   Summary: Returns the x-intercept of a vertical line. Return value undefined if line has finite slope. 
	   Returns: The x-intercept of a vertical line. Return value undefined if line has finite slope. */
	double Line2D::XIntercept() const
	{
		return _xIntercept;
	}

	/* FiniteSlope
	   Summary: Returns true if the line has finite slope, false otherwise. 
	   Returns: True if the line has finite slope, false otherwise. */
	bool Line2D::FiniteSlope() const
	{
		return _finiteSlope;
	}

}