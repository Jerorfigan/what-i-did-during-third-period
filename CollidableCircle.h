#pragma once

/* CollidableCircle represents the circular area of an object that can be collided with. */

#include <iostream>

#include "Circle2D.h"
#include "CollidableArea.h"

namespace PROJECT_NAME
{

	class CollidableCircle : public CollidableArea
	{
	public:
		/* Initializes a CollidableCircle from a Circle2D. */
		CollidableCircle( Circle2D circle );

		/* CollidesWith 
		   Summary: Returns true if this CollidableArea overlaps otherArea. 
		   Returns: True if this CollidableArea overlaps otherArea. */
		virtual bool CollidesWith( const CollidableArea* otherArea ) const;

		/* GetLineSegments 
		   Summary: Returns a vector filled with the line segments that comprise this CollidableArea. 
		   Returns: A vector filled with the line segments that comprise this CollidableArea. */
		virtual std::vector< LineSegment2D > GetLineSegments() const;

		/* UpdatePosition
		   Summary: Updates the position of this CollidableArea given a reference point (ex: the center of the area). 
		   Parameters:
		      referencePoint: The reference point indicating how to update the position of the CollidableArea. */
		virtual void UpdatePosition( const Point2D &referencePoint );

	private:
		Circle2D _circle;

		friend class CollidableLineSegment;
		friend class CollidableRectangle;
		friend std::ostream& operator<<( std::ostream &outputStream, const CollidableCircle &collidableCircle );
	};

	/* Output operator for a CollidableCircle object. */
	std::ostream& operator<<( std::ostream &outputStream, const CollidableCircle &collidableCircle );

}