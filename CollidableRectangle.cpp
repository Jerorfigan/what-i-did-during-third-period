/* CollidableRectangle implementation. */

#include "stdafx.h"

#include "CollidableCircle.h"
#include "CollidableLineSegment.h"
#include "CollidableRectangle.h"

namespace PROJECT_NAME
{

	/* Initializes a CollidableRectangle from a Rectangle2D. */
	CollidableRectangle::CollidableRectangle( const Rectangle2D &rect ) : _rect( rect )
	{
		_minX = _rect.Left();
		_maxX = _rect.Right();
		_minY = _rect.Bottom();
		_maxY = _rect.Top();
	}

	/* CollidesWith 
	   Summary: Returns true if this CollidableArea overlaps otherArea. 
	   Returns: True if this CollidableArea overlaps otherArea. */
	bool CollidableRectangle::CollidesWith( const CollidableArea* otherArea ) const
	{
		bool collisionFound = false;

		/* otherArea is one of the following:
		   CollidableRectangle
		   CollidableLineSegment
		   CollidableCircle

		   Determine which and then call the appropriate method to test for 
		   intersection of geometry. */
		
		if( const CollidableRectangle *collidableRectPtr = dynamic_cast< const CollidableRectangle* >( otherArea ) )
		{
			collisionFound = _rect.Intersects( collidableRectPtr->_rect );
		}
		else if( const CollidableLineSegment *collidableLineSegPtr = dynamic_cast< const CollidableLineSegment* >( otherArea ) ) 
		{
			collisionFound = _rect.Intersects( collidableLineSegPtr->_lineSeg );
		}
		else if( const CollidableCircle *collidableCirclePtr = dynamic_cast< const CollidableCircle* >( otherArea ) )
		{
			collisionFound = collidableCirclePtr->_circle.Intersects( _rect );
		}
		else
		{
			// shouldn't get here...blow up!
			assert( 1 == 0 );
		}

		return collisionFound;
	}

	/* GetLineSegments 
	   Summary: Returns a vector filled with the line segments that comprise this CollidableArea. 
	   Returns: A vector filled with the line segments that comprise this CollidableArea. */
	std::vector< LineSegment2D > CollidableRectangle::GetLineSegments() const
	{
		std::vector< LineSegment2D > lineSegments;
		lineSegments.push_back( LineSegment2D( _rect.TopLeft(), _rect.TopRight() ) );
		lineSegments.push_back( LineSegment2D( _rect.BottomLeft(), _rect.BottomRight() ) );
		lineSegments.push_back( LineSegment2D( _rect.BottomLeft(), _rect.TopLeft() ) );
		lineSegments.push_back( LineSegment2D( _rect.BottomRight(), _rect.TopRight() ) );
		return lineSegments;
	}

	/* Resize
		Summary: Resizes the collidable rectangle. 
		Parameters:
		    newWidth/newHeight: The new dimensions for the collidable rectangle. */
	void CollidableRectangle::Resize( double newWidth, double newHeight )
	{	
		_rect = Rectangle2D( _rect.Center(), newWidth, newHeight );

		_minX = _rect.Left();
		_maxX = _rect.Right();
		_minY = _rect.Bottom();
		_maxY = _rect.Top();
	}

	/* UpdatePosition
	   Summary: Updates the position of this CollidableArea given a reference point (ex: the center of the area). 
	   Parameters:
	      referencePoint: The reference point indicating how to update the position of the CollidableArea. */
	void CollidableRectangle::UpdatePosition( const Point2D &referencePoint )
	{
		_rect = Rectangle2D( referencePoint, _rect.Width(), _rect.Height() );	

		_minX = _rect.Left();
		_maxX = _rect.Right();
		_minY = _rect.Bottom();
		_maxY = _rect.Top();
	}

	/* GetRect 
	   Summary: Returns the underlying rectangle object for this CollidableRectangle. 
	   Returns: The underlying rectangle object for this CollidableRectangle. */
	const Rectangle2D& CollidableRectangle::GetRect() const
	{
		return _rect;
	}

	/* Output operator for a CollidableRectangle object. */
	std::ostream& operator<<( std::ostream &outputStream, const CollidableRectangle &collidableRect )
	{
		outputStream << "minX = " << collidableRect._minX << std::endl 
			<< "maxX = " << collidableRect._maxX << std::endl
			<< "minY = " << collidableRect._minY << std::endl
			<< "maxY = " << collidableRect._maxY << std::endl
			<< "topLeft: " << collidableRect._rect.TopLeft() << std::endl
			<< "topRight: " << collidableRect._rect.TopRight() << std::endl
			<< "bottomLeft: " << collidableRect._rect.BottomLeft() << std::endl
			<< "bottomRight: " << collidableRect._rect.BottomRight();

		return outputStream;
	}

}