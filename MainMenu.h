#pragma once

/* Defines the functionality of the main menu. */

#include "Game.h"
#include "Menu.h"

namespace PROJECT_NAME
{
	
	class MainMenu
	{
		// Constructors
	public:
		MainMenu();

		// Methods
	public:
		/* Process 
		   Summary: Processes the Main menu and returns the next game state. */
		Game::GameState Process();

		// Data
	private:
		struct MainMenuData
		{
			enum Selection { Start, Settings, Exit };

			Selection mSelection;
		};

		Menu mMenu;
		MainMenuData mData;

		// Menu event handlers (which are made friends)
		friend void StartButtonClicked( void* menuData );
		friend void SettingsButtonClicked( void* menuData );
		friend void ExitButtonClicked( void* menuData );
	};
}