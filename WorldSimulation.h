#pragma once

/* WorldSimulation defines the functionality for simulating a physical world in Box2D. */

#include "Box2D/Box2D.h"
#include "GameObject.h"

namespace PROJECT_NAME
{
	class WorldSimulation : public DynamicGameObject
	{
		// Internal classes
	private:
		class ContactListener : public b2ContactListener
		{
		public:
			void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
		};

	public:
		/* Initializes a WorldSimulation. */
		WorldSimulation();

		/* SyncWithMap
		   Summary: Updates the world simulation to mirror the current Map object: this includes
		            creating edge shapes for all the MapLine objects, rotating kinematic bodies for
					all the SawBlade objects, etc. */
		void SyncWithMap();

		/* Update
		   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
					last update (which should be accounted for in this update). 
		   Parameters: 
			  elapsedTime: The time elapsed in seconds since last update. */
		virtual void Update( double elapsedTime );

		/* SetFriction 
		   Summary: Sets the friction value of all the static fixtures in the Box2D world.*/
		void SetFriction( float32 friction );
		/* SetRestitution 
		   Summary: Sets the restitution value of all the static fixtures in the Box2D world.*/
		void SetRestitution( float32 restitution );

		/* GetWorld 
		   Summary: Returns a reference to the b2World instance. 
		   Returns: A reference to the b2World instance. */
		b2World& GetWorld();
		/* GetTimeStep 
		   Summary: Returns the timestep used in updating the physics world (in seconds). 
		   Returns: The timestep used in updating the physics world (in seconds). */
		float32 GetTimeStep() const;
		/* GetWorldStaticBody
		   Summary: Returns the world static body. This is the static body upon which all static fixtures
		            are to be placed.
		   Returns: The world static body. */
		b2Body* GetWorldStaticBody();

		/* GetLastContactForce
		   Summary: Returns the normalized force (between 0 and 1) of the most recent contact (if there was one) 
		            since the last call to this function or 0 if there wasn't one. Each time this function 
					is called, the contact force is reset to 0 in preparation for the next contact. 
		   Returns: The normalized force of the most recent contact or 0. */
		double GetLastContactForce();

		void ToggleContactComputation( bool turnOn );

	private:
		/* Replicates all the map lines by creating Box2D objects to represent them in the world 
		   simulation. */
		void ReplicateMapLines();
		/* Replicates all the map boundaries by creating Box2D objects to represent them in the world 
		   simulation. */
		void ReplicateMapBoundaries(); 
		/* Replicates all the saw blades by creating Box2D objects to represent them in the world 
		   simulation. */
		void ReplicateSawBlades();

		b2World _world;
		b2Body *_staticBody;

		const float32 _physicsTimeStep; // in seconds
		float32 _worldFriction;
		float32 _worldRestitution;
		const int32 _velocityIterations;
		const int32 _positionIterations;

		ContactListener _contactListener;
		double _lastContactForce;
		bool _computeContacts;

		// Friend declarations
		friend class ContactListener;
	};
}