#pragma once

/* The Map class manages minimum bounding rectangles for all objects on the map and is used to detect 
   collisions. In addition, it contains functionality to simulate hand drawing all the map objects
   (which is utilized at the beginning of every level). */

#include <set>
#include <string>
#include <vector>

#include "GameObject.h"
#include "Handle.h"
#include "Handle.cpp"
#include "Point2D.h"
#include "Rectangle2D.h"

namespace PROJECT_NAME
{

	class Map : public GameObject
	{
	public:
		typedef std::vector< std::pair< GameObject::ObjectId, std::pair< const double*, const double* > > > AxisAlignedBounds;

		/* Initializes a map from a map file. */
		Map( std::string mapFile );

		/* GetObjectsCollidingWith 
		   Summary: Returns a std::vector containing pointers to all the CollidableGameObjects that collide 
					with targetObject.
		   Parameters:
			  id: The id of the object to find all colliding objects for. 
		   Returns: A std::vector containing pointers to all the CollidableGameObjects that collide 
					with targetObject. */
		std::vector< CollidableGameObject* > GetObjectsCollidingWith( GameObject::ObjectId targetObject );

		/* GetSpawnPoint
		   Summary: Returns the spawning point for this map. 
		   Returns: The spawning point for this map. */
		const Point2D& GetSpawnPoint() const;

		/* GetMapBounds 
		   Summary: Returns the boundary rectangle for this map.
		   Returns: The boundary rectangle for this map. */
		const Rectangle2D& GetMapBounds() const;

		/* RegisterObject
		   Summary: Registers the specified collidable object with this Map. Registration means this 
		            map will keep track of the minimum bounding rectangle for the specified object. Once 
					registered, the specified object may use this map to check for collisions.
		   Parameters:
		      objectId: The id of the object to register. */
		void RegisterObject( GameObject::ObjectId objectId );

	private:
		/* Parses the MapLine data and creates the MapLine object. */
		void CreateMapLine( std::stringstream &lineTokenizer );
		/* Parses the MapExit data and creates the MapExit object. */
		void CreateMapExit( std::stringstream &lineTokenizer );
		/* Parses the SawBlade data and creates the SawBlade object. */
		void CreateSawBlade( std::stringstream &lineTokenizer );
		/* Parses the DeathRay data and creates the DeathRay object. */
		void CreateDeathRay( std::stringstream &lineTokenizer );
		/* Parses the WreckingBall data and creates the WreckingBall object. */
		void CreateWreckingBall( std::stringstream &lineTokenizer );
		/* Parses the FlameTrap data and creates the FlameTrap object. */
		void CreateFlameTrap( std::stringstream &lineTokenizer );

		/* Parses the map boundary data. */
		void ReadMapBounds( std::stringstream &lineTokenizer );

		/* Returns a set containing the ids of the objects whose minimum bounding rectangles overlap 
		   targetObject's minimum bounding rectangle. */
		std::set< GameObject::ObjectId > GetObjectsOverlapping( GameObject::ObjectId targetObject );

		/* Returns a set containing the ids of the objects whose axis-aligned bounds overlap 
		   targetObject's axis-aligned bounds. Assumes AxisAlignedBounds is sorted in ascending order of 
		   lower bound. */
		std::set< GameObject::ObjectId > GetObjectsOverlappingWithRespectToAxis( GameObject::ObjectId targetObject, 
			const AxisAlignedBounds &axisAlignedBounds ) const;

		/* Utility function used in sorting the axis aligned bounds containers. Returns true if the first bounding pair 
		   should come before the second bounding pair. */
		static bool CompareBoundingPairs( Map::AxisAlignedBounds::iterator firstBoundingPairItr, 
			Map::AxisAlignedBounds::iterator secondBoundingPairItr );

		/* xAxisAlignedBounds and yAxisAlignedBounds store sorted minimum bounding rectangle info for all the objects
		   on the map. These are used in the collision detection process to prune object pairs that don't need
		   to be checked for collision due to the fact that their bounds don't overlap. The bounding pairs contained 
		   within are sorted by lower bound. */
		AxisAlignedBounds xAxisAlignedBounds;
		AxisAlignedBounds yAxisAlignedBounds;
		
		Point2D _spawnPoint;
		Rectangle2D _mapBounds;
	};

}