
#include "stdafx.h"

#include <time.h>

#include "Game.h"
#include "UnitTestDriver.h"

using namespace PROJECT_NAME;

int main()
{
	// Seed the random number generator.
	srand( clock() );

	// Start by running all unit tests. Program will fail if any unit tests don't pass.
	PROJECT_NAME_UnitTests::UnitTestDriver();

	Game::Start();

	return 0;
}
