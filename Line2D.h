#pragma once

/* Defines the functionality for a line in 2D. */ 

#include "Point2D.h"

namespace PROJECT_NAME
{

	class Line2D
	{
	public:
		/* Initializes a line with a finite slope of 0 and y-intercept of 0. */
		Line2D(); 
		/* Initializes a line from the passed in points on the line. */
		Line2D( Point2D point1, Point2D point2 );

		/* Contains
		   Summary: Determines whether this line contains point. 
		   Parameters: 
			  point: The Point2D object to test for containment.
		   Returns: True if this line contains point, false otherwise. */
		bool Contains( const Point2D &point ) const;
		/* Intersects
		   Summary: Determines whether this line intersects the passed-in line. If the lines
					are equal, this function will return false, thus equality must be determined
					with the == operator.
		   Parameters: 
			  line: The line to test for intersection with this line.
		   Returns: True if this line intersects the passed-in line, false otherwise. */
		bool Intersects( const Line2D &line, Point2D &intersectionPoint ) const;

		/* operator: ==
		   Summary: Tests two lines for equality. Two lines are equal if they have finite slope and
					their slope and y-intercepts match or if they have infinite slope and their 
					x-intercepts match.
		   Parameters: 
			  line: The line to test for equality with this line.
		   Returns: True if the lines are equal. */
		bool operator==( const Line2D &line ) const;
		/* operator: !=
		   Summary: Tests two lines for inequality. Two lines are unequal if they're not equal (see 
			        definition of == operator).
		   Parameters: 
			  line: The line to test for inequality with this line.
		   Returns: True if the lines are unequal. */
		bool operator!=( const Line2D &line ) const;

		/* Slope
		   Summary: Returns the slope of the line. Return value undefined if line has infinite slope. 
		   Returns: The slope of the line. Return value undefined if line has infinite slope. */
		double Slope() const;
		/* YIntercept
		   Summary: Returns the y-intercept of the line. Return value undefined if line has infinite slope. 
		   Returns: The y-intercept of the line. Return value undefined if line has infinite slope. */
		double YIntercept() const;
		/* XIntercept
		   Summary: Returns the x-intercept of a vertical line. Return value undefined if line has finite slope. 
		   Returns: The x-intercept of a vertical line. Return value undefined if line has finite slope. */
		double XIntercept() const;
		/* FiniteSlope
		   Summary: Returns true if the line has finite slope, false otherwise. 
		   Returns: True if the line has finite slope, false otherwise. */
		bool FiniteSlope() const;

	private:
		double _slope;
		double _yIntercept;
		double _xIntercept;
		bool _finiteSlope;
	};

}