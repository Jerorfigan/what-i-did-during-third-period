#pragma once

/* Defines the functionality for a 2D Cartesian point. */

#include <iostream>

namespace PROJECT_NAME
{

	struct Vector2D;

	struct Point2D
	{
		/* Enumeration defining the terms for the general location of a point in the 2D Cartesian plane. */
		enum PointLocation { FirstQuadrant, SecondQuadrant, ThirdQuadrant, FourthQuadrant, 
			                 PositiveXAxis, NegativeXAxis, PositiveYAxis, NegativeYAxis, Origin };

		/* Default constructor initializes a point with _x and _y equal to 0. */
		Point2D();
		/* Constructor that initializes a point from passed in x and y values. */
		Point2D( double x, double y );
		/* Constructor that initializes a point from a vector. */
		Point2D( const Vector2D &vec );

		/* PointLocation 
		   Summary: Computes the general location of the point ( i.e. first quadrant, positive x-axis, origin, etc).
		   Returns: The general location of the point. */
		PointLocation GetPointLocation() const;

		/* operator: == 
		   Summary: Tests whether two points are equal. Points are equal if _x and _y components are both equal.
		   Parameters: 
			  point: The point to test for equality with.
		   Returns: True if the points are equal, false otherwise. */
		bool operator==( const Point2D &point ) const;
		/* operator: != 
		   Summary: Tests whether two points are unequal. Points are unequal if they're not equal (see definition of
		            == operator).
		   Parameters: 
			  point: The point to test for inequality with.
		   Returns: True if the points are unequal, false otherwise. */
		bool operator!=( const Point2D &point ) const;

		/* operator: += 
		   Summary: Adds a vector to this point and stores the result in this point.
		   Parameters:
		      vector: The vector to add to this point. 
		   Returns: A reference to this point. */
		Point2D& operator+=( const Vector2D &vector );

		double _x;
		double _y;
	};

	/* Output operator for a Point2D object. */
	std::ostream& operator<<( std::ostream &outputStream, const Point2D &point );

}