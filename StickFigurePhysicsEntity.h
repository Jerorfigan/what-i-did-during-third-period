#pragma once

/* StickFigurePhysicsEntity defines the functionality for representing the Box2D physics entity of a
   StickFigure. The StickFigure object will own an instance of this class, which it can use to keep
   track of its corresponding Box2D physics entity. */

#include "GameObject.h"
#include "CollidableGameObject.h"
#include "CollidableRectangle.h"
#include "Rectangle2D.h"

namespace PROJECT_NAME
{
	class StickFigurePhysicsEntity : public CollidableGameObject
	{
	public:
		// Initializes a StickFigurePhysicsEntity from a rectangle defining its collidable area.
		StickFigurePhysicsEntity( Rectangle2D collidableArea );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea();

	private:
		CollidableRectangle _collidableArea;

		friend class StickFigure;
	};
}