#pragma once

/* All global template functions are declared here. */

namespace PROJECT_NAME
{

	/* Sorts the range denoted by random-access iterator pair start and end using the insertion sort
	   algorithm. Sort is in-place. The default comparison done on the elements in the range is <, which 
	   sorts the range in ascending order. The default comparison may be overrided by passing in a 
	   pointer to a comparator function taking two IterType arguments and returning a bool. This 
	   function should return true if the element referenced by the first iterator should come before the 
	   element referenced by the second iterator and false otherwise. */
	template < typename IterType >
	void InsertionSort( IterType &start, IterType &end, 
		bool (*firstComesBeforeSecond)( IterType, IterType ) = 0 );

	template < typename NumericType >
	std::string GetString( NumericType num );
}

#include "GlobalTemplateFunctions.cpp"