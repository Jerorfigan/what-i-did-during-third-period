#pragma once

/* Defines the structure of a window event. */

#include <string>

#include "KeyboardKey.h"
#include "MouseButton.h"
#include "Point2D.h"

namespace PROJECT_NAME
{

	struct GameEvent
	{
		enum EventType { Closed, KeyPressed, None, MouseMoved, MouseButtonPressed, MouseButtonReleased,
						 MouseEnteredWindow, MouseLeftWindow, MenuExit, LevelTransition };

		EventType _event;
		KeyboardKey::Key _key;
		char _charCode;
		MouseButton::Button _mouseButton;
		Point2D _mouseHotSpot;
	};

}