#pragma once

/* Defines the functionality of a sound manager, which manages all the particular sounds associated with a game object. */

#include <map>
#include <string>
#include <vector>

#include "IAudioProvider.h"

namespace PROJECT_NAME
{

	class SoundManager
	{
		// Internal classes
	private:
		struct SoundData
		{
			IAudioProvider::AudioId mId;
			bool mPlayNow;
			bool mLoop;
			bool mTruncate;
			float mVolume;
		};

		// Typedefs
	private:
		typedef std::map< std::string, SoundData > SoundDataList;
		typedef std::vector< std::string > SoundNameList;

		// Constructors
	public:
		SoundManager( std::string soundFile );

		// Methods
	public:
		void QueueToPlay( std::string soundName, float volume = 100, bool loop = false, bool forceReplay = false, bool truncate = true );
		void Update();

	private:
		bool IsInSoundList( const SoundNameList &list, SoundNameList::value_type itemToLookFor );

		// Data
	private:
		SoundDataList mSoundDataList;
		SoundNameList mPlayList;
		SoundNameList mPlayQueue;
	};

}