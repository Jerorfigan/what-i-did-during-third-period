/* Background implementation. */

#include "stdafx.h"

#include "Background.h"
#include "Game.h"
#include "Point2D.h"

namespace PROJECT_NAME
{
	/* Initializes a Background from a background art file and a Rectangle2D indicating the 
	   area of the image that contains the content (as opposed to black buffer area). By default
	   the entire image will be considered content. */
	Background::Background( std::string backgroundArtFile, const Rectangle2D &contentArea ) : 
		GameObject( "background"),
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( backgroundArtFile ), 
		"background" )
	{
		if( contentArea.Width() != 0 )
		{
			// Content area was specified, so position the background image such that the top left corner of the 
			// content area sits at the origin. 
			Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( GraphicsId(), 
				Point2D( -contentArea.Left(), -contentArea.Bottom() ) );
		}
	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void Background::Draw( double interpolation )
	{
		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), Game::GetGameAssetManager().GetAppWindow() );
	}

}