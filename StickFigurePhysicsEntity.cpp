/* StickFigurePhysicsEntity */

#include "stdafx.h"

#include "StickFigurePhysicsEntity.h"

namespace PROJECT_NAME
{

	// Initializes a StickFigurePhysicsEntity from a rectangle defining its collidable area.
	StickFigurePhysicsEntity::StickFigurePhysicsEntity( Rectangle2D collidableArea ) : 
		GameObject( "stick_figure_physics_entity" ), 
		CollidableGameObject( "stick_figure_physics_entity", 
		KinematicState( collidableArea.Center(), Vector2D( 0, 0 ), Vector2D( 0, 0 ) ), false ),
		_collidableArea( collidableArea )
	{

	}

	/* GetCollidableArea
		Summary: Gets the CollidableArea for this CollidableGameObject. 
		Returns: The CollidableArea for this CollidableGameObject. */
	CollidableArea* StickFigurePhysicsEntity::GetCollidableArea()
	{
		return &_collidableArea;
	}

}