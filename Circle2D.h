#pragma once

/* Circle2D defines the functionality for a circle in 2D. */

#include "LineSegment2D.h"
#include "Point2D.h"
#include "Rectangle2D.h"

namespace PROJECT_NAME
{

	class Circle2D
	{
	public:
		// Initializes a circle at (0,0) with a radius of 1.
		Circle2D();
		// Initializes a circle at center with the specified radius.
		Circle2D( Point2D center, double radius );

		/* Contains 
		   Summary: Returns true if this circle contains the passed in point. 
		   Parameters:
		      point: The point to test against. 
		   Returns: True if this circle contains the passed in point. */
		bool Contains( Point2D point ) const;
		/* Intersects
		   Summary: Determines whether this circle intersects the passed in rectangle. 
		   Parameters: 
		      rect: The rectangle to test for intersection against. 
		   Returns: True if this circle intersects the passed in Rectangle. */
		bool Intersects( const Rectangle2D &rect ) const;
		/* Intersects
		   Summary: Determines whether this circle intersects the passed in line segment. 
		   Parameters: 
		      lineSeg: The line segment to test for intersection against. 
		   Returns: True if this circle intersects the passed in line segment. */
		bool Intersects( const LineSegment2D &lineSeg ) const;
		/* Intersects
		   Summary: Determines whether this circle intersects the passed in circle. 
		   Parameters: 
		      circle: The circle to test for intersection against. 
		   Returns: True if this circle intersects the passed in circle. */
		bool Intersects( const Circle2D &circle ) const;

		/* Center
		   Summary: Returns the center point of the circle. 
		   Returns: The center point of the circle. */
		const Point2D& Center() const;
		/* Radius
		   Summary: Returns the radius of the circle. 
		   Returns: The radius of the circle. */
		double Radius() const;

	private:
		Point2D _center;
		double _radius;
	};

}