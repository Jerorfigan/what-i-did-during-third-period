#pragma once

/* Timer class defines the functionality to get the time elapsed since a reset point, as
   well as suspending program execution. */

#include "Windows.h"

namespace PROJECT_NAME
{

	class Timer
	{
	public:
		/* Initializes a timer that measures time elapsed since system start. */
		Timer();

		/* Sleep 
		   Summary: Suspends program execution for a duration in seconds. 
		   Parameters: 
		      seconds: The amount of time in seconds to sleep. */
		static void Sleep( double seconds );

		/* Reset
		   Summary: Creates a point of reference for ElapsedTime, which will return the
	                amount of time that has passed since the timer was last reset. */
		void Reset();
		/* GetElapsedTime
		   Summary: Returns the amount of time in seconds that has passed since the timer 
		            was last reset. 
		   Returns: The amount of time in seconds that has passed since the timer 
		            was last reset.  */
		double GetElapsedTime();

	private:
		DWORD _millisecondsAtReset;
	};

}