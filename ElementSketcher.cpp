/* ElementSketcher implementation. */

#include "stdafx.h"

#include "ElementSketcher.h"

namespace PROJECT_NAME
{

	/* Initializes an ElementSketcher from an object id. */
	ElementSketcher::ElementSketcher( GameObject::ObjectId objectId ) : _objectId( objectId )
	{

	}

	/* GetObjectId 
	   Summary: Returns the id of the object tied to this ElementSketcher. 
	   Returns: The id of the object tied to this ElementSketcher. */
	GameObject::ObjectId ElementSketcher::GetObjectId() const
	{
		return _objectId;
	}

	/* Virtual destructor to allow deletion of derived objects through ElementSketcher pointer. */
	ElementSketcher::~ElementSketcher()
	{
	}

}