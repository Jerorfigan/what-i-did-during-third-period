#pragma once

#include "GameEvent.h"

/* Declaration of game event-related functions. */

namespace PROJECT_NAME
{
	/* CheckForGameEvent 
	   Summary: Checks for an event in the graphics provider's queue as well as the game event queue. 
	   Parameters:
			gameEvent: Filled with the event info if one is found. 
	   Returns: true if an event is found. */
	bool CheckForGameEvent( GameEvent &gameEvent );

}