#pragma once

namespace PROJECT_NAME
{

	class IntSequence
	{
	public:
		typedef std::size_t SequenceValue;
		/* Initializes an IntSequence with an initial value (1 is default). */
		IntSequence( SequenceValue initialValue = 1 );

		/* Value 
		   Summary: Returns the value of the current sequence.
		   Returns: The value of the current sequence. */
		SequenceValue Value();

		/* Operator: ++ (prefix)
		   Summary: Increment the sequence by 1.
		   Returns: A reference to the incremented sequence value. */
		SequenceValue& operator++();
		/* Operator: ++ (postfix)
		   Summary: Increment the sequence by 1.
		   Returns: The value of the pre-incremented sequence value. */
		SequenceValue operator++( int );

		/* Reset 
		   Summary: Resets the current sequence value to its initial value. */
		void Reset();
	private:
		SequenceValue _sequenceValue;
		SequenceValue _initialValue;
	};

}