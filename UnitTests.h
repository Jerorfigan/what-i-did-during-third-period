#pragma once

/* Declares all the unit test functions. */

namespace PROJECT_NAME_UnitTests
{

	/* Unit test for the Line2D class. */
	void Line2DUnitTest();
	/* Unit test for the LineSegment2D class. */
	void LineSegment2DUnitTest();
	/* Unit test for the Point2D class. */
	void Point2DUnitTest();
	/* Unit test for the Rectangle2D class. */
	void Rectangle2DUnitTest();
	/* Unit test for the Vector2D class. */
	void Vector2DUnitTest();
	/* Unit test for the Graphics interface. */
	void GraphicsInterfaceTest();
	/* Unit test for the global template functions. */
	void GlobalTemplateFunctionsTest();

}