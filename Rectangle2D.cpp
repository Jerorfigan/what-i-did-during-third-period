/* Implements the functionality for an axis-aligned rectangle in 2D. */

#include "stdafx.h"

#include <vector>

#include "LineSegment2D.h"
#include "Rectangle2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME
{

	/* Default constructor initializes a rectangle with all four points set to (0,0). */
	Rectangle2D::Rectangle2D() : _topLeft( Point2D() ), _topRight( Point2D() ), 
	_bottomLeft( Point2D() ), _bottomRight( Point2D() )
	{

	}

	/* Constructor that initializes a rectangle from passed in points. */
	Rectangle2D::Rectangle2D( Point2D topLeft, Point2D topRight, Point2D bottomLeft, Point2D bottomRight ) :
	_topLeft( topLeft ), _topRight( topRight ), _bottomLeft( bottomLeft ), _bottomRight( bottomRight )
	{
		// Do some verification to make sure we got valid points: confirm top left and bottom right points have the 
		// expected relationship to their adjacent points.
		assert( _topLeft._x == _bottomLeft._x && _topLeft._x < _topRight._x );
		assert( _topLeft._y > _bottomLeft._y && _topLeft._y == _topRight._y );
		assert( _bottomRight._x == _topRight._x && _bottomRight._y == _bottomLeft._y );

	}

	/* Initializes a Rectangle2D from a center point and a width and height. */
	Rectangle2D::Rectangle2D( Point2D center, double width, double height )
	{
		assert( width >= 0 );
		assert( height >= 0 );
		_topLeft = Point2D( center._x - width * 0.5, center._y + height * 0.5 );
		_topRight = Point2D( center._x + width * 0.5, center._y + height * 0.5 );
		_bottomLeft = Point2D( center._x - width * 0.5, center._y - height * 0.5 );
		_bottomRight = Point2D( center._x + width * 0.5, center._y - height * 0.5 );
	}

	/* Initializes the smallest rectangle enclosing a vector given its origin. If the vector follows the
	   x or y-axis, the initialized rectangle will get the passed-in axial width. */
	Rectangle2D::Rectangle2D( Point2D origin, const Vector2D &vector, double axialWidth )
	{
		// Initialize two points:
		// Relative Vector Point - The point form of the vector relative to the passed-in origin.
		// Absolute Vector Point - The point form of the vector relative to (0,0).
		Point2D relVectorPoint( vector );
		Point2D absVectorPoint( vector + Vector2D( origin ) );

		// Derive the rectangle vertices using the location of the relative vector point, as well as the origin and
		// absolute vector point.
		if( relVectorPoint.GetPointLocation() == Point2D::Origin )
		{
			Rectangle2D();
		}
		else if( relVectorPoint.GetPointLocation() == Point2D::PositiveXAxis )
		{
			_topLeft = Point2D( origin._x, origin._y + 0.5 * axialWidth );
			_topRight = Point2D( absVectorPoint._x, absVectorPoint._y + 0.5 * axialWidth );
			_bottomLeft = Point2D( origin._x, origin._y - 0.5 * axialWidth );
			_bottomRight = Point2D( absVectorPoint._x, absVectorPoint._y - 0.5 * axialWidth );
		} 
		else if( relVectorPoint.GetPointLocation() == Point2D::NegativeXAxis )
		{
		    _topLeft = Point2D( absVectorPoint._x, absVectorPoint._y + 0.5 * axialWidth ); 
			_topRight = Point2D( origin._x, origin._y + 0.5 * axialWidth );
			_bottomLeft = Point2D( absVectorPoint._x, absVectorPoint._y - 0.5 * axialWidth ); 
			_bottomRight = Point2D( origin._x, origin._y - 0.5 * axialWidth );
		}
		else if( relVectorPoint.GetPointLocation() == Point2D::PositiveYAxis )
		{
			_topLeft = Point2D( absVectorPoint._x - 0.5 * axialWidth, absVectorPoint._y );
			_topRight = Point2D( absVectorPoint._x + 0.5 * axialWidth, absVectorPoint._y );
			_bottomLeft = Point2D( origin._x - 0.5 * axialWidth, origin._y );
			_bottomRight = Point2D( origin._x + 0.5 * axialWidth, origin._y );
		}
		else if( relVectorPoint.GetPointLocation() == Point2D::NegativeYAxis )
		{
			_topLeft = Point2D( origin._x - 0.5 * axialWidth, origin._y ); 
			_topRight = Point2D( origin._x + 0.5 * axialWidth, origin._y ); 
			_bottomLeft = Point2D( absVectorPoint._x - 0.5 * axialWidth, absVectorPoint._y );
			_bottomRight = Point2D( absVectorPoint._x + 0.5 * axialWidth, absVectorPoint._y );
		}
		else if( relVectorPoint.GetPointLocation() == Point2D::FirstQuadrant )
		{
			_topLeft = Point2D( origin._x, absVectorPoint._y );
			_topRight = Point2D( absVectorPoint._x, absVectorPoint._y );
			_bottomLeft = Point2D( origin._x, origin._y );
			_bottomRight = Point2D( absVectorPoint._x, origin._y );
		}
		else if( relVectorPoint.GetPointLocation() == Point2D::SecondQuadrant )
		{
			_topLeft = Point2D( absVectorPoint._x, absVectorPoint._y );
			_topRight = Point2D( origin._x, absVectorPoint._y );
			_bottomLeft = Point2D( absVectorPoint._x, origin._y );
			_bottomRight = Point2D( origin._x, origin._y );
		}
		else if( relVectorPoint.GetPointLocation() == Point2D::ThirdQuadrant )
		{
			_topLeft = Point2D( absVectorPoint._x, origin._y );
			_topRight = Point2D( origin._x, origin._y );
			_bottomLeft = Point2D( absVectorPoint._x, absVectorPoint._y );
			_bottomRight = Point2D( origin._x, absVectorPoint._y );
		}
		else // Fourth Quadrant
		{
			_topLeft = Point2D( origin._x, origin._y ); 
			_topRight = Point2D( absVectorPoint._x, origin._y );
			_bottomLeft = Point2D( origin._x, absVectorPoint._y );
			_bottomRight = Point2D( absVectorPoint._x, absVectorPoint._y );
		}
	}

	/* Width
	   Summary: Returns the width of the rectangle. 
	   Returns: The width of the rectangle. */
	double Rectangle2D::Width() const
	{
		return _topRight._x - _topLeft._x; 
	}

	/* Height
	   Summary: Returns the height of the rectangle.
	   Returns: The height of the rectangle. */
	double Rectangle2D::Height() const
	{
		return _topLeft._y - _bottomLeft._y;
	}

	/* Top
	   Summary: Returns the Y coordinate of the points comprising the top edge of the rectangle. 
	   Returns: The Y coordinate of the points comprising the top edge of the rectangle. */
	double Rectangle2D::Top() const
	{
		return _topLeft._y;
	}

	/* Bottom
	   Summary: Returns the Y coordinate of the points comprising the bottom edge of the rectangle. 
	   Returns: The Y coordinate of the points comprising the bottom edge of the rectangle. */
	double Rectangle2D::Bottom() const
	{
		return _bottomLeft._y;
	}

	/* Left
	   Summary: Returns the X coordinate of the points comprising the left edge of the rectangle. 
	   Returns: The X coordinate of the points comprising the left edge of the rectangle. */
	double Rectangle2D::Left() const
	{
		return _topLeft._x;
	}

	/* Right
	   Summary: Returns the X coordinate of the points comprising the right edge of the rectangle. 
	   Returns: The X coordinate of the points comprising the right edge of the rectangle. */
	double Rectangle2D::Right() const
	{
		return _topRight._x;
	}

	/* Center
	   Summary: Computes the center point of the rectangle. 
	   Returns: The center point of the rectangle. */
	Point2D Rectangle2D::Center() const
	{
		return Point2D( 0.5 * ( _topLeft._x + _topRight._x ), 
						0.5 * ( _topLeft._y + _bottomLeft._y ) );
	}

	/* Contains
	   Summary: Determines whether this rectangle contains the passed-in point. 
	   Parameters: 
		  point: The point to test for containment.
	   Returns: True if this rectangle contains the passed-in point, false otherwise. */
	bool Rectangle2D::Contains( const Point2D &point ) const
	{
		bool containsPoint = false;

		if( point._x >= Left() && point._x <= Right() &&
			point._y <= Top() && point._y >= Bottom() )
		{
			containsPoint = true;
		}

		return containsPoint;
	}

	/* Intersects
	   Summary: Determines whether this rectangle intersects the passed-in rectangle. 
	   Parameters: 
		  rect: The rectangle to check for intersection with.
	   Returns: True if this rectangle intersects passed-in rectangle, false otherwise. */
	bool Rectangle2D::Intersects( const Rectangle2D &rect ) const
	{
		// This rectangle intersects the passed-in rectangle if 1) this rectangle contains
		// any of the vertices of the passed-in rectangle or vice versa or 2) any of this 
		// rectangle's sides intersect any of the passed-in rectangle's sides.

		bool intersects = false;

		typedef std::vector< Point2D > Points;
		Points theseVertices, rectsVertices;

		theseVertices.push_back( _topLeft );
		theseVertices.push_back( _topRight );
		theseVertices.push_back( _bottomLeft );
		theseVertices.push_back( _bottomRight );

		rectsVertices.push_back( rect._topLeft );
		rectsVertices.push_back( rect._topRight );
		rectsVertices.push_back( rect._bottomLeft );
		rectsVertices.push_back( rect._bottomRight );

		for( std::size_t index = 0; index < 4; ++index )
		{
			if( Contains( rectsVertices[ index ] ) ||
				rect.Contains( theseVertices[ index ] ) )
			{
				intersects = true;
				break;
			}
		}

		if( !intersects )
		{
			/* Haven't detected intersection using vertex method so now need to investigate
			   intersection of sides. */
			
			typedef std::vector< LineSegment2D > LineSegments;

			LineSegments theseSides;
			theseSides.push_back( LineSegment2D( _topLeft, _topRight ) );
			theseSides.push_back( LineSegment2D( _bottomLeft, _topLeft ) );
			theseSides.push_back( LineSegment2D( _bottomLeft, _bottomRight ) );
			theseSides.push_back( LineSegment2D( _bottomRight, _topRight ) );

			for( LineSegments::const_iterator currentSegment = theseSides.begin();
				 currentSegment != theseSides.end(); ++currentSegment )
			{
				if( rect.Intersects( *currentSegment ) )
				{
					intersects = true;
					break;
				}
			}
		}

		return intersects;
	}

	/* Intersects
	   Summary: Determines whether this rectangle intersects the passed-in line segment. 
	   Parameters:
		  lineSeg: The line segment to check for intersection with.
	   Returns: True if this rectangle intersects passed-in line segment, false otherwise. */
	bool Rectangle2D::Intersects( const LineSegment2D &lineSeg ) const
	{
		// A line segment intersects a rectangle if 1) the rectangle contains the start and/or end
		// point of the line segment or 2) the line segment intersects with one or more of the rectangle's
		// sides.

		bool intersects = false;

		if( Contains( lineSeg.Start() ) || Contains( lineSeg.End() ) )
		{
			intersects = true;
		}
		else 
		{
			typedef std::vector< LineSegment2D > LineSegments;
			LineSegments _sides;
			_sides.push_back( LineSegment2D( _topLeft, _topRight ) );
			_sides.push_back( LineSegment2D( _bottomLeft, _topLeft ) );
			_sides.push_back( LineSegment2D( _bottomLeft, _bottomRight ) );
			_sides.push_back( LineSegment2D( _bottomRight, _topRight ) );

			for( LineSegments::const_iterator currentSegment = _sides.begin(); 
				 currentSegment != _sides.end(); ++currentSegment )
			{
				if( lineSeg.Intersects( *currentSegment ) )
				{
					intersects = true;
					break;
				}
			}
		}

	    return intersects;
	}

	/* TopLeft
	   Summary: Returns the top-left vertex of this rectangle. 
	   Returns: The top-left vertex of this rectangle. */
	const Point2D& Rectangle2D::TopLeft() const
	{
		return _topLeft;
	}

	/* TopRight
	   Summary: Returns the top-right vertex of this rectangle. 
	   Returns: The top-right vertex of this rectangle. */
	const Point2D& Rectangle2D::TopRight() const
	{
		return _topRight;
	}

	/* BottomLeft
	   Summary: Returns the bottom-left vertex of this rectangle. 
	   Returns: The bottom-left vertex of this rectangle. */
	const Point2D& Rectangle2D::BottomLeft() const
	{
		return _bottomLeft;
	}

	/* BottomRight
	   Summary: Returns the bottom-right vertex of this rectangle. 
	   Returns: The bottom-right vertex of this rectangle. */
	const Point2D& Rectangle2D::BottomRight() const
	{
		return _bottomRight;
	}

}