/* FlameTrap implementation. */

#include "stdafx.h"

#include "FlameTrap.h"
#include "Game.h"
#include "Rectangle2D.h"

namespace PROJECT_NAME
{

		/* Initializes a dummy FlameTrap. Used when getting all objects of type FlameTrap from 
		   GameObjectManager. */
		FlameTrap::FlameTrap() : GameObject( "dummy" ), CollidableGameObject( "dummy" ),
			DynamicGameObject( 1, "dummy" ),
			VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString("dummy"),
			"dummy" ), _collidableArea( LineSegment2D() ), 
			_spriteAnimator( RESOURCE_PATH( "Animation/FlameTrap/FlameTrapAnimationFile.txt" ), GraphicsId(), _position )
		{

		}

		/* Initializes a FlameTrap from an object id, a position, and a vector direction for the flame. */
		FlameTrap::FlameTrap( GameObject::ObjectId objectId, Point2D position, Vector2D flameDirection ) : 
			GameObject( objectId ), CollidableGameObject( objectId ), DynamicGameObject( 1, objectId ), 
			VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Placeholder.png" ) ), 
			objectId ), _position( position ), _flameDirection( flameDirection ), _collidableArea( LineSegment2D() ), 
			_spriteAnimator( RESOURCE_PATH( "Animation/FlameTrap/FlameTrapAnimationFile.txt" ), GraphicsId(), _position ),
			_currentState( Idle )
		{
			_flameTimer.Reset();

			// Force _flameDirection to be a unit vector.
			_flameDirection = _flameDirection.UnitVector();

			// Initialize the flame angle based on the angle of the passed-in flame direction vector.
			_flameAngle = -( _flameDirection.Angle() - 90.0 );

			// Play the flame idle animation since we're initially in the idle state.
			_spriteAnimator.PlayAnimation( "FlameIdle", true, &_flameAngle );
		}

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		CollidableArea* FlameTrap::GetCollidableArea()
		{
			return &_collidableArea;
		}

		/* Update
		   Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
					last update (which should be accounted for in this update). 
		   Parameters: 
			  elapsedTime: The time elapsed in seconds since last update. */
		void FlameTrap::Update( double elapsedTime )
		{
			switch( _currentState )
			{
			case Idle:
				{
					// Check if it's time to spout.
					if( _flameTimer.GetElapsedTime() > _flameSpoutPeriod )
					{
						// It's time to spout, so update the flame trap's collidable area to be a line segment 
						// that consists of the flame trap position and the point offset from the flame trap
						// position in the direction of _flameDirection and _flameRange distance away.

						Point2D offsetPoint = _flameRange * _flameDirection + _position;
						_collidableArea = LineSegment2D( _position, offsetPoint );

						// Play the flame spout animation.
						_spriteAnimator.PlayAnimation( "FlameSpouting", true, &_flameAngle );

						// Transition to the Spouting state.
						_currentState = Spouting;

						// Reset the flame timer so that it may be used to time the flame spouting period.
						_flameTimer.Reset();
					}
				}
				break;
			case Spouting:
				{
					// Check if we're done spouting.
					if( _flameTimer.GetElapsedTime() > _flameSpoutDuration )
					{
						// We're done spouting so set the flame trap's collidable area to be line segment of
						// zero length with both points at the origin (so that it can't be collided with).

						_collidableArea = LineSegment2D();

						// Play the flame idle animation.
						_spriteAnimator.PlayAnimation( "FlameIdle", true, &_flameAngle );

						// Transition to the idle state.
						_currentState = Idle;

						// Reset the flame timer so that it may be used to time the flame idle period.
						_flameTimer.Reset();
					}
				}
				break;
			}

			_spriteAnimator.Update( elapsedTime );
		}

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		void FlameTrap::Draw( double interpolation )
		{
			Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
				Game::GetGameAssetManager().GetAppWindow() );
		}

		const double FlameTrap::_flameRange = 100.0;
		const double FlameTrap::_flameSpoutPeriod = 3.0;
		const double FlameTrap::_flameSpoutDuration = 2.0;
}