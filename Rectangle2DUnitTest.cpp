/* Defines the unit test for the Rectangle2D class. */

#include "stdafx.h"

#include "LineSegment2D.h"
#include "Rectangle2D.h"
#include "Vector2D.h"

namespace PROJECT_NAME_UnitTests
{

	using namespace PROJECT_NAME;

	void Rectangle2DUnitTest()
	{
		/* Test 1: Exercise the default constructor and verify that it initializes a rectanle with all
		           four vertices set to (0,0). */
		{
			Rectangle2D rect;
			assert( rect.TopLeft() == Point2D( 0, 0 ) );
			assert( rect.TopRight() == Point2D( 0, 0 ) );
			assert( rect.BottomLeft() == Point2D( 0, 0 ) );
			assert( rect.BottomRight() == Point2D( 0, 0 ) );
		}

		/* Test 2: Exercise the constructor that takes four points and verify it initializes correctly. */
		{
			Rectangle2D rect( Point2D( 1, 5 ), Point2D( 5, 5 ),
				Point2D( 1, 2 ), Point2D( 5, 2 ) );
			assert( rect.TopLeft() == Point2D( 1, 5 ) );
			assert( rect.TopRight() == Point2D( 5, 5 ) );
			assert( rect.BottomLeft() == Point2D( 1, 2 ) );
			assert( rect.BottomRight() == Point2D( 5, 2 ) );
		}

		/* Test 3: Exercise the constructor that takes a center point, width and height and verify that it 
		           initializes correctly. */
		{
			Rectangle2D rect( Point2D( 3, 3 ), 4, 5 );
			assert( rect.TopLeft() == Point2D( 1, 5.5 ) );
			assert( rect.TopRight() == Point2D( 5, 5.5 ) );
			assert( rect.BottomLeft() == Point2D( 1, 0.5 ) );
			assert( rect.BottomRight() == Point2D( 5, 0.5 ) );
		}

		/* Test 4: Exercise the constructor that takes an origin, vector, and axial width. */
		
		// Case 1: Vector lies on positive x-axis relative to origin
		{
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( 4, 0 ), 1.0 );
			assert( rect.TopLeft() == Point2D( 5, 5.5 ) );
			assert( rect.TopRight() == Point2D( 9, 5.5 ) );
			assert( rect.BottomLeft() == Point2D( 5, 4.5 ) );
			assert( rect.BottomRight() == Point2D( 9, 4.5 ) );
		}

		// Case 2: Vector lies on negative x-axis relative to origin
	    {
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( -4, 0 ), 1.0 );
			assert( rect.TopLeft() == Point2D( 1, 5.5 ) );
			assert( rect.TopRight() == Point2D( 5, 5.5 ) );
			assert( rect.BottomLeft() == Point2D( 1, 4.5 ) );
			assert( rect.BottomRight() == Point2D( 5, 4.5 ) );
		}

		// Case 3: Vector lies on positive y-axis relative to origin
		{
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( 0, 4 ), 1.0 );
			assert( rect.TopLeft() == Point2D( 4.5, 9 ) );
			assert( rect.TopRight() == Point2D( 5.5, 9 ) );
			assert( rect.BottomLeft() == Point2D( 4.5, 5 ) );
			assert( rect.BottomRight() == Point2D( 5.5, 5 ) );
		}

		// Case 4: Vector lies on negative y-axis relative to origin
		{
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( 0, -4 ), 1.0 );
			assert( rect.TopLeft() == Point2D( 4.5, 5 ) );
			assert( rect.TopRight() == Point2D( 5.5, 5 ) );
			assert( rect.BottomLeft() == Point2D( 4.5, 1 ) );
			assert( rect.BottomRight() == Point2D( 5.5, 1 ) );
		}

		// Case 5: Vector lies in first quadrant
		{
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( 4, 4 ), 1.0 );
			assert( rect.TopLeft() == Point2D( 5, 9 ) );
			assert( rect.TopRight() == Point2D( 9, 9 ) );
			assert( rect.BottomLeft() == Point2D( 5, 5 ) );
			assert( rect.BottomRight() == Point2D( 9, 5 ) );
		}

		// Case 6: Vector lies in second quadrant
		{
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( -4, 4 ), 1.0 );
			assert( rect.TopLeft() == Point2D( 1, 9 ) );
			assert( rect.TopRight() == Point2D( 5, 9 ) );
			assert( rect.BottomLeft() == Point2D( 1, 5 ) );
			assert( rect.BottomRight() == Point2D( 5, 5 ) );
		}

		// Case 7: Vector lies in third quadrant
		{
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( -4, -4 ), 1.0 );
			assert( rect.TopLeft() == Point2D( 1, 5 ) );
			assert( rect.TopRight() == Point2D( 5, 5 ) );
			assert( rect.BottomLeft() == Point2D( 1, 1 ) );
			assert( rect.BottomRight() == Point2D( 5, 1 ) );
		}

	    // Case 8: Vector lies in fourth quadrant
		{
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( 4, -4 ), 1.0 );
			assert( rect.TopLeft() == Point2D( 5, 5 ) );
			assert( rect.TopRight() == Point2D( 9, 5 ) );
			assert( rect.BottomLeft() == Point2D( 5, 1 ) );
			assert( rect.BottomRight() == Point2D( 9, 1 ) );
		}

	    // Case 9: <0,0> Vector
		{
			Rectangle2D rect( Point2D( 5, 5 ), Vector2D( 0, 0 ), 1.0 );
			assert( rect.TopLeft() == Point2D() );
			assert( rect.TopRight() == Point2D() );
			assert( rect.BottomLeft() == Point2D() );
			assert( rect.BottomRight() == Point2D() );
		}
	
		/* Test 4: Exercise the Rectangle::Width, Rectangle::Height, Rectangle2D::Top, Rectangle2D::Bottom, 
		           Rectangle2D::Left, Rectangle2D::Right, and Rectangle2D::Center functions and verify that 
				   they evaluate the rectangle width/height/boundaries/center correctly. */
		{
			Rectangle2D rect( Point2D( 1, 5 ), Point2D( 5, 5 ),
				Point2D( 1, 2 ), Point2D( 5, 2 ) );
			assert( rect.Width() == 4 );
			assert( rect.Height() == 3 );
			assert( rect.Top() == 5 );
			assert( rect.Bottom() == 2 );
			assert( rect.Left() == 1 );
			assert( rect.Right() == 5 );
			assert( rect.Center() == Point2D( 3, 3.5 ) );
		}

		/* Test 5: Exercise the Rectangle2D::Contains function and verify that it determines whether a 
		           rectangle contains a point correctly. */
		{
			Rectangle2D rect( Point2D( 1, 5 ), Point2D( 5, 5 ),
				Point2D( 1, 2 ), Point2D( 5, 2 ) );
			assert( rect.Contains( Point2D( 2, 3 ) ) );
			assert( !rect.Contains( Point2D( 24, 3 ) ) );
			assert( rect.Contains( Point2D( 1, 2 ) ) );
		}

		/* Test 6: Exercise the Rectangle2D::Intersects( const Rectangle2D& rect ) function and verify it 
		           determines whether a rectangle intersects another rectangle correctly. */
		
		// Case 1: rect1 contains at least one vertex from rect2.
		{
			Rectangle2D rect1( Point2D( 1, 5 ), Point2D( 5, 5 ),
				Point2D( 1, 2 ), Point2D( 5, 2 ) );
			Rectangle2D rect2( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			assert( rect1.Intersects( rect2 ) );
			assert( rect2.Intersects( rect1 ) );
		}

		// Case 2: rect2 falls entirely within the boundary of rect1.
		{
			Rectangle2D rect1( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			Rectangle2D rect2( Point2D( 6, 6.5 ), Point2D( 7, 6.5 ),
				Point2D( 6, 4.5 ), Point2D( 7, 4.5 ) );
			assert( rect1.Intersects( rect2 ) );
			assert( rect2.Intersects( rect1 ) );
		}

		// Case 3: No vertices from rect1 fall within the boundary of rect2 or vice versa, yet sides overlap.
		{
			Rectangle2D rect1( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			Rectangle2D rect2( Point2D( 3, 6.5 ), Point2D( 9, 6.5 ),
				Point2D( 3, 4.5 ), Point2D( 9, 4.5 ) );
			assert( rect1.Intersects( rect2 ) );
			assert( rect2.Intersects( rect1 ) );
		}

		// Case 4: rect1 and rect2 do not intersect.
		{
			Rectangle2D rect1( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			Rectangle2D rect2( Point2D( 8.5, 6.5 ), Point2D( 9, 6.5 ),
				Point2D( 8.5, 4.5 ), Point2D( 9, 4.5 ) );
			assert( !rect1.Intersects( rect2 ) );
			assert( !rect2.Intersects( rect1 ) );
		}

		/* Test 7: Exercise the Rectangle2D::Intersects( const LineSegment2D& lineSeg ) function and verify 
		           that it determines whether a rectangle intersects a line segment correctly. */
		
		// Case 1: rect contains the starting point of the line.
		{
			Rectangle2D rect( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			LineSegment2D line( Point2D( 5, 6 ), Point2D( 9, 7 ) );
			assert( rect.Intersects( line ) );
		}

	    // Case 2: rect contains the entire line.
		{
			Rectangle2D rect( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			LineSegment2D line( Point2D( 5, 6 ), Point2D( 7, 6 ) );
			assert( rect.Intersects( line ) );
		}

	    // Case 3: line overlaps one of rect's sides.
		{
			Rectangle2D rect( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			LineSegment2D line( Point2D( 5, 4 ), Point2D( 7, 4 ) );
			assert( rect.Intersects( line ) );
		}

		// Case 4: rect contains neither the start nor end point of the line segment, yet the line segment overlaps 2
		//         of rect's sides
		{
			Rectangle2D rect( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			LineSegment2D line( Point2D( 3, 5 ), Point2D( 9, 6 ) );
			assert( rect.Intersects( line ) );
		}

		// Case 5: rect and line do not intersect.
		{
			Rectangle2D rect( Point2D( 4, 7 ), Point2D( 8, 7 ),
				Point2D( 4, 4 ), Point2D( 8, 4 ) );
			LineSegment2D line( Point2D( 2, 12 ), Point2D( 3, 5 ) );
			assert( !rect.Intersects( line ) );
		}

	}

}