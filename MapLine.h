#pragma once

/* MapLine represents a static, collidable line on the map. */

#include "CollidableGameObject.h"
#include "CollidableLineSegment.h"
#include "IGraphicsProvider.h"
#include "LineSegment2D.h"
#include "SpriteAnimator.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class MapLine : public VisibleGameObject, public CollidableGameObject
	{
	public:
		enum MapLineType { Wall, BloodPit, Ice };
		enum DrawDirection { StartToEnd, EndToStart };

		/* Initializes a dummy MapLine. Used when getting all objects of type MapLine from 
		   GameObjectManager. */
		MapLine( GameObject::ObjectId objectId = "dummy" );

		/* Initializes a MapLine given its object id, a LineSegment2D, its type, the image file
		   for its graphic component, and a DrawDirection. */
		MapLine( GameObject::ObjectId objectId, const LineSegment2D &lineSeg, MapLineType type, 
			DrawDirection drawDirection );

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );

		/* GetCollidableArea
		   Summary: Gets the CollidableArea for this CollidableGameObject. 
		   Returns: The CollidableArea for this CollidableGameObject. */
		virtual CollidableArea* GetCollidableArea();

		/* GetType
		   Summary: Returns the type of this map line (as defined by MapLineType enum). 
		   Returns: The type of this map line (as defined by MapLineType enum). */
		MapLineType GetType() const;

		/* GetLineSegment 
		   Summary: Returns the line segment component of this MapLine. 
		   Returns: The line segment component of this MapLine. */
		const LineSegment2D& GetLineSegment() const;

	private:
		LineSegment2D _lineSegment;
		CollidableLineSegment _collidableArea;
		MapLineType _type;
		DrawDirection _drawDirection;
		SpriteAnimator _spriteAnimator;
		/* _subRect and _angle are used in animating the drawing of the mapline */
		Rectangle2D _subRect; 
		double _angle;

		static const double _mapLineWidth;

		friend class MapLineSketcher;
	};

}