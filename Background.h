#pragma once

/* Background class represents a background sprite that will cover the entire app window 
   upon which other game objects will be drawn. */

#include <string>

#include "Rectangle2D.h"
#include "VisibleGameObject.h"

namespace PROJECT_NAME
{

	class Background : public VisibleGameObject
	{
	public:
		/* Initializes a Background from a background art file and a Rectangle2D indicating the 
		   area of the image that contains the content (as opposed to black buffer area). By default
		   the entire image will be considered content. */
		Background( std::string backgroundArtFile, const Rectangle2D &contentArea = Rectangle2D() );

		/* Draw
		   Summary: Draws this VisibleGameObject. 
		   Parameters: 
		      interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                 the opportunity to interpolate their drawing methods to improve
							 animations of fast moving objects. */
		virtual void Draw( double interpolation );
	};

}