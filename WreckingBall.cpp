/* WreckingBall implementation. */

#include "stdafx.h"

#include <cmath>

#include "Box2DConversions.h"
#include "Game.h"
#include "Vector2D.h"
#include "WorldSimulation.h"
#include "WreckingBall.h"

namespace PROJECT_NAME
{
	WreckingBall::WreckingBall() : GameObject( "dummy" ), CollidableGameObject( "dummy" ), 
		DynamicGameObject( 1, "dummy" ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeString("dummy"), "dummy" ),
		_ropeLength( 0 ), _collidableArea( Circle2D() ), _wreckingBallRope( Point2D(), 0 ), _maxVelocityFound( false ),
		_ballIsVertical( false )
	{

	}

	/* Initializes a WreckingBall from an object id, point of rotation, starting angle, and rope length. */
	WreckingBall::WreckingBall( GameObject::ObjectId objectId, Point2D rotationPoint, double startingAngle, 
		double ropeLength ) : GameObject( objectId ), CollidableGameObject( objectId ), 
		DynamicGameObject( 0, objectId ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Traps/WreckingBall/WreckingBall.png" ) ), 
			objectId ), _rotationPoint( rotationPoint ), _ropeLength( ropeLength), _collidableArea( Circle2D( Point2D(), _ballRadius ) ),
		_wreckingBallRope( rotationPoint, ropeLength - static_cast< double >( _wreckingBallRopeTruncation ) ),
		_maxVelocityFound( false ), _ballIsVertical( false )
	{
		// Compute ball position from rotation point, starting angle and rope length.
		Vector2D ballPositionOffset( _ropeLength * cos( Radians( startingAngle ) ), 
			_ropeLength * sin( Radians( startingAngle ) ) );

		_ballPosition = rotationPoint + ballPositionOffset;

		// Initialize the Box2D simulated components of the WreckingBall.
		InitializeSimulatedComponents();

		// Define the center of the WreckingBall's sprite.
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableCenter( GraphicsId(), 
			Point2D( _spriteWidth / 2.0, _spriteHeight / 2.0 ) );
	}

	/* GetCollidableArea
		Summary: Gets the CollidableArea for this CollidableGameObject. 
		Returns: The CollidableArea for this CollidableGameObject. */
	CollidableArea* WreckingBall::GetCollidableArea()
	{
		return &_collidableArea;
	}

	/* Update
		Summary: Updates the state of this DynamicGameObject given the time elapsed in seconds since the 
				last update (which should be accounted for in this update). 
		Parameters: 
			elapsedTime: The time elapsed in seconds since last update. */
	void WreckingBall::Update( double elapsedTime )
	{
		// Because the ball will gradually lose energy in the simulation due to approximations done 
		// in the physics math, we're going to check for when the ball is near vertical position and 
		// refresh the velocity so that the pendulum motion remains indefinitely.
		
		if( _dynamicBall->GetPosition().x > -0.5 + _rotationPoint._x * PIXELS_TO_METERS && 
			_dynamicBall->GetPosition().x < 0.5 + _rotationPoint._x * PIXELS_TO_METERS )
		{
			if( !_ballIsVertical )
			{
				if( !_maxVelocityFound )
				{
					_maxVelocityFound = true;
					_maxVelocity = _dynamicBall->GetLinearVelocity();
				}
				else
				{
					b2Vec2 updateVelocity;
					if( _prevBallPosition._x < _ballPosition._x )
					{
						if( _maxVelocity.x > 0 )
						{
							updateVelocity = _maxVelocity;
						}
						else
						{
							updateVelocity = -_maxVelocity;
						}
					}
					else
					{
						if( _maxVelocity.x > 0 )
						{
							updateVelocity = -_maxVelocity;
						}
						else
						{
							updateVelocity = _maxVelocity;
						}
					}
					_dynamicBall->SetLinearVelocity( updateVelocity );
				}
			}
			_ballIsVertical = true;
		}
		else
		{
			_ballIsVertical = false;
		}

		// Update position, linear/angular velocity of kinematic body to mirror dynamic one.
		_kinematicBall->SetTransform( _dynamicBall->GetPosition(), _dynamicBall->GetAngle() );
		_kinematicBall->SetLinearVelocity( _dynamicBall->GetLinearVelocity() );
		_kinematicBall->SetAngularVelocity( _dynamicBall->GetAngularVelocity() );

		// Compute the new position of the ball in pixels from the simulated ball position.
		_prevBallPosition = _ballPosition;
		_ballPosition = Point2D( _dynamicBall->GetPosition().x * METERS_TO_PIXELS, 
			_dynamicBall->GetPosition().y * METERS_TO_PIXELS );

		// Update the collidable area.
		_collidableArea.UpdatePosition( _ballPosition );

		// Update the angle of the wrecking ball rope.
		_wreckingBallRope._prevAngle = _wreckingBallRope._angle;
		_wreckingBallRope._angle = -( ( Vector2D( _ballPosition ) - Vector2D( _rotationPoint ) ).Angle() - 90.0 );
		_wreckingBallRope._spriteAnimator.Update( elapsedTime );
	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void WreckingBall::Draw( double interpolation )
	{
		// Draw the ball at the interpolated position between its previous position and current 
		// position. This means the visual of the ball will slightly lag behind the actual ball,
		// but its swinging animation should look much smoother.

		Vector2D prevPos( _prevBallPosition );
		Vector2D currPos( _ballPosition );
		Vector2D interpolationVector = interpolation * ( currPos - prevPos );

		// Draw the wrecking ball.
		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawablePosition( GraphicsId(), 
			_prevBallPosition + interpolationVector );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );

		// Draw the wrecking ball rope.
		_wreckingBallRope.Draw( interpolation );
	}

	/* Deletes the Box2D simulated components of the WreckingBall. */
	WreckingBall::~WreckingBall()
	{
		// If the WorldSimulation object is still around, we need to delete the dynamic and kinematic bodies we created in it
		// for this WreckingBall.
		Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );
		if( worldSimulationHandle.Valid() )
		{
			b2World &world = worldSimulationHandle->GetWorld();

			world.DestroyBody( _dynamicBall );
			world.DestroyBody( _kinematicBall );
		}
	}

	/* Initializes the Box2D simulated components of the WreckingBall. */
	void WreckingBall::InitializeSimulatedComponents()
	{
		/* The WreckingBall is made up of two simulated components: a Box2D dynamic body repsenting the ball as it moves
		   in a pendulum motion, and a Box2D kinematic body that represents the body of the ball that collides with
		   other objects. The reason for having two simulated bodies for the ball is that I don't want the ball's
		   pendulum motion to be interfered with when collisions occur. To preserve that, it has the dynamic body
		   which will not collide with anything and will simply follow a pendulum motion. Meanwhile, I will update
		   the position, linear/angular velocity of the kinematic body to mirror that of the dynamic body. The 
		   kinematic body will be able to collide with other objects, and because it is a kinematic body, it has the
		   property that it will not be redirected by collisions, which is exactly the behavior I want. */

		// Get the WorldSimulation object, which holds the Box2D b2World object.
		Handle< WorldSimulation > worldSimulationHandle = Game::GetGameObjectManager().GetGameObject( "WorldSimulation" );
		assert( worldSimulationHandle.Valid() );

		b2World &world = worldSimulationHandle->GetWorld();

		// Create the dynamic ball body definition.
		b2BodyDef dynamicBallBodyDef;
		dynamicBallBodyDef.type = b2_dynamicBody;
		dynamicBallBodyDef.position.Set( 
			static_cast< float32 >( _ballPosition._x * PIXELS_TO_METERS ), 
			static_cast< float32 >( _ballPosition._y * PIXELS_TO_METERS ) );
		dynamicBallBodyDef.linearDamping = 0;
		dynamicBallBodyDef.angularDamping = 0;

		// Create the ball circle shape.
		b2CircleShape ballShape;
		ballShape.m_p.Set( 0, 0 );
		ballShape.m_radius = static_cast< float32 >( _ballRadius * PIXELS_TO_METERS );

		// Create the dynamic ball fixture definition.
		// Note: I'm setting the dynamic ball to only collide with other objects from collision category 16, 
		// which will be an empty category. Therefore, the dynamic ball should not collide with anything.
		b2FixtureDef dynamicBallFixture;
		dynamicBallFixture.shape = &ballShape;
		dynamicBallFixture.density = 3.0f;
		dynamicBallFixture.friction = 0.8f;
		dynamicBallFixture.restitution = 0.6f;
		dynamicBallFixture.filter.categoryBits = 0x0002;
		dynamicBallFixture.filter.maskBits = 0x0010; 

		// Create the dynamic ball and set its fixture.
		_dynamicBall = world.CreateBody( &dynamicBallBodyDef );
		_dynamicBall->CreateFixture( &dynamicBallFixture );

		// Create the distance joint between the world static body and the dynamic body for the wrecking ball.
		b2DistanceJointDef distJointDef;
		distJointDef.bodyA = _dynamicBall;
		distJointDef.bodyB = worldSimulationHandle->GetWorldStaticBody();
		distJointDef.localAnchorA.Set( 0, 0 );
		distJointDef.localAnchorB.Set( 
			static_cast< float32 >( _rotationPoint._x * PIXELS_TO_METERS ), 
			static_cast< float32 >( _rotationPoint._y * PIXELS_TO_METERS ) );
		distJointDef.length = static_cast< float32 >( _ropeLength * PIXELS_TO_METERS );

		world.CreateJoint( &distJointDef );

		// Create the kinematic ball body definition.
		b2BodyDef kinematicBallBodyDef;
		kinematicBallBodyDef.type = b2_kinematicBody;
		kinematicBallBodyDef.position.Set( 
			static_cast< float32 >( _ballPosition._x * PIXELS_TO_METERS ), 
			static_cast< float32 >( _ballPosition._y * PIXELS_TO_METERS ) );
		kinematicBallBodyDef.linearDamping = 0;
		kinematicBallBodyDef.angularDamping = 0;

		// Create the kinematic ball fixture definition.
		b2FixtureDef kinematicBallFixtureDef;
		kinematicBallFixtureDef.shape = &ballShape;
		kinematicBallFixtureDef.filter.categoryBits = 0x0001;
		kinematicBallFixtureDef.filter.maskBits = 0x0001;

		// Create the kinematic ball and set its fixture.
		_kinematicBall = world.CreateBody( &kinematicBallBodyDef );
		_kinematicBall->CreateFixture( &kinematicBallFixtureDef );
	}

	const double WreckingBall::_ballRadius = 22.5; // in pixels

	/* WreckingBallRope implementation. */

	WreckingBall::WreckingBallRope::WreckingBallRope( Point2D rotationPoint, double ropeLength ) : 
		GameObject( "WreckingBallRope" ), 
		VisibleGameObject( Game::GetGameServiceManager().GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/Placeholder.png" ) ), 
			"WreckingBallRope" ), _rotationPoint( rotationPoint ), _angle( 0 ), _prevAngle( 0 ),
		_spriteAnimator( RESOURCE_PATH( "Animation/WreckingBall/WreckingBallAnimationFile.txt" ), GraphicsId(), _rotationPoint )
	{
		double subrectHeight = ropeLength;
		if( subrectHeight <= 0 )
		{
			subrectHeight = 1.0;
		}
		_subRect = Rectangle2D( Point2D( 0, subrectHeight ), 
			Point2D( _wreckingBallRopeWidth, subrectHeight ), 
			Point2D( 0, 0 ), Point2D( _wreckingBallRopeWidth, 0 ) );

		_spriteAnimator.PlayAnimation( "WreckingBallRope", true, &_angle, &_subRect );
	}

	/* Draw
		Summary: Draws this VisibleGameObject. 
		Parameters: 
		    interpolation: An interpolation value between 0 and 1 that gives derived classes 
			                the opportunity to interpolate their drawing methods to improve
							animations of fast moving objects. */
	void WreckingBall::WreckingBallRope::Draw( double interpolation )
	{
		// Draw the wrecking ball rope at the interpolated angle between its previous angle
		// and its current angle.
		double interpolatedAngle = interpolation * ( _angle - _prevAngle ) + _prevAngle;

		Game::GetGameServiceManager().GetGraphicsProvider()->SetDrawableRotation( GraphicsId(), interpolatedAngle );

		Game::GetGameServiceManager().GetGraphicsProvider()->DrawDrawable( GraphicsId(), 
			Game::GetGameAssetManager().GetAppWindow() );
	}

}