/* Game implementation. */

#include "stdafx.h"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <vector>
#include <Windows.h>

#include "Background.h"
#include "ElementSketcher.h"
#include "Game.h"
#include "GameObject.h"
#include "GameSaveData.h"
#include "GameSaver.h"
#include "GlobalTemplateFunctions.h"
#include "Handle.h"
#include "Handle.cpp"
#include "IAudioProvider.h"
#include "IGraphicsProvider.h"
#include "KeyboardKey.h"
#include "LevelIndicator.h"
#include "MainMenu.h"
#include "Map.h"
#include "MapLine.h"
#include "MapLineSketcher.h"
#include "Menu.h"
#include "Pencil.h"
#include "Point2D.h"
#include "PushButton.h"
#include "SelectProfileMenu.h"
#include "SettingsMenu.h"
#include "SFMLAudioProvider.h"
#include "SFMLGraphicsProvider.h"
#include "StickFigure.h"
#include "Timer.h"
#include "GameEventFunctions.h"
#include "VisibleGameObject.h"
#include "WorldSimulation.h"
#include "WorldSketcher.h"

namespace PROJECT_NAME
{
	/* Start
	   Summary: Starts the game by initializing game resouces and calling GameLoop. */
	void Game::Start()
	{
		// Prevent multiple calls to Game::Start.
		assert( _gameState == Initializing );

		// Register the services used by the game.
		RegisterServices();

		// Initialize the application window.
		typedef std::vector< IGraphicsProvider::DisplayResolution > DisplayResolutions;
		DisplayResolutions validDisplayResolutions = 
			Game::GetGameServiceManager().GetGraphicsProvider()->GetSupportedDisplayResolutions();

		assert( validDisplayResolutions.size() > 0 );

		_gameAssetManager.SetAppWindow( 
			_gameServiceManager.GetGraphicsProvider()->InitializeWindow( _gameSpace, "What I Did For Third Period", 
				validDisplayResolutions[0]._width, validDisplayResolutions[0]._height, true ) );

		// Get the resolution of the application window that was set internally within the graphics provider. 
		_displayResolution = 
			_gameServiceManager.GetGraphicsProvider()->GetDisplayResolution( _gameAssetManager.GetAppWindow() );

		// Initialize mouse cursor resource.
		_cursor = _gameServiceManager.GetGraphicsProvider()->InitializeSprite( RESOURCE_PATH( "Art/UI/Cursor.png" ) );

		// Load map file names.
		LoadMapFileNames();

		// Init classroom ambience.
		classRoomAmbience = _gameServiceManager.GetAudioProvider()->InitSound( RESOURCE_PATH( "Audio/Music/ClassroomAmbience.wav" ), true );

		// Set initial game state.
		_gameState = SelectProfileMenu;

		// Call the game state manager to get things rolling.
		GameStateManager();
	}

	/* GetGameObjectManager 
	   Summary: Returns the game object manager used by this game. 
	   Returns: The game object manager used by this game. */
	GameObjectManager& Game::GetGameObjectManager()
	{
		return _gameObjectManager;
	}

	/* GetGameAssetManager
	   Summary: Returns the game asset manager used by this game.
	   Returns: The game asset manager used by this game. */
	GameAssetManager& Game::GetGameAssetManager()
	{
		return _gameAssetManager;
	}

	/* GetGetGameServiceManager
	   Summary: Returns the game service manager used by this game.
	   Returns: The game service manager used by this game. */
	GameServiceManager& Game::GetGameServiceManager()
	{
		return _gameServiceManager;
	}

	/* GetDisplayResolution 
	   Summary: Returns the display resolution that the game is currently running at. 
	   Returns: The display resolution that the game is currently running at. */
	const IGraphicsProvider::DisplayResolution& Game::GetDisplayResolution()
	{
		return _displayResolution;
	}

	/* GetGameSpace 
	   Summary: Returns the rectangle representing the game space. The game space is a virtual space
	            containing every point that an object in the game could occupy. 
	   Returns: The rectangle representing the game space. */
	const Rectangle2D& Game::GetGameSpace()
	{
		return _gameSpace;
	}

	/* GetGameUpdatePeriod
		Summary: Returns the fixed time in seconds between each game state update.
		Returns: The fixed time in seconds between each game state update. */
	double Game::GetGameUpdatePeriod()
	{
		return 1.0 / static_cast< double >( _gameStateUpdateFrequency );
	}

	/* GetCursorResource 
		Summary: Returns the resource id of the mouse cursor. 
		Returns: The resource id of the mouse cursor. */
	IGraphicsProvider::ResourceId Game::GetCursorResource()
	{
		return _cursor;
	}

	void Game::SetSelectedProfile( std::string profileName )
	{
		mSelectedProfile = profileName;
	}

	/* Registers all the services used by this game. */
	void Game::RegisterServices()
	{
		_gameServiceManager.SetGraphicsProvider( Handle< IGraphicsProvider >( new SFMLGraphicsProvider() ) );

		_gameServiceManager.SetAudioProvider( Handle< IAudioProvider >( new SFMLAudioProvider() ) );
	}

	/* GameStateManager manages the different states that the game can be in and calls the appropriate 
        functions for handling said states. These functions in turn handle switching between states. States 
		include: Main Menu, Starting New Game, Playing, etc. */
	void Game::GameStateManager()
	{
		// Game variables that persist throughout the lifetime of the game.
		PROJECT_NAME::SelectProfileMenu selectProfileMenu;
		PROJECT_NAME::MainMenu mainMenu;
		PROJECT_NAME::SettingsMenu settingsMenu;

		while( _gameState != Shutdown )
		{
			switch( _gameState )
			{
			case SelectProfileMenu:
				{
					_gameState = selectProfileMenu.Process();

					GameSaver gameSaver;

					// Check if there's a saved game for this profile and load it.
					if( gameSaver.LoadSave( RESOURCE_PATH( "SavedGames/" + mSelectedProfile + ".sav" ) ) )
					{
						GameSaveData gameSaveData = gameSaver.GetGameSaveData();

						mCurrentMapIndex = gameSaveData.currentMapIndex;
					}
				}
				break;
			case MainMenu:
				{
					// Play classroom ambience.
					if( !_gameServiceManager.GetAudioProvider()->IsSoundPlaying( classRoomAmbience ) ) 
					{
						_gameServiceManager.GetAudioProvider()->PlaySound( classRoomAmbience, 100, true );
					}

					_gameState = mainMenu.Process();

					if( _gameState == StartingLevel )
						_gameServiceManager.GetAudioProvider()->StopAllSounds();
				}
				break;
			case SettingsMenu:
				{
					_gameState = settingsMenu.Process();
				}
				break;
			case StartingLevel:
				{
					// Create the background object
					_gameObjectManager.AddGameObject( Handle< Background >( 
						new Background( RESOURCE_PATH( "Art/NotebookBackground.jpg" ), Rectangle2D( Point2D( 800, 800 ), 1600, 900 ) ) ) );

					// Draw the background before anything else.
					_gameServiceManager.GetGraphicsProvider()->PrepareWindowForDrawing( Game::GetGameAssetManager().GetAppWindow() );
					_gameObjectManager.DrawAllVisibleGameObjects( 0 );
					_gameServiceManager.GetGraphicsProvider()->DisplayWindow( Game::GetGameAssetManager().GetAppWindow() );

					// Create the world simulation object.
					_gameObjectManager.AddGameObject( Handle< WorldSimulation >( new WorldSimulation() ) );

					// Create the map object
					_gameObjectManager.AddGameObject( Handle< Map >( new Map( mMaps[ mCurrentMapIndex ] ) ) );

					// Get a handle to the world simulation object and sync it with the map object.
					Handle< WorldSimulation > worldSimulationHandle = _gameObjectManager.GetGameObject( "WorldSimulation" );
					assert( worldSimulationHandle.Valid() );
					worldSimulationHandle->SyncWithMap();

					// Get a handle to the map object, which we'll use below.
					GameObject::ObjectId mapId = "map";
					Handle< Map > mapHandle = _gameObjectManager.GetGameObject( mapId );
					assert( mapHandle.Valid() );

					// Create stick figure object
					_gameObjectManager.AddGameObject( Handle< StickFigure >( new StickFigure( mapHandle->GetSpawnPoint() ) ) );
					mapHandle->RegisterObject( "stick_figure" );

					// Create the pencil object
					_gameObjectManager.AddGameObject( Handle< Pencil >( new Pencil() ) );

					// Create a level indicator to indicate which level the player is player.
					_gameObjectManager.AddGameObject( Handle< LevelIndicator >( new LevelIndicator( "Level " + GetString( mCurrentMapIndex + 1 ) ) ) );

					Timer::Sleep( 2.0 );

					// Create the world sketcher and tell it to sketch the world.
					WorldSketcher worldSketcher;
					worldSketcher.SketchWorld();

					_gameState = Playing;
				}
				break;
			case Playing:
				{
					// Fire up the game loop!
					GameLoop();
				}
				break;
			case Exiting:
				{
					_gameObjectManager.DestroyAllGameObjects();

					_gameState = Shutdown;

					// Save game
					GameSaver gameSaver;
					GameSaveData gameSaveData;
					
					gameSaveData.currentMapIndex = mCurrentMapIndex;

					gameSaver.SetGameSaveData( gameSaveData );
					gameSaver.SaveGame( RESOURCE_PATH( "SavedGames/" + mSelectedProfile + ".sav" ) );
				}
				break;
			}
		}
	}	

	void Game::GameLoop()
	{
		/* The game loop is constructed such that we draw the game state as fast as possible, while 
		   we only update it at a fixed rate, controlled by Game::_gameUpdateFrequency. _updateTimer,
		   nextUpdateTime, maxSkippedFrames and skippedFrameCounter are used to control the frequency 
		   of the game state updates. */

		// lastUpdateTime is the elapsed time in milliseconds since system start that the game state 
		// was last updated, which we'll initialize to be the current elapsed time.
		DWORD lastUpdateTime = GetTickCount(); 
		const int maxSkippedFrames = 5; 
		int skippedFrameCounter = 0;

		// Get the game update period in milliseconds.
		DWORD gameUpdatePeriod = static_cast< DWORD >(
			1000.0 * GetGameUpdatePeriod() );

		while( _gameState == Playing )
		{
			// Check for window close event
			GameEvent gameEvent;
			while( CheckForGameEvent( gameEvent ) )
			{
				if( gameEvent._event == GameEvent::Closed )
				{
					_gameState = Exiting;
					break;
				} 
				else if( gameEvent._event == GameEvent::KeyPressed &&
					gameEvent._key == KeyboardKey::Escape )
				{
					_gameState = MainMenu;

					// Destroy all game objects since we're ending the current session of the game. This prepares it
					// for starting a new game.
					_gameObjectManager.DestroyAllGameObjects();

					break;
				}
				else if( gameEvent._event == GameEvent::LevelTransition )
				{
					if( ++mCurrentMapIndex > mMaps.size() - 1 )
					{
						mCurrentMapIndex = 0;

						_gameState = MainMenu;
					}
					else
					{
						_gameState = StartingLevel;
					}

					_gameObjectManager.DestroyAllGameObjects();
				}
			}

			if( _gameState == Playing )
			{
				_gameServiceManager.GetGraphicsProvider()->PrepareWindowForDrawing( Game::GetGameAssetManager().GetAppWindow() );

				skippedFrameCounter = -1;

				// Check if its time to do an update. If we're running behind (due to low FPS), we may have to do multiple updates
				// (thus skipping frames) to catch up. Up to maxSkippedFrames can be skipped, after that the game will resign 
				// itself to running slower.
				while( static_cast< DWORD >( GetTickCount() - lastUpdateTime ) >= gameUpdatePeriod && skippedFrameCounter < maxSkippedFrames )
				{
					_gameObjectManager.UpdateAllDynamicGameObjects();

					lastUpdateTime += gameUpdatePeriod;

					++skippedFrameCounter;
				}

				// Because, in most scenarios, we'll be drawing the game state far more than we'll be updating it, we're going to pass an interpolation
				// value between 0 and 1 to the draw methods of our game objects. This will allow the game objects to draw themselves at intermediate 
				// points between their current position and future position on the next update, thus smoothing out the animation of fast moving objects.
				double interpolation = Min( static_cast< double >( GetTickCount() - lastUpdateTime ) / static_cast< double >( gameUpdatePeriod ), 1.0 );

				_gameObjectManager.DrawAllVisibleGameObjects( interpolation ); 

				_gameServiceManager.GetGraphicsProvider()->DisplayWindow( Game::GetGameAssetManager().GetAppWindow() );
			}
		}
	}

	/* Reads in the list naming all the map files. */
	void Game::LoadMapFileNames()
	{
		std::ifstream inputFileStream( mMapFileList.c_str() );
		std::string buffer;

		while( std::getline( inputFileStream, buffer ) )
		{
			// Skip blank lines and comments.
			if( buffer.empty() || buffer[0] == '#' )
				continue;

			// trim white space from buffer
			std::stringstream s;
			s << buffer;
			buffer.clear();
			s >> buffer;

			mMaps.push_back( RESOURCE_PATH( "Maps/MapFiles/" + buffer + ".txt" ) );
		}
	}

	Game::GameState Game::_gameState = Initializing;

	GameObjectManager Game::_gameObjectManager;
	GameAssetManager Game::_gameAssetManager;
	GameServiceManager Game::_gameServiceManager;

	IGraphicsProvider::DisplayResolution Game::_displayResolution;
	const Rectangle2D Game::_gameSpace( Point2D( 800, 450 ), 1600, 900 );

	std::size_t Game::_gameStateUpdateFrequency = 25;

	IGraphicsProvider::ResourceId Game::_cursor;

	std::vector< std::string > Game::mMaps;
	std::size_t Game::mCurrentMapIndex = 0;
	const std::string Game::mMapFileList = RESOURCE_PATH( "Maps/MapList.txt" );

	IAudioProvider::AudioId Game::classRoomAmbience;

	std::string Game::mSelectedProfile;
}