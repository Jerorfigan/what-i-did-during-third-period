#pragma once 

/* Defines an enumeration of all the keyboard keys that will be referenced in the game. */

namespace PROJECT_NAME
{

	struct KeyboardKey
	{
		enum Key { Escape, Up, Down, Left, Right, Space, Character };
	};

}