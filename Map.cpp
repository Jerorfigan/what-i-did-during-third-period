/* Map implementation. */

#include "stdafx.h"

#include <algorithm>
#include <fstream>
#include <iterator>
#include <sstream>

#include "DeathRay.h"
#include "FlameTrap.h"
#include "Game.h"
#include "GlobalTemplateFunctions.h"
#include "Map.h"
#include "MapExit.h"
#include "MapLine.h"
#include "SawBlade.h"
#include "WreckingBall.h"

namespace PROJECT_NAME
{

	/* Initializes a map from a map file. */
	Map::Map( std::string mapFile ) : GameObject( "map" )
	{
		/* Read in map entity data from file and create each map entity. */

		std::ifstream inputFileStream( mapFile.c_str() );
		assert( inputFileStream );
		
		std::string lineBuffer;

		// Read each line from the file
		while( std::getline( inputFileStream, lineBuffer ) )
		{
			// Skip comments and blank lines
			if( lineBuffer.empty() || lineBuffer[0] == '#' )
			{
				continue;
			}
			
			std::stringstream lineTokenizer( lineBuffer );
			std::string token;
			std::string junk;

			// Read each token from the line
			while( lineTokenizer >> token )
			{
				if( token == "MapLine" )
				{
					CreateMapLine( lineTokenizer ); 
				}
				else if( token == "SpawnPoint" )
				{
					lineTokenizer >> junk >> _spawnPoint._x >> junk >> _spawnPoint._y >> junk;
				}
				else if( token == "MapExit" )
				{
					CreateMapExit( lineTokenizer );
				}
				else if( token == "MapBounds" )
				{
					ReadMapBounds( lineTokenizer );
				}
				else if( token == "SawBlade" )
				{
					CreateSawBlade( lineTokenizer );
				}
				else if( token == "DeathRay" )
				{
					CreateDeathRay( lineTokenizer );
				}
				else if( token == "WreckingBall" )
				{
					CreateWreckingBall( lineTokenizer );
				}
				else if( token == "FlameTrap" )
				{
					CreateFlameTrap( lineTokenizer );
				}
				else
				{
					// Unknown token...blow up!
					assert( 1 == 0 );
				}
			}
		}

	}

	/* GetObjectsCollidingWith 
	   Summary: Returns a std::vector containing pointers to all the CollidableGameObjects that collide 
	            with targetObject.
	   Parameters:
		  id: The id of the object to find all colliding objects for. 
	   Returns: A std::vector containing pointers to all the CollidableGameObjects that collide 
	            with targetObject. */
	std::vector< CollidableGameObject* > Map::GetObjectsCollidingWith( GameObject::ObjectId targetObject )
	{
		typedef std::set< GameObject::ObjectId > ObjectIds;

		std::vector< CollidableGameObject* > collidingObjects;

		// overlappingObjects will contain all objects whose minimum bounding rectangles overlap targetObject's,
		// but don't necessarily collide with target object. 
		ObjectIds overlappingObjects = 
			GetObjectsOverlapping( targetObject );

		// Get target object from object manager and cast it to a collidable object handle so that we may
		// perform collision logic on it.
		Handle< CollidableGameObject > targetObjectHandle = Game::GetGameObjectManager().GetGameObject( targetObject );
		assert( targetObjectHandle.Valid() );

		// Iterate through overlappingObjects, get each overlapping object from object manager, cast it to 
		// a collidable handle, and test if it collides with target object.
		for( ObjectIds::const_iterator objectItr = overlappingObjects.begin();
			 objectItr != overlappingObjects.end(); ++objectItr )
		{
			Handle< CollidableGameObject > collidableObjectHandle = Game::GetGameObjectManager().GetGameObject( *objectItr );
			assert( collidableObjectHandle.Valid() );

			if( targetObjectHandle->GetCollidableArea()->CollidesWith( 
				collidableObjectHandle->GetCollidableArea() ) )
			{
				collidingObjects.push_back( &*collidableObjectHandle );
			}
		}

		return collidingObjects;
	}

	/* GetSpawnPoint
	   Summary: Returns the spawning point for this map. 
	   Returns: The spawning point for this map. */
	const Point2D& Map::GetSpawnPoint() const
	{
		return _spawnPoint;
	}

	/* GetMapBounds 
		   Summary: Returns the boundary rectangle for this map.
		   Returns: The boundary rectangle for this map. */
	const Rectangle2D& Map::GetMapBounds() const
	{
		return _mapBounds;
	}

	/* RegisterObject
	   Summary: Registers the specified collidable object with this Map. Registration means this 
	            map will keep track of the minimum bounding rectangle for the specified object. Once 
				registered, the specified object may use this map to check for collisions.
	   Parameters:
	      objectId: The id of the object to register. */
	void Map::RegisterObject( GameObject::ObjectId objectId )
	{
		Handle< CollidableGameObject > collidableGameObjectHandle = Game::GetGameObjectManager().GetGameObject( objectId );
		assert( collidableGameObjectHandle.Valid() );

		// Register minimum bounding rect info
		xAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&collidableGameObjectHandle->GetCollidableArea()->GetMinX(), 
			&collidableGameObjectHandle->GetCollidableArea()->GetMaxX() ) ) );
        yAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&collidableGameObjectHandle->GetCollidableArea()->GetMinY(), 
			&collidableGameObjectHandle->GetCollidableArea()->GetMaxY() ) ) );
	}

	/* Parses the MapLine data and creates the MapLine object. */
	void Map::CreateMapLine( std::stringstream &lineTokenizer )
	{
		GameObject::ObjectId objectId;
		double startX = 0, startY = 0, endX = 0, endY = 0;
		MapLine::MapLineType mapLineType; 
		int mapLineTypeInt;
		MapLine::DrawDirection drawDirection;
		int drawDirectionInt;
		std::string junk;
		
		// Parse the MapLine data 
		lineTokenizer >> objectId >> junk >> startX >> junk >> startY >> junk >> junk >> endX >>
			junk >> endY >> junk >> mapLineTypeInt >> drawDirectionInt;

		// Verifies that the data was parsed correctly and that it is valid
		assert( lineTokenizer );
		assert( !objectId.empty() );
		assert( mapLineTypeInt == 0 || mapLineTypeInt == 1 || mapLineTypeInt == 2 );
		assert( drawDirectionInt == 0 || drawDirectionInt == 1 );

		mapLineType = static_cast< MapLine::MapLineType >( mapLineTypeInt );
		drawDirection = static_cast< MapLine::DrawDirection >( drawDirectionInt );

		// Create the MapLine object
		Handle< MapLine > mapLine( new MapLine( objectId, 
			LineSegment2D( Point2D( startX, startY ), Point2D( endX, endY ) ), mapLineType, 
			drawDirection ) );

		Game::GetGameObjectManager().AddGameObject( mapLine );

		// Register minimum bounding rect info
		xAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&mapLine->GetCollidableArea()->GetMinX(), 
			&mapLine->GetCollidableArea()->GetMaxX() ) ) );
        yAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&mapLine->GetCollidableArea()->GetMinY(), 
			&mapLine->GetCollidableArea()->GetMaxY() ) ) );
	}

	/* Parses the MapExit data and creates the MapExit object. */
	void Map::CreateMapExit( std::stringstream &lineTokenizer )
	{
		std::string junk;
		Point2D position;

		lineTokenizer >> junk >> position._x >> junk >> position._y >> junk;

		assert( lineTokenizer );

		// Create the MapExit object
		Handle< MapExit > mapExit( new MapExit( "map_exit", position ) );

		Game::GetGameObjectManager().AddGameObject( mapExit );

		// Register minimum bounding rect info
		xAxisAlignedBounds.push_back( std::make_pair( "map_exit", std::make_pair( 
			&mapExit->GetCollidableArea()->GetMinX(), 
			&mapExit->GetCollidableArea()->GetMaxX() ) ) );
        yAxisAlignedBounds.push_back( std::make_pair( "map_exit", std::make_pair( 
			&mapExit->GetCollidableArea()->GetMinY(), 
			&mapExit->GetCollidableArea()->GetMaxY() ) ) );
	}

	/* Parses the SawBlade data and creates the SawBlade object. */
	void Map::CreateSawBlade( std::stringstream &lineTokenizer )
	{
		GameObject::ObjectId objectId;
		std::string junk;
		Point2D position;

		lineTokenizer >> objectId >> junk >> position._x >> junk >> position._y >> junk;

		assert( lineTokenizer );

		// Create the SawBlade object
		//SawBlade *sawBladePtr = new SawBlade( objectId, position );
		Handle< SawBlade > sawBlade( new SawBlade( objectId, position ) );

		Game::GetGameObjectManager().AddGameObject( sawBlade );

		// Register minimum bounding rect info
		xAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&sawBlade->GetCollidableArea()->GetMinX(), 
			&sawBlade->GetCollidableArea()->GetMaxX() ) ) );
        yAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&sawBlade->GetCollidableArea()->GetMinY(), 
			&sawBlade->GetCollidableArea()->GetMaxY() ) ) );
	}

	/* Parses the DeathRay data and creates the DeathRay object. */
	void Map::CreateDeathRay( std::stringstream &lineTokenizer )
	{
		GameObject::ObjectId objectId;
		std::string junk;
		Point2D origin;
		double startingAngle, lowerAngleLimit, upperAngleLimit,
			angularVelocity;

		lineTokenizer >> objectId >> junk >> origin._x >> junk >> origin._y >> junk >>
			startingAngle >> lowerAngleLimit >> upperAngleLimit >> angularVelocity;

		assert( lineTokenizer );

		// Create the DeathRay object.
		Handle< DeathRay > deathRay( new DeathRay( objectId, origin, startingAngle, lowerAngleLimit, 
			upperAngleLimit, angularVelocity ) );

		Game::GetGameObjectManager().AddGameObject( deathRay );

		// Register minimum bounding rect info
		xAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&deathRay->GetCollidableArea()->GetMinX(), 
			&deathRay->GetCollidableArea()->GetMaxX() ) ) );
        yAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&deathRay->GetCollidableArea()->GetMinY(), 
			&deathRay->GetCollidableArea()->GetMaxY() ) ) );
	}

	/* Parses the WreckingBall data and creates the WreckingBall object. */
	void Map::CreateWreckingBall( std::stringstream &lineTokenizer )
	{
		GameObject::ObjectId objectId;
		std::string junk;
		Point2D rotationPoint;
		double startingAngle, ropeLength;

		lineTokenizer >> objectId >> junk >> rotationPoint._x >> junk >> rotationPoint._y >> junk >>
			startingAngle >> ropeLength;

		assert( lineTokenizer );

		// Create the WreckingBall object.
		Handle< WreckingBall > wreckingBall( new WreckingBall( objectId, rotationPoint, startingAngle, ropeLength ) );

		Game::GetGameObjectManager().AddGameObject( wreckingBall );

		// Register minimum bounding rect info
		xAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&wreckingBall->GetCollidableArea()->GetMinX(), 
			&wreckingBall->GetCollidableArea()->GetMaxX() ) ) );
        yAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&wreckingBall->GetCollidableArea()->GetMinY(), 
			&wreckingBall->GetCollidableArea()->GetMaxY() ) ) );
	}

	/* Parses the FlameTrap data and creates the FlameTrap object. */
	void Map::CreateFlameTrap( std::stringstream &lineTokenizer )
	{
		GameObject::ObjectId objectId;
		std::string junk;
		Point2D position;
		Vector2D flameDirection;

		lineTokenizer >> objectId >> junk >> position._x >> junk >> position._y >> junk >> junk >>
			flameDirection._x >> junk >> flameDirection._y >> junk;

		assert( lineTokenizer );

		// Create the WreckingBall object.
		Handle< FlameTrap > flameTrap( new FlameTrap( objectId, position, flameDirection ) );

		Game::GetGameObjectManager().AddGameObject( flameTrap );

		// Register minimum bounding rect info
		xAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&flameTrap->GetCollidableArea()->GetMinX(), 
			&flameTrap->GetCollidableArea()->GetMaxX() ) ) );
        yAxisAlignedBounds.push_back( std::make_pair( objectId, std::make_pair( 
			&flameTrap->GetCollidableArea()->GetMinY(), 
			&flameTrap->GetCollidableArea()->GetMaxY() ) ) );
	}

	/* Parses the map boundary data. */
	void Map::ReadMapBounds( std::stringstream &lineTokenizer )
	{
		Point2D topLeft, topRight, bottomLeft, bottomRight;
		std::string junk;

		lineTokenizer >> junk >> topLeft._x >> junk >> topLeft._y >> junk 
			>> junk >> topRight._x >> junk >> topRight._y >> junk
			>> junk >> bottomLeft._x >> junk >> bottomLeft._y >> junk
			>> junk >> bottomRight._x >> junk >> bottomRight._y >> junk;

		assert( lineTokenizer );
		_mapBounds = Rectangle2D( topLeft, topRight, bottomLeft, bottomRight );

		// Create 4 invisible walls at the map boundary.
		Handle< MapLine> boundaryWall = Handle< MapLine >( new MapLine( "boundary_wall1", 
			LineSegment2D( _mapBounds.TopLeft(), _mapBounds.TopRight() ), MapLine::Wall, MapLine::StartToEnd ) );
		boundaryWall->SetVisibility( false );
		Game::GetGameObjectManager().AddGameObject( boundaryWall );
		RegisterObject( "boundary_wall1" );

		boundaryWall = Handle< MapLine >( new MapLine( "boundary_wall2", 
			LineSegment2D( _mapBounds.TopRight(), _mapBounds.BottomRight() ), MapLine::Wall, MapLine::StartToEnd ) );
		boundaryWall->SetVisibility( false );
		Game::GetGameObjectManager().AddGameObject( boundaryWall );
		RegisterObject( "boundary_wall2" );

		boundaryWall = Handle< MapLine >( new MapLine( "boundary_wall3", 
			LineSegment2D( _mapBounds.BottomLeft(), _mapBounds.BottomRight() ), MapLine::Wall, MapLine::StartToEnd ) );
		boundaryWall->SetVisibility( false );
		Game::GetGameObjectManager().AddGameObject( boundaryWall );
		RegisterObject( "boundary_wall3" );

		boundaryWall = Handle< MapLine >( new MapLine( "boundary_wall4", 
			LineSegment2D( _mapBounds.TopLeft(), _mapBounds.BottomLeft() ), MapLine::Wall, MapLine::StartToEnd ) );
		boundaryWall->SetVisibility( false );
		Game::GetGameObjectManager().AddGameObject( boundaryWall );
		RegisterObject( "boundary_wall4" );
	}

	/* Returns a set containing the ids of the objects whose minimum bounding rectangles overlap 
	   targetObject's minimum bounding rectangle. */
	std::set< GameObject::ObjectId > Map::GetObjectsOverlapping( GameObject::ObjectId targetObject )
	{
		/* Start by sorting the x and y-axis aligned bounding pairs. Then we'll find the x-axis bounding pair
		   for this object and see which other x-axis bounding pairs overlap it. Then we'll do the same for the 
		   y-axis bounding pairs. Only objects with bounding pairs that overlap this object's bounding pair in 
		   both axes are used to create possible collision pairs. */

		// Sort the axis-aligned bounding pairs.
		InsertionSort( xAxisAlignedBounds.begin(), xAxisAlignedBounds.end(), CompareBoundingPairs );
		InsertionSort( yAxisAlignedBounds.begin(), yAxisAlignedBounds.end(), CompareBoundingPairs );

		// Find the set of objects that overlap targetObject with respect to the x-axis and with respect to the 
		// y-axis independently, and then find the intersection of those two sets, which gives the set of objects 
		// whose bounding rectangles overlap targetObject's.
		std::set< GameObject::ObjectId > objectsOverlappingWithRespectToXAxis =
			GetObjectsOverlappingWithRespectToAxis( targetObject, xAxisAlignedBounds );

		std::set< GameObject::ObjectId > objectsOverlappingWithRespectToYAxis =
			GetObjectsOverlappingWithRespectToAxis( targetObject, yAxisAlignedBounds );

		std::set< GameObject::ObjectId > overlappingObjects;

		std::set_intersection( objectsOverlappingWithRespectToXAxis.begin(), objectsOverlappingWithRespectToXAxis.end(),
							   objectsOverlappingWithRespectToYAxis.begin(), objectsOverlappingWithRespectToYAxis.end(),
							   std::inserter( overlappingObjects, overlappingObjects.begin() ) );
	
		return overlappingObjects;
	}

	/* Returns a set containing the ids of the objects whose axis-aligned bounds overlap 
	   targetObject's axis-aligned bounds. Assumes AxisAlignedBounds is sorted in ascending order of 
	   lower bound. */
	std::set< GameObject::ObjectId > Map::GetObjectsOverlappingWithRespectToAxis( GameObject::ObjectId targetObject, 
		const AxisAlignedBounds &axisAlignedBounds ) const
	{
		std::set< GameObject::ObjectId > objectsOverlappingTargetObject;

		AxisAlignedBounds::const_iterator boundingPairItr;
		for( boundingPairItr = axisAlignedBounds.begin(); 
			 boundingPairItr != axisAlignedBounds.end(); ++boundingPairItr )
		{
			if( boundingPairItr->first == targetObject )
			{
				/* boundingPairItr now refers to this object's bounding pair. Now create another iterator
				   that we can use to check the bounding pairs adjacent to this one for overlap. */

				// iterate left and check for overlap
				AxisAlignedBounds::const_iterator testItr = boundingPairItr;
				while( testItr != axisAlignedBounds.begin() ) 
				{
					if( *boundingPairItr->second.first <= *(--testItr)->second.second )
					{
						objectsOverlappingTargetObject.insert( testItr->first );
					}
				}
				// iterate right and check for overlap
				testItr = boundingPairItr;
				while( testItr != axisAlignedBounds.end() - 1 ) 
				{
					if( *boundingPairItr->second.second >= *(++testItr)->second.first )
					{
						objectsOverlappingTargetObject.insert( testItr->first );
					}
				}
				break;
			}
		}
		// Verify that we found this object's x-axis-aligned bounding pair
		assert( boundingPairItr != axisAlignedBounds.end() );

		return objectsOverlappingTargetObject;
	}

	/* Utility function used in sorting the axis aligned bounds containers. Returns true if the first bounding pair 
	   should come before the second bounding pair. */
	bool Map::CompareBoundingPairs( AxisAlignedBounds::iterator firstBoundingPairItr, 
		AxisAlignedBounds::iterator secondBoundingPairItr )
	{
		return *firstBoundingPairItr->second.first < *secondBoundingPairItr->second.first;
	}

}