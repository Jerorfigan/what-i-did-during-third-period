#pragma once

/* Implements the functionality of a control that can be used to select values from a list via
   a left and right arrow. */

#include <string>
#include <vector>

#include "IGraphicsProvider.h"
#include "Point2D.h"
#include "Rectangle2D.h"
#include "UserControl.h"

namespace PROJECT_NAME
{

	class ArrowSelector : public UserControl
	{
		// Constructors
	public:
		ArrowSelector( std::string id, unsigned int menuIndex, Point2D arrowSelectorPos, std::string labelImage, 
			std::vector< std::string > selections, void (*onActivation)( void*, ArrowSelector* ) = 0, std::size_t defaultSelectionIndex = 0 );

		// Methods
	public:
		std::size_t GetSelectedIndex() const;

	private:
		void RepositionSelectionString();

		// Virtual methods
	public:
		virtual void Init( void* menuData );

		virtual void Draw( void* menuData );

		virtual void OnHoverOver( void* menuData );
		virtual void OnHoverAway( void* menuData );
		virtual void OnActivation( void* menuData );
		virtual void OnDeactivation( void* menuData );
		virtual void OnGainedFocus( void* menuData );
		virtual void OnLostFocus( void* menuData );
		virtual void OnKeyboardChar( void* menuData, char typedChar );

		virtual bool IsTargettedByMouse( Point2D mouseHotspot );

		// Data
	private:
		std::vector< std::string > mSelections;
		Point2D mPosition;
		Rectangle2D mLeftArrowRect;
		Rectangle2D mRightArrowRect;
		IGraphicsProvider::ResourceId mLabel;
		IGraphicsProvider::ResourceId mLeftArrow;
		IGraphicsProvider::ResourceId mRightArrow;
		bool mLeftArrowTargetted;
		std::size_t mCurrentSelectionIndex;

		void (*mOnActivation)( void*, ArrowSelector* );

		// Static data
	private:
		static const std::string mLeftArrowImage;
		static const std::string mRightArrowImage;
		static const int mArrowWidth = 60; // in pixels
		static const int mRightArrowOffset = 250; // in pixels
		static const int mLabelOffset = 20; // in pixels
		static const int mValueOffsetY = 10; // in pixels
	};

}