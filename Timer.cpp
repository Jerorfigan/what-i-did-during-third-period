/* Timer implementation. */

#include "stdafx.h"

#include "Timer.h"
#include "Windows.h"

namespace PROJECT_NAME
{

	/* Initializes a timer that measures time elapsed since application start. */
	Timer::Timer() : _millisecondsAtReset( 0 )
	{

	}

	/* Sleep 
	   Summary: Suspends program execution for a duration in seconds. 
	   Parameters: 
	      seconds: The amount of time in seconds to sleep. */
	void Timer::Sleep( double seconds )
	{
		// Call Windows.h sleep routine.
		::Sleep( static_cast< DWORD >( seconds * 1000 ) );
	}

	/* Reset
	   Summary: Creates a point of reference for ElapsedTime, which will return the
	            amount of time that has passed since the timer was last reset. */
	void Timer::Reset()
	{
		_millisecondsAtReset = GetTickCount();
	}

	/* GetElapsedTime
	   Summary: Returns the amount of time in seconds that has passed since the timer 
	            was last reset. 
	   Returns: The amount of time in seconds that has passed since the timer 
	            was last reset.  */
	double Timer::GetElapsedTime()
	{
		DWORD currentMilliseconds = GetTickCount();

		// Handle rollover scenario.
		if( currentMilliseconds < _millisecondsAtReset )
		{
			_millisecondsAtReset = 0;
		}

		return static_cast< double >( currentMilliseconds - _millisecondsAtReset ) / 1000.0;
	}

}